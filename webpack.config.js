const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: {
    'stylesheets/bootstrap': './app/assets/stylesheets/xivo_bootstrap.less',
    'javascripts/app' : ['./app/assets/javascripts/callbacks.js', './app/assets/javascripts/rights.js', './app/assets/javascripts/app.config.js'],
    'javascripts/vendor': ['angular', 'jquery', 'bootstrap', 'angular-ui-bootstrap', 'moment',
                           'eonasdan-bootstrap-datetimepicker', "angular-translate","angular-translate-loader-partial"]
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'target/web/public/main/', 'dist')
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      moment: 'moment',
      "window.jQuery":"jquery",
    }),
    new webpack.optimize.CommonsChunkPlugin({ name: 'javascripts/vendor', filename: 'javascripts/vendor.js' }),
    new ExtractTextPlugin('stylesheets/bootstrap.css'),
    new CopyWebpackPlugin([
        { from: 'node_modules/bootstrap/fonts/*', to: 'bootstrap/fonts', flatten: true }
    ]),
    new webpack.optimize.UglifyJsPlugin({
      include: /vendor\.js/
    })
  ],
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        exclude:[/public/, /node_modules/],
        loader: "eslint-loader"
      },
      {
        test: require.resolve('jquery'),
        loader: 'expose-loader?jQuery!expose-loader?$'
      },
      {
        test: require.resolve('angular'),
        loader: 'expose-loader?angular!expose-loader?angular'
      },
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env'],
            cacheDirectory: true
          }
        }
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'less-loader']
        })
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)([a-z0-9=\.]+)?$/,
        loader: 'url-loader?limit=100000'
      }
    ]
  }
};
