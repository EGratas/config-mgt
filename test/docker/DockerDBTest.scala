package docker

import anorm._
import com.spotify.docker.client.{DefaultDockerClient, DockerClient}
import com.whisk.docker.DockerFactory
import com.whisk.docker.impl.spotify.SpotifyDockerFactory
import org.slf4j.{Logger, LoggerFactory}

import java.sql.{Connection, DriverManager}
import java.util.Properties
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class DockerDBTest extends DockerPostgresService with DockerPlayConfig {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  private val client: DockerClient = DefaultDockerClient.fromEnv().build()

  // Start postgres container
  logger.info("Starting database container")
  startAllOrFail()

  // Wait for postgres service
  val ready: Boolean =
    Await.result(isContainerReady(postgresContainer), 2.minutes)
  val databaseIp: String =
    Await.result(postgresContainer.getIpAddresses().map(_.head), 2.minutes)
  val connection: Connection =
    getConnection(PostgresUser, PostgresPassword, "postgres")

  logger.info("Populating database container")
  // Create databases
  SQL(
    "CREATE USER " + playConfig(
      "db.xc.username"
    ) + " WITH PASSWORD '" + playConfig("db.xc.password") + "'"
  ).execute()(connection)
  SQL("CREATE DATABASE xc WITH OWNER " + playConfig("db.xc.username"))
    .execute()(connection)

  SQL(
    "CREATE USER " + playConfig(
      "db.xivo.username"
    ) + " WITH PASSWORD '" + playConfig("db.xivo.password") + "'"
  ).execute()(connection)
  SQL("CREATE DATABASE xivo WITH OWNER " + playConfig("db.xivo.username"))
    .execute()(connection)

  sys.ShutdownHookThread {
    stopAllQuietly()
  }

  // Create structures
  val connectionXivo: Connection = getConnection(
    playConfig("db.xivo.username"),
    playConfig("db.xivo.password"),
    "xivo"
  )

  val connectionXivoAdmin: Connection =
    getConnection(PostgresUser, PostgresPassword, "xivo")

  SQL(
    "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\""
  ).execute()(connectionXivoAdmin)

  SQL(
    "select uuid_generate_v4()"
  ).execute()(connectionXivo)

  executeSqlFile("docker/create-xivo-db.sql")(connectionXivo)

  val connectionXc: Connection = getConnection(
    playConfig("db.xc.username"),
    playConfig("db.xc.password"),
    "xc"
  )

  val connectionXcAdmin: Connection =
    getConnection(PostgresUser, PostgresPassword, "xc")

  SQL(
    "CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\""
  ).execute()(connectionXcAdmin)

  SQL(
    "select uuid_generate_v4()"
  ).execute()(connectionXc)

  executeSqlFile("docker/create-xc-db.sql")(connectionXc)

  logger.info("Database container populated")

  def isContainerReady: Future[Boolean] = isContainerReady(postgresContainer)

  def getConnection(
      user: String,
      password: String,
      database: String
  ): Connection = {
    Class.forName("org.postgresql.Driver")
    val uri = s"jdbc:postgresql://$databaseIp/$database"

    val props = new Properties
    props.setProperty("user", user)
    props.setProperty("password", password)
    DriverManager.getConnection(uri, props)
  }

  def executeSqlFile(
      filename: String
  )(implicit connection: Connection): AnyVal = {
    val createTable = scala.io.Source
      .fromInputStream(
        getClass.getClassLoader.getResourceAsStream(filename)
      )
      .mkString
    try {
      val sql = SQL(createTable)
      sql.execute()
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }

  override implicit def dockerFactory: DockerFactory =
    new SpotifyDockerFactory(client)
}

object DockerDBTest extends DockerDBTest {}
