package docker

import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Application
import play.api.db.DBApi
import play.api.inject.guice.GuiceApplicationBuilder
import testutils.PlayShouldSpec

import java.sql.Connection

/**
  */
trait DockerPlayConfig {
  val playConfig = Map(
    "play.evolutions.enabled"            -> "false",
    "play.http.context"                  -> "/configmgt",
    "authentication.xivo.restapi.token"  -> "abcedfghi",
    "authentication.confd.restapi.token" -> "abcedfghi",
    "db.xc.username"                     -> "xc",
    "db.xc.password"                     -> "xc",
    "db.xivo.username"                   -> "xivo",
    "db.xivo.password"                   -> "xivo"
  )
}

abstract class DockerPlaySpec
    extends PlayShouldSpec
    with DockerPlayConfig
    with GuiceOneAppPerSuite
    with BeforeAndAfterEach
    with MockitoSugar {

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfig)
    .build()
}

abstract class DockerPlayWithDbSpec
    extends PlayShouldSpec
    with DockerPlayConfig
    with GuiceOneAppPerSuite
    with BeforeAndAfterEach
    with MockitoSugar {

  def withConnection[A](name: String = "xivo")(block: Connection => A): A = {
    val dbApi = app.injector.instanceOf(classOf[DBApi])
    dbApi.database(name).withConnection(block)
  }

  val playDatabaseIp: String = DockerDBTest.databaseIp
  val playConfigWithDatabase: Map[String, String] = playConfig ++ Map(
    "db.xivo.url" -> s"jdbc:postgresql://$playDatabaseIp/xivo",
    "db.xc.url"   -> s"jdbc:postgresql://$playDatabaseIp/xc"
  )

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .build()

}
