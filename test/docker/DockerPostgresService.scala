package docker

import java.sql.DriverManager
import java.util.Properties

import com.whisk.docker._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

/**
  */
trait DockerPostgresService extends DockerKit with DockerPlayConfig {
  override val StartContainersTimeout: FiniteDuration = 2.minutes

  val PostgresUser     = "postgres"
  val PostgresPassword = "avencall"

  val postgresContainer: DockerContainer = DockerContainer("postgres:9.4.4")
    .withEnv(
      s"POSTGRES_USER=$PostgresUser",
      s"POSTGRES_PASSWORD=$PostgresPassword"
    )
    .withReadyChecker(
      PostgresReadyChecker()
        .within(100.millis)
        .looped(20, 1250.millis)
    )

  abstract override def dockerContainers: List[DockerContainer] =
    postgresContainer :: super.dockerContainers

  case class PostgresReadyChecker() extends DockerReadyChecker {
    override def apply(container: DockerContainerState)(implicit
        docker: DockerCommandExecutor,
        ec: ExecutionContext
    ): Future[Boolean] = {

      Class.forName("org.postgresql.Driver")

      val props = new Properties()
      props.setProperty("user", PostgresUser)
      props.setProperty("password", PostgresPassword)

      container
        .getIpAddresses()(docker, ec)
        .map(_.head)(ec)
        .map(ip => {
          s"jdbc:postgresql://$ip/postgres"
        })(ec)
        .flatMap(uri =>
          Future {
            try {
              // Establish a test connection
              DriverManager.getConnection(uri, props)
              true
            } catch {
              case _: java.net.ConnectException =>
                false
            }
          }(ec)
        )(ec)

    }
  }

}
