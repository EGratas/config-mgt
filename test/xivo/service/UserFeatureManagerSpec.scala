package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Second, Seconds, Span}
import xivo.model.{GenericBsFilter, UserFeature}

import java.util.UUID
import scala.util.{Failure, Success}

class UserFeatureManagerSpec extends DockerPlayWithDbSpec with ScalaFutures {

  override protected def beforeEach(): Unit = {
    withConnection() { implicit c =>
      SQL("TRUNCATE userfeatures, user_line, userpreferences").execute()
    }
  }
  implicit val pc: PatienceConfig =
    PatienceConfig(Span(20, Seconds), Span(1, Second))

  "xivo.service.UserFeatureManager" should {
    "create xivo user" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val created = userFeatureManager.create(u)

      created.isSuccess shouldBe true
      created.get.id shouldNot be(None)
    }

    "get a xivo user" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser = userFeatureManager.create(u).get

      val fetchedUser = userFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe true
      fetchedUser.get shouldEqual createdUser
    }

    "update a xivo user" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser =
        userFeatureManager.create(u).get.copy(passwdclient = "xxxx")
      userFeatureManager.update(createdUser) match {
        case Success(_) =>
        case Failure(t) =>
          t.printStackTrace()
          fail(t)
      }

      val fetchedUser = userFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe true
      fetchedUser.get shouldEqual createdUser
    }

    "fail updating a non existing xivo user" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        Some(1),
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      userFeatureManager.delete(1)

      userFeatureManager.update(u).isSuccess shouldBe false
    }

    "delete a xivo user" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser = userFeatureManager.create(u).get

      userFeatureManager.delete(createdUser.id.get)

      val fetchedUser = userFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe false
    }

    "fail deleting a non existing xivo user" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])

      userFeatureManager.delete(1)

      userFeatureManager.delete(1).isSuccess shouldBe false
    }

    "get all xivo users" in {
      val userFeatureManager =
        app.injector.instanceOf(classOf[UserFeatureManager])
      val u1 = UserFeature(
        None,
        UUID.randomUUID().toString,
        "James",
        "Bond",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbond",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val u2 = UserFeature(
        None,
        UUID.randomUUID().toString,
        "Jason",
        "Bornes",
        None,
        None,
        None,
        None,
        None,
        30,
        5,
        0,
        "jbornes",
        "mypass",
        None,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        "",
        0,
        "",
        0,
        "",
        "",
        "",
        "",
        "",
        GenericBsFilter.No,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        "",
        None,
        0,
        None
      )

      val createdUser1 = userFeatureManager.create(u1).get
      val createdUser2 = userFeatureManager.create(u2).get

      val fetchedUsers = userFeatureManager.all()

      fetchedUsers.isSuccess shouldBe true
      fetchedUsers.get should contain.allOf(createdUser1, createdUser2)
    }
  }

  "get a xivo user by username" in {
    val userFeatureManager =
      app.injector.instanceOf(classOf[UserFeatureManager])
    val u = UserFeature(
      None,
      UUID.randomUUID().toString,
      "Test",
      "LoginSearch",
      None,
      None,
      None,
      None,
      None,
      30,
      5,
      0,
      "tlogin",
      "mypass",
      None,
      1,
      0,
      1,
      0,
      0,
      0,
      0,
      "",
      0,
      "",
      0,
      "",
      "",
      "",
      "",
      "",
      GenericBsFilter.No,
      None,
      None,
      None,
      None,
      None,
      None,
      None,
      None,
      0,
      "",
      None,
      0,
      None
    )

    val createdUser = userFeatureManager.create(u).get
    val fetchedUser = userFeatureManager.get(createdUser.loginclient)

    fetchedUser.isSuccess shouldBe true
    fetchedUser.get shouldEqual createdUser
  }
}
