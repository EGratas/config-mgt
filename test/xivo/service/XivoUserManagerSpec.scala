package xivo.service

import _root_.ws.model.LineType
import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils.UserIaxCategory
import model._
import xivo.model.{FindXivoUserResponse, XivoUser, XivoUserContact}

import scala.util.{Failure, Success}

class XivoUserManagerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE userfeatures, linefeatures, user_line, usersip, userpreferences"
      ).execute()
      SQL("TRUNCATE userlabels").execute()
      SQL("TRUNCATE labels").execute()
      SQL("TRUNCATE entity").execute()
      SQL("TRUNCATE trunkfeatures, mediaserver").execute()

      super.beforeEach()
    }

  def generateOnePhoneUser: Long =
    withConnection() { implicit c =>
      Utils.insertLineFeature(1, "sip", "default", "1000", 1234)
      Utils.insertEntity(1, "xivo", "XiVO test")
      Utils.insertXivoUser(1, "James", "Bond", Some("jbond"), Some(1), Some(1))
      Utils.insertUserLine(1, 1, 1)
      Utils.insertLabel(1, "blue", "")
      Utils.insertLabel(2, "red", "")
      Utils.insertLabel(3, "green", "")
      Utils.insertUserLabel(1, 1, 1)
      Utils.insertUserLabel(2, 1, 2)
      Utils.insertUserLabel(3, 1, 3)
      Utils.insertMediaServer(1, "default", "MDS Main")
    }

  def generateUAandSCCPUsers: Long =
    withConnection() { implicit c =>
      val protocolId =
        Utils.insertUserSip("1hlnzr8t", UserIaxCategory.user, "{{webrtc,ua}}")
      Utils.insertLineFeature(
        1,
        "sip",
        "default",
        "1000",
        1234,
        Some(protocolId),
        name = Some("1hlnzr8t")
      )
      Utils.insertLineFeature(2, "sccp", "default", "1010", 5678)
      Utils.insertEntity(1, "xivo", "XiVO test")
      Utils.insertXivoUser(1, "James", "Bond", Some("jbond"), Some(1), Some(1))
      Utils.insertXivoUser(2, "John", "Wayne", Some("jwayne"), Some(2), Some(1))
      Utils.insertUserLine(1, 1, 1)
      Utils.insertUserLine(1, 2, 2)
      Utils.insertLabel(1, "blue", "")
      Utils.insertLabel(2, "red", "")
      Utils.insertLabel(3, "green", "")
      Utils.insertUserLabel(1, 1, 1)
      Utils.insertUserLabel(2, 1, 2)
      Utils.insertUserLabel(3, 1, 3)
      Utils.insertUserLabel(4, 2, 1)
      Utils.insertUserLabel(5, 2, 2)
      Utils.insertMediaServer(1, "default", "MDS Main")
    }

  "get one Xivo user as a contact" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    Utils.insertEntity(1, "xivo", "XiVO test")
    Utils.insertXivoUser(
      1,
      "Jean-Pierre",
      "Thomasset",
      Some("JeanPierreThomasset"),
      Some(1),
      Some(1),
      email = Some("jpthomasset@xivo.solutions"),
      mobilePhoneNumber = Some("0612345678")
    )

    Utils.insertXivoUser(
      2,
      "Alvin",
      "Sevil",
      Some("AlvinSevil"),
      Some(2),
      Some(1),
      email = Some("asevil@xivo.solutions")
    )

    Utils.insertLineFeature(
      2,
      "sip",
      "default",
      "0678945612",
      5678
    )

    Utils.insertUserLine(1, 1, 2)
    Utils.insertLabel(1, "blue", "")
    Utils.insertLabel(2, "green", "")
    Utils.insertLabel(3, "red", "")
    Utils.insertUserLabel(1, 1, 1)
    Utils.insertUserLabel(2, 1, 2)
    Utils.insertUserLabel(3, 1, 3)
    Utils.insertIncall(4, "0123220009")
    Utils.insertIncall(5, "0123220015")
    Utils.insertDialAction("answer", "incall", 4, "user", Some("1"))
    Utils.insertDialAction("answer", "incall", 5, "user", Some("1"))
    Utils.insertDialAction("answer", "incall", 9, "queue", Some("1"))
    Utils.insertDialAction("noanswer", "queue", 8, "voicemail", Some("1"))
    Utils.insertIncall(9, "0123220016")
    Utils.insertQueueFeature(8, "QA")

    val result = List(
      XivoUserContact(
        1,
        "Jean-Pierre",
        "Thomasset",
        Some("jpthomasset@xivo.solutions"),
        Some("0612345678"),
        Some("0678945612"),
        List("blue", "green", "red"),
        List("0123220009", "0123220015")
      ),
      XivoUserContact(
        2,
        "Alvin",
        "Sevil",
        Some("asevil@xivo.solutions"),
        Some(""),
        None,
        List(),
        List()
      )
    )

    xivoUserManager.getAllAsContact match {
      case Success(usersAsContact) =>
        usersAsContact should contain theSameElementsAs result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get one Xivo user without line and labels".stripMargin in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      Utils.insertEntity(1, "xivo", "XiVO test")
      Utils.insertXivoUser(1, "James", "Bond", Some("jbond"), Some(1), Some(1))

      val result = List(
        XivoUser(
          1,
          "James Bond",
          None,
          None,
          None,
          "XiVO test",
          None,
          None,
          disabled = false,
          None
        )
      )

      xivoUserManager.getAll match {
        case Success(users) =>
          users should contain theSameElementsAs result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "get one Xivo user with labels lexically ordered" in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      generateOnePhoneUser

      val result = List(
        XivoUser(
          1,
          "James Bond",
          Some(1234),
          Some(LineType.PHONE),
          Some("1000"),
          "XiVO test",
          Some("default"),
          Some("MDS Main"),
          disabled = false,
          Some(List("blue", "green", "red"))
        )
      )

      xivoUserManager.getAll match {
        case Success(users) =>
          users should contain theSameElementsAs result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "get all Xivo users lexically ordered" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers

    val result = List(
      XivoUser(
        1,
        "James Bond",
        Some(1234),
        Some(LineType.UA),
        Some("1000"),
        "XiVO test",
        Some("default"),
        Some("MDS Main"),
        disabled = false,
        Some(List("blue", "green", "red"))
      ),
      XivoUser(
        2,
        "John Wayne",
        Some(5678),
        Some(LineType.SCCP),
        Some("1010"),
        "XiVO test",
        Some("default"),
        Some("MDS Main"),
        disabled = false,
        Some(List("blue", "red"))
      )
    )

    xivoUserManager.getAll match {
      case Success(users) =>
        users should contain theSameElementsAs result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find one Xivo user" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers

    val result = FindXivoUserResponse(
      2,
      List(
        XivoUser(
          2,
          "John Wayne",
          Some(5678),
          Some(LineType.SCCP),
          Some("1010"),
          "XiVO test",
          Some("default"),
          Some("MDS Main"),
          disabled = false,
          Some(List("blue", "red"))
        )
      )
    )

    xivoUserManager.find(List.empty, 1, 1) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find Xivo users lexically ordered" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
    Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

    val result = FindXivoUserResponse(
      4,
      List(
        XivoUser(
          2,
          "John Wayne",
          Some(5678),
          Some(LineType.SCCP),
          Some("1010"),
          "XiVO test",
          Some("default"),
          Some("MDS Main"),
          disabled = false,
          Some(List("blue", "red"))
        ),
        XivoUser(
          3,
          "Zack Bond",
          None,
          None,
          None,
          "XiVO test",
          None,
          None,
          disabled = false,
          None
        )
      )
    )

    xivoUserManager.find(List.empty, 2, 2) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find Xivo user with filters" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
    Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

    val result = FindXivoUserResponse(
      1,
      List(
        XivoUser(
          3,
          "Zack Bond",
          None,
          None,
          None,
          "XiVO test",
          None,
          None,
          disabled = false,
          None
        )
      )
    )

    val filters = List(
      DynamicFilter("fullName", Some(OperatorLike), Some("Zack%"))
    )

    xivoUserManager.find(filters, 0, 5) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get the media server display name and technical name" in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      generateOnePhoneUser

      val result = List(
        XivoUser(
          1,
          "James Bond",
          Some(1234),
          Some(LineType.PHONE),
          Some("1000"),
          "XiVO test",
          Some("default"),
          Some("MDS Main"),
          disabled = false,
          Some(List("blue", "green", "red"))
        )
      )

      xivoUserManager.getAll match {
        case Success(users) =>
          users shouldBe result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "sort Xivo users by name DESC" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
    Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

    val result =
      FindXivoUserResponse(
        4,
        List(
          XivoUser(
            3,
            "Zack Bond",
            None,
            None,
            None,
            "XiVO test",
            None,
            None,
            disabled = false,
            None
          ),
          XivoUser(
            2,
            "John Wayne",
            Some(5678),
            Some(LineType.SCCP),
            Some("1010"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "red"))
          ),
          XivoUser(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.UA),
            Some("1000"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          ),
          XivoUser(
            4,
            "Aaron Bond",
            None,
            None,
            None,
            "XiVO test",
            None,
            None,
            disabled = false,
            None
          )
        )
      )

    xivoUserManager.find(
      List(DynamicFilter("fullName", None, None, Some(OrderDesc))),
      0,
      5
    ) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "sort Xivo users by name ASC" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
    Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

    val result =
      FindXivoUserResponse(
        4,
        List(
          XivoUser(
            4,
            "Aaron Bond",
            None,
            None,
            None,
            "XiVO test",
            None,
            None,
            disabled = false,
            None
          ),
          XivoUser(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.UA),
            Some("1000"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          ),
          XivoUser(
            2,
            "John Wayne",
            Some(5678),
            Some(LineType.SCCP),
            Some("1010"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "red"))
          ),
          XivoUser(
            3,
            "Zack Bond",
            None,
            None,
            None,
            "XiVO test",
            None,
            None,
            disabled = false,
            None
          )
        )
      )

    xivoUserManager.find(
      List(DynamicFilter("fullName", None, None, Some(OrderAsc))),
      0,
      5
    ) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find Xivo user with filters on labels" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
    Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

    val result = FindXivoUserResponse(
      1,
      List(
        XivoUser(
          1,
          "James Bond",
          Some(1234),
          Some(LineType.UA),
          Some("1000"),
          "XiVO test",
          Some("default"),
          Some("MDS Main"),
          disabled = false,
          Some(List("blue", "green", "red"))
        )
      )
    )

    val filters = List(
      DynamicFilter(
        "labels",
        Some(OperatorContainsAll),
        None,
        None,
        Some(List("green"))
      )
    )

    xivoUserManager.find(filters, 0, 5) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find Xivo user with filters on lineType" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
    Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

    val result = FindXivoUserResponse(
      1,
      List(
        XivoUser(
          1,
          "James Bond",
          Some(1234),
          Some(LineType.UA),
          Some("1000"),
          "XiVO test",
          Some("default"),
          Some("MDS Main"),
          disabled = false,
          Some(List("blue", "green", "red"))
        )
      )
    )

    val filters = List(
      DynamicFilter("lineType", Some(OperatorEq), Some("ua"))
    )

    xivoUserManager.find(filters, 0, 5) match {
      case Success(users) =>
        users shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find Xivo user with filters on provisioning" in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      generateUAandSCCPUsers
      Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
      Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

      val result = FindXivoUserResponse(
        1,
        List(
          XivoUser(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.UA),
            Some("1000"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          )
        )
      )

      val filters = List(
        DynamicFilter("provisioning", Some(OperatorIlike), Some("%23%"))
      )

      xivoUserManager.find(filters, 0, 5) match {
        case Success(users) =>
          users shouldBe result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "find Xivo user with filters on labels with multiple match" in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      generateUAandSCCPUsers
      Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
      Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

      val result = FindXivoUserResponse(
        2,
        List(
          XivoUser(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.UA),
            Some("1000"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          ),
          XivoUser(
            2,
            "John Wayne",
            Some(5678),
            Some(LineType.SCCP),
            Some("1010"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "red"))
          )
        )
      )

      val filters = List(
        DynamicFilter(
          "labels",
          Some(OperatorContainsAll),
          None,
          None,
          Some(List("blue", "red"))
        )
      )

      xivoUserManager.find(filters, 0, 5) match {
        case Success(users) =>
          users shouldBe result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "find Xivo user with filters on labels with multiple match, where condition and ordering" in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      generateUAandSCCPUsers
      Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
      Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

      val result = FindXivoUserResponse(
        2,
        List(
          XivoUser(
            2,
            "John Wayne",
            Some(5678),
            Some(LineType.SCCP),
            Some("1010"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "red"))
          ),
          XivoUser(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.UA),
            Some("1000"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          )
        )
      )

      val filters = List(
        DynamicFilter(
          "labels",
          Some(OperatorContainsAll),
          None,
          None,
          Some(List("blue", "red"))
        ),
        DynamicFilter(
          "entity",
          Some(OperatorEq),
          Some("XiVO test"),
          None,
          None
        ),
        DynamicFilter("fullName", None, None, Some(OrderDesc), None)
      )

      xivoUserManager.find(filters, 0, 5) match {
        case Success(users) =>
          users shouldBe result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "find Xivo user with filters on labels with multiple match, where condition and ordering and pagination" in withConnection() {
    implicit c =>
      val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
      generateUAandSCCPUsers
      Utils.insertXivoUser(3, "Zack", "Bond", Some("zbond"), Some(1), Some(1))
      Utils.insertXivoUser(4, "Aaron", "Bond", Some("abond"), Some(1), Some(1))

      val result = FindXivoUserResponse(
        2,
        List(
          XivoUser(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.UA),
            Some("1000"),
            "XiVO test",
            Some("default"),
            Some("MDS Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          )
        )
      )

      val filters = List(
        DynamicFilter(
          "labels",
          Some(OperatorContainsAll),
          None,
          None,
          Some(List("blue", "red"))
        ),
        DynamicFilter(
          "entity",
          Some(OperatorEq),
          Some("XiVO test"),
          None,
          None
        ),
        DynamicFilter("fullName", None, None, Some(OrderDesc), None)
      )

      xivoUserManager.find(filters, 1, 1) match {
        case Success(users) =>
          users shouldBe result
        case Failure(f) => fail(f.getMessage)
      }
  }

  "validate a XiVO user" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(
      3,
      "Zack",
      "Bond",
      Some("zbond"),
      Some(1),
      Some(1),
      password = Some("zbond")
    )

    xivoUserManager.validateUserPassword("zbond", "zbond") match {
      case Success(_) => succeed
      case Failure(f) => fail(f.getMessage)
    }
  }

  "not validate a XiVO user" in withConnection() { implicit c =>
    val xivoUserManager = app.injector.instanceOf(classOf[XivoUserManager])
    generateUAandSCCPUsers
    Utils.insertXivoUser(
      3,
      "Zack",
      "Bond",
      Some("zbond"),
      Some(1),
      Some(1),
      password = Some("zbond")
    )

    xivoUserManager.validateUserPassword("zbond", "notzbond") match {
      case Success(_) => fail()
      case Failure(_) => succeed
    }
  }
}
