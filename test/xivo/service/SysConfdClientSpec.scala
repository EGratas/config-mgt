package xivo.service

import docker.DockerPlaySpec
import org.mockito.Mockito.when
import org.scalatest.concurrent.ScalaFutures
import play.api.Configuration
import play.api.http.Status.OK
import play.api.routing.sird._
import play.api.test.WsTestClient
import play.core.server.Server

import scala.concurrent.duration.DurationDouble
import scala.concurrent.{Await, ExecutionContext}

class SysConfdClientSpec extends DockerPlaySpec with ScalaFutures {
  implicit val executionContext: ExecutionContext =
    app.injector.instanceOf[ExecutionContext]

  val config: Configuration = mock[Configuration]

  val sysConfdHost    = "localhost"
  val sysConfdPort    = 8668
  val sysConfdTimeout = 5000

  when(config.get[String]("sysconfd.host")).thenReturn(sysConfdHost)
  when(config.getOptional[Int]("sysconfd.port")).thenReturn(Some(sysConfdPort))
  when(config.getOptional[Int]("sysconfd.timeout"))
    .thenReturn(Some(sysConfdTimeout))

  def withSysConfdClient[T](block: SysConfdClient => T): T = {
    Server.withRouterFromComponents() { components =>
      import components.{defaultActionBuilder => Action}
      import play.api.mvc.Results._
      { case POST(p"/exec_request_handlers") =>
        Action {
          Ok("null")
        }
      }
    } { implicit port =>
      WsTestClient.withClient(client =>
        block(new SysConfdClient(config, client) {
          override val baseUri: String = ""
        })
      )
    }
  }

  "SysConfdClient" should {
    "return a success on queue configuration reload" in withSysConfdClient {
      client =>
        val result =
          Await.result(client.reloadAsteriskQueueConfiguration(), 0.5.seconds)
        result.status shouldBe OK
    }
  }

}
