package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils

import scala.util.{Failure, Success}

class StaticSipManagerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE staticsip").execute()
      super.beforeEach()
    }

  "get stun address" in withConnection() { implicit c =>
    val staticSipManager = app.injector.instanceOf(classOf[StaticSipManager])
    val result           = SipConfig(Some("stun:stun.l.google.com:19302"))

    Utils.insertStunAddress(1, Some(result.stunAddress.get))

    staticSipManager.getStunAddress() match {
      case Success(stunAddr) =>
        stunAddr shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "Return None if no stun address is found" in withConnection() { implicit c =>
    val staticSipManager = app.injector.instanceOf(classOf[StaticSipManager])

    staticSipManager.getStunAddress() match {
      case Success(stunAddr) =>
        stunAddr shouldBe SipConfig(None)
      case Failure(f) => fail(f.getMessage)
    }
  }

  "Return None if no value is configured" in withConnection() { implicit c =>
    val staticSipManager = app.injector.instanceOf(classOf[StaticSipManager])
    Utils.insertStunAddress(2, None)

    staticSipManager.getStunAddress() match {
      case Success(stunAddr) =>
        stunAddr shouldBe SipConfig(None)
      case Failure(f) => fail(f.getMessage)
    }
  }
}
