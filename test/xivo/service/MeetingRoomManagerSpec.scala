package xivo.service

import anorm.{AnormException, SQL}
import docker.DockerPlayWithDbSpec
import model._
import org.mockito.Mockito.when
import org.postgresql.util.PSQLException
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.{FindMeetingRoomResult, MeetingRoomConfig, RoomTypes}
import xivo.service.MeetingRoomManager.{
  createGetFilter,
  createPersonalUserIdFilter,
  createRoomFilter,
  NO_FILTER
}

import java.sql.Timestamp
import java.time.Instant
import java.util.UUID
import scala.collection.immutable.LazyList
import scala.util.{Failure, Random, Success}

class MeetingRoomManagerSpec extends DockerPlayWithDbSpec {

  class Helper() {
    val config: MeetingRoomConfiguration = mock[MeetingRoomConfiguration]

    implicit lazy val app: Application = new GuiceApplicationBuilder()
      .configure(playConfigWithDatabase)
      .overrides(bind[MeetingRoomConfiguration].to(config))
      .build()
  }

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE meetingroom").execute()
      super.beforeEach()
    }

  "create a static meeting room" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    val meetingRoom =
      MeetingRoomConfig(
        None,
        "My Conf Room 2",
        Some("1000"),
        None,
        None,
        RoomTypes.Static,
        None
      )

    meetingRoomManager.create(meetingRoom) match {
      case Failure(f) => fail(f.getMessage)
      case Success(room) =>
        val expected = meetingRoom.copy(
          id = room.id,
          uuid = room.uuid,
          pinTimestamp = room.pinTimestamp,
          alias = room.alias
        )
        room shouldEqual expected
    }
  }

  "create a personal meeting room" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    val meetingRoom =
      MeetingRoomConfig(
        None,
        "My Conf Room 2",
        Some("1000"),
        None,
        None,
        RoomTypes.Personal,
        Some(2)
      )

    meetingRoomManager.create(meetingRoom) match {
      case Failure(f) => fail(f.getMessage)
      case Success(room) =>
        val expected = meetingRoom.copy(
          id = room.id,
          uuid = room.uuid,
          pinTimestamp = room.pinTimestamp,
          alias = room.alias
        )
        room shouldEqual expected
    }
  }

  "create a personal meeting room without a number" in new Helper() {
    withConnection() { implicit c =>
      val uuid   = UUID.randomUUID()
      val number = 1000

      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])

      when(config.intervalRange).thenReturn(
        List.range(1000, 1002).map(_.toString)
      )

      Utils.insertMeetingRooms(
        2,
        "My Conf Room 1",
        number,
        None,
        uuid,
        RoomTypes.Static,
        None
      )

      val meetingRoom =
        MeetingRoomConfig(
          None,
          "My Conf Room 2",
          None,
          None,
          None,
          RoomTypes.Static,
          None
        )

      meetingRoomManager.create(meetingRoom) match {
        case Failure(f) => fail(f.getMessage)
        case Success(room) =>
          val expected = meetingRoom.copy(
            id = room.id,
            number = room.number,
            uuid = room.uuid,
            pinTimestamp = room.pinTimestamp,
            alias = room.alias
          )
          room shouldEqual expected
          room.number.get shouldBe "1001"
      }
    }
  }

  "not create a personal meeting room if no number is available" in new Helper() {
    withConnection() { implicit c =>
      val uuid = UUID.randomUUID()

      val meetingRoomManager: MeetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])

      when(config.intervalRange).thenReturn(
        List.range(1000, 1002).map(_.toString)
      )

      Utils.insertMeetingRooms(
        1,
        "My Conf Room 1",
        1000,
        None,
        uuid,
        RoomTypes.Static,
        None
      )

      Utils.insertMeetingRooms(
        2,
        "My Conf Room 2",
        1001,
        None,
        uuid,
        RoomTypes.Static,
        None
      )

      val meetingRoom =
        MeetingRoomConfig(
          None,
          "My Conf Room 3",
          None,
          None,
          None,
          RoomTypes.Static,
          None
        )

      meetingRoomManager.create(meetingRoom) match {
        case Failure(_) => succeed
        case Success(_) => fail("room should not be created")
      }
    }
  }

  "create a personal meeting room with random alphanumeric alias" in withConnection() {
    implicit c =>
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])

      val meetingRoom =
        MeetingRoomConfig(
          None,
          "Conf Room",
          None,
          None,
          None,
          RoomTypes.Static,
          None
        )

      meetingRoomManager.create(meetingRoom) match {
        case Failure(f) => fail(f.getMessage)
        case Success(room) =>
          room.alias.get.matches("^[a-z0-9]4-[a-z0-9]4$")
      }
  }

  "generate another random alphanumeric alias if already taken" in withConnection() {
    implicit c =>
      val config: MeetingRoomConfiguration = mock[MeetingRoomConfiguration]
      val random: Random                   = mock[Random]

      implicit lazy val otherApp: Application = new GuiceApplicationBuilder()
        .configure(playConfigWithDatabase)
        .overrides(bind[MeetingRoomConfiguration].to(config))
        .overrides(bind[Random].to(random))
        .build()

      val meetingRoomManager =
        otherApp.injector.instanceOf(classOf[MeetingRoomManager])

      val meetingRoom1 =
        MeetingRoomConfig(
          None,
          "Conf Room 1",
          Some("1234"),
          None,
          None,
          RoomTypes.Static,
          None
        )

      when(random.nextInt()).thenReturn(0, 1)
      val firstAliasTry  = LazyList('a', 'a', 'a', 'a')
      val secondAliasTry = LazyList('b', 'b', 'b', 'b')
      when(random.alphanumeric).thenReturn(
        firstAliasTry,
        firstAliasTry,
        firstAliasTry,
        firstAliasTry,
        secondAliasTry,
        secondAliasTry
      )

      meetingRoomManager.create(meetingRoom1) match {
        case Failure(f) => fail(f.getMessage)
        case Success(room) =>
          room.alias.get shouldEqual ("aaaa-aaaa")
      }

      val meetingRoom2 =
        MeetingRoomConfig(
          None,
          "Conf Room 2",
          Some("4321"),
          None,
          None,
          RoomTypes.Static,
          None
        )

      meetingRoomManager.create(meetingRoom2) match {
        case Failure(f) => fail(f.getMessage)
        case Success(room) =>
          room.alias.get shouldEqual ("bbbb-bbbb")
      }
  }

  "get a static meeting room" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected =
      MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1000"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

    val filter =
      createRoomFilter(RoomTypes.Static) ++ createGetFilter("id", "1")

    meetingRoomManager.get(filter) match {
      case Failure(f)    => fail(f.getMessage)
      case Success(room) => room shouldEqual expected
    }
  }

  "get a personal meeting room" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      uuid,
      RoomTypes.Personal,
      Some(2),
      ts
    )

    val expected =
      MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1000"),
        None,
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

    val filter = createPersonalUserIdFilter(2) ++ createRoomFilter(
      RoomTypes.Personal
    ) ++ createGetFilter("id", "1")

    meetingRoomManager.get(filter) match {
      case Failure(f)    => fail(f.getMessage)
      case Success(room) => room shouldEqual expected
    }
  }

  "get a meeting room by number" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected =
      MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1000"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

    val filter = List(DynamicFilter("number", Some(OperatorEq), Some("1000")))

    meetingRoomManager.get(filter) match {
      case Failure(f)    => fail(f.getMessage)
      case Success(room) => room shouldEqual expected
    }
  }

  "get a non existing meeting room" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      roomType = RoomTypes.Static,
      userId = None
    )

    val filter = createGetFilter("id", "2")

    val res = meetingRoomManager.get(filter)
    assertThrows[AnormException](res.get)
    res.isFailure shouldBe true
  }

  "get a list of meeting rooms" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])

    Utils.insertMeetingRooms(
      1,
      "Limonest Conf Room",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Toulouse Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Personal,
      Some(2),
      ts
    )

    val expected = List(
      MeetingRoomConfig(
        Some(1),
        "Limonest Conf Room",
        Some("1000"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      ),
      MeetingRoomConfig(
        Some(2),
        "Prague Conf Room",
        Some("1001"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      ),
      MeetingRoomConfig(
        Some(3),
        "Toulouse Conf Room",
        Some("1002"),
        None,
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )
    )

    meetingRoomManager.getAll(NO_FILTER) match {
      case Failure(f) => fail(f.getMessage)
      case Success(rooms) =>
        rooms should contain theSameElementsInOrderAs expected
    }
  }

  "get a list of static meeting rooms" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])

    Utils.insertMeetingRooms(
      1,
      "Limonest Conf Room",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Toulouse Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Personal,
      Some(2),
      ts
    )

    val expected = List(
      MeetingRoomConfig(
        Some(1),
        "Limonest Conf Room",
        Some("1000"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      ),
      MeetingRoomConfig(
        Some(2),
        "Prague Conf Room",
        Some("1001"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )
    )

    val filters = MeetingRoomManager.createRoomFilter(RoomTypes.Static)

    meetingRoomManager.getAll(filters) match {
      case Failure(f) => fail(f.getMessage)
      case Success(rooms) =>
        rooms should contain theSameElementsInOrderAs expected
    }
  }

  "get a list of personal meeting rooms" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])

    Utils.insertMeetingRooms(
      1,
      "Limonest Conf Room",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Toulouse Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Personal,
      Some(2),
      ts
    )

    val expected = List(
      MeetingRoomConfig(
        Some(3),
        "Toulouse Conf Room",
        Some("1002"),
        None,
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )
    )

    val filters = MeetingRoomManager.createPersonalUserIdFilter(
      2
    ) ++ MeetingRoomManager.createRoomFilter(RoomTypes.Personal)

    meetingRoomManager.getAll(filters) match {
      case Failure(f) => fail(f.getMessage)
      case Success(rooms) =>
        rooms should contain theSameElementsInOrderAs expected
    }
  }

  "get an empty list of meeting rooms" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])

    val expected = List()

    meetingRoomManager.getAll(NO_FILTER) match {
      case Failure(f)     => fail(f.getMessage)
      case Success(rooms) => rooms shouldEqual expected
    }
  }

  "update a meeting room" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now().minusSeconds(100))
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val newMeetingRoom = MeetingRoomConfig(
      Some(1),
      "My Conf Room 2",
      Some("1002"),
      Some("1234"),
      None,
      RoomTypes.Static,
      None
    )

    val expected = MeetingRoomConfig(
      Some(1),
      "My Conf Room 2",
      Some("1002"),
      Some("1234"),
      Some(uuid),
      RoomTypes.Static,
      None
    )

    meetingRoomManager.update(newMeetingRoom)

    val filter = createGetFilter("id", "1")

    meetingRoomManager.get(filter) match {
      case Failure(f) => fail(f.getMessage)
      case Success(room) =>
        room.pinTimestamp should not equal ts
        room shouldEqual expected.copy(pinTimestamp = room.pinTimestamp)
    }
  }

  "update a meeting room without updating pin timestamp" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now().minusSeconds(100))
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "My Conf Room 1",
        1000,
        Some("1234"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )

      val newMeetingRoom = MeetingRoomConfig(
        Some(1),
        "My Conf Room 2",
        Some("1002"),
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

      val expected = MeetingRoomConfig(
        Some(1),
        "My Conf Room 2",
        Some("1002"),
        Some("1234"),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      meetingRoomManager.update(newMeetingRoom)

      val filter = createGetFilter("id", "1")

      meetingRoomManager.get(filter) match {
        case Failure(f) => fail(f.getMessage)
        case Success(room) =>
          room.pinTimestamp should not equal ts
          room shouldEqual expected.copy(pinTimestamp = room.pinTimestamp)
      }
  }

  "update a meeting room without id" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now().minusSeconds(100))
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val newMeetingRoom =
      MeetingRoomConfig(
        None,
        "My Conf Room 2",
        Some("1002"),
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

    val expected =
      MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1000"),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

    meetingRoomManager.update(newMeetingRoom) match {
      case Failure(f) =>
        f.getMessage should include("Cannot update a meeting room")
      case Success(_) => fail()
    }

    val filter = createGetFilter("id", "1")

    meetingRoomManager.get(filter) match {
      case Failure(f)    => fail(f.getMessage)
      case Success(room) => room shouldEqual expected
    }
  }

  "update a meeting room violating constraint" in withConnection() {
    val uuid = UUID.randomUUID()
    implicit c =>
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "My Conf Room 1",
        1000,
        None,
        uuid,
        RoomTypes.Static,
        None
      )
      Utils.insertMeetingRooms(
        2,
        "My Conf Room 2",
        1001,
        None,
        uuid,
        RoomTypes.Static,
        None
      )

      val newMeetingRoom = MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1001"),
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

      val res = meetingRoomManager.update(newMeetingRoom)
      assertThrows[PSQLException](res.get)
      res.failed.get.getMessage should include("violates unique constraint")
  }

  "update a non-existent meeting room" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])

    val meetingRoom = MeetingRoomConfig(
      Some(1),
      "My Conf Room 2",
      Some("1002"),
      Some("1234"),
      None,
      RoomTypes.Static,
      None
    )
    meetingRoomManager.update(meetingRoom) match {
      case Failure(_) => succeed
      case Success(_) => fail()
    }
  }

  "update a meeting room without updating uuid" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now().minusSeconds(100))
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])

      Utils.insertMeetingRooms(
        1,
        "My Conf Room 1",
        1000,
        Some("1234"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )

      val newMeetingRoom = MeetingRoomConfig(
        Some(1),
        "My Conf Room 2",
        Some("1002"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None
      )

      val expected = MeetingRoomConfig(
        Some(1),
        "My Conf Room 2",
        Some("1002"),
        Some("1234"),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      meetingRoomManager.update(newMeetingRoom).get shouldEqual expected

      val filter = createGetFilter("id", "1")

      meetingRoomManager.get(filter) match {
        case Failure(f)    => fail(f.getMessage)
        case Success(room) => room shouldEqual expected
      }
  }

  "not update a personal meeting room with different user id" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "My Conf Room 1",
        1000,
        None,
        uuid,
        RoomTypes.Personal,
        Some(2)
      )

      val newMeetingRoom = MeetingRoomConfig(
        Some(1),
        "My Conf Room 2",
        Some("1002"),
        Some("1234"),
        None,
        RoomTypes.Personal,
        Some(3)
      )

      val res = meetingRoomManager.update(newMeetingRoom)

      assertThrows[Exception](res.get)
      res.failed.get.getMessage should include(
        "Cannot update a meeting room with id Some(1)"
      )
  }

  "delete a static meeting room" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      roomType = RoomTypes.Static,
      userId = None
    )

    val filter =
      createRoomFilter(RoomTypes.Static) ++ createGetFilter("id", "1")

    meetingRoomManager.delete(filter) match {
      case Failure(f) => fail(f.getMessage)
      case Success(_) => succeed
    }
  }

  "delete a personal meeting room" in withConnection() { implicit c =>
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      roomType = RoomTypes.Personal,
      userId = Some(2)
    )

    val filter = createRoomFilter(RoomTypes.Personal) ++ createGetFilter(
      "id",
      "1"
    ) ++ createPersonalUserIdFilter(2)

    meetingRoomManager.delete(filter) match {
      case Failure(f) => fail(f.getMessage)
      case Success(_) => succeed
    }
  }

  "delete a non existent meeting room" in withConnection() { implicit c =>
    val differentId = "2"
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1000,
      None,
      roomType = RoomTypes.Static,
      userId = None
    )

    val filter =
      createRoomFilter(RoomTypes.Static) ++ createGetFilter("id", differentId)

    meetingRoomManager.delete(filter) match {
      case Failure(_) => succeed
      case Success(_) => fail()
    }
  }

  "find one meeting room" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "My Conf Room 1",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected = FindMeetingRoomResult(
      1,
      List(
        MeetingRoomConfig(
          Some(1),
          "My Conf Room 1",
          Some("1001"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        )
      )
    )

    meetingRoomManager.find(List.empty, 0, 1) match {
      case Success(room) =>
        room shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find meeting rooms lexically ordered" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Limonest Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Paris Conf Room",
      1003,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      4,
      "Toulouse Conf Room",
      1004,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected = FindMeetingRoomResult(
      4,
      List(
        MeetingRoomConfig(
          Some(2),
          "Limonest Conf Room",
          Some("1002"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        ),
        MeetingRoomConfig(
          Some(3),
          "Paris Conf Room",
          Some("1003"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        )
      )
    )

    meetingRoomManager.find(List.empty, 0, 2) match {
      case Success(room) =>
        room shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find meeting rooms lexically ordered with offset" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now())
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "Prague Conf Room",
        1001,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        2,
        "Limonest Conf Room",
        1002,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        3,
        "Paris Conf Room",
        1003,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        4,
        "Toulouse Conf Room",
        1004,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )

      val expected = FindMeetingRoomResult(
        4,
        List(
          MeetingRoomConfig(
            Some(1),
            "Prague Conf Room",
            Some("1001"),
            None,
            Some(uuid),
            RoomTypes.Static,
            None,
            Some(ts)
          ),
          MeetingRoomConfig(
            Some(4),
            "Toulouse Conf Room",
            Some("1004"),
            None,
            Some(uuid),
            RoomTypes.Static,
            None,
            Some(ts)
          )
        )
      )

      meetingRoomManager.find(List.empty, 2, 2) match {
        case Success(room) =>
          room shouldBe expected
        case Failure(f) => fail(f.getMessage)
      }
  }

  "find meeting room with filters on display name" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now())
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "Prague Conf Room",
        1001,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        2,
        "Limonest Conf Room",
        1002,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )

      val filters = List(
        DynamicFilter("displayName", Some(OperatorLike), Some("Prague%"))
      )

      val expected = FindMeetingRoomResult(
        1,
        List(
          MeetingRoomConfig(
            Some(1),
            "Prague Conf Room",
            Some("1001"),
            None,
            Some(uuid),
            RoomTypes.Static,
            None,
            Some(ts)
          )
        )
      )

      meetingRoomManager.find(filters, 0, 5) match {
        case Success(room) =>
          room shouldBe expected
        case Failure(f) => fail(f.getMessage)
      }
  }

  "sort meeting rooms by name DESC" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Limonest Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Paris Conf Room",
      1003,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      4,
      "Toulouse Conf Room",
      1004,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected = FindMeetingRoomResult(
      4,
      List(
        MeetingRoomConfig(
          Some(4),
          "Toulouse Conf Room",
          Some("1004"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        ),
        MeetingRoomConfig(
          Some(1),
          "Prague Conf Room",
          Some("1001"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        )
      )
    )

    meetingRoomManager.find(
      List(DynamicFilter("displayName", None, None, Some(OrderDesc))),
      0,
      2
    ) match {
      case Success(rooms) =>
        rooms shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "sort meeting rooms by name ASC" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Limonest Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Paris Conf Room",
      1003,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      4,
      "Toulouse Conf Room",
      1004,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected = FindMeetingRoomResult(
      4,
      List(
        MeetingRoomConfig(
          Some(2),
          "Limonest Conf Room",
          Some("1002"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        ),
        MeetingRoomConfig(
          Some(3),
          "Paris Conf Room",
          Some("1003"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        )
      )
    )

    meetingRoomManager.find(
      List(DynamicFilter("displayName", None, None, Some(OrderAsc))),
      0,
      2
    ) match {
      case Success(rooms) =>
        rooms shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find meeting rooms with filters on number" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now())
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "Prague Conf Room",
        1001,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        2,
        "Limonest Conf Room",
        1002,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        3,
        "Paris Conf Room",
        1003,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        4,
        "Toulouse Conf Room",
        1004,
        None,
        uuid,
        RoomTypes.Static,
        None,
        ts
      )

      val expected = FindMeetingRoomResult(
        1,
        List(
          MeetingRoomConfig(
            Some(4),
            "Toulouse Conf Room",
            Some("1004"),
            None,
            Some(uuid),
            RoomTypes.Static,
            None,
            Some(ts)
          )
        )
      )

      meetingRoomManager.find(
        List(
          DynamicFilter("number", Some(OperatorEq), Some("1004"), None, None)
        ),
        0,
        5
      ) match {
        case Success(rooms) =>
          rooms shouldBe expected
        case Failure(f) => fail(f.getMessage)
      }
  }

  "find meeting rooms with filters on name" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "Prague Conf Room",
      1001,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Limonest Conf Room",
      1002,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Paris Conf Room",
      1003,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      4,
      "Toulouse Conf Room",
      1004,
      None,
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected = FindMeetingRoomResult(
      1,
      List(
        MeetingRoomConfig(
          Some(1),
          "Prague Conf Room",
          Some("1001"),
          None,
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        )
      )
    )

    meetingRoomManager.find(
      List(
        DynamicFilter(
          "display_name",
          Some(OperatorEq),
          Some("Prague Conf Room"),
          None,
          None
        )
      ),
      0,
      5
    ) match {
      case Success(rooms) =>
        rooms shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find meeting rooms with filters on pin" in withConnection() { implicit c =>
    val uuid = UUID.randomUUID()
    val ts   = Timestamp.from(Instant.now())
    ts.setNanos(0)
    val meetingRoomManager =
      app.injector.instanceOf(classOf[MeetingRoomManager])
    Utils.insertMeetingRooms(
      1,
      "Prague Conf Room",
      1001,
      Some("1111"),
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      2,
      "Limonest Conf Room",
      1002,
      Some("2222"),
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      3,
      "Paris Conf Room",
      1003,
      Some("1111"),
      uuid,
      RoomTypes.Static,
      None,
      ts
    )
    Utils.insertMeetingRooms(
      4,
      "Toulouse Conf Room",
      1004,
      Some("3333"),
      uuid,
      RoomTypes.Static,
      None,
      ts
    )

    val expected = FindMeetingRoomResult(
      2,
      List(
        MeetingRoomConfig(
          Some(3),
          "Paris Conf Room",
          Some("1003"),
          Some("1111"),
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        ),
        MeetingRoomConfig(
          Some(1),
          "Prague Conf Room",
          Some("1001"),
          Some("1111"),
          Some(uuid),
          RoomTypes.Static,
          None,
          Some(ts)
        )
      )
    )

    meetingRoomManager.find(
      List(
        DynamicFilter("userPin", Some(OperatorEq), Some("1111"), None, None)
      ),
      0,
      5
    ) match {
      case Success(rooms) =>
        rooms shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "find meeting rooms with filters on personal room type" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now())
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "Prague Conf Room",
        1001,
        Some("1111"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        2,
        "Limonest Conf Room",
        1002,
        Some("2222"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        3,
        "Paris Conf Room",
        1003,
        Some("1111"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        4,
        "Toulouse Conf Room",
        1004,
        Some("3333"),
        uuid,
        RoomTypes.Personal,
        Some(2),
        ts
      )

      val expected = FindMeetingRoomResult(
        1,
        List(
          MeetingRoomConfig(
            Some(4),
            "Toulouse Conf Room",
            Some("1004"),
            Some("3333"),
            Some(uuid),
            RoomTypes.Personal,
            Some(2),
            Some(ts)
          )
        )
      )

      val findFilters = MeetingRoomManager.createRoomFilter(
        RoomTypes.Personal
      ) ++ MeetingRoomManager.createPersonalUserIdFilter(2)

      meetingRoomManager.find(
        findFilters,
        0,
        5
      ) match {
        case Success(rooms) =>
          rooms shouldBe expected
        case Failure(f) => fail(f.getMessage)
      }
  }

  "find meeting rooms with filters on static room type" in withConnection() {
    implicit c =>
      val uuid = UUID.randomUUID()
      val ts   = Timestamp.from(Instant.now())
      ts.setNanos(0)
      val meetingRoomManager =
        app.injector.instanceOf(classOf[MeetingRoomManager])
      Utils.insertMeetingRooms(
        1,
        "Prague Conf Room",
        1001,
        Some("1111"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        3,
        "Paris Conf Room",
        1003,
        Some("1111"),
        uuid,
        RoomTypes.Static,
        None,
        ts
      )
      Utils.insertMeetingRooms(
        4,
        "Toulouse Conf Room",
        1004,
        Some("3333"),
        uuid,
        RoomTypes.Personal,
        Some(2),
        ts
      )

      val expected = FindMeetingRoomResult(
        2,
        List(
          MeetingRoomConfig(
            Some(3),
            "Paris Conf Room",
            Some("1003"),
            Some("1111"),
            Some(uuid),
            RoomTypes.Static,
            None,
            Some(ts)
          ),
          MeetingRoomConfig(
            Some(1),
            "Prague Conf Room",
            Some("1001"),
            Some("1111"),
            Some(uuid),
            RoomTypes.Static,
            None,
            Some(ts)
          )
        )
      )

      val findFilters = MeetingRoomManager.createRoomFilter(RoomTypes.Static)

      meetingRoomManager.find(
        findFilters,
        0,
        5
      ) match {
        case Success(rooms) =>
          rooms shouldBe expected
        case Failure(f) => fail(f.getMessage)
      }
  }
}
