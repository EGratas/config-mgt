package xivo.service

import anorm.SQL
import cti.models.{CtiStatus, CtiStatusActionLegacy, CtiStatusLegacy}
import docker.DockerPlayWithDbSpec
import model.Utils
import play.api.libs.ws.WSResponse

import scala.util.{Failure, Success}

class CtiStatusManagerSpec extends DockerPlayWithDbSpec {

  val wsResponse: WSResponse = mock[WSResponse]

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      super.beforeEach()
      SQL("TRUNCATE ctistatus").execute()
      SQL("TRUNCATE ctipresences").execute()
      SQL("TRUNCATE cti_profile").execute()
    }


  def insertDummyData(): Unit = {
    withConnection() { implicit c =>
      Utils.insertCtiStatus(1, "available1", "available1", "queueunpause_all", "green", "", 1)
      Utils.insertCtiStatus(1, "pause1", "pause1", "queuepause_all", "red", "", 1)
      Utils.insertCtiStatus(1, "logoff1", "logoff1", "agentlogoff", "grey", "", 1)
      Utils.insertCtiStatus(2, "available2", "available2", "", "green", "", 1)
      Utils.insertCtiStatus(2, "pause2", "pause2", "enablednd(true),queuepause_all(true)", "red", "", 1)
      Utils.insertCtiStatus(2, "logoff2", "logoff2", "agentlogoff", "grey", "", 1)

      Utils.insertCtiPresences(1, "xivo", "De base non supprimable", 0)
      Utils.insertCtiPresences(2, "custom", "Supprimable", 1)

      Utils.insertCtiProfiles(1, "Supervisor", 1, 1)
      Utils.insertCtiProfiles(2, "Client", 1, 1)
      Utils.insertCtiProfiles(3, "Switchboard", 1, 1)
      Utils.insertCtiProfiles(4, "Agent", 2, 1)
    }
  }


  "get Cti Status Legacy for api" in withConnection() { implicit c =>
    val ctiStatusManager = app.injector.instanceOf(classOf[CtiStatusManager])
    insertDummyData()

    val result: Map[String, List[CtiStatusLegacy]] =
      Map[String, List[CtiStatusLegacy]](
        "1" -> List(
          CtiStatusLegacy(
            Option("available1"),
            Option("available1"),
            Option("green"),
            List(CtiStatusActionLegacy("queueunpause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("pause1"),
            Option("pause1"),
            Option("red"),
            List(CtiStatusActionLegacy("queuepause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("logoff1"),
            Option("logoff1"),
            Option("grey"),
            List(CtiStatusActionLegacy("agentlogoff", Option("")))
          )
        ),
        "4" -> List(
          CtiStatusLegacy(
            Option("available2"),
            Option("available2"),
            Option("green"),
            List()
          ),
          CtiStatusLegacy(
            Option("pause2"),
            Option("pause2"),
            Option("red"),
            List(
              CtiStatusActionLegacy("enablednd", Option("true")),
              CtiStatusActionLegacy("queuepause_all", Option("true"))
            )
          ),
          CtiStatusLegacy(
            Option("logoff2"),
            Option("logoff2"),
            Option("grey"),
            List(CtiStatusActionLegacy("agentlogoff", Option("")))
          )
        ),
        "2" -> List(
          CtiStatusLegacy(
            Option("available1"),
            Option("available1"),
            Option("green"),
            List(CtiStatusActionLegacy("queueunpause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("pause1"),
            Option("pause1"),
            Option("red"),
            List(CtiStatusActionLegacy("queuepause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("logoff1"),
            Option("logoff1"),
            Option("grey"),
            List(CtiStatusActionLegacy("agentlogoff", Option("")))
          )
        ),
        "3" -> List(
          CtiStatusLegacy(
            Option("available1"),
            Option("available1"),
            Option("green"),
            List(CtiStatusActionLegacy("queueunpause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("pause1"),
            Option("pause1"),
            Option("red"),
            List(CtiStatusActionLegacy("queuepause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("logoff1"),
            Option("logoff1"),
            Option("grey"),
            List(CtiStatusActionLegacy("agentlogoff", Option("")))
          )
        )
      )

    ctiStatusManager.getLegacy match {
      case Success(ctistatus) =>
        ctistatus shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get Cti Status when have custom pause for api" in withConnection() { implicit c =>
    val ctiStatusManager = app.injector.instanceOf(classOf[CtiStatusManager])
    insertDummyData()

    val result: Map[String, List[CtiStatus]] = Map[String, List[CtiStatus]](
      "1" -> List(
        CtiStatus(Option("logoff1"), Option("logoff1"), Option(2)),
          CtiStatus(Option("pause1"), Option("pause1"), Option(1)),
          CtiStatus(Option("available1"), Option("available1"), Option(0))
      ),
      "2" -> List(
        CtiStatus(Option("logoff1"), Option("logoff1"), Option(2)),
          CtiStatus(Option("pause1"), Option("pause1"), Option(1)),
          CtiStatus(Option("available1"), Option("available1"), Option(0))
      ),
      "3" -> List(
        CtiStatus(Option("logoff1"), Option("logoff1"), Option(2)),
          CtiStatus(Option("pause1"), Option("pause1"), Option(1)),
          CtiStatus(Option("available1"), Option("available1"), Option(0))
      ),
      "4" -> List(
        CtiStatus(Option("logoff2"), Option("logoff2"), Option(2)),
        CtiStatus(Option("pause2"), Option("pause2"), Option(1)),
          CtiStatus(Option("available2"), Option("available2"), Option(0))
      )
    )

    ctiStatusManager.get match {
      case Success(ctistatus) =>
        ctistatus shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get Cti Status when no custom pause for api" in withConnection() { implicit c =>
    val ctiStatusManager = app.injector.instanceOf(classOf[CtiStatusManager])

      Utils.insertCtiStatus(1, "available", "available", "queueunpause_all", "green", "", 1)
      Utils.insertCtiStatus(1, "logoff", "logoff", "agentlogoff", "grey", "", 1)

      Utils.insertCtiPresences(1, "xivo", "De base non supprimable", 0)

      Utils.insertCtiProfiles(1, "Supervisor", 1, 1)

    val result: Map[String, List[CtiStatus]] = Map[String, List[CtiStatus]](
      "1" -> List(
        CtiStatus(Option("logoff"), Option("logoff"), Option(2)),
        CtiStatus(Option("available"), Option("available"), Option(0)),
        CtiStatus(None, Option("Pause"), Option(1))
      )
    )

    ctiStatusManager.get match {
      case Success(ctistatus) =>
        ctistatus shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }
}
