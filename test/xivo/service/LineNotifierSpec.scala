package xivo.service

import docker.DockerPlayWithDbSpec
import eu.timepit.refined.refineMV
import org.mockito.Mockito._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import ws.model.{Line, LineType}
import xivo.model.rabbitmq._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class LineNotifierSpec extends DockerPlayWithDbSpec {

  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]
  val confdclient: ConfdClient = mock[ConfdClient]

  val lineTypes = List(
    LineType.PHONE,
    LineType.UA,
    LineType.WEBRTC,
    LineType.SCCP,
    LineType.CUSTOM
  )

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .overrides(bind[ConfdClient].to(confdclient))
    .build()

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      reset(rabbitPublisher)
      reset(confdclient)
      super.beforeEach()
    }

  private def getEndpointCreatedEventType(
      lineType: LineType.Type
  ): RabbitMessageType =
    lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => SipEndpointCreated
      case LineType.SCCP                                  => SccpEndpointCreated
      case LineType.CUSTOM                                => CustomEndpointCreated
    }

  private def getEndpointEditedEventType(
      lineType: LineType.Type
  ): RabbitMessageType =
    lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => SipEndpointEdited
      case LineType.SCCP                                  => SccpEndpointEdited
      case LineType.CUSTOM                                => CustomEndpointEdited
    }

  private def getEndpointDeletedEventType(
      lineType: LineType.Type
  ): RabbitMessageType =
    lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => SipEndpointDeleted
      case LineType.SCCP                                  => SccpEndpointDeleted
      case LineType.CUSTOM                                => CustomEndpointDeleted
    }

  "send message to rabbitmq to notify of line creation" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid              = 123
    val userid              = 456
    val endpointid          = 789
    val device: Line.Device = refineMV("c3d29985f8f8403aafbebec638e5c12e")

    lineTypes.foreach(lineType => {
      val line = Line(
        Some(lineid),
        lineType,
        refineMV("default"),
        refineMV("1000"),
        refineMV("test"),
        Some(device),
        Some(refineMV("1hlnzr8t")),
        Some(1),
        Some(1234)
      )

      reset(rabbitPublisher)
      reset(confdclient)
      notifier.onUserLineCreated(userid, line, endpointid)
      verify(rabbitPublisher).publish(
        ObjectEvent(getEndpointCreatedEventType(line.lineType), endpointid)
      )
      verify(rabbitPublisher).publish(ObjectEvent(LineCreated, lineid))
      verify(rabbitPublisher).publish(
        UserLineEvent(UserLineAssociated, userid, lineid)
      )
      verify(confdclient).deviceUpdate(device)
      verify(rabbitPublisher).publish(
        IpbxEvent(List(IpbxSipReload, IpbxDialplanReload, IpbxSccpReload))
      )
    })
  }

  "do not send message to confd on line creation when device is empty" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid              = 123
    val userid              = 456
    val endpointid          = 789
    val device: Line.Device = refineMV("")

    val line = Line(
      Some(lineid),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(device),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )

    reset(confdclient)
    notifier.onUserLineCreated(userid, line, endpointid)
    verify(confdclient, never()).deviceUpdate(device)
  }

  "send message to rabbitmq to notify of line edition" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid              = 123
    val userid              = 456
    val endpointid          = 789
    val device: Line.Device = refineMV("c3d29985f8f8403aafbebec638e5c12e")

    lineTypes.foreach(lineType => {
      val line = Line(
        Some(lineid),
        lineType,
        refineMV("default"),
        refineMV("1000"),
        refineMV("test"),
        Some(device),
        Some(refineMV("1hlnzr8t")),
        Some(1),
        Some(1234)
      )

      reset(rabbitPublisher)
      reset(confdclient)
      notifier.onUserLineEdited(userid, line, line, endpointid)
      verify(rabbitPublisher).publish(
        ObjectEvent(getEndpointEditedEventType(line.lineType), endpointid)
      )
      verify(rabbitPublisher).publish(ObjectEvent(LineEdited, lineid))
      verify(confdclient).deviceUpdate(device)
      verify(rabbitPublisher).publish(
        IpbxEvent(List(IpbxSipReload, IpbxDialplanReload, IpbxSccpReload))
      )
    })
  }

  "do not send message to confd on line edition when device is empty" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid              = 123
    val userid              = 456
    val endpointid          = 789
    val device: Line.Device = refineMV("")

    val line = Line(
      Some(lineid),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(device),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )

    reset(confdclient)
    notifier.onUserLineEdited(userid, line, line, endpointid)
    verify(confdclient, never()).deviceUpdate(device)
  }

  "send message to confd on line edition for old and new line" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid                 = 123
    val userid                 = 456
    val endpointid             = 789
    val olddevice: Line.Device = refineMV("oldchap")
    val newdevice: Line.Device = refineMV("newlad")

    val oldline = Line(
      Some(lineid),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(olddevice),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    val newline = Line(
      Some(lineid),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(newdevice),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )

    reset(confdclient)
    notifier.onUserLineEdited(userid, oldline, newline, endpointid)
    verify(confdclient).deviceUpdate(olddevice)
    verify(confdclient).deviceUpdate(newdevice)
  }

  "send message to rabbitmq to notify of line deletion" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid              = 123
    val userid              = 456
    val endpointid          = 789
    val device: Line.Device = refineMV("c3d29985f8f8403aafbebec638e5c12e")

    lineTypes.foreach(lineType => {
      val line = Line(
        Some(lineid),
        lineType,
        refineMV("default"),
        refineMV("1000"),
        refineMV("test"),
        Some(device),
        Some(refineMV("1hlnzr8t")),
        Some(1),
        Some(1234)
      )

      reset(rabbitPublisher)
      reset(confdclient)
      notifier.onUserLineDeleted(userid, line, endpointid)
      verify(rabbitPublisher).publish(
        ObjectEvent(getEndpointDeletedEventType(line.lineType), endpointid)
      )
      verify(rabbitPublisher).publish(ObjectEvent(LineDeleted, lineid))
      verify(rabbitPublisher).publish(
        UserLineEvent(UserLineDissociated, userid, lineid)
      )
      verify(confdclient).deviceUpdate(device)
      verify(rabbitPublisher).publish(
        IpbxEvent(List(IpbxSipReload, IpbxDialplanReload, IpbxSccpReload))
      )
    })
  }

  "do not send message to confd on line deletion when device is empty" in {
    val notifier = app.injector.instanceOf(classOf[LineNotifier])

    val lineid              = 123
    val userid              = 456
    val endpointid          = 789
    val device: Line.Device = refineMV("")

    val line = Line(
      Some(lineid),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(device),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )

    reset(confdclient)
    notifier.onUserLineDeleted(userid, line, endpointid)
    verify(confdclient, never()).deviceUpdate(device)
  }

}
