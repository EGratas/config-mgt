package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import xivo.model.Xivo

import scala.util.{Failure, Success}

class XivoManagerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE infos").execute()

      super.beforeEach()
    }

  "get xivo uuid from database" in withConnection() { implicit c =>
    val xivoManager = app.injector.instanceOf(classOf[XivoManager])
    val uuid        = java.util.UUID.randomUUID()
    Utils.insertXivoUUID(uuid)

    xivoManager.get() match {
      case Success(xivo) =>
        xivo shouldBe Xivo(uuid)

      case Failure(f) =>
        fail(f.getMessage)
    }

  }

}
