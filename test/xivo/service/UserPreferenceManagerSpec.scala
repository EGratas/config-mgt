package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import org.mockito.Mockito.{reset, times, verify, verifyNoMoreInteractions}
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.rabbitmq.{
  ObjectEvent,
  UserPreferenceCreated,
  UserPreferenceDeleted,
  UserPreferenceEdited
}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import scala.util.{Failure, Success}

class UserPreferenceManagerSpec extends DockerPlayWithDbSpec {

  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE userfeatures, user_line, userpreferences").execute()
      super.beforeEach()
    }

  "get all user preferences" in withConnection() { implicit c =>
    val userPreferenceManager =
      app.injector.instanceOf(classOf[UserPreferenceManager])

    Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))
    Utils.insertUserPreference(7, "preferred_device", "phone", "String")
    Utils.insertUserPreference(7, "other_option", "some_value", "String")

    val result = UserPreferences(
      List(
        UserPreference(7, "other_option", "some_value", "String"),
        UserPreference(7, "preferred_device", "phone", "String")
      )
    )
    userPreferenceManager.getAll(7) match {
      case Success(usrPref) =>
        usrPref shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get a specific user preference" in withConnection() { implicit c =>
    val userPreferenceManager =
      app.injector.instanceOf(classOf[UserPreferenceManager])

    Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))
    Utils.insertUserPreference(7, "preferred_device", "phone", "String")
    Utils.insertUserPreference(7, "other_option", "some_value", "String")

    val result = UserPreference(7, "preferred_device", "phone", "String")
    userPreferenceManager.get(result.userId, result.key) match {
      case Success(usrPref) =>
        usrPref shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "create a user preference" in withConnection() { implicit c =>
    val userPreferenceManager =
      app.injector.instanceOf(classOf[UserPreferenceManager])

    Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))

    val result = UserPreference(7, "preferred_device", "phone", "String")
    userPreferenceManager.create(result) match {
      case Success(pk) =>
        pk shouldBe 7
      case Failure(f) =>
        f.printStackTrace()
        fail(f)
    }

    val fetchedPreferences = userPreferenceManager.getAll(7)

    fetchedPreferences.isSuccess shouldBe true
    fetchedPreferences.get shouldEqual UserPreferences(List(result))

    verify(rabbitPublisher).publish(
      ObjectEvent(UserPreferenceCreated, 7)
    )
  }

  "return error if user preference already exist" in withConnection() {
    implicit c =>
      val userPreferenceManager =
        app.injector.instanceOf(classOf[UserPreferenceManager])

      Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))
      Utils.insertUserPreference(7, "preferred_device", "phone", "String")
      val result = UserPreference(7, "preferred_device", "phone", "String")

      userPreferenceManager.create(result) match {
        case Success(_) =>
          fail(
            "Trying to create already existing preference should return an error"
          )
        case Failure(f) =>
          f shouldBe a[PreferenceAlreadyExistException]
      }

      val fetchedPreferences = userPreferenceManager.getAll(7)

      fetchedPreferences.isSuccess shouldBe true
      fetchedPreferences.get shouldEqual UserPreferences(List(result))
  }

  "update a user preference" in withConnection() { implicit c =>
    val userPreferenceManager =
      app.injector.instanceOf(classOf[UserPreferenceManager])

    Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))
    Utils.insertUserPreference(7, "preferred_device", "phone", "String")
    Utils.insertUserPreference(7, "other_option", "some_value", "String")

    val result = UserPreference(7, "preferred_device", "webrtc", "String")
    userPreferenceManager.update(result) match {
      case Success(nbUpdated) =>
        nbUpdated shouldBe 1
      case Failure(f) =>
        f.printStackTrace()
        fail(f)
    }

    val fetchedPreferences = userPreferenceManager.getAll(7)

    fetchedPreferences.isSuccess shouldBe true
    fetchedPreferences.get shouldEqual UserPreferences(
      List(UserPreference(7, "other_option", "some_value", "String"), result)
    )

    verify(rabbitPublisher).publish(
      ObjectEvent(UserPreferenceEdited, 7)
    )
  }

  "Delete a user preference" in withConnection() { implicit c =>
    val userPreferenceManager =
      app.injector.instanceOf(classOf[UserPreferenceManager])

    Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))
    Utils.insertUserPreference(7, "preferred_device", "phone", "String")
    Utils.insertUserPreference(7, "other_option", "some_value", "String")

    val result = UserPreference(7, "other_option", "some_value", "String")
    userPreferenceManager.delete(7, "preferred_device") match {
      case Success(nbUpdated) =>
        nbUpdated shouldBe 1
      case Failure(f) =>
        f.printStackTrace()
        fail(f)
    }

    val fetchedPreferences = userPreferenceManager.getAll(7)

    fetchedPreferences.isSuccess shouldBe true
    fetchedPreferences.get shouldEqual UserPreferences(List(result))

    verify(rabbitPublisher).publish(
      ObjectEvent(UserPreferenceDeleted, 7)
    )
  }

  "Create or update a preference" in withConnection() { implicit c =>
    reset(rabbitPublisher)
    val userPreferenceManager =
      app.injector.instanceOf(classOf[UserPreferenceManager])

    Utils.insertXivoUser(7, "James", "Bond", Some("jbond"), Some(1), Some(1))
    Utils.insertUserPreference(7, "preferred_device", "phone", "String")

    val preferenceCreation =
      UserPreference(7, "preferred_device", "webrtc", "String")

    val preferenceUpdate =
      UserPreference(7, "other_key", "some value", "String")

    (for {
      _                 <- userPreferenceManager.createOrUpdate(preferenceCreation)
      _                 <- userPreferenceManager.createOrUpdate(preferenceUpdate)
      preferenceUpdated <- userPreferenceManager.get(7, "preferred_device")
      if preferenceUpdated.value == "webrtc"
      preferenceCreated <- userPreferenceManager.get(7, "other_key")
      if preferenceCreated.value == "some value"
    } yield preferenceCreated) shouldBe Success(preferenceUpdate)

    verify(rabbitPublisher, times(1)).publish(
      ObjectEvent(UserPreferenceCreated, 7)
    )
    verify(rabbitPublisher, times(1)).publish(
      ObjectEvent(UserPreferenceEdited, 7)
    )
    verifyNoMoreInteractions(rabbitPublisher)
  }
}
