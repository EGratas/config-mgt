package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import model.Utils.{TrunkProtocol, UserCustomCategory, UserIaxCategory}
import org.mockito.Mockito._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import ws.controllers.route.RouteHelper
import xivo.model._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import scala.util.{Failure, Success}

class RouteManagerSpec extends DockerPlayWithDbSpec {

  val rabbitPublisher = mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      reset(rabbitPublisher)
      SQL(
        "TRUNCATE route, routecontext, routemediaserver, routetrunk, trunkfeatures, mediaserver, usersip"
      ).execute()

      super.beforeEach()
    }

  "RouteManager" should {
    "create route" in {
      val dialPattern  = DialPattern("XXXX", None, None, None)
      val routeManager = app.injector.instanceOf(classOf[RouteManager])
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule  = List(RichSchedule(1, "evening", true))
      val richRightList = List(RichRight(1, "deny", true))
      val richRouteNoId = RichRouteNoId(
        List(dialPattern),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        richRightList,
        Some("sub"),
        internal = false
      )

      routeManager.createRoute(richRouteNoId) match {
        case Success(created) =>
          created shouldBe richRouteNoId
            .withId(created.id)
            .copy(dialPattern =
              richRouteNoId.dialpattern.map(p =>
                DialPattern(
                  RouteHelper.toAsteriskPattern(p.pattern),
                  p.target,
                  p.regexp,
                  p.callerid
                )
              )
            )
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get highest priority" in {
      val dialPattern  = DialPattern("XXXX", None, None, None)
      val routeManager = app.injector.instanceOf(classOf[RouteManager])
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule  = List(RichSchedule(1, "evening", true))
      val richRightList = List(RichRight(1, "deny", true))
      val richRouteNoId = RichRouteNoId(
        List(dialPattern),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        richRightList,
        None,
        internal = false
      )

      routeManager.createRoute(richRouteNoId)

      routeManager.getLastPriority match {
        case Success(priority) =>
          priority shouldBe 2
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get highest priority if table is empty" in {
      val routeManager = app.injector.instanceOf(classOf[RouteManager])

      routeManager.getLastPriority match {
        case Success(priority) =>
          priority shouldBe 1
        case Failure(f) => fail(f.getMessage)
      }
    }

    "delete route" in {
      val dialPattern  = DialPattern("XXXX", None, None, None)
      val routeManager = app.injector.instanceOf(classOf[RouteManager])
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule = List(RichSchedule(1, "evening", true))

      val richRouteNoId = RichRouteNoId(
        List(dialPattern),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        List(),
        None,
        internal = false
      )
      val route = PlainRoute(
        1,
        List(dialPattern),
        None,
        None,
        1,
        List(),
        List(),
        List(),
        List(),
        List(),
        false
      )

      val created = routeManager.createRoute(richRouteNoId).get
      routeManager.createRoutePattern(created.id, dialPattern).get

      routeManager.delete(created.id) match {
        case Success(deleted) =>
          deleted shouldBe route.copy(id = created.id)
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get route" in {
      val routeManager = app.injector.instanceOf(classOf[RouteManager])
      val dialPattern  = DialPattern("XXXX", None, None, None)
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule  = List(RichSchedule(1, "evening", true))
      val richRightList = List(RichRight(1, "deny", true))
      val richRouteNoId = RichRouteNoId(
        List(dialPattern),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        richRightList,
        Some("sub"),
        internal = false
      )
      val route = PlainRoute(
        1,
        List(dialPattern),
        Some("sub"),
        None,
        1,
        List(),
        List(),
        List(),
        List(),
        List(),
        false
      )

      val created = routeManager.createRoute(richRouteNoId).get
      routeManager.createRoutePattern(created.id, dialPattern).get

      routeManager.get(created.id) match {
        case Success(r) =>
          r shouldBe route.copy(id = created.id)
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get all routes" in withConnection() { implicit c =>
      val mdsManager   = app.injector.instanceOf(classOf[MediaServerManager])
      val routeManager = app.injector.instanceOf(classOf[RouteManager])

      val dialPattern = DialPattern("XXXX", None, None, None)
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule  = List(RichSchedule(1, "evening", true))
      val richRightList = List(RichRight(1, "deny", true))
      val richRouteNoId = RichRouteNoId(
        List(dialPattern),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        richRightList,
        None,
        internal = false
      )
      val route = PlainRoute(
        1,
        List(dialPattern),
        None,
        None,
        1,
        List(),
        List(),
        List(),
        List(),
        List(),
        false
      )

      val mdsId = mdsManager
        .create(MediaServerConfigNoId("mds0", "MDS 0", Some("1.1.1.1")))
        .get
        .id
      val trunkFeatureId: Long =
        Utils.insertTrunkfeatures("sip", 1, 0, 0, "", mdsId.toInt).toLong

      val created1 = routeManager.createRoute(richRouteNoId).get
      val created2 = routeManager.createRoute(richRouteNoId).get
      routeManager.createRoutePattern(created1.id, dialPattern).get
      routeManager.createRoutePattern(created2.id, dialPattern).get
      routeManager.createRouteTrunk(created1.id, trunkFeatureId, 1).get

      routeManager.getAll match {
        case Success(r) =>
          r should contain theSameElementsAs List(
            route.copy(id = created1.id, trunkIds = List(SortedTrunkIds(1, 1))),
            route.copy(id = created2.id)
          )
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get route with raw data" in withConnection() { implicit c =>
      val mdsManager   = app.injector.instanceOf(classOf[MediaServerManager])
      val routeManager = app.injector.instanceOf(classOf[RouteManager])

      val dialPattern = DialPattern("XXXX", None, None, None)
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule  = List(RichSchedule(1, "evening", true))
      val richRightList = List(RichRight(1, "deny", true))
      val richRouteNoId = RichRouteNoId(
        List(dialPattern),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        richRightList,
        Some("sub"),
        internal = true
      )

      val mdsId = mdsManager
        .create(MediaServerConfigNoId("mds0", "MDS 0", Some("1.1.1.1")))
        .get
        .id
      val trunkFeatureId: Long =
        Utils.insertTrunkfeatures("sip", 1, 0, 0, "", mdsId.toInt).toLong

      val expected = PlainRoute(
        1,
        List(dialPattern),
        Some("sub"),
        None,
        1,
        List("default"),
        List(1),
        List(SortedTrunkIds(trunkFeatureId, 1)),
        List(1),
        List(1),
        internal = true
      )

      val created = routeManager.createRoute(richRouteNoId).get
      routeManager.createRoutePattern(created.id, dialPattern).get
      routeManager.createRouteContext(created.id, "default").get
      routeManager.createRouteMediaserver(created.id, 1).get
      routeManager.createRouteTrunk(created.id, trunkFeatureId, 1).get
      routeManager.createRouteSchedule(created.id, 1).get
      routeManager.createRouteRight(created.id, 1).get

      routeManager.get(created.id) match {
        case Success(r) =>
          r shouldBe expected.copy(id = created.id)
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get trunk feature with sip trunk" in withConnection() { implicit c =>
      val mdsManager   = app.injector.instanceOf(classOf[MediaServerManager])
      val routeManager = app.injector.instanceOf(classOf[RouteManager])

      val mdsId = mdsManager
        .create(MediaServerConfigNoId("mds0", "MDS 0", Some("1.1.1.1")))
        .get
        .id

      val userSipId = Utils.insertUserSip("sip-trunk1", UserIaxCategory.trunk)
      val trunkFeatureId = Utils
        .insertTrunkfeatures("sip", userSipId.toInt, 0, 0, "", mdsId.toInt)
        .toLong

      routeManager.getTrunkAll match {
        case Success(trunks) =>
          trunks shouldBe List(TrunkSip(trunkFeatureId, "sip-trunk1"))
        case Failure(f) => fail(f.getMessage)
      }
    }

    "get trunk feature with custom trunk" in withConnection() { implicit c =>
      val mdsManager   = app.injector.instanceOf(classOf[MediaServerManager])
      val routeManager = app.injector.instanceOf(classOf[RouteManager])

      val mdsId = mdsManager
        .create(MediaServerConfigNoId("mds0", "MDS 0", Some("1.1.1.1")))
        .get
        .id
      val userCustomId = Utils.insertUserCustom(
        "custom-1",
        UserCustomCategory.trunk,
        "SIP/abcd",
        TrunkProtocol.custom
      )
      val trunkFeatureId = Utils
        .insertTrunkfeatures(
          "custom",
          userCustomId.toInt,
          0,
          0,
          "",
          mdsId.toInt
        )
        .toLong

      routeManager.getTrunkAll match {
        case Success(trunks) =>
          trunks shouldBe List(TrunkCustom(trunkFeatureId, "SIP/abcd", ""))
        case Failure(f) => fail(f.getMessage)
      }
    }

    "ignore deleted trunk in usersip but still existing in trunkfeatures" in withConnection() {
      implicit c =>
        val mdsManager   = app.injector.instanceOf(classOf[MediaServerManager])
        val routeManager = app.injector.instanceOf(classOf[RouteManager])

        val mdsId = mdsManager
          .create(MediaServerConfigNoId("mds0", "MDS 0", Some("1.1.1.1")))
          .get
          .id
        Utils.insertTrunkfeatures("sip", 1, 0, 0, "", mdsId.toInt).toLong

        routeManager.getTrunkAll match {
          case Success(trunks) => trunks shouldBe List()
          case Failure(f)      => fail(f.getMessage)
        }
    }
  }
}
