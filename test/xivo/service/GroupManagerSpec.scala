package xivo.service

import anorm.SqlParser.bool
import anorm.{AnormException, SQL, SqlParser}
import docker.DockerPlayWithDbSpec
import model.Utils
import org.scalatest
import play.api.Application
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.{GroupError, _}

import java.sql.Connection
import scala.util.{Failure, Right, Success}

class GroupManagerSpec extends DockerPlayWithDbSpec {
  val idFuncKeySpeedDialType    = 86
  val idFuncKeyGroupDestination = 2

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .build()

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        """TRUNCATE queuemember, groupfeatures, func_key_dest_group,
          | userfeatures, queue, callerid, mediaserver, extensions, func_key_type,
          |  func_key_destination_type, context CASCADE""".stripMargin
      )
        .execute()
      Utils.insertGroupFeature(1, "support", "2005", Some("my_subroutine"))
      Utils.insertGroupFeature(2, "admin", "2006")
      Utils.insertFuncKey(idFuncKeySpeedDialType, idFuncKeyGroupDestination)
      Utils.insertFuncKey(idFuncKeySpeedDialType, idFuncKeyGroupDestination)
      Utils.insertFuncKeyDestGroup(1, idFuncKeyGroupDestination, 1)
      Utils.insertFuncKeyDestGroup(2, idFuncKeyGroupDestination, 2)
      Utils.insertQueue("support", "default", "rrmemory", 1, 0, "group")
      Utils.insertQueue("admin", "default", "rrmemory", 1, 0, "group")
      Utils.insertCallerId(None, "", "group", 1)
      Utils.insertCallerId(None, "", "group", 2)
      Utils.insertMediaServer(1, "default", "main")
      Utils.insertMediaServer(2, "second", "second mds")
      Utils.insertExtension("default", "2005", "group", "1")
      Utils.insertExtension("default", "2006", "group", "2")
      Utils.insertXivoUser(
        86,
        "James",
        "Bond",
        Some("jbond"),
        None,
        Some(0)
      )
      Utils.insertXivoUser(
        50,
        "Bruce",
        "Wayne",
        Some("bwayne"),
        None,
        Some(0)
      )
      Utils.insertXivoUser(
        18,
        "Peter",
        "Parker",
        Some("pparker"),
        None,
        Some(0)
      )
      Utils.insertXivoUser(
        191,
        "Carrie",
        "Bou",
        Some("cbou"),
        None,
        Some(0)
      )
      Utils.insertFuncKeyType(idFuncKeySpeedDialType, "speeddial")
      Utils.insertFuncKeyDestinationType(idFuncKeyGroupDestination, "group")
      Utils.insertContext("default")
      Utils.insertContext("default2")
      super.beforeEach()
    }

  def getGroupExtension(
      groupId: Long
  )(implicit c: Connection): Option[Extension] =
    SQL(s"""SELECT id, commented, context, exten, type, typeval
        |FROM extensions where type = 'group'
        |AND typeval = {groupId}""".stripMargin)
      .on(
        "groupId" -> groupId.toString
      )
      .as(Extension.extensionMapping.singleOpt)

  private def checkExtensionIsValid(
      groupId: Long,
      context: String,
      number: String
  )(implicit
      c: Connection
  ): scalatest.Assertion = {
    getGroupExtension(groupId)
      .map(ext => {
        val expectedEither = Extension.createRefinedExtensionFromString(
          ext.id,
          0,
          context,
          number,
          ExtenNumberType.group,
          groupId.toString
        )
        expectedEither match {
          case Left(value)     => fail(s"Could not parse extension : $value")
          case Right(expected) => expected shouldEqual ext
        }
      })
      .getOrElse(fail(s"Extension does not exist"))
  }

  private def checkExtensionIsDeleted(
      groupId: Long
  )(implicit c: Connection) =
    getGroupExtension(groupId) match {
      case Some(ext) => fail(s"Extension $ext found, it should be deleted")
      case None      => succeed
    }

  def getFunKeyIdFromDestGroup(groupId: Long, idDestinationTypeGroup: Long)(
      implicit c: Connection
  ): Option[Long] =
    SQL(
      s"""
          |SELECT func_key_id
          |FROM func_key_dest_group
          |WHERE group_id = {groupId}
          |AND destination_type_id = {idDestinationTypeGroup}
          |""".stripMargin
    ).on(
      "groupId"                -> groupId,
      "idDestinationTypeGroup" -> idDestinationTypeGroup
    ).as(SqlParser.long("func_key_id").singleOpt)

  def isFuncKeyExist(
      funcKeyId: Long,
      idDestinationTypeGroup: Long,
      idTypeSpeedDial: Long
  )(implicit c: Connection): Boolean =
    SQL(s"""
             | SELECT EXISTS (SELECT id FROM func_key
             | WHERE id = {funcKeyId}
             | AND type_id = {idTypeSpeedDial}
             | AND destination_type_id = {idDestinationTypeGroup})
             |""".stripMargin)
      .on(
        "funcKeyId"              -> funcKeyId,
        "idTypeSpeedDial"        -> idTypeSpeedDial,
        "idDestinationTypeGroup" -> idDestinationTypeGroup
      )
      .executeQuery()
      .as(bool("exists").single)

  def checkFuncKeysAreValid(
      groupId: Long
  )(implicit c: Connection): scalatest.Assertion = {

    getFunKeyIdFromDestGroup(groupId, idFuncKeyGroupDestination)
      .map(funKeyId => {
        val exist = isFuncKeyExist(
          funKeyId,
          idFuncKeyGroupDestination,
          idFuncKeySpeedDialType
        )
        exist shouldBe true
      })
      .getOrElse(fail("FuncKeyDestGroup is not found"))
  }

  def isFuncKeyDeleted(
      funcKeyId: Long,
      groupId: Long,
      idDestinationTypeGroup: Long
  )(implicit c: Connection): scalatest.Assertion =
    getFunKeyIdFromDestGroup(groupId, idDestinationTypeGroup) match {
      case Some(value) =>
        fail(s"FunckeyDestGroup $value found, should be deleted")
      case None =>
        isFuncKeyExist(
          funcKeyId,
          idDestinationTypeGroup,
          idFuncKeySpeedDialType
        ) shouldBe false
    }

  "GroupManager" should {
    "Add a user to a Group" in {
      withConnection() { implicit c =>
        val userId  = 86
        val groupId = 2

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        val expected = QueueMember(
          "admin",
          groupId,
          s"Local/id-$userId@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          userId,
          "Local",
          QueueCategory.Group,
          1
        )
        groupManager.addUserToGroup(groupId, userId) match {
          case Left(error) => fail(s"Error when adding user to group : $error")
          case Right(_) =>
            Utils.getQueueMember(groupId, userId) match {
              case Failure(f) =>
                fail(
                  s"The user was not correctly inserted to the database : $f"
                )
              case Success(qm) => qm shouldEqual expected
            }
        }
      }
    }

    "Remove a user from a Group" in {
      withConnection() { implicit c =>
        val userId    = 86
        val groupId   = 1
        val groupName = "support"
        val qm = QueueMember(
          groupName,
          groupId,
          s"Local/id-$userId@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          userId,
          "Local",
          QueueCategory.Group,
          0
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])

        Utils.insertQueueMember(qm, "user", "group")
        groupManager.removeUserFromGroup(groupId, userId) match {
          case Left(error) =>
            fail(s"Error when removing user to group : $error")
          case Right(value) =>
            Utils.getQueueMember(groupId, userId) match {
              case Failure(f: AnormException) => ()
              case Failure(f) =>
                fail(
                  s"Expected fail when try to retrieve deleted user (To be changed to a success) : $f"
                )
              case Success(qm) =>
                fail(s"User should have been deleted but found $qm")
            }
        }
      }
    }

    "Return a UserNotFoundError when adding a user in a group if the user does not exist" in {
      withConnection() { implicit c =>
        val userId    = 404
        val groupId   = 2
        val groupName = "support"

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.addUserToGroup(groupId, userId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              UserNotFound,
              s"User $userId does not exist"
            )
          case Right(_) =>
            fail(
              s"User $userId should not have been added to group $groupName"
            )
        }
      }
    }

    "Return a GroupNotFoundError when adding a user in a group if the group does not exist" in {
      withConnection() { implicit c =>
        val userId  = 86
        val groupId = 404

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.addUserToGroup(groupId, userId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              GroupNotFound,
              s"Group $groupId does not exist"
            )
          case Right(_) =>
            fail(
              s"User $userId should not have been added to group $groupId"
            )
        }
      }
    }

    "Return a UserNotFoundError when removing a user in a group if the user does not exist" in {
      withConnection() { implicit c =>
        val userId  = 404
        val groupId = 2

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.removeUserFromGroup(groupId, userId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              UserNotFound,
              s"User $userId does not exist"
            )
          case Right(_) =>
            fail(
              s"User $userId should not have been removed from group $groupId"
            )
        }
      }
    }

    "Return a GroupNotFoundError when removing a user in a group if the group does not exist" in {
      withConnection() { implicit c =>
        val userId  = 86
        val groupId = 404

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.removeUserFromGroup(groupId, userId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              GroupNotFound,
              s"Group $groupId does not exist"
            )
          case Right(_) =>
            fail(
              s"User $userId should not have been removed to group $groupId"
            )
        }
      }
    }

    "Return a UserAlreadyInGroup error when a user is already in the group we want to add them in" in {
      withConnection() { implicit c =>
        val userId    = 86
        val groupId   = 1
        val groupName = "support"
        val qm = QueueMember(
          groupName,
          groupId,
          s"Local/id-$userId@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          userId,
          "Local",
          QueueCategory.Group,
          0
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])

        Utils.insertQueueMember(qm, "user", "group")
        groupManager.addUserToGroup(groupId, userId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              UserAlreadyInGroup,
              s"User $userId is already in group $groupName"
            )
          case util.Right(_) =>
            fail(
              s"User $userId should not have been added to group $groupId"
            )
        }

      }
    }

    "Return a UserNotInGroup error when a user is not in the group we want to remove them" in {
      withConnection() { implicit c =>
        val userId  = 86
        val groupId = 1
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.removeUserFromGroup(groupId, userId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              UserNotInGroup,
              s"User $userId was not in group $groupId"
            )
          case util.Right(_) =>
            fail(
              s"User $userId should not have been removed from group $groupId"
            )
        }

      }
    }

    "Set newly added user to last position into group" in {
      withConnection() { implicit c =>
        val userId    = 191
        val groupId   = 1
        val groupName = "support"

        val qm1 = QueueMember(
          groupName,
          groupId,
          s"Local/id-86@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          86,
          "Local",
          QueueCategory.Group,
          1
        )

        val qm2 = QueueMember(
          groupName,
          groupId,
          s"Local/id-50@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          50,
          "Local",
          QueueCategory.Group,
          2
        )

        val qm3 = QueueMember(
          groupName,
          groupId,
          s"Local/id-18@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          18,
          "Local",
          QueueCategory.Group,
          3
        )

        val qmOther = QueueMember(
          "admin",
          2,
          s"Local/id-18@usercallback",
          0,
          0,
          QueueMemberUserType.User,
          18,
          "Local",
          QueueCategory.Group,
          18
        )

        Utils.insertQueueMember(qm1, "user", "group")
        Utils.insertQueueMember(qm2, "user", "group")
        Utils.insertQueueMember(qm3, "user", "group")
        Utils.insertQueueMember(qmOther, "user", "group")

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.addUserToGroup(groupId, userId) match {
          case Left(error) => fail(s"Error when adding user to group : $error")
          case Right(_) =>
            Utils.getQueueMember(groupId, userId) match {
              case Failure(f) =>
                fail(
                  s"The user was not correctly inserted to the database : $f"
                )
              case Success(qm) => qm.position shouldEqual 4
            }
        }
      }
    }

    "Get all groups" in {
      withConnection() { implicit c =>
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.getAllGroups match {
          case Left(error)   => fail(s"Error when retrieving all groups: $error")
          case Right(groups) => groups.length shouldEqual 2
        }
      }
    }

    "Get a group by id" in {
      withConnection() { implicit c =>
        val groupId = 1
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.getGroupById(groupId) match {
          case Left(error)  => fail(s"Error when retrieving group by id: $error")
          case Right(group) => group.id shouldEqual groupId
        }
      }
    }

    "Create a group with minimal properties" in {
      withConnection() { implicit c =>
        val context = "default"
        val data = CreateGroup(
          "test",
          context,
          "second"
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.createGroup(data) match {
          case Left(error) =>
            fail(s"Error when creating group: $error")
          case Right(group) =>
            group shouldEqual data
            checkExtensionIsDeleted(group.id)
            checkFuncKeysAreValid(group.id)
        }
      }
    }

    """Create a group with some properties,
      | check timeout and preprocessSubroutine is set to None
      | when empty value is given
      | and check extension is not created with empty number""".stripMargin in {
      withConnection() { implicit c =>
        val context = "default"
        val data = CreateGroup(
          name = "test",
          context = context,
          mds = "second",
          allowCallerRecord = true,
          onHoldMusic = "music",
          timeout = Some(0),
          ringingStrategy = RingingStrategyType.random,
          preprocessSubroutine = Some(""),
          number = ""
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.createGroup(data) match {
          case Left(error) =>
            fail(s"Error when creating group: $error")
          case Right(group) =>
            group shouldEqual data
            checkExtensionIsDeleted(group.id)
            checkFuncKeysAreValid(group.id)
        }
      }
    }

    "Create a group with all properties" in {
      withConnection() { implicit c =>
        val number  = "3005"
        val context = "default"
        val data = CreateGroup(
          name = "test",
          context = context,
          mds = "second",
          number = number,
          allowTransferUser = false,
          allowTransferCall = true,
          allowCallerRecord = true,
          allowCalleeRecord = true,
          ignoreForward = false,
          preprocessSubroutine = Some("sous-routine"),
          onHoldMusic = "music",
          timeout = Some(30),
          retryTime = 2,
          ringingStrategy = RingingStrategyType.random,
          ringInUse = true,
          callerId = CallerId(Some(CallerIdModeType.prepend), "test")
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.createGroup(data) match {
          case Left(error) =>
            fail(s"Error when creating group: $error")
          case Right(group) =>
            group shouldEqual data
            checkExtensionIsValid(group.id, context, number)
            checkFuncKeysAreValid(group.id)
        }
      }
    }

    "Don't create group with wrong context" in {
      withConnection() { implicit c =>
        val number  = "3005"
        val context = "unknown"
        val data = CreateGroup(
          "test",
          context,
          "second",
          number
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.createGroup(data) match {
          case Left(error) =>
            error.error shouldBe ContextNotFound
          case Right(group) =>
            fail(s"Group should not have been created, not existing context")
        }
      }
    }

    "Update a group with empty number with minimal properties" in {
      withConnection() { implicit c =>
        val createGroup = CreateGroup(
          name = "test",
          context = "default",
          mds = "second"
        )
        val updateGroup = UpdateGroup(
          number = Some("")
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        (for {
          initialGroup <- groupManager.createGroup(createGroup)
          updatedGroup <- groupManager.updateGroup(initialGroup.id, updateGroup)
        } yield {
          updatedGroup shouldEqual updatedGroup
          checkExtensionIsDeleted(updatedGroup.id)
          checkFuncKeysAreValid(updatedGroup.id)
        }).left.map(error => fail(s"Error when updating group: $error"))
      }
    }

    """Update a group with some properties,
      | check timeout and preprocessSubroutine is reset to None
      | when empty value is given
      | and check extension is deleted if empty value is given""".stripMargin in {
      withConnection() { implicit c =>
        val groupId = 1
        val data = UpdateGroup(
          name = Some("test_renaming"),
          context = Some("default2"),
          mds = Some("second"),
          number = Some(""),
          ignoreForward = Some(true),
          onHoldMusic = Some("music"),
          timeout = Some(0),
          preprocessSubroutine = Some(""),
          callerId = Some(CallerId(Some(CallerIdModeType.prepend), "test"))
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        (for {
          initialGroup <- groupManager.getGroupById(groupId)
          updatedGroup <- groupManager.updateGroup(groupId, data)
        } yield {
          updatedGroup shouldEqual data
          checkExtensionIsDeleted(
            initialGroup.id
          )
          checkFuncKeysAreValid(initialGroup.id)
        }).left.map(error =>
          fail(s"Error when updating group $groupId: $error")
        )
      }
    }

    """Update a group without extension and check extension is created if non empty value is given""".stripMargin in {
      withConnection() { implicit c =>
        val context = "default"
        val createGroup = CreateGroup(
          name = "test",
          context = context,
          mds = "second"
        )
        val updateGroup = UpdateGroup(
          number = Some("2345")
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        (for {
          initialGroup <- groupManager.createGroup(createGroup)
          updatedGroup <- groupManager.updateGroup(initialGroup.id, updateGroup)
        } yield {
          updatedGroup shouldEqual updateGroup
          checkExtensionIsValid(
            updatedGroup.id,
            updatedGroup.context,
            updatedGroup.number
          )
          checkFuncKeysAreValid(initialGroup.id)
        }).left.map(error => fail(s"Error when updating group : $error"))
      }
    }

    "Update a group with all properties" in {
      withConnection() { implicit c =>
        val groupId = 1
        val number  = "3005"
        val context = "default"
        val data = UpdateGroup(
          name = Some("test_renaming"),
          context = Some(context),
          mds = Some("second"),
          number = Some(number),
          allowTransferUser = Some(false),
          allowTransferCall = Some(false),
          allowCallerRecord = Some(false),
          allowCalleeRecord = Some(false),
          ignoreForward = Some(true),
          preprocessSubroutine = Some("sous-routine"),
          onHoldMusic = Some("music"),
          timeout = Some(30),
          retryTime = Some(2),
          ringingStrategy = Some(RingingStrategyType.random),
          ringInUse = Some(true),
          callerId = Some(CallerId(Some(CallerIdModeType.prepend), "test"))
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.updateGroup(groupId, data) match {
          case Left(error) =>
            fail(s"Error when updating group $groupId: $error")
          case Right(group) =>
            group shouldEqual data
            checkExtensionIsValid(groupId, context, number)
            checkFuncKeysAreValid(group.id)
        }
      }
    }

    "Don't update a group with wrong context" in {
      withConnection() { implicit c =>
        val groupId = 1
        val data =
          UpdateGroup(
            Some("test_renaming"),
            Some("fake_context"),
            Some("second")
          )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.updateGroup(groupId, data) match {
          case Left(error) =>
            error.error shouldBe ContextNotFound
          case Right(group) => {
            fail(
              s"Group should not have been updated with a non existing context"
            )
          }
        }
      }
    }

    "Delete a group" in {
      withConnection() { implicit c =>
        val groupId = 2
        getFunKeyIdFromDestGroup(groupId, idFuncKeyGroupDestination)
          .map(funcKeyId => {
            val groupManager =
              app.injector.instanceOf(classOf[GroupManager])
            groupManager.deleteGroup(groupId) match {
              case Left(error) =>
                fail(s"Error when deleting group: $error")
              case Right(groupId) =>
                groupId shouldEqual groupId
                checkExtensionIsDeleted(groupId)
                isFuncKeyDeleted(funcKeyId, groupId, idFuncKeyGroupDestination)
            }
          })
          .getOrElse(
            fail(
              "FuncKey is not created before trying to delete the group, test can't be validated"
            )
          )
      }
    }

    "Delete a group without extension" in {
      withConnection() { implicit c =>
        val createGroup = CreateGroup(
          name = "test",
          context = "default",
          mds = "second"
        )
        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        (for {
          initialGroup <- groupManager.createGroup(createGroup)
          funcKeyId <- getFunKeyIdFromDestGroup(
            initialGroup.id,
            idFuncKeyGroupDestination
          ).toRight(
            "FuncKey is not created before trying to delete the group, test can't be validated"
          )
          groupId <- groupManager.deleteGroup(initialGroup.id)
        } yield {
          groupId shouldEqual initialGroup.id
          checkExtensionIsDeleted(groupId)
          isFuncKeyDeleted(funcKeyId, groupId, idFuncKeyGroupDestination)
        }).left.foreach(error => fail(s"Error when deleting group: $error"))
      }
    }

    "Return a GroupNotFoundError when retrieving group by id if the group does not exist" in {
      withConnection() { implicit c =>
        val groupId = 404

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.getGroupById(groupId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              GroupNotFound,
              s"Group $groupId does not exist"
            )
          case Right(_) =>
            fail(
              s"Group $groupId should not have been found"
            )
        }
      }
    }

    "Return a GroupNotFoundError when deleting group if the group does not exist" in {
      withConnection() { implicit c =>
        val groupId = 404

        val groupManager =
          app.injector.instanceOf(classOf[GroupManager])
        groupManager.deleteGroup(groupId) match {
          case Left(error) =>
            error shouldEqual GroupError(
              GroupNotFound,
              s"Group $groupId does not exist"
            )
          case Right(_) =>
            fail(
              s"Group $groupId should not have been found"
            )
        }
      }
    }
  }
}
