package xivo.service

import docker.DockerPlayWithDbSpec
import xivo.model.{Queue, QueueCategory, QueueMonitorType}

import scala.util.{Failure, Success}

class QueueSpec extends DockerPlayWithDbSpec {
  "xivo.service.QueueManager" should {
    "create a xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")

      val q = Queue(
        "queue1",
        None,
        None,
        None,
        None,
        Option.empty[QueueMonitorType.Type],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      queueManager.create(q) match {
        case Success(created) => created shouldBe (q)
        case Failure(t) => {
          t.printStackTrace()
          fail(t)
        }
      }
    }

    "get a xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")

      val q = Queue(
        "queue1",
        None,
        None,
        None,
        None,
        Some(QueueMonitorType.No),
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      val created = queueManager.create(q).get

      queueManager.get(created.name) match {
        case Success(fetched) => fetched shouldBe (created)
        case Failure(t) => {
          t.printStackTrace()
          fail(t)
        }
      }
    }

    "update a xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")

      val q = Queue(
        "queue1",
        None,
        None,
        None,
        None,
        Option.empty[QueueMonitorType.Type],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      val created = queueManager.create(q).get.copy(context = Some("aaa"))

      queueManager.update(created)

      queueManager.get(created.name) match {
        case Success(fetched) => fetched shouldBe (created)
        case Failure(t) => {
          t.printStackTrace()
          fail(t)
        }
      }
    }

    "fail updating a non existent xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")

      val q = Queue(
        "queue1",
        None,
        None,
        None,
        None,
        Option.empty[QueueMonitorType.Type],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      queueManager.update(q).isSuccess shouldBe (false)
    }

    "delete a xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")

      val q = Queue(
        "queue1",
        None,
        None,
        None,
        None,
        Option.empty[QueueMonitorType.Type],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      queueManager.create(q) match {
        case Success(c) => c
        case Failure(t) => fail(t)
      }

      queueManager.delete("queue1") match {
        case Success(q) => queueManager.get("queue1").isSuccess shouldBe (false)
        case Failure(t) => t.printStackTrace(); fail(t)
      }
    }

    "fail deleting a non existent xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")

      queueManager.delete("queue1").isSuccess shouldBe (false)
    }

    "get all xivo queue" in {
      val queueManager = app.injector.instanceOf(classOf[QueueManager])
      queueManager.delete("queue1")
      queueManager.delete("queue2")

      val q1 = Queue(
        "queue1",
        None,
        None,
        None,
        None,
        Option.empty[QueueMonitorType.Type],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      val q2 = Queue(
        "queue2",
        None,
        None,
        None,
        None,
        Option.empty[QueueMonitorType.Type],
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        0,
        0,
        None,
        None,
        0,
        0,
        QueueCategory.Queue,
        "app",
        1,
        "no",
        0,
        0,
        0,
        None,
        60,
        0,
        "yes",
        5,
        None
      )

      val created1 = queueManager.create(q1).get
      val created2 = queueManager.create(q2).get

      val fetched = queueManager.all()
      fetched.isSuccess shouldBe (true)
      fetched.get should contain.allOf(created1, created2)
    }
  }
}
