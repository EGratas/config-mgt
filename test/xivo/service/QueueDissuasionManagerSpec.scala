package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import xivo.model.{QueueDissuasionQueue, QueueDissuasionSoundFile}

import scala.util.{Failure, Success}

class QueueDissuasionManagerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE dialaction").execute()
      SQL("TRUNCATE queuefeatures").execute()

      super.beforeEach()
    }

  "xivo.service.QueueDissuasionManager" should {
    "get a dissuasion file from a queue id if the dissuasion is a sound" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          42,
          "sound",
          Some("/var/lib/xivo/sounds/playback/testQueue_file_example_WAV_1MG")
        )
        val expected =
          Some(QueueDissuasionSoundFile(None, "testQueue_file_example_WAV_1MG"))

        queueDissuasionManager.get(42) match {
          case Success(created) => created shouldBe expected
          case Failure(t) =>
            t.printStackTrace()
            fail(t)
        }
    }

    "get a dissuasion file from a queue id if the dissuasion is a queue" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          42,
          "queue",
          Some("24")
        )

        Utils.insertQueueFeature(24, "dissuasion_queue")

        val expected = Some(QueueDissuasionQueue(None, "dissuasion_queue"))

        queueDissuasionManager.get(42) match {
          case Success(created) => created shouldBe expected
          case Failure(t) =>
            t.printStackTrace()
            fail(t)
        }
    }

    "get a dissuasion file from a queue id if the dissuasion is neither a queue nor a sound" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          42,
          "group",
          Some("/var/lib/xivo/sounds/playback/testQueue_file_example_WAV_1MG")
        )

        val expected = None

        queueDissuasionManager.get(42) match {
          case Success(created) => created shouldBe expected
          case Failure(t) =>
            t.printStackTrace()
            fail(t)
        }
    }

    "get a dissuasion file from a queue id if the dissuasion is neither a queue nor a sound even if actionarg is null" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          42,
          "group"
        )

        val expected = None

        queueDissuasionManager.get(42) match {
          case Success(created) => created shouldBe expected
          case Failure(t) =>
            t.printStackTrace()
            fail(t)
        }
    }

    "return QueueDissuasionSoundFile when update a queue dissuasion to sound file" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])
        val queueId: Long = 42
        val queueName     = "testQueue"
        val newFileName   = "testQueue_newFile"
        val expected      = QueueDissuasionSoundFile(Some(queueId), newFileName)

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          queueId,
          "sound",
          Some("/var/lib/xivo/sounds/playback/queue1_OldFileName")
        )

        Utils.insertQueueFeature(42, queueName)

        queueDissuasionManager.dissuasionAccessToSoundFile(
          queueId,
          newFileName
        ) match {
          case Success(created) => created shouldBe expected
          case Failure(t) =>
            t.printStackTrace()
            fail(t)
        }
    }

    "return QueueDissuasionQueue when update a dissuasion queue" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])
        val queueId: Long      = 42
        val queueName          = "testQueue"
        val otherQueueName     = "testOtherQueue"
        val otherQueueId: Long = 756
        val expected           = QueueDissuasionQueue(Some(queueId), otherQueueName)

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          queueId,
          "sound",
          Some("/var/lib/xivo/sounds/playback/queue1_OldFileName")
        )

        Utils.insertQueueFeature(42, queueName)
        Utils.insertQueueFeature(756, otherQueueName)

        queueDissuasionManager.dissuasionAccessToOtherQueue(
          queueId,
          otherQueueId,
          otherQueueName
        ) match {
          case Success(created) => created shouldBe expected
          case Failure(t) =>
            t.printStackTrace()
            fail(t)
        }
    }

    "fail if asked to redirect a queue to itself" in withConnection() {
      implicit c =>
        val queueDissuasionManager =
          app.injector.instanceOf(classOf[QueueDissuasionManager])
        val queueId: Long = 42

        Utils.insertDialAction(
          "chanunavail",
          "queue",
          queueId,
          "sound",
          Some("/var/lib/xivo/sounds/playback/queue1_OldFileName")
        )

        Utils.insertQueueFeature(42, "dissuasion_queue")

        queueDissuasionManager.dissuasionAccessToOtherQueue(
          queueId,
          queueId,
          "queueName"
        ) shouldBe a[Failure[_]]
    }
  }
}
