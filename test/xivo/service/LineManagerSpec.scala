package xivo.service

import anorm.SQL
import anorm.SqlParser.scalar
import common.refined.anorm._
import docker.DockerPlayWithDbSpec
import eu.timepit.refined.refineMV
import model.Utils
import model.Utils.{UserIaxCategory, insertUserSip}
import org.mockito.Mockito._
import play.api.Application
import play.api.http.Status
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.WSResponse
import ws.model.{Line, LineType}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import scala.concurrent.Future
import scala.util.{Failure, Success}

class LineManagerSpec extends DockerPlayWithDbSpec {

  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]
  val lineNotifier: LineNotifier = mock[LineNotifier]
  val confdclient: ConfdClient   = mock[ConfdClient]
  val wsResponse: WSResponse     = mock[WSResponse]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .overrides(bind[LineNotifier].to(lineNotifier))
    .overrides(bind[ConfdClient].to(confdclient))
    .build()

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      reset(rabbitPublisher)
      reset(lineNotifier)
      reset(confdclient)

      SQL(
        "TRUNCATE userfeatures, linefeatures, extensions, user_line, userpreferences"
      ).execute()
      SQL("TRUNCATE usersip").execute()
      SQL("TRUNCATE usercustom").execute()
      SQL("TRUNCATE sccpline").execute()
      SQL("TRUNCATE sccpdevice").execute()
      SQL("TRUNCATE entity").execute()

      super.beforeEach()
    }

  "get user line for PHONE line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val sipid =
      Utils.insertUserSip(peername, UserIaxCategory.user, "{}", context)
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sip",
      "test",
      "1000",
      1234,
      Some(sipid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context,
      1
    )

    Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.PHONE,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.get(10) match {
      case Success(line) =>
        line shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get user line for WEBRTC line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val sipid = Utils.insertUserSip(
      peername,
      UserIaxCategory.user,
      "{{webrtc,yes}}",
      context
    )
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sip",
      "test",
      "1000",
      1234,
      Some(sipid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )

    Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.WEBRTC,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.get(10) match {
      case Success(line) =>
        line shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get user line for UA line " in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val sipid = Utils.insertUserSip(
      peername,
      UserIaxCategory.user,
      "{{webrtc,ua}}",
      context
    )
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sip",
      "test",
      "1000",
      1234,
      Some(sipid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )

    Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.get(10) match {
      case Success(line) =>
        line shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get user line for SCCP Line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sccp",
      "test",
      "1000",
      1234,
      None,
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )

    Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.SCCP,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.get(10) match {
      case Success(line) =>
        line shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "get user line for CUSTOM Line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "custom",
      "test",
      "1000",
      1234,
      None,
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )

    Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.CUSTOM,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.get(10) match {
      case Success(line) =>
        line shouldBe result
      case Failure(f) => fail(f.getMessage)
    }
  }

  "delete user line for PHONE line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val exten    = "1000"
    val sipid =
      Utils.insertUserSip(peername, UserIaxCategory.user, "{}", context)
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sip",
      "test",
      exten,
      1234,
      Some(sipid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )
    val extenid = Utils.insertExtension(context, exten, "user", userid.toString)

    val userline = Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.PHONE,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.delete(userid.toLong) match {
      case Success(line) =>
        line shouldBe result
        lineManager.get(userid.toLong) shouldBe a[Failure[_]]
        SQL("SELECT count(*) from linefeatures where id={id}")
          .on(Symbol("id") -> lineid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from user_line where id={id}")
          .on(Symbol("id") -> userline)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from usersip where id={id}")
          .on(Symbol("id") -> sipid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from extensions where id={id}")
          .on(Symbol("id") -> extenid)
          .as(scalar[Long].single) shouldBe 0
        verify(lineNotifier).onUserLineDeleted(userid, line, sipid)
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "delete user line for WEBRTC line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val exten    = "1000"
    val sipid = Utils.insertUserSip(
      peername,
      UserIaxCategory.user,
      "{{webrtc,yes}}",
      context
    )
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sip",
      "test",
      exten,
      1234,
      Some(sipid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )
    val extenid = Utils.insertExtension(context, exten, "user", userid.toString)

    val userline = Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.WEBRTC,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.delete(userid.toLong) match {
      case Success(line) =>
        line shouldBe result
        lineManager.get(userid.toLong) shouldBe a[Failure[_]]
        SQL("SELECT count(*) from linefeatures where id={id}")
          .on(Symbol("id") -> lineid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from user_line where id={id}")
          .on(Symbol("id") -> userline)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from usersip where id={id}")
          .on(Symbol("id") -> sipid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from extensions where id={id}")
          .on(Symbol("id") -> extenid)
          .as(scalar[Long].single) shouldBe 0
        verify(lineNotifier).onUserLineDeleted(userid, line, sipid)
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "delete user line for UA line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val exten    = "1000"
    val sipid = Utils.insertUserSip(
      peername,
      UserIaxCategory.user,
      "{{webrtc,ua}}",
      context
    )
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sip",
      "test",
      exten,
      1234,
      Some(sipid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )
    val extenid = Utils.insertExtension(context, exten, "user", userid.toString)

    val userline = Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.UA,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.delete(userid.toLong) match {
      case Success(line) =>
        line shouldBe result
        lineManager.get(userid.toLong) shouldBe a[Failure[_]]
        SQL("SELECT count(*) from linefeatures where id={id}")
          .on(Symbol("id") -> lineid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from user_line where id={id}")
          .on(Symbol("id") -> userline)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from usersip where id={id}")
          .on(Symbol("id") -> sipid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from extensions where id={id}")
          .on(Symbol("id") -> extenid)
          .as(scalar[Long].single) shouldBe 0
        verify(lineNotifier).onUserLineDeleted(userid, line, sipid)
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "delete user line for CUSTOM line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val exten    = "1000"
    val customid = Utils.insertUserCustom(
      "",
      Utils.UserCustomCategory.user,
      "Local/1000@default",
      Utils.TrunkProtocol.custom,
      Some(context)
    )
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "custom",
      "test",
      exten,
      1234,
      Some(customid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )
    val extenid = Utils.insertExtension(context, exten, "user", userid.toString)

    val userline = Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.CUSTOM,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.delete(userid.toLong) match {
      case Success(line) =>
        line shouldBe result
        lineManager.get(userid.toLong) shouldBe a[Failure[_]]
        SQL("SELECT count(*) from linefeatures where id={id}")
          .on(Symbol("id") -> lineid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from user_line where id={id}")
          .on(Symbol("id") -> userline)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from usercustom where id={id}")
          .on(Symbol("id") -> customid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from extensions where id={id}")
          .on(Symbol("id") -> extenid)
          .as(scalar[Long].single) shouldBe 0
        verify(lineNotifier).onUserLineDeleted(userid, line, customid)
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "delete user line for SCCP line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val peername = "1hlnzr8t"
    val context  = "default"
    val exten    = "1000"

    Utils.insertSccpDevice("SEPA8B1D4FBCC20", "SEPA8B1D4FBCC20", exten)
    val sccpid = Utils.insertSccpLine(exten, context, "James Bond", exten)
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))
    val lineid = Utils.insertLineFeature(
      1,
      "sccp",
      "test",
      exten,
      1234,
      Some(sccpid),
      Some("c3d29985f8f8403aafbebec638e5c12e"),
      Some(peername),
      context
    )
    val extenid = Utils.insertExtension(context, exten, "user", userid.toString)

    val userlineid = Utils.insertUserLine(1, userid, lineid)

    val result = Line(
      Some(1),
      LineType.SCCP,
      refineMV("default"),
      refineMV("1000"),
      refineMV("test"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      Some(refineMV("1hlnzr8t")),
      Some(1),
      Some(1234)
    )
    lineManager.delete(userid.toLong) match {
      case Success(line) =>
        line shouldBe result
        lineManager.get(userid.toLong) shouldBe a[Failure[_]]
        SQL("SELECT count(*) from linefeatures where id={id}")
          .on(Symbol("id") -> lineid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from user_line where id={id}")
          .on(Symbol("id") -> userlineid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from sccpdevice where line={exten}")
          .on(Symbol("exten") -> exten)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from sccpline where id={id}")
          .on(Symbol("id") -> sccpid)
          .as(scalar[Long].single) shouldBe 0
        SQL("SELECT count(*) from extensions where id={id}")
          .on(Symbol("id") -> extenid)
          .as(scalar[Long].single) shouldBe 0
        verify(lineNotifier).onUserLineDeleted(userid, line, sccpid)
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "create user PHONE line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))

    val lineTemplate = Line(
      None,
      LineType.PHONE,
      refineMV("default"),
      refineMV("1000"),
      refineMV("site1"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      None,
      Some(1),
      Some(1234)
    )

    lineManager.create(userid, lineTemplate) match {
      case Success(line) =>
        line.name shouldBe a[Some[_]]
        line.id shouldBe a[Some[_]]
        // Check other fields
        line shouldBe lineTemplate.copy(
          id = line.id,
          name = line.name,
          provisioningId = line.provisioningId
        )

        SQL(
          "SELECT count(*) from linefeatures where id={id} and protocol='sip'"
        )
          .on(Symbol("id") -> line.id)
          .as(scalar[Long].single) shouldBe 1
        SQL(
          "SELECT count(*) from user_line where user_id={userid} and main_line='t' and main_user='t' and line_id={lineid}"
        )
          .on(
            Symbol("userid") -> userid,
            Symbol("lineid") -> line.id
          )
          .as(scalar[Long].single) shouldBe 1
        SQL(
          "SELECT count(*) from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
        )
          .on(Symbol("lineid") -> line.id)
          .as(scalar[Long].single) shouldBe 1
        SQL(
          "SELECT count(*) from extensions where exten={exten} and context={context} and type='user'::extenumbers_type and typeval={userid}::varchar"
        )
          .on(
            Symbol("exten")   -> line.extension,
            Symbol("context") -> line.context,
            Symbol("userid")  -> userid
          )
          .as(scalar[Long].single) shouldBe 1
        val endpointid =
          SQL("SELECT l.protocolid FROM linefeatures l where l.id={lineid}")
            .on(Symbol("lineid") -> line.id)
            .as(scalar[Long].single)
        verify(lineNotifier).onUserLineCreated(userid, line, endpointid)
      case Failure(f) =>
        fail(f.getMessage)
    }

  }

  "update user PHONE line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))

    val lineTemplate = Line(
      None,
      LineType.PHONE,
      refineMV("default"),
      refineMV("1000"),
      refineMV("site1"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      None,
      Some(1),
      Some(1234)
    )
    val lineTemplateUpdated = lineTemplate.copy(
      lineType = LineType.WEBRTC,
      extension = refineMV("1002"),
      site = refineMV("site2"),
      lineNum = Some(2)
    )

    val lineCreated = lineManager.create(userid, lineTemplate)
    SQL(
      "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
    )
      .on(Symbol("lineid") -> lineCreated.get.id)
      .as(scalar[String].single) shouldBe "\"James Bond\" <1000>"

    lineManager.update(userid, lineTemplateUpdated) match {
      case Success(line) =>
        line shouldBe lineTemplate.copy(
          id = line.id,
          lineType = LineType.WEBRTC,
          name = line.name,
          extension = lineTemplateUpdated.extension,
          site = lineTemplateUpdated.site,
          lineNum = Some(2),
          provisioningId = line.provisioningId
        )
        val endpointid =
          SQL("SELECT l.protocolid FROM linefeatures l where l.id={lineid}")
            .on(Symbol("lineid") -> line.id)
            .as(scalar[Long].single)
        verify(lineNotifier).onUserLineEdited(
          userid,
          lineCreated.get,
          line,
          endpointid
        )
      case Failure(f) =>
        fail(f.getMessage)
    }
    SQL(
      "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
    )
      .on(Symbol("lineid") -> lineCreated.get.id)
      .as(scalar[String].single) shouldBe "\"James Bond\" <1002>"
  }

  "not update user PHONE line and associated tables if error" in withConnection() {
    implicit c =>
      val lineManager = app.injector.instanceOf(classOf[LineManager])
      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid1 = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )
      val userid2 =
        Utils.insertXivoUser(12, "John", "Wayne", Some("jwayne"), None, None)

      val lineTemplate1 = Line(
        None,
        LineType.PHONE,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
        None,
        Some(1),
        Some(1234)
      )
      val lineTemplate2 = Line(
        None,
        LineType.PHONE,
        refineMV("default"),
        refineMV("1002"),
        refineMV("site1"),
        Some(refineMV("e21c5e836cebebfaa3048f8f58992d3c")),
        None,
        Some(1),
        Some(1234)
      )

      val lineCreated1 = lineManager.create(userid1, lineTemplate1)
      lineManager.create(userid2, lineTemplate2)

      val lineTemplateUpdated = lineTemplate1.copy(
        extension = refineMV("1002"),
        site = refineMV("site2")
      )

      lineManager.update(userid1, lineTemplateUpdated) match {
        case Failure(f) =>
          f shouldBe a[Exception]

          SQL(
            "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
          )
            .on(Symbol("lineid") -> lineCreated1.get.id)
            .as(scalar[String].single) shouldBe "\"James Bond\" <1000>"
          SQL(
            "SELECT exten from extensions where exten={exten} and context={context} and type='user'::extenumbers_type and typeval={userid}::varchar"
          )
            .on(
              Symbol("exten")   -> lineCreated1.get.extension,
              Symbol("context") -> lineCreated1.get.context,
              Symbol("userid")  -> userid1
            )
            .as(scalar[String].single) shouldBe "1000"
        case _ => fail()
      }
  }

  "notify upon deletion only when transaction is completed" in withConnection() {
    implicit c =>
      val confdclient = app.injector.instanceOf(classOf[ConfdClient])
      val customNotifier = new LineNotifier(rabbitPublisher, confdclient) {
        override def onUserLineDeleted(
            userid: Long,
            line: Line,
            endpointid: Long
        ): Unit = {
          // Check database is updated when calling notifier
          SQL("SELECT count(*) from linefeatures where id={id}")
            .on(Symbol("id") -> line.id.get)
            .as(scalar[Long].single) shouldBe 0
          SQL("SELECT count(*) from user_line where user_id={userid}")
            .on(Symbol("userid") -> userid)
            .as(scalar[Long].single) shouldBe 0
          SQL("SELECT count(*) from usersip where id={id}")
            .on(Symbol("id") -> endpointid)
            .as(scalar[Long].single) shouldBe 0
        }
      }

      val lineManager = app.injector.instanceOf(classOf[LineManager])

      Utils.insertEntity(1, "xivo", "XiVO test")

      val peername = "1hlnzr8t"
      val context  = "default"
      val exten    = "1000"
      val sipid =
        Utils.insertUserSip(peername, UserIaxCategory.user, "{}", context)
      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )
      val lineid = Utils.insertLineFeature(
        1,
        "sip",
        "test",
        exten,
        1234,
        Some(sipid),
        Some("c3d29985f8f8403aafbebec638e5c12e"),
        Some(peername),
        context
      )
      Utils.insertExtension(context, exten, "user", userid.toString)

      Utils.insertUserLine(1, userid, lineid)

      lineManager.delete(userid.toLong) match {
        case Success(_) =>
          lineManager.get(userid.toLong) shouldBe a[Failure[_]]
        case Failure(f) =>
          fail(f.getMessage)
      }
  }

  "notify upon creation only when transaction is completed" in withConnection() {
    implicit c =>
      val confdclient = app.injector.instanceOf(classOf[ConfdClient])
      val customNotifier = new LineNotifier(rabbitPublisher, confdclient) {
        override def onUserLineCreated(
            userid: Long,
            line: Line,
            endpointid: Long
        ): Unit = {
          // Check database is updated when calling notifier
          SQL("SELECT count(*) from linefeatures where id={id}")
            .on(Symbol("id") -> line.id.get)
            .as(scalar[Long].single) shouldBe 1
          SQL("SELECT count(*) from user_line where user_id={userid}")
            .on(Symbol("userid") -> userid)
            .as(scalar[Long].single) shouldBe 1
          SQL("SELECT count(*) from usersip where id={id}")
            .on(Symbol("id") -> endpointid)
            .as(scalar[Long].single) shouldBe 1
        }
      }

      val lineManager = app.injector.instanceOf(classOf[LineManager])

      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )

      val lineTemplate = Line(
        None,
        LineType.PHONE,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
        None,
        Some(1),
        Some(1234)
      )

      lineManager.create(userid, lineTemplate) match {
        case Success(line) =>
          line.name shouldBe a[Some[_]]
          line.id shouldBe a[Some[_]]

        case Failure(f) =>
          fail(f.getMessage)
      }
  }

  "notify upon update only when transaction is completed" in withConnection() {
    implicit c =>
      val confdclient = app.injector.instanceOf(classOf[ConfdClient])
      val customNotifier = new LineNotifier(rabbitPublisher, confdclient) {
        override def onUserLineEdited(
            userid: Long,
            oldline: Line,
            newline: Line,
            endpointid: Long
        ): Unit = {
          // Check database is updated when calling notifier
          SQL(
            "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
          )
            .on(Symbol("lineid") -> newline.id.get)
            .as(scalar[String].single) shouldBe "\"James Bond\" <1002>"
        }
      }

      val lineManager = app.injector.instanceOf(classOf[LineManager])

      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )

      val lineTemplate = Line(
        None,
        LineType.PHONE,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
        None,
        Some(1),
        Some(1234)
      )
      val lineTemplateUpdated = lineTemplate.copy(
        extension = refineMV("1002"),
        site = refineMV("site2")
      )

      val lineCreated = lineManager.create(userid, lineTemplate)

      SQL(
        "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
      )
        .on(Symbol("lineid") -> lineCreated.get.id)
        .as(scalar[String].single) shouldBe "\"James Bond\" <1000>"

      lineManager.update(userid, lineTemplateUpdated) match {
        case Success(_) =>

        case Failure(f) =>
          fail(f.getMessage)
      }
  }

  "create user sccp line" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])

    stub(wsResponse.status).toReturn(Status.OK)
    Utils.insertEntity(1, "xivo", "XiVO test")

    val exten = "1000"
    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))

    val lineTemplate = Line(
      None,
      LineType.SCCP,
      refineMV("default"),
      refineMV("1000"),
      refineMV("site1"),
      Some(refineMV("753298a8f1ce4e32b0fde5cf27af449d")),
      None,
      Some(1),
      Some(1234)
    )
    val mac = "a8:b1:d4:fb:cc:20"

    stub(wsResponse.status).toReturn(Status.OK)
    stub(wsResponse.json).toReturn(Json.obj("mac" -> mac))
    when(confdclient.deviceGet(refineMV("753298a8f1ce4e32b0fde5cf27af449d")))
      .thenReturn(Future.successful(wsResponse))

    lineManager.create(userid, lineTemplate) match {
      case Success(line) =>
        line.name shouldBe a[Some[_]]
        line.id shouldBe a[Some[_]]
        // Check other fields
        line shouldBe lineTemplate.copy(
          id = line.id,
          name = line.name,
          provisioningId = line.provisioningId
        )

        SQL(
          "SELECT count(*) from linefeatures where id={id} and protocol='sccp'"
        )
          .on(Symbol("id") -> line.id)
          .as(scalar[Long].single) shouldBe 1
        SQL(
          "SELECT count(*) from user_line where user_id={userid} and main_line='t' and main_user='t' and line_id={lineid}"
        )
          .on(
            Symbol("userid") -> userid,
            Symbol("lineid") -> line.id
          )
          .as(scalar[Long].single) shouldBe 1
        SQL("SELECT name from sccpdevice where line={exten}")
          .on(Symbol("exten") -> exten)
          .as(scalar[String].single) shouldBe "SEPA8B1D4FBCC20"
        SQL("SELECT device from sccpdevice where line={exten}")
          .on(Symbol("exten") -> exten)
          .as(scalar[String].single) shouldBe "SEPA8B1D4FBCC20"
        SQL(
          "SELECT count(*) from extensions where exten={exten} and context={context} and type='user'::extenumbers_type and typeval={userid}::varchar"
        )
          .on(
            Symbol("exten")   -> line.extension,
            Symbol("context") -> line.context,
            Symbol("userid")  -> userid
          )
          .as(scalar[Long].single) shouldBe 1
        val endpointid =
          SQL("SELECT l.protocolid FROM linefeatures l where l.id={lineid}")
            .on(Symbol("lineid") -> line.id)
            .as(scalar[Long].single)
        verify(lineNotifier).onUserLineCreated(userid, line, endpointid)
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "update user sccp line to no device" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))

    val lineTemplate = Line(
      None,
      LineType.SCCP,
      refineMV("default"),
      refineMV("1000"),
      refineMV("site1"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      None,
      Some(1),
      Some(1234)
    )
    val lineTemplateUpdated = lineTemplate.copy(
      extension = refineMV("1001"),
      device = None,
      lineNum = None
    )

    val mac = "a8:b1:d4:fb:cc:20"

    stub(wsResponse.status).toReturn(Status.OK)
    stub(wsResponse.json).toReturn(Json.obj("mac" -> mac))
    when(confdclient.deviceGet(refineMV("c3d29985f8f8403aafbebec638e5c12e")))
      .thenReturn(Future.successful(wsResponse))

    val lineCreated = lineManager.create(userid, lineTemplate)
    SQL("SELECT cid_num from sccpline")
      .on(Symbol("lineid") -> lineCreated.get.id)
      .as(scalar[String].single) shouldBe "1000"
    SQL("SELECT name from sccpline")
      .on(Symbol("lineid") -> lineCreated.get.id)
      .as(scalar[String].single) shouldBe "1000"
    SQL("SELECT line from sccpdevice")
      .on(Symbol("exten") -> lineCreated.get.extension)
      .as(scalar[String].single) shouldBe "1000"

    lineManager.update(userid, lineTemplateUpdated) match {
      case Success(line) =>
        line shouldBe lineTemplate.copy(
          id = line.id,
          name = line.name,
          extension = lineTemplateUpdated.extension,
          site = lineTemplateUpdated.site,
          device = None,
          lineNum = Some(1),
          provisioningId = line.provisioningId
        )
        val endpointid =
          SQL("SELECT l.protocolid FROM linefeatures l where l.id={lineid}")
            .on(Symbol("lineid") -> line.id)
            .as(scalar[Long].single)
        verify(lineNotifier).onUserLineEdited(
          userid,
          lineCreated.get,
          line,
          endpointid
        )
      case Failure(f) =>
        fail(f.getMessage)
    }
    SQL("SELECT cid_num from sccpline")
      .on(Symbol("lineid") -> lineCreated.get.id)
      .as(scalar[String].single) shouldBe "1001"
    SQL("SELECT name from sccpline")
      .on(Symbol("lineid") -> lineCreated.get.id)
      .as(scalar[String].single) shouldBe "1001"
    SQL("SELECT count(*) from sccpdevice")
      .on(Symbol("exten") -> lineCreated.get.extension)
      .as(scalar[Long].single) shouldBe 0
  }

  "update user sccp line from no device to new device" in withConnection() {
    implicit c =>
      val lineManager = app.injector.instanceOf(classOf[LineManager])
      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )

      val lineTemplate = Line(
        None,
        LineType.SCCP,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        None,
        None,
        None,
        Some(1234)
      )
      val lineTemplateUpdated = lineTemplate.copy(
        extension = refineMV("1000"),
        device = Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
        lineNum = Some(1)
      )

      val mac = "a8:b1:d4:fb:cc:20"

      stub(wsResponse.status).toReturn(Status.OK)
      stub(wsResponse.json).toReturn(Json.obj("mac" -> mac))
      when(confdclient.deviceGet(refineMV("c3d29985f8f8403aafbebec638e5c12e")))
        .thenReturn(Future.successful(wsResponse))

      lineManager.create(userid, lineTemplate)

      lineManager.update(userid, lineTemplateUpdated) match {
        case Success(line) =>
          line shouldBe lineTemplate.copy(
            id = line.id,
            name = line.name,
            extension = lineTemplateUpdated.extension,
            site = lineTemplateUpdated.site,
            device = Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
            lineNum = Some(1),
            provisioningId = line.provisioningId
          )
        case Failure(f) =>
          fail(f.getMessage)
      }
      SQL("SELECT line from sccpdevice")
        .as(scalar[String].single) shouldBe "1000"
      SQL("SELECT name from sccpdevice where line={exten}")
        .on(Symbol("exten") -> lineTemplateUpdated.extension)
        .as(scalar[String].single) shouldBe "SEPA8B1D4FBCC20"
      SQL("SELECT device from sccpdevice where line={exten}")
        .on(Symbol("exten") -> lineTemplateUpdated.extension)
        .as(scalar[String].single) shouldBe "SEPA8B1D4FBCC20"
  }

  "update user sccp line from old device to new device" in withConnection() {
    implicit c =>
      val lineManager = app.injector.instanceOf(classOf[LineManager])
      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )

      val lineTemplate = Line(
        None,
        LineType.SCCP,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        Some(refineMV("aaaaaaaaaaaaaaaaaaaaaa")),
        None,
        None,
        Some(1234)
      )
      val lineTemplateUpdated = lineTemplate.copy(
        extension = refineMV("1000"),
        device = Some(refineMV("bbbbbbbbbbbbbbbbbbbbbb")),
        lineNum = Some(1)
      )

      val oldMac = "aa:aa:aa:aa:aa:aa"
      val mac    = "bb:bb:bb:bb:bb:bb"

      stub(wsResponse.status).toReturn(Status.OK)
      stub(wsResponse.json).toReturn(Json.obj("mac" -> oldMac))

      val wsResponse2 = mock[WSResponse]
      stub(wsResponse2.status).toReturn(Status.OK)
      stub(wsResponse2.json).toReturn(Json.obj("mac" -> mac))

      when(confdclient.deviceGet(refineMV("aaaaaaaaaaaaaaaaaaaaaa")))
        .thenReturn(Future.successful(wsResponse))
      when(confdclient.deviceGet(refineMV("bbbbbbbbbbbbbbbbbbbbbb")))
        .thenReturn(Future.successful(wsResponse2))

      lineManager.create(userid, lineTemplate)

      lineManager.update(userid, lineTemplateUpdated) match {
        case Success(line) =>
          line shouldBe lineTemplate.copy(
            id = line.id,
            name = line.name,
            extension = lineTemplateUpdated.extension,
            site = lineTemplateUpdated.site,
            device = Some(refineMV("bbbbbbbbbbbbbbbbbbbbbb")),
            lineNum = Some(1),
            provisioningId = line.provisioningId
          )
        case Failure(f) =>
          fail(f.getMessage)
      }
      SQL("SELECT line from sccpdevice")
        .as(scalar[String].single) shouldBe "1000"
      SQL("SELECT name from sccpdevice where line={exten}")
        .on(Symbol("exten") -> lineTemplateUpdated.extension)
        .as(scalar[String].single) shouldBe "SEPBBBBBBBBBBBB"
      SQL("SELECT device from sccpdevice where line={exten}")
        .on(Symbol("exten") -> lineTemplateUpdated.extension)
        .as(scalar[String].single) shouldBe "SEPBBBBBBBBBBBB"
  }

  "update user sccp line from old broken device to new device" in withConnection() {
    implicit c =>
      val lineManager = app.injector.instanceOf(classOf[LineManager])
      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1)
      )

      val lineTemplate = Line(
        None,
        LineType.SCCP,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        Some(refineMV("")),
        None,
        None,
        Some(1234)
      )
      val lineTemplateUpdated = lineTemplate.copy(
        extension = refineMV("1000"),
        device = Some(refineMV("bbbbbbbbbbbbbbbbbbbbbb")),
        lineNum = Some(1)
      )

      val mac = "bb:bb:bb:bb:bb:bb"

      stub(wsResponse.status).toReturn(Status.OK)
      stub(wsResponse.json).toReturn(Json.obj("mac" -> mac))

      when(confdclient.deviceGet(refineMV("bbbbbbbbbbbbbbbbbbbbbb")))
        .thenReturn(Future.successful(wsResponse))

      lineManager.create(userid, lineTemplate)

      lineManager.update(userid, lineTemplateUpdated) match {
        case Success(line) =>
          line shouldBe lineTemplate.copy(
            id = line.id,
            name = line.name,
            extension = lineTemplateUpdated.extension,
            site = lineTemplateUpdated.site,
            device = Some(refineMV("bbbbbbbbbbbbbbbbbbbbbb")),
            lineNum = Some(1),
            provisioningId = line.provisioningId
          )
        case Failure(f) =>
          fail(f.getMessage)
      }
      SQL("SELECT line from sccpdevice")
        .as(scalar[String].single) shouldBe "1000"
      SQL("SELECT name from sccpdevice where line={exten}")
        .on(Symbol("exten") -> lineTemplateUpdated.extension)
        .as(scalar[String].single) shouldBe "SEPBBBBBBBBBBBB"
      SQL("SELECT device from sccpdevice where line={exten}")
        .on(Symbol("exten") -> lineTemplateUpdated.extension)
        .as(scalar[String].single) shouldBe "SEPBBBBBBBBBBBB"
  }

  "get current usersip options for a userId" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])

    val options    = "{{webrtc,yes},{someoption,42}}"
    val expected   = Array(Array("webrtc", "yes"), Array("someoption", "42"))
    val protocolId = insertUserSip("abcd", UserIaxCategory.user, options)

    lineManager.getOptions(protocolId) match {
      case Success(result) => result shouldBe expected
      case _               => fail("Cannot retrieve options in database")
    }
  }

  "update lineType from ua to webrtc" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])

    val options  = Array(Array("webrtc", "ua"), Array("otherValue", "value"))
    val expected = Array(Array("webrtc", "yes"), Array("otherValue", "value"))

    lineManager.generationUserSipOption(
      LineType.WEBRTC,
      options
    ) shouldBe expected
  }

  "update lineType from webrtc to phone" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])

    val options  = Array(Array("otherValue", "value"), Array("webrtc", "yes"))
    val expected = Array(Array("otherValue", "value"))

    lineManager.generationUserSipOption(
      LineType.PHONE,
      options
    ) shouldBe expected
  }

  "update lineType from phone to ua" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])

    val options  = Array(Array("otherValue", "value"))
    val expected = Array(Array("otherValue", "value"), Array("webrtc", "ua"))

    lineManager.generationUserSipOption(LineType.UA, options) shouldBe expected
  }

  "update sip Line type only if it changed" in withConnection() { implicit c =>
    val lineManager = app.injector.instanceOf(classOf[LineManager])

    val userid =
      Utils.insertXivoUser(10, "James", "Bond", Some("jbond"), Some(1), Some(1))

    val lineTemplate = Line(
      None,
      LineType.PHONE,
      refineMV("default"),
      refineMV("1000"),
      refineMV("site1"),
      Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
      None,
      Some(1),
      Some(1234)
    )
    val lineTemplateUpdated = lineTemplate.copy(
      extension = refineMV("1002"),
      site = refineMV("site2"),
      lineNum = Some(2)
    )

    lineManager.create(userid, lineTemplate)

    lineManager.update(userid, lineTemplateUpdated) match {
      case Success(line) =>
        line shouldBe lineTemplate.copy(
          id = line.id,
          name = line.name,
          extension = lineTemplateUpdated.extension,
          site = lineTemplateUpdated.site,
          lineNum = Some(2),
          provisioningId = line.provisioningId
        )
      case Failure(f) =>
        fail(f.getMessage)
    }
  }

  "doesn't append or change <phone> if already existing in caller id" in withConnection() {
    implicit c =>
      val lineManager = app.injector.instanceOf(classOf[LineManager])
      Utils.insertEntity(1, "xivo", "XiVO test")

      val userid = Utils.insertXivoUser(
        10,
        "James",
        "Bond",
        Some("jbond"),
        Some(1),
        Some(1),
        callerId = Some("\"James Bond\" <2000>")
      )

      val lineTemplate = Line(
        None,
        LineType.PHONE,
        refineMV("default"),
        refineMV("1000"),
        refineMV("site1"),
        Some(refineMV("c3d29985f8f8403aafbebec638e5c12e")),
        None,
        Some(1),
        Some(1234)
      )
      val lineTemplateUpdated = lineTemplate.copy(
        lineType = LineType.WEBRTC,
        extension = refineMV("1002"),
        site = refineMV("site2"),
        lineNum = Some(2)
      )

      val lineCreated = lineManager.create(userid, lineTemplate)
      SQL(
        "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
      )
        .on(Symbol("lineid") -> lineCreated.get.id)
        .as(scalar[String].single) shouldBe "\"James Bond\" <2000>"

      lineManager.update(userid, lineTemplateUpdated) match {
        case Success(line) =>
          line shouldBe lineTemplate.copy(
            id = line.id,
            lineType = LineType.WEBRTC,
            name = line.name,
            extension = lineTemplateUpdated.extension,
            site = lineTemplateUpdated.site,
            lineNum = Some(2),
            provisioningId = line.provisioningId
          )
          val endpointid =
            SQL("SELECT l.protocolid FROM linefeatures l where l.id={lineid}")
              .on(Symbol("lineid") -> line.id)
              .as(scalar[Long].single)
          verify(lineNotifier).onUserLineEdited(
            userid,
            lineCreated.get,
            line,
            endpointid
          )
        case Failure(f) =>
          fail(f.getMessage)
      }
      SQL(
        "SELECT callerid from usersip us inner join linefeatures l on us.id=l.protocolid where l.id={lineid}"
      )
        .on(Symbol("lineid") -> lineCreated.get.id)
        .as(scalar[String].single) shouldBe "\"James Bond\" <2000>"
  }
}
