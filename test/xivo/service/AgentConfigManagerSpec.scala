package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import xivo.model._

class AgentConfigManagerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE agentfeatures").execute()
      SQL("TRUNCATE queuemember").execute()
      SQL("TRUNCATE userfeatures, user_line, userpreferences").execute()
      SQL("TRUNCATE queuefeatures").execute()

      super.beforeEach()
    }

  "xivo.service.AgentManager" should {
    "get a xivo agent" in withConnection() { implicit c =>
      val agentManager = app.injector.instanceOf(classOf[AgentConfigManager])
      val member = List(
        QueueMember(
          "queue1",
          1L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1L,
          "Agent",
          QueueCategory.Queue,
          1
        ),
        QueueMember(
          "queue2",
          2L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1L,
          "Agent",
          QueueCategory.Queue,
          1
        )
      )
      val agent =
        Agent(1L, "Agent", "One", "1001", "default", member, 1L, Some(1L))

      Utils.insertAgentFeatures(agent)
      Utils.insertXivoUser(1, "User", "One", Some("login1"), Some(1), Some(1))
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")
      Utils.insertQueueMember(member(0), "agent", "queue")
      Utils.insertQueueMember(member(1), "agent", "queue")

      val fetchedAgent = agentManager.getById(1L)

      fetchedAgent.isSuccess shouldBe true
      fetchedAgent.get shouldEqual agent
    }

    "get a xivo agent without associated user" in withConnection() {
      implicit c =>
        val agentManager = app.injector.instanceOf(classOf[AgentConfigManager])
        val member = List(
          QueueMember(
            "queue1",
            1L,
            "Agent/1011",
            penalty = 1,
            0,
            QueueMemberUserType.Agent,
            1L,
            "Agent",
            QueueCategory.Queue,
            1
          ),
          QueueMember(
            "queue2",
            2L,
            "Agent/1011",
            penalty = 1,
            0,
            QueueMemberUserType.Agent,
            1L,
            "Agent",
            QueueCategory.Queue,
            1
          )
        )
        val agent =
          Agent(1L, "Agent", "One", "1001", "default", member, 1L, None)

        Utils.insertAgentFeatures(agent)
        Utils.insertQueueFeature(1, "queue1", "Queue 1")
        Utils.insertQueueFeature(2, "queue2", "Queue 2")
        Utils.insertQueueMember(member(0), "agent", "queue")
        Utils.insertQueueMember(member(1), "agent", "queue")

        val fetchedAgent = agentManager.getById(1L)

        fetchedAgent.isSuccess shouldBe true
        fetchedAgent.get shouldEqual agent
    }

    "get all xivo agents" in withConnection() { implicit c =>
      val agentManager = app.injector.instanceOf(classOf[AgentConfigManager])

      val member = List(
        QueueMember(
          "queue1",
          1L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1L,
          "Agent",
          QueueCategory.Queue,
          1
        ),
        QueueMember(
          "queue2",
          2L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          2L,
          "Agent",
          QueueCategory.Queue,
          1
        )
      )

      val uId1: Long =
        Utils.insertXivoUser(1, "User", "One", Some("login1"), Some(1), Some(1))
      val uId2: Long =
        Utils.insertXivoUser(2, "User", "Two", Some("login2"), Some(2), Some(1))

      val agent1 = Agent(
        1L,
        "Agent",
        "One",
        "1001",
        "default",
        List(member(0)),
        1L,
        Some(uId1)
      )
      val agent2 = Agent(
        2L,
        "Agent",
        "Two",
        "1002",
        "default",
        List(member(1)),
        1L,
        Some(uId2)
      )

      Utils.insertAgentFeatures(agent1)
      Utils.insertAgentFeatures(agent2)

      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")
      Utils.insertQueueMember(member(0), "agent", "queue")
      Utils.insertQueueMember(member(1), "agent", "queue")

      val fetched = agentManager.all()

      fetched.isSuccess shouldBe true
      fetched.get should contain.allOf(agent1, agent2)
    }

    "get all xivo agents without any user" in withConnection() { implicit c =>
      val agentManager = app.injector.instanceOf(classOf[AgentConfigManager])

      val member = List(
        QueueMember(
          "queue1",
          1L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1L,
          "Agent",
          QueueCategory.Queue,
          1
        ),
        QueueMember(
          "queue2",
          2L,
          "Agent/1011",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          2L,
          "Agent",
          QueueCategory.Queue,
          1
        )
      )

      val agent1 =
        Agent(1L, "Agent", "One", "1001", "default", List(member(0)), 1L, None)
      val agent2 =
        Agent(2L, "Agent", "Two", "1002", "default", List(member(1)), 1L, None)

      Utils.insertAgentFeatures(agent1)
      Utils.insertAgentFeatures(agent2)
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")
      Utils.insertQueueMember(member(0), "agent", "queue")
      Utils.insertQueueMember(member(1), "agent", "queue")

      val fetched = agentManager.all()

      fetched.isSuccess shouldBe true
      fetched.get should contain.allOf(agent1, agent2)
    }
  }
}
