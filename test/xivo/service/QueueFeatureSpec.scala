package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.RecordingModeType
import xivo.model.QueueFeature
import xivo.model.rabbitmq._

import scala.util.{Failure, Success}

class QueueFeatureSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE queuefeatures").execute()

      super.beforeEach()
    }

  "xivo.service.QueueFeatureManager" should {
    "create xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val created = queueFeatureManager.create(q)

      created.isSuccess shouldBe (true)
      created.get.id shouldNot be(None)
    }

    "get a xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val createdUser = queueFeatureManager.create(q).get

      val fetchedUser = queueFeatureManager.get(createdUser.id.get)

      fetchedUser.isSuccess shouldBe (true)
      fetchedUser.get shouldEqual (createdUser)
    }

    "update a xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val createdQueue =
        queueFeatureManager.create(q).get.copy(displayname = "Rename queue 1")
      queueFeatureManager.update(createdQueue) match {
        case Success(q) =>
        case Failure(t) => {
          t.printStackTrace()
          fail(t)
        }
      }

      val fetchedQueue = queueFeatureManager.get(createdQueue.id.get)

      fetchedQueue.isSuccess shouldBe (true)
      fetchedQueue.get shouldEqual (createdQueue)
    }

    "fail updating a non existing xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        Some(1),
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      queueFeatureManager.delete(1)

      queueFeatureManager.update(q).isSuccess shouldBe (false)
    }

    "delete a xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val created = queueFeatureManager.create(q).get

      queueFeatureManager.delete(created.id.get)

      val fetchedUser = queueFeatureManager.get(created.id.get)

      fetchedUser.isSuccess shouldBe (false)
    }

    "fail deleting a non existing xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])

      queueFeatureManager.delete(1)

      queueFeatureManager.delete(1).isSuccess shouldBe (false)
    }

    "get all xivo QueueFeature" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      queueFeatureManager.deleteByName("queue2")
      val q1 = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val q2 = QueueFeature(
        None,
        "queue2",
        "My second Queue",
        "3001",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val created1 = queueFeatureManager.create(q1).get
      val created2 = queueFeatureManager.create(q2).get

      val fetched = queueFeatureManager.all()

      fetched.isSuccess shouldBe (true)
      fetched.get should contain.allOf(created1, created2)
    }

    "get a xivo QueueFeature by name" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val createdUser = queueFeatureManager.create(q).get

      val fetchedUser = queueFeatureManager.getByName("queue1")

      fetchedUser.isSuccess shouldBe (true)
      fetchedUser.get shouldEqual (createdUser)

    }

    "get the ID of a queue from its name" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])

      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val id = queueFeatureManager.create(q).get.id

      queueFeatureManager.getQueueIdByName("queue1") shouldEqual id

    }

    "know if a queue does not exist" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])

      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      queueFeatureManager.create(q)

      queueFeatureManager.getQueueIdByName("wrong_queue_name") shouldEqual None
    }

    "delete a xivo QueueFeature by name" in {
      val queueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      queueFeatureManager.deleteByName("queue1")
      val q = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      val created = queueFeatureManager.create(q).get

      queueFeatureManager.deleteByName(created.name)

      val fetchedUser = queueFeatureManager.get(created.id.get)

      fetchedUser.isSuccess shouldBe (false)
    }
  }

  "xivo.model.QueueFeature" should {
    "have an implicit for RabbitMessage creation" in {
      import ToRabbitMessage.ops._
      val q = QueueFeature(
        Some(1),
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      q.toRabbitMessage(ObjectCreated) shouldBe ObjectEvent(QueueCreated, 1)
      q.toRabbitMessage(ObjectEdited) shouldBe ObjectEvent(QueueEdited, 1)
      q.toRabbitMessage(ObjectDeleted) shouldBe ObjectEvent(QueueDeleted, 1)
    }
  }
}
