package xivo.service

import docker.DockerPlaySpec
import eu.timepit.refined.refineMV
import org.mockito.Mockito._
import org.scalatest.concurrent.ScalaFutures
import play.api.http.Status.NO_CONTENT
import play.api.libs.ws._
import play.api.mvc.Results._
import play.api.routing.Router
import play.api.routing.sird._
import play.api.test._
import play.api.{BuiltInComponentsFromContext, Configuration}
import play.core.server.Server
import play.filters.HttpFiltersComponents
import ws.model.Line

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

class ConfdClientSpec extends DockerPlaySpec with ScalaFutures {

  implicit val executionContext: ExecutionContext =
    app.injector.instanceOf[ExecutionContext]

  val config: Configuration  = mock[Configuration]
  val wsclient: WSClient     = mock[WSClient]
  val wsrequest: WSRequest   = mock[WSRequest]
  val wsresponse: WSResponse = mock[WSResponse]

  val xivoHost              = "localhost"
  val confdtoken            = "abcdefg"
  val confdport             = 9486
  val confdtimeout          = 5000
  val deviceid: Line.Device = refineMV("hijklmno")

  when(config.get[String]("xivohost")).thenReturn(xivoHost)
  when(config.get[String]("authentication.confd.restapi.token"))
    .thenReturn(confdtoken)
  when(config.getOptional[Int]("confdport")).thenReturn(Some(confdport))
  when(config.getOptional[Int]("confdtimeout")).thenReturn(Some(confdtimeout))

  def withConfdClient[T](block: ConfdClient => T): T = {
    Server.withApplicationFromContext() { context =>
      new BuiltInComponentsFromContext(context) with HttpFiltersComponents {
        override def router: Router =
          Router.from {
            case GET(p"/devices/hijklmno/update") =>
              Action(NoContent)
            case GET(p"/devices/hijklmno") =>
              Action(NoContent)
            case GET(p"/devices/unknownDeviceId") =>
              Action(NotFound)
          }
      }.application
    } { implicit port =>
      WsTestClient.withClient { client =>
        block(new ConfdClient(config, client) {
          override val baseUri: String = ""
        })
      }
    }
  }

  "build device update request" in {
    val confd = new ConfdClient(config, wsclient) {
      override def request(url: String): WSRequest = {
        url shouldBe s"https://$xivoHost:$confdport/1.1/devices/$deviceid/update"
        when(wsrequest.withMethod("GET")).thenReturn(wsrequest)
        when(wsrequest.execute()).thenReturn(Future.successful(wsresponse))
        wsrequest
      }
    }
    confd.deviceUpdate(deviceid)
  }

  "include confd token in the request" in {
    withConfdClient { client =>
      val req = client.request("http://localhost")
      req.header("X-Auth-Token").get shouldEqual "abcdefg"
    }
  }

  "return a success on device get" in {
    withConfdClient { client =>
      val result = Await.result(client.deviceGet(deviceid), 2.seconds)
      result.status shouldEqual NO_CONTENT
    }
  }

  "return a success on device update" in {
    withConfdClient { client =>
      val result = Await.result(client.deviceUpdate(deviceid), 2.seconds)
      result.status shouldEqual NO_CONTENT
    }
  }

  "handle error status code" in {
    withConfdClient { client =>
      client
        .deviceGet(refineMV("unknownDeviceId"))
        .failed
        .futureValue shouldBe an[ConfdServiceException]
    }
  }
}
