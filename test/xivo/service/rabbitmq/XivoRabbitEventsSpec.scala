package xivo.service.rabbitmq

import play.api.libs.json._
import xivo.model.rabbitmq._
import xivo.model.Xivo
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class XivoRabbitEventsSpec extends AnyWordSpec with Matchers {
  implicit val xivo: Xivo = Xivo(java.util.UUID.randomUUID)

  "A RabbitMessage" should {
    "be created from json" in {
      val event = ObjectEvent(QueueEdited, 1)
      val json: JsValue = Json.parse(
        s"""
          | {"data": {"id": 1}, "name": "queue_edited", "origin_uuid": "${xivo.uuid}"}
          | """.stripMargin
      )

      json.as[ObjectEvent] shouldBe event
    }

    "be serialiazed to json" in {
      val event = ObjectEvent(QueueEdited, 1)
      val json: JsValue = Json.parse(
        s"""
          | {"data": {"id": 1}, "name": "queue_edited", "origin_uuid": "${xivo.uuid}"}
          | """.stripMargin
      )

      Json.toJson(event) shouldBe json
    }
  }

  "A RabbitMessageType" should {
    "be created from json" in {
      val checks = Map(
        "queue_edited"            -> QueueEdited,
        "queue_created"           -> QueueCreated,
        "queue_deleted"           -> QueueDeleted,
        "mediaserver_edited"      -> MediaServerEdited,
        "mediaserver_created"     -> MediaServerCreated,
        "mediaserver_deleted"     -> MediaServerDeleted,
        "user_preference_edited"  -> UserPreferenceEdited,
        "user_preference_created" -> UserPreferenceCreated,
        "user_preference_deleted" -> UserPreferenceDeleted
      )

      checks.foreach(i => {
        val jsonStr = s""""${i._1}""""

        Json.parse(jsonStr).as[RabbitMessageType] shouldBe i._2
      })
    }
  }
}
