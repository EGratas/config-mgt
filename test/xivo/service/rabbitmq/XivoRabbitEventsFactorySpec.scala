package xivo.service.rabbitmq

import com.rabbitmq.client._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import play.api.Configuration
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class XivoRabbitEventsFactorySpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar {

  class Helper {
    val factory: XivoRabbitEventsFactory    = mock[XivoRabbitEventsFactory]
    val channel: Channel                    = mock[Channel]
    val configuration: Configuration        = mock[Configuration]
    val busConfiguration: ConnectionFactory = mock[ConnectionFactory]
    val connection: Connection              = mock[Connection]

    val consumerTag = "the-consumer-tag"

    when(factory.channel).thenReturn(channel)
    when(factory.queueName).thenReturn("test-queue")
    when(factory.consumerTag).thenReturn(consumerTag)
    when(factory.exchangeName).thenReturn("xivo")
    when(factory.routingKeys).thenReturn(List("key1"))
  }

  "A XivoRabbitEventsFactory" should {
    "create a connection and a channel" in {
      val configuration: Configuration        = mock[Configuration]
      val busConfiguration: ConnectionFactory = mock[ConnectionFactory]
      val connection: Connection              = mock[Connection]
      val channel: Channel                    = mock[Channel]

      when(busConfiguration.newConnection).thenReturn(connection)
      when(connection.createChannel).thenReturn(channel)

      new XivoRabbitEventsFactory(configuration, busConfiguration)

      verify(busConfiguration).newConnection
      verify(connection).createChannel
    }

    "close the connection" in new Helper {
      when(busConfiguration.newConnection).thenReturn(connection)
      when(connection.createChannel).thenReturn(channel)

      val factoryUnderTest: XivoRabbitEventsFactory =
        new XivoRabbitEventsFactory(configuration, busConfiguration)

      factoryUnderTest.shutdownConnection()

      verify(channel).close()
      connection.close()
    }
  }
}
