package xivo.service.rabbitmq

import com.rabbitmq.client._
import org.mockito.Mockito._
import org.scalatestplus.mockito.MockitoSugar
import play.api.Configuration
import xivo.model.rabbitmq._
import xivo.model.Xivo
import xivo.service.XivoManager
import scala.util.Success
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

case class DummyObject(id: Long, text: String)
object DummyObject {
  implicit val rabbitMessage: ToRabbitMessage[DummyObject] = new ToRabbitMessage[DummyObject] {
    def toRabbitMessage(o: DummyObject, u: ObjectUpdateMode): ObjectEvent =
      ObjectEvent(QueueEdited, o.id)
  }
}

class XivoRabbitEventsPublisherSpec
    extends AnyWordSpec
    with Matchers
    with MockitoSugar {
  class Helper {
    val factory: XivoRabbitEventsFactory = mock[XivoRabbitEventsFactory]
    val channel: Channel                 = mock[Channel]
    val envelope: Envelope               = mock[Envelope]
    val xivoManager: XivoManager         = mock[XivoManager]
    val configuration: Configuration     = mock[Configuration]
    implicit val xivo: Xivo = Xivo(java.util.UUID.randomUUID)

    val consumerTag = "the-consumer-tag"

    when(factory.channel).thenReturn(channel)
    when(factory.queueName).thenReturn("test-queue")
    when(factory.consumerTag).thenReturn(consumerTag)
    when(factory.exchangeName).thenReturn("xivo")
    when(factory.routingKeys).thenReturn(List("key1"))
    when(factory.rabbitUsername).thenReturn("test")
    when(xivoManager.get()).thenReturn(Success(xivo))

    val rabbitPublisher =
      new XivoRabbitEventsPublisher(configuration, factory, xivoManager)
  }

  "A XivoRabbitEventsConsumer" should {
    "publish message based on ToRabbitMessage typeclass" in new Helper {
      val model = DummyObject(123, "Tada !")
      val expected: Array[Byte] =
        s"""{"data":{"id":123},"name":"queue_edited","origin_uuid":"${xivo.uuid}"}"""
          .getBytes()

      rabbitPublisher.publish(model, ObjectEdited)

      verify(channel).basicPublish(
        "xivo",
        "config.queue.edited",
        rabbitPublisher.publishProps,
        expected
      )
    }
  }
}
