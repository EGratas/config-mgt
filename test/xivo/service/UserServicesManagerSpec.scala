package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import org.mockito.Mockito._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import ws.model.{PartialUserServices, UserForward, UserServices}
import xivo.model.rabbitmq.{ObjectEvent, UserServicesEdited}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import scala.util.{Failure, Success}

class UserServicesManagerSpec extends DockerPlayWithDbSpec {

  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE userfeatures, linefeatures, extensions, user_line, userpreferences"
      ).execute()
      SQL("TRUNCATE entity").execute()
      reset(rabbitPublisher)
      super.beforeEach()
    }

  "get user services" in withConnection() { implicit c =>
    val manager = app.injector.instanceOf(classOf[UserServicesManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val expected = UserServices(
      dndEnabled = true,
      UserForward(enabled = false, "1234"),
      UserForward(enabled = true, "5678"),
      UserForward(enabled = false, "7890")
    )

    val userid = Utils.insertXivoUser(
      10,
      "James",
      "Bond",
      Some("jbond"),
      Some(1),
      Some(1),
      expected.dndEnabled,
      expected.unconditional.enabled,
      expected.unconditional.destination,
      expected.noanswer.enabled,
      expected.noanswer.destination,
      expected.busy.enabled,
      expected.busy.destination
    )

    manager.getServices(userid) match {
      case Success(services) =>
        services shouldBe expected
      case Failure(f) => fail(f.getMessage)
    }
  }

  "update all user services" in withConnection() { implicit c =>
    val manager = app.injector.instanceOf(classOf[UserServicesManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val newservices = PartialUserServices(
      Some(true),
      Some(UserForward(enabled = false, "1234")),
      Some(UserForward(enabled = true, "5678")),
      Some(UserForward(enabled = false, "7890"))
    )

    val userid = Utils.insertXivoUser(
      10,
      "James",
      "Bond",
      Some("jbond"),
      Some(1),
      Some(1),
      newservices.dndEnabled.get,
      newservices.unconditional.get.enabled,
      newservices.unconditional.get.destination,
      newservices.noanswer.get.enabled,
      newservices.noanswer.get.destination,
      newservices.busy.get.enabled,
      newservices.busy.get.destination
    )

    manager.updatePartialServices(userid, newservices) shouldBe Success(
      newservices
    )
    verify(rabbitPublisher).publish(ObjectEvent(UserServicesEdited, userid))
  }

  "update one user service" in withConnection() { implicit c =>
    val manager = app.injector.instanceOf(classOf[UserServicesManager])
    Utils.insertEntity(1, "xivo", "XiVO test")

    val update = PartialUserServices(Some(true), None, None, None)

    val userid = Utils.insertXivoUser(
      10,
      "James",
      "Bond",
      Some("jbond"),
      Some(1),
      Some(1),
      update.dndEnabled.get
    )

    manager.updatePartialServices(userid, update) shouldBe Success(update)
    verify(rabbitPublisher).publish(ObjectEvent(UserServicesEdited, userid))
  }
}
