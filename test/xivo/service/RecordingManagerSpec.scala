package xivo.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model._
import org.mockito.Mockito._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.QueueFeature
import xivo.model.rabbitmq._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import java.util.NoSuchElementException
import scala.util.{Failure, Success, Try}

class RecordingManagerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE queuefeatures").execute()

      super.beforeEach()
    }

  "xivo.service.RecordingManager" should {

    "compute recording mode 'recorded' to apply for queue id when recording_mode is Recorded and recording_activated is true" in new Helper {
      val qf: QueueFeature =
        createQueue("queue_recording", "3000", RecordingModeType.Recorded, 1)
      recordingManager.computeRecordingModeToApply(qf.id.get) should be(
        Success(RecordingMode(RecordingModeType.Recorded))
      )
    }

    "compute recording mode 'notrecorded' to apply for queue id whenever recording_activated is false" in new Helper {
      val qf: QueueFeature =
        createQueue("queue_recording", "3000", RecordingModeType.Recorded, 0)
      recordingManager.computeRecordingModeToApply(qf.id.get) should be(
        Success(RecordingMode(RecordingModeType.NotRecorded))
      )
    }

    "compute recording mode 'recordedondemand' to apply for queue id when recording_mode is RecordedOnDemand and recording_activated is true" in new Helper {
      val qf: QueueFeature = createQueue(
        "queue_recording",
        "3000",
        RecordingModeType.RecordedOnDemand,
        1
      )
      recordingManager.computeRecordingModeToApply(qf.id.get) should be(
        Success(RecordingMode(RecordingModeType.RecordedOnDemand))
      )
    }

    "get recording config for queue id" in new Helper {
      val qf: QueueFeature =
        createQueue("queue_recording", "3000", RecordingModeType.Recorded, 1)
      recordingManager.getRecordingConfig(qf.id.get) should be(
        Success(RecordingConfig(activated = true, RecordingModeType.Recorded))
      )
    }

    "set recording config for queue id" in new Helper {
      val qf: QueueFeature =
        createQueue("queue_recording", "3000", RecordingModeType.NotRecorded, 0)
      val recCfg =
        RecordingConfig(activated = true, RecordingModeType.RecordedOnDemand)

      recordingManager.setRecordingConfig(qf.id.get, recCfg) should be(
        Success(
          RecordingConfig(activated = true, RecordingModeType.RecordedOnDemand)
        )
      )
      verify(rabbitPublisher).publish(
        qf.copy(
          recording_mode = RecordingModeType.RecordedOnDemand,
          recording_activated = 1
        ),
        ObjectEdited
      )
      recordingManager.getRecordingConfig(qf.id.get) should be(
        Success(
          RecordingConfig(activated = true, RecordingModeType.RecordedOnDemand)
        )
      )
    }

    "get recording status when all activated recorded or recordedondemand queues" in new Helper {
      val qf1: QueueFeature =
        createQueue("queue_recording_1", "3001", RecordingModeType.Recorded, 1)
      val qf2: QueueFeature = createQueue(
        "queue_recording_2",
        "3002",
        RecordingModeType.RecordedOnDemand,
        1
      )
      val qf3: QueueFeature =
        createQueue("queue_recording_3", "3003", RecordingModeType.Recorded, 1)

      recordingManager.getRecordingQueuesStatus should be(
        Success(
          RecordingQueuesStatus(
            all = true,
            Map(
              qf1.id -> RecordingModeType.Recorded,
              qf2.id -> RecordingModeType.RecordedOnDemand,
              qf3.id -> RecordingModeType.Recorded
            )
          )
        )
      )
    }

    "get recording status when only some activated queues" in new Helper {
      val qf1: QueueFeature =
        createQueue("queue_recording_1", "3001", RecordingModeType.Recorded, 1)
      createQueue("queue_recording_2", "3002", RecordingModeType.NotRecorded, 0)
      createQueue("queue_recording_3", "3003", RecordingModeType.NotRecorded, 1)
      val qf4: QueueFeature =
        createQueue("queue_recording_4", "3004", RecordingModeType.Recorded, 0)
      val qf5: QueueFeature = createQueue(
        "queue_recording_5",
        "3005",
        RecordingModeType.RecordedOnDemand,
        1
      )

      recordingManager.getRecordingQueuesStatus should be(
        Success(
          RecordingQueuesStatus(
            all = false,
            Map(
              qf1.id -> RecordingModeType.Recorded,
              qf4.id -> RecordingModeType.NotRecorded,
              qf5.id -> RecordingModeType.RecordedOnDemand
            )
          )
        )
      )
    }

    "get recording status when no queues" in new Helper {
      recordingManager.getRecordingQueuesStatus should be(
        Success(RecordingQueuesStatus(all = false, Map()))
      )
    }

    "set recording activated for recorded or recordedondemand queues" in new Helper {
      createQueue("queue_recording_0", "3000", RecordingModeType.NotRecorded, 0)
      createQueue(
        "queue_recording_100",
        "3100",
        RecordingModeType.NotRecorded,
        1
      )
      val qf1: QueueFeature =
        createQueue("queue_recording_1", "3001", RecordingModeType.Recorded, 0)
      val qf2: QueueFeature = createQueue(
        "queue_recording_2",
        "3002",
        RecordingModeType.RecordedOnDemand,
        0
      )
      val qf3: QueueFeature =
        createQueue("queue_recording_3", "3003", RecordingModeType.Recorded, 1)

      recordingManager.setRecordingQueuesStatus(true) should be(
        Success(
          RecordingQueuesStatusPublish(
            all = true,
            Map(
              qf1.id -> RecordingModeType.Recorded,
              qf2.id -> RecordingModeType.RecordedOnDemand
            )
          )
        )
      )

      verify(rabbitPublisher).publish(
        qf1.copy(recording_activated = 1),
        ObjectEdited
      )
      verify(rabbitPublisher).publish(
        qf2.copy(recording_activated = 1),
        ObjectEdited
      )
      verifyNoMoreInteractions(rabbitPublisher)
    }

    "set recording de-activated for recorded or recordedondemand queues" in new Helper {
      createQueue("queue_recording_0", "3000", RecordingModeType.NotRecorded, 0)
      createQueue(
        "queue_recording_100",
        "3100",
        RecordingModeType.NotRecorded,
        1
      )
      val qf1: QueueFeature =
        createQueue("queue_recording_1", "3001", RecordingModeType.Recorded, 0)
      val qf2: QueueFeature = createQueue(
        "queue_recording_2",
        "3002",
        RecordingModeType.RecordedOnDemand,
        0
      )
      val qf3: QueueFeature =
        createQueue("queue_recording_3", "3003", RecordingModeType.Recorded, 1)

      recordingManager.setRecordingQueuesStatus(false) should be(
        Success(
          RecordingQueuesStatusPublish(
            all = false,
            Map(
              qf3.id -> RecordingModeType.NotRecorded
            )
          )
        )
      )

      verify(rabbitPublisher).publish(
        qf3.copy(recording_activated = 0),
        ObjectEdited
      )
      verifyNoMoreInteractions(rabbitPublisher)
    }

    "get NoSuchElementException when queueId is not existing" in new Helper {
      queueFeatureManager.delete(12)

      val f: Try[RecordingMode] =
        recordingManager.computeRecordingModeToApply(12)
      f shouldBe a[Failure[_]]
      f.failed.get shouldBe a[NoSuchElementException]
    }

    "forward Exception if any" in {
      val queueFeatureManagerMock = mock[QueueFeatureManager]
      val rabbitPublisherMock     = mock[XivoRabbitEventsPublisher]
      stub(queueFeatureManagerMock.get(-1))
        .toReturn(Failure(new ArrayIndexOutOfBoundsException()))

      val mockedApp: Application = new GuiceApplicationBuilder()
        .configure(playConfigWithDatabase)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisherMock))
        .overrides(bind[QueueFeatureManager].to(queueFeatureManagerMock))
        .build()

      val recordingManager: RecordingManager =
        mockedApp.injector.instanceOf(classOf[RecordingManager])

      val f: Try[RecordingMode] =
        recordingManager.computeRecordingModeToApply(-1)
      f shouldBe a[Failure[_]]
      f.failed.get shouldBe a[ArrayIndexOutOfBoundsException]
    }
  }

  class Helper {
    val queueFeatureManager: QueueFeatureManager =
      app.injector.instanceOf(classOf[QueueFeatureManager])
    val rabbitPublisher: XivoRabbitEventsPublisher =
      mock[XivoRabbitEventsPublisher]
    val recordingManager: RecordingManager =
      new RecordingManager(queueFeatureManager, rabbitPublisher)

    def createQueue(
        name: String,
        number: String,
        recordingType: RecordingModeType.Type,
        recordingActivated: Int
    ): QueueFeature = {
      val q = QueueFeature(
        None,
        name,
        "Recording Queue",
        number,
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        recordingType,
        recordingActivated
      )

      queueFeatureManager.create(q).get
    }
  }
}
