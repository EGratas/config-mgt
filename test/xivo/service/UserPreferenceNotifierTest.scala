package xivo.service

import docker.DockerPlaySpec
import org.mockito.Mockito.verify
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import xivo.model.rabbitmq.{
  ObjectEvent,
  UserPreferenceCreated,
  UserPreferenceDeleted,
  UserPreferenceEdited
}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class UserPreferenceNotifierTest extends DockerPlaySpec {

  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfig)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  "UserPreferenceNotifier" should {
    "send message to rabbitmq to notify of preference creation" in {
      val notifier = app.injector.instanceOf(classOf[UserPreferenceNotifier])
      val userId   = 1

      notifier.onUserPreferenceCreated(userId)
      verify(rabbitPublisher).publish(
        ObjectEvent(UserPreferenceCreated, userId)
      )
    }

    "send message to rabbitmq to notify of preference edition" in {
      val notifier = app.injector.instanceOf(classOf[UserPreferenceNotifier])
      val userId   = 1

      notifier.onUserPreferenceEdited(userId)
      verify(rabbitPublisher).publish(ObjectEvent(UserPreferenceEdited, userId))
    }

    "send message to rabbitmq to notify of preference deletion" in {
      val notifier = app.injector.instanceOf(classOf[UserPreferenceNotifier])
      val userId   = 1

      notifier.onUserPreferenceDeleted(userId)
      verify(rabbitPublisher).publish(
        ObjectEvent(UserPreferenceDeleted, userId)
      )
    }
  }
}
