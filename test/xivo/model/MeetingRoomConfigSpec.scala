package xivo.model

import docker.DockerPlaySpec
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}

class MeetingRoomConfigSpec extends DockerPlaySpec {

  class Helper() {}

  "MeetingRoomConfig" should {
    "read from json" in new Helper() {
      val json: JsValue = Json.parse(
        """{"displayName":"My Conf Room 1","number":"1111","userPin":"1234","roomType":"static"}"""
      )

      val expected: MeetingRoomConfig = new MeetingRoomConfig(
        None,
        "My Conf Room 1",
        Some("1111"),
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

      json.validate[MeetingRoomConfig] match {
        case JsSuccess(m, _) => m shouldEqual expected
        case JsError(_)      => fail()
      }
    }

    "read from json with no room type" in new Helper() {
      val json: JsValue = Json.parse(
        """{"displayName":"My Conf Room 1","number":"1111","userPin":"1234"}"""
      )

      val expected: MeetingRoomConfig = new MeetingRoomConfig(
        None,
        "My Conf Room 1",
        Some("1111"),
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

      json.validate[MeetingRoomConfig] match {
        case JsSuccess(m, _) => m shouldEqual expected
        case JsError(_)      => fail()
      }
    }

    "read from json with no number" in new Helper() {
      val json: JsValue = Json.parse(
        """{"displayName":"My Conf Room 1","userPin":"1234"}"""
      )

      val expected: MeetingRoomConfig = new MeetingRoomConfig(
        None,
        "My Conf Room 1",
        None,
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

      json.validate[MeetingRoomConfig] match {
        case JsSuccess(m, _) => m shouldEqual expected
        case JsError(_)      => fail()
      }
    }

    "read from json with empty string as number" in new Helper() {
      val json: JsValue = Json.parse(
        """{"displayName":"My Conf Room 1","number":"","userPin":"1234","roomType":"static"}"""
      )

      json.validate[MeetingRoomConfig] match {
        case JsSuccess(_, _) => fail()
        case JsError(_)      =>
      }
    }

    "read from json with empty string userPin" in new Helper() {
      val json: JsValue = Json.parse(
        """{"displayName":"My Conf Room 1","number":"1111","userPin":"","roomType":"static"}"""
      )

      val expected: MeetingRoomConfig = new MeetingRoomConfig(
        None,
        "My Conf Room 1",
        Some("1111"),
        None,
        None,
        RoomTypes.Static,
        None
      )

      json.validate[MeetingRoomConfig] match {
        case JsSuccess(m, _) => m shouldEqual expected
        case JsError(_)      => fail()
      }
    }

    "write to json" in new Helper() {
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1111"),
        Some("1234"),
        None,
        RoomTypes.Static,
        None
      )

      Json.toJson(meetingRoom) shouldEqual Json.obj(
        "id"          -> 1,
        "displayName" -> "My Conf Room 1",
        "number"      -> "1111",
        "userPin"     -> "1234",
        "roomType"    -> "static"
      )
    }
  }
}
