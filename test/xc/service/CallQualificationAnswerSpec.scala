package xc.service

import anorm.SQL
import docker.DockerPlayWithDbSpec
import xc.model.CallQualificationAnswer

import java.sql.Timestamp
import scala.util.{Failure, Success, Try}

class CallQualificationAnswerSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE qualification_answers").execute()
      SQL("ALTER SEQUENCE qualification_answers_id_seq RESTART WITH 1")
        .execute()

      super.beforeEach()
    }

  class Helper() {
    val manager: CallQualificationAnswerManagerImpl =
      app.injector.instanceOf(classOf[CallQualificationAnswerManagerImpl])
    val answer = CallQualificationAnswer(
      sub_qualification_id = 1,
      time = "2017-05-26 17:25:38",
      callid = "callid1",
      agent = 1,
      queue = 1,
      firstName = "first",
      lastName = "last",
      comment = "some comment",
      customData = "some custom data"
    )

    val fromRefTime: String          = "2016-01-01"
    val toRefTime: String            = "2018-12-12"
    val qualificationTime: Timestamp = manager.toTimestamp("2017-05-26")

    def createQualificationAnswer(): Try[Option[Long]] = {
      val manager: CallQualificationAnswerManagerImpl =
        app.injector.instanceOf(classOf[CallQualificationAnswerManagerImpl])
      manager.create(answer)
    }
  }

  "xc.service.CallQualificationAnswer" should {

    "create an answer" in new Helper {
      manager.create(answer) match {
        case Success(res) => res shouldEqual Some(1)
        case Failure(t)   => fail(t)
      }
    }

    "get all answers" in new Helper {
      createQualificationAnswer()

      manager.all() match {
        case Success(res) => res shouldEqual List(answer)
        case Failure(t)   => fail(t)
      }
    }

    "get single answer" in new Helper {
      createQualificationAnswer()

      manager.all(1) match {
        case Success(res) => res shouldEqual List(answer)
        case Failure(t)   => fail(t)
      }
    }

    "get all answers by queue" in new Helper {
      val answers = List(
        answer,
        answer.copy(time = "2010-05-26 17:25:38"),
        answer.copy(time = "2018-05-26 17:25:38")
      )

      manager.create(answers.head)
      manager.create(answers(1))
      manager.create(answers(2))

      manager.allByQueue(1L, fromRefTime, toRefTime) match {
        case Success(res) =>
          res should contain theSameElementsAs List(answers.head, answers(2))
        case Failure(t) => fail(t)
      }
    }

    "delete an answer" in new Helper {
      createQualificationAnswer()

      manager.delete(1) match {
        case Success(res) => res shouldEqual 1
        case Failure(t)   => fail(t)
      }
    }

    "update an answer" in new Helper {
      createQualificationAnswer()

      manager.all(1) match {
        case Success(res) => res shouldEqual List(answer)
        case Failure(t)   => fail(t)
      }

      val newAnswer = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2017-05-26 17:25:38",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "edited",
        lastName = "edited",
        comment = "some comment",
        customData = "some custom data"
      )

      manager.update(1, newAnswer) match {
        case Success(res) => res shouldEqual 1
        case Failure(t)   => fail(t)
      }
      manager.all(1) match {
        case Success(res) => res shouldEqual List(newAnswer)
        case Failure(t)   => fail(t)
      }
    }
  }

}
