package xc.service

import anorm._
import docker.DockerPlayWithDbSpec
import model.RecordingModeType
import xc.model.Queue
import xivo.model.QueueFeature
import xivo.service.QueueFeatureManager

import scala.util.{Failure, Success}

class QueueSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection("xc") { implicit c =>
      SQL("DELETE FROM xc.queue_members_default").execute()
      SQL("DELETE FROM xc.queues CASCADE").execute()
      super.beforeEach()
    }

  "xc.service.QueueManager" should {
    "create a Queue" in {
      val manager = app.injector.instanceOf(classOf[QueueManager])
      val q       = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)

      manager.create(q) match {
        case Success(created) => created shouldEqual (q)
        case Failure(t)       => fail(t)
      }

    }

    "get a Queue" in {
      val manager = app.injector.instanceOf(classOf[QueueManager])
      val q       = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)
      manager.create(q)
      manager.get(1) match {
        case Success(fetched) => fetched shouldEqual (q)
        case Failure(t)       => fail(t)
      }
    }

    "get a Queue from a xivo QueueFeature if non existent " in {
      val manager = app.injector.instanceOf(classOf[QueueManager])
      val xivoQueueFeatureManager =
        app.injector.instanceOf(classOf[QueueFeatureManager])
      xivoQueueFeatureManager.deleteByName("queue1")

      val xq = QueueFeature(
        None,
        "queue1",
        "My first Queue",
        "3000",
        None,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        "",
        "",
        None,
        None,
        0,
        None,
        None,
        0,
        RecordingModeType.NotRecorded,
        0
      )

      xivoQueueFeatureManager.create(xq) match {
        case Failure(t) =>
          fail("Cannot create xivo UserFeature as prerequisites", t)
        case Success(createdXivo) =>
          val id = createdXivo.id.get
          manager.delete(id)
          manager.get(id) match {
            case Success(fetched) =>
              fetched.id shouldEqual (id)
              fetched.name shouldEqual (createdXivo.name)
              fetched.displayName shouldEqual (createdXivo.displayname)
              fetched.number shouldEqual (createdXivo.number)
            case Failure(t) => fail(t)
          }
      }

    }

    "get all Queues" in {
      val manager = app.injector.instanceOf(classOf[QueueManager])
      val q1      = Queue(1, "queue1", "My First queue", "3000")
      val q2      = Queue(2, "queue2", "My Second queue", "3001")

      manager.delete(1)
      manager.delete(2)
      manager.create(q1)
      manager.create(q2)

      manager.all() match {
        case Success(fetched) => fetched should contain.allOf(q1, q2)
        case Failure(t)       => fail(t)
      }

    }

    "update a Queue" in {
      val manager = app.injector.instanceOf(classOf[QueueManager])
      val q1      = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)
      manager.create(q1)

      val q2 = q1.copy(displayName = "Rename queue")

      manager.update(q2) match {
        case Success(fetched) => fetched shouldEqual (q2)
        case Failure(t)       => fail(t)
      }

      manager.get(q2.id) match {
        case Success(fetched) => fetched shouldEqual (q2)
        case Failure(t)       => fail(t)
      }
    }

    "delete a Queue" in {
      val manager = app.injector.instanceOf(classOf[QueueManager])
      val q       = Queue(1, "queue1", "My First queue", "3000")

      manager.delete(1)
      manager.create(q)

      manager.delete(1) match {
        case Success(fetched) => fetched shouldEqual (q)
        case Failure(t)       => fail(t)
      }
    }
  }
}
