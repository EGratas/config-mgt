package xc.service

import anorm._
import docker.DockerPlayWithDbSpec
import xc.model._

import scala.util.{Failure, Success, Try}

class CallQualificationSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE qualifications").execute()
      SQL("TRUNCATE subqualifications").execute()
      SQL("TRUNCATE queue_qualification").execute()
      SQL("TRUNCATE qualification_answers").execute()
      SQL("ALTER SEQUENCE qualifications_id_seq RESTART WITH 1").execute()
      SQL("ALTER SEQUENCE subqualifications_id_seq RESTART WITH 1").execute()

      super.beforeEach()
    }

  class Helper() {
    val manager: CallQualificationManagerImpl =
      app.injector.instanceOf(classOf[CallQualificationManagerImpl])

    def createQualification(): Try[Option[Long]] = {
      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "qualif1", sq)

      manager.create(qualif)
    }
  }

  "xc.service.CallQualification" should {
    "create a qualification" in new Helper {
      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "qualif1", sq)

      manager.create(qualif) match {
        case Success(res) => res shouldEqual Some(1)
        case Failure(t)   => fail(t)
      }
    }

    "get all qualifications" in new Helper {
      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "qualif1", sq)

      createQualification()

      manager.all() match {
        case Success(fetched) =>
          fetched.head.name shouldEqual qualif.name
          fetched.head.subQualification should contain theSameElementsAs sq
        case Failure(t) => fail(t)
      }
    }

    "get single qualification" in new Helper {
      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "qualif1", sq)

      createQualification()

      manager.all(1) match {
        case Success(fetched) => fetched shouldEqual List(qualif)
        case Failure(t)       => fail(t)
      }
    }

    "delete single qualification" in new Helper {
      createQualification()

      manager.deleteQualification(1) match {
        case Success(fetched) => fetched shouldEqual 1
        case Failure(t)       => fail(t)
      }

      manager.all() match {
        case Success(fetched) => fetched shouldEqual List()
        case Failure(t)       => fail(t)
      }
    }

    "delete single sub qualification" in new Helper {
      createQualification()

      manager.deleteSubQualification(1) match {
        case Success(fetched) => fetched shouldEqual 1
        case Failure(t)       => fail(t)
      }

      manager.all() match {
        case Success(fetched) =>
          fetched shouldEqual List(
            CallQualification(
              Some(1),
              "qualif1",
              List(SubQualification(Some(2), "subqualif2"))
            )
          )
        case Failure(t) => fail(t)
      }
    }

    "update single qualification" in new Helper {
      val sq = List(
        SubQualification(Some(1), "edited"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "edited", sq)

      createQualification()

      manager.update(1, qualif) match {
        case Success(fetched) => fetched shouldEqual 1
        case Failure(t)       => fail(t)
      }

      manager.all(1) match {
        case Success(fetched) =>
          fetched.head.name shouldEqual qualif.name

        case Failure(t) => fail(t)
      }
    }

    "update single subqualification" in new Helper {
      val sq = SubQualification(Some(1), "edited")

      createQualification()

      manager.updateSubQualification(sq) match {
        case Success(fetched) => fetched shouldEqual 1
        case Failure(t)       => fail(t)
      }

      manager.all(1) match {
        case Success(fetched) =>
          fetched.head.subQualification should contain(sq)
        case Failure(t) => fail(t)
      }
    }

    "assign qualification to the queue" in new Helper {
      val simpleQualif = SimpleCallQualification(1, Some("qualif3"))

      manager.assign(1, simpleQualif) match {
        case Success(fetched) => fetched shouldEqual Some(1)
        case Failure(t)       => fail(t)
      }
    }

    "unassign qualification from the queue" in new Helper {
      val simpleQualif = SimpleCallQualification(1, Some("qualif3"))

      manager.assign(1, simpleQualif)

      manager.unassign(1, simpleQualif) match {
        case Success(fetched) => fetched shouldEqual 1
        case Failure(t)       => fail(t)
      }
    }

    "get all qualifications by queue" in new Helper {
      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif       = CallQualification(Some(1), "qualif1", sq)
      val simpleQualif = SimpleCallQualification(1, None)
      val queueId      = 1L

      createQualification()
      manager.assign(queueId, simpleQualif)

      manager.allByQueue(queueId) match {
        case Success(fetched) =>
          fetched.head.name shouldEqual qualif.name
          fetched.head.subQualification should contain theSameElementsAs sq
        case Failure(t) => fail(t)
      }
    }
  }
}
