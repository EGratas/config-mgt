package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import controllers.Secured
import docker.DockerPlaySpec
import org.mockito.Mockito.{stub, verify}
import play.api.http.Status._
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers.{
  call,
  contentAsString,
  defaultAwaitTimeout,
  status,
  writeableOf_AnyContentAsEmpty,
  writeableOf_AnyContentAsJson,
  DELETE,
  GET,
  POST,
  PUT
}
import xivo.service._

import scala.util.{Failure, Success}

class XivoUserPreferencesSpec extends DockerPlaySpec {

  def withMocks(
      test: (UserPreferenceManager, XivoUserPreferences) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer

    val userPreferenceManager = mock[UserPreferenceManager]

    val controller = new XivoUserPreferences(
      userPreferenceManager,
      app.injector.instanceOf[Secured]
    )
    test(userPreferenceManager, controller)
    ()
  }

  "XivoUserLine controller" should {

    "Return notFound if the user is not found" in withMocks {
      (userPreferenceManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val userId = 1
          stub(userPreferenceManager.getAll(userId))
            .toReturn(Failure(AnormException("not found")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAll(userId), rq)

          val expected: Long = 1
          status(res) shouldEqual NOT_FOUND
          verify(userPreferenceManager).getAll(expected)
        }
    }

    "get user's preferences" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId = 1

          stub(userPreferenceManager.getAll(userId)).toReturn(
            Success(
              UserPreferences(
                List(UserPreference(1, "preferred_device", "phone", "String"))
              )
            )
          )

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAll(userId), rq)

          status(res) shouldEqual OK
          verify(userPreferenceManager).getAll(userId)

          contentAsString(
            res
          ) shouldBe "[{\"key\":\"preferred_device\",\"value\":\"phone\",\"value_type\":\"String\"}]"
        }
    }

    "create user's preference" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId         = 1
          val key            = "PREFERRED_DEVICE"
          val userPreference = UserPreference(userId, key, "phone", "String")
          val payload        = UserPreferencePayload("phone", "String")

          stub(userPreferenceManager.create(userPreference))
            .toReturn(Success(1))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(payload))

          val res = call(controller.create(userId, key), rq)

          status(res) shouldEqual OK
          verify(userPreferenceManager).create(userPreference)

          contentAsString(
            res
          ) shouldBe "{\"value\":\"phone\",\"value_type\":\"String\"}"
        }
    }

    "return a 409 error if trying to create preference already existing for the user" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId         = 1
          val key            = "MOBILE_APP_INFO"
          val userPreference = UserPreference(userId, key, "phone", "String")
          val payload        = UserPreferencePayload("phone", "String")

          stub(userPreferenceManager.create(userPreference))
            .toReturn(Failure(PreferenceAlreadyExistException("Some error")))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(payload))

          val res = call(controller.create(userId, key), rq)

          status(res) shouldEqual CONFLICT
          verify(userPreferenceManager).create(userPreference)
        }
    }

    "update user's preferences" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId         = 1
          val key            = "preferred_device"
          val userPreference = UserPreference(userId, key, "webrtc", "String")
          val payload        = UserPreferencePayload("webrtc", "String")

          stub(userPreferenceManager.update(userPreference))
            .toReturn(Success(1))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(payload))

          val res = call(controller.update(userId, key), rq)

          status(res) shouldEqual OK
          verify(userPreferenceManager).update(userPreference)

          contentAsString(
            res
          ) shouldBe "{\"value\":\"webrtc\",\"value_type\":\"String\"}"
        }
    }

    "return 404 on update non existing user's preference" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId         = 1
          val key            = "preferred_device"
          val userPreference = UserPreference(userId, key, "webrtc", "String")
          val payload        = UserPreferencePayload("webrtc", "String")

          stub(userPreferenceManager.update(userPreference))
            .toReturn(Success(0))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(payload))

          val res = call(controller.update(userId, key), rq)

          status(res) shouldEqual NOT_FOUND
          verify(userPreferenceManager).update(userPreference)
        }
    }

    "get one user preference" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId = 1
          val key    = "preferred_device"

          stub(userPreferenceManager.get(userId, key)).toReturn(
            Success(UserPreference(1, "preferred_device", "phone", "String"))
          )

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.get(userId, key), rq)

          status(res) shouldEqual OK
          verify(userPreferenceManager).get(userId, key)

          contentAsString(
            res
          ) shouldBe "{\"value\":\"phone\",\"value_type\":\"String\"}"
        }
    }

    "delete user preference" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId = 1
          val key    = "preferred_device"

          stub(userPreferenceManager.delete(userId, key)).toReturn(Success(1))

          val rq = FakeRequest(DELETE, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.delete(userId, key), rq)

          status(res) shouldEqual NO_CONTENT
          verify(userPreferenceManager).delete(userId, key)
        }
    }

    "return 404 on deletion of non existing user's preference" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId = 1
          val key    = "preferred_device"

          stub(userPreferenceManager.delete(userId, key)).toReturn(Success(0))

          val rq = FakeRequest(DELETE, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")

          val res = call(controller.delete(userId, key), rq)

          status(res) shouldEqual NOT_FOUND
          verify(userPreferenceManager).delete(userId, key)
        }
    }

    "return 400 on when creating an unexpected key" in withMocks {
      (userPreferenceManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val userId  = 1
          val key     = "WRONG_KEY"
          val payload = UserPreferencePayload("phone", "String")

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(payload))

          val res = call(controller.create(userId, key), rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }
  }
}
