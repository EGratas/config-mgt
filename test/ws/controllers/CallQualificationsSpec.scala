package ws.controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import org.mockito.Mockito._
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import xc.model.{CallQualification, SimpleCallQualification, SubQualification}
import xc.service.CallQualificationManager

import scala.util.{Failure, Success}

class CallQualificationsSpec extends DockerPlaySpec {

  def withMocks(
      test: (CallQualificationManager, CallQualifications, WSClient) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer

    val callQualificationManager = mock[CallQualificationManager]
    val wsClient                 = mock[WSClient]
    val configuration            = mock[Configuration]

    val controller = new CallQualifications(
      callQualificationManager,
      wsClient,
      configuration,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(callQualificationManager, controller, wsClient)
    ()
  }

  "CallQualifications" should {
    "convert to json" in {
      val sq = List(SubQualification(Some(1), "subqualif1"))
      val q  = CallQualification(Some(1), "qualif1", sq)

      Json.toJson(q) shouldEqual Json.obj(
        "id"                -> q.id,
        "name"              -> q.name,
        "subQualifications" -> sq
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse(
        """{"id": 1, "name": "qualif1",
          | "subQualifications": [{"id": 1, "name": "subqualif1"}, {"id": 2, "name": "subqualif2"}]}""".stripMargin
      )

      val sq = List(
        SubQualification(Some(1), "subqualif1"),
        SubQualification(Some(2), "subqualif2")
      )
      val qualif = CallQualification(Some(1), "qualif1", sq)

      json.validate[CallQualification] match {
        case JsSuccess(q, _) => q shouldEqual qualif
        case JsError(_)      => fail()
      }
    }

    "get list of all qualifications" in withMocks {
      (callQualificationManager, controller, _) =>
        {
          import xc.model.CallQualification._
          implicit val materializer: Materializer = app.materializer

          val sq = List(SubQualification(Some(1), "subqualif1"))

          val callQualificationList = List(
            CallQualification(Some(1), "qualif1", sq),
            CallQualification(Some(2), "qualif2", sq),
            CallQualification(Some(3), "qualif3", sq)
          )

          stub(callQualificationManager.all())
            .toReturn(Success(callQualificationList))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAll, rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).all()

          contentAsJson(res) shouldEqual Json.toJson(callQualificationList)
        }
    }

    "get list of all qualifications by id" in withMocks {
      (callQualificationManager, controller, _) =>
        {
          import xc.model.CallQualification._

          implicit val materializer: Materializer = app.materializer

          val qualificationId: Long = 1

          val sq = List(SubQualification(Some(1), "subqualif1"))

          val callQualificationList = List(
            CallQualification(Some(1), "qualif1", sq),
            CallQualification(Some(2), "qualif2", sq),
            CallQualification(Some(3), "qualif3", sq)
          )

          stub(callQualificationManager.all(qualificationId))
            .toReturn(Success(callQualificationList))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.get(qualificationId), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).all(qualificationId)

          contentAsJson(res) shouldEqual Json.toJson(callQualificationList)
        }
    }

    "get list of all qualifications by queue id" in withMocks {
      (callQualificationManager, controller, _) =>
        {
          import xc.model.CallQualification._

          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1

          val sq = List(SubQualification(Some(1), "subqualif1"))

          val callQualificationList = List(
            CallQualification(Some(1), "qualif1", sq),
            CallQualification(Some(2), "qualif2", sq),
            CallQualification(Some(3), "qualif3", sq)
          )

          stub(callQualificationManager.allByQueue(queueId))
            .toReturn(Success(callQualificationList))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getByQueue(queueId), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).allByQueue(queueId)

          contentAsJson(res) shouldEqual Json.toJson(callQualificationList)
        }
    }

    "create qualification" in withMocks {
      (callQualificationManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val json: JsValue = Json.parse(
            """{"id": 1, "name": "qualif1",
            | "subQualifications": [{"id": 1, "name": "subqualif1"}, {"id": 2, "name": "subqualif2"}]}""".stripMargin
          )

          val sq = List(
            SubQualification(Some(1), "subqualif1"),
            SubQualification(Some(2), "subqualif2")
          )
          val q = CallQualification(Some(1), "qualif1", sq)

          stub(callQualificationManager.create(q))
            .toReturn(Success(Some(1.toLong)))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.create, rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).create(q)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "update qualification" in withMocks {
      (callQualificationManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val json: JsValue = Json.parse(
            """{"id": 1, "name": "qualif1",
            | "subQualifications": [{"id": 1, "name": "subqualif1"}, {"id": 2, "name": "subqualif2"}]}""".stripMargin
          )

          val sq = List(
            SubQualification(Some(1), "subqualif1"),
            SubQualification(Some(2), "subqualif2")
          )
          val q = CallQualification(Some(1), "qualif1", sq)

          stub(callQualificationManager.update(1, q)).toReturn(Success(1))
          stub(
            callQualificationManager.updateSubQualification(
              org.mockito.Matchers.any[SubQualification]
            )
          ).toReturn(Success(1))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.update(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).update(1, q)
          verify(callQualificationManager).updateSubQualification(sq.head)
          verify(callQualificationManager).updateSubQualification(sq(1))

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "delete single qualification" in withMocks {
      (callQualificationManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          stub(callQualificationManager.deleteQualification(1))
            .toReturn(Success(1))

          val rq = FakeRequest(DELETE, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.deleteQualification(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).deleteQualification(1)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "delete single sub qualification" in withMocks {
      (callQualificationManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          stub(callQualificationManager.deleteSubQualification(1))
            .toReturn(Success(1))

          val rq = FakeRequest(DELETE, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.deleteSubQualification(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).deleteSubQualification(1)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "assign qualification" in withMocks {
      (callQualificationManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val json: JsValue = Json.parse("""{"id": 1}""".stripMargin)

          val q = SimpleCallQualification(1, None)

          stub(callQualificationManager.assign(1.toLong, q))
            .toReturn(Success(Some(1.toLong)))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.assign(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).assign(1, q)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "unassign qualification" in withMocks {
      (callQualificationManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val json: JsValue = Json.parse("""{"id": 1}""".stripMargin)

          val q = SimpleCallQualification(1, None)

          stub(callQualificationManager.unassign(1, q)).toReturn(Success(1))

          val rq = FakeRequest(DELETE, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.unassign(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationManager).unassign(1, q)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "return InternalServerError if failed to get list of all qualifications" in withMocks {
      (callQualificationManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          stub(callQualificationManager.all())
            .toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAll, rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(callQualificationManager).all()
        }
    }

    "return InternalServerError if failed to get list of all qualifications per queue" in withMocks {
      (callQualificationManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1

          stub(callQualificationManager.all(queueId))
            .toReturn(Failure(new NoSuchElementException("failed")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.get(queueId), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(callQualificationManager).all(queueId)
        }
    }

    "return Forbidden if failed if not authenticated user" in withMocks {
      (callQualificationManager, controller, wsClient) =>
        {
          implicit val materializer: Materializer = app.materializer
          val wsRequest: WSRequest                = mock[WSRequest]

          stub(wsClient.url(org.mockito.Matchers.any[String]))
            .toReturn(wsRequest)

          val rq  = FakeRequest(GET, "/")
          val res = call(controller.getAll, rq)

          status(res) shouldEqual FORBIDDEN
          verifyZeroInteractions(callQualificationManager)
        }
    }
  }
}
