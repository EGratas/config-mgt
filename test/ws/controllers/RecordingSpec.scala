package ws.controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import model._
import org.mockito.Matchers._
import org.mockito.Mockito._
import play.api.Configuration
import play.api.http.Status
import play.api.libs.json.Json
import play.api.libs.ws.{DefaultWSCookie, WSClient, WSRequest, WSResponse}
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import xivo.service.RecordingManager

import scala.concurrent.Future
import scala.util.{Failure, Success}

class RecordingSpec extends DockerPlaySpec {

  def withMocks(test: (RecordingManager, Recording, WSClient) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer
    val recordingManager                    = mock[RecordingManager]
    val wsClient                            = mock[WSClient]
    val configuration                       = mock[Configuration]
    val secured                             = app.injector.instanceOf[controllers.Secured]

    val controller =
      new Recording(recordingManager, wsClient, configuration, secured)
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(recordingManager, controller, wsClient)
    ()
  }

  "Recording" should {

    "get recording mode to apply for a queueId" in withMocks {
      (recordingManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1
          val recordingMode = RecordingMode(RecordingModeType.Recorded)

          stub(recordingManager.computeRecordingModeToApply(queueId))
            .toReturn(Success(recordingMode))

          val rq  = FakeRequest(GET, "/")
          val res = call(controller.getRecordingModeToApply(queueId), rq)

          val expected_queueId: Long = 1

          status(res) shouldEqual OK
          verify(recordingManager).computeRecordingModeToApply(expected_queueId)

          contentAsJson(res) shouldEqual Json.obj("mode" -> "recorded")
        }
    }

    "return Not Found if tried to get recording mode to apply for a non-existent queueId" in withMocks {
      (recordingManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1
          stub(recordingManager.computeRecordingModeToApply(queueId))
            .toReturn(Failure(new NoSuchElementException(s"failed")))

          val rq  = FakeRequest(GET, "/")
          val res = call(controller.getRecordingModeToApply(queueId), rq)

          val expected_queueId: Long = 1

          status(res) shouldEqual NOT_FOUND
          verify(recordingManager).computeRecordingModeToApply(expected_queueId)
        }
    }

    "return Internal Server Error if failed to get recording mode to apply for a queueId" in withMocks {
      (recordingManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1
          stub(recordingManager.computeRecordingModeToApply(queueId))
            .toReturn(Failure(new Exception("failed")))

          val rq  = FakeRequest(GET, "/")
          val res = call(controller.getRecordingModeToApply(queueId), rq)

          val expected_queueId: Long = 1

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(recordingManager).computeRecordingModeToApply(expected_queueId)
        }
    }

    "return authentication error when asking RecordingConfig for a queueId without cookie" in withMocks {
      (_, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val rq  = FakeRequest(GET, "/")
          val res = call(controller.getRecordingConfig(1), rq)

          status(res) shouldEqual FORBIDDEN
        }
    }

    "return RecordingConfig for a queueId" in withMocks {
      (recordingManager, controller, wsClient) =>
        {
          implicit val materializer: Materializer = app.materializer

          val recordingConfig =
            RecordingConfig(activated = true, RecordingModeType.Recorded)

          val queueId: Long = 1
          stub(recordingManager.getRecordingConfig(queueId))
            .toReturn(Success(recordingConfig))

          val wsRequest: WSRequest   = mock[WSRequest]
          val wsResponse: WSResponse = mock[WSResponse]

          stub(wsResponse.status).toReturn(Status.OK)
          stub(wsRequest.addCookies(any[DefaultWSCookie])).toReturn(wsRequest)
          stub(wsRequest.get()).toReturn(Future.successful(wsResponse))
          stub(wsClient.url(anyString())).toReturn(wsRequest)

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getRecordingConfig(queueId), rq)

          val expected_queueId: Long = 1

          status(res) shouldEqual OK
          verify(recordingManager).getRecordingConfig(expected_queueId)
        }
    }

    "update RecordingConfig for a queueId" in withMocks {
      (recordingManager, controller, wsClient) =>
        {
          implicit val materializer: Materializer = app.materializer

          val recordingConfig = RecordingConfig(
            activated = true,
            RecordingModeType.RecordedOnDemand
          )

          val queueId: Long = 1
          stub(recordingManager.setRecordingConfig(queueId, recordingConfig))
            .toReturn(Success(recordingConfig))

          val wsRequest: WSRequest   = mock[WSRequest]
          val wsResponse: WSResponse = mock[WSResponse]

          stub(wsResponse.status).toReturn(Status.OK)
          stub(wsRequest.addCookies(any[DefaultWSCookie])).toReturn(wsRequest)
          stub(wsRequest.get()).toReturn(Future.successful(wsResponse))
          stub(wsClient.url(anyString())).toReturn(wsRequest)

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(recordingConfig))
          val res = call(controller.setRecordingConfig(queueId), rq)

          val expected_queueId: Long = 1

          status(res) shouldEqual OK
          verify(recordingManager).setRecordingConfig(
            expected_queueId,
            recordingConfig
          )
        }
    }

    "return RecordingStatus for all queueId" in withMocks {
      (recordingManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val recordingModeQ1 = RecordingModeType.Recorded
          val recordingModeQ2 = RecordingModeType.RecordedOnDemand
          val recordingModeQ3 = RecordingModeType.Recorded

          val recordingStatus = RecordingQueuesStatus(
            all = true,
            Map(
              Some(1L) -> recordingModeQ1,
              Some(2L) -> recordingModeQ2,
              Some(3L) -> recordingModeQ3
            )
          )

          stub(recordingManager.getRecordingQueuesStatus)
            .toReturn(Success(recordingStatus))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getRecordingStatus(), rq)

          status(res) shouldEqual OK
          verify(recordingManager).getRecordingQueuesStatus

          contentAsJson(res) shouldEqual Json.obj(
            "all" -> true,
            "queues" -> Json.obj(
              "1" -> "recorded",
              "2" -> "recordedondemand",
              "3" -> "recorded"
            )
          )
        }
    }

    "set RecordingStatus with defined state for all queueId" in withMocks {
      (recordingManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val recordingModeQ1 = RecordingModeType.Recorded
          val recordingModeQ2 = RecordingModeType.RecordedOnDemand

          val recordingStatus = RecordingQueuesStatusPublish(
            all = true,
            Map(Some(1L) -> recordingModeQ1, Some(2L) -> recordingModeQ2)
          )

          stub(recordingManager.setRecordingQueuesStatus(activate = true))
            .toReturn(Success(recordingStatus))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.setRecordingStatus("on"), rq)

          status(res) shouldEqual OK
          verify(recordingManager).setRecordingQueuesStatus(true)

          contentAsJson(res) shouldEqual Json.obj(
            "all"    -> true,
            "queues" -> Json.obj("1" -> "recorded", "2" -> "recordedondemand")
          )
        }
    }
  }
}
