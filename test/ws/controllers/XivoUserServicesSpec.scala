package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import controllers.Secured
import docker.DockerPlaySpec
import org.mockito.Mockito._
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import ws.model.{PartialUserServices, UserForward, UserServices}
import xivo.service.UserServicesManager

import scala.util.{Failure, Success}

class XivoUserServicesSpec extends DockerPlaySpec {

  def withMocks(test: (UserServicesManager, XivoUserServices) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer

    val servicesManager = mock[UserServicesManager]

    val controller =
      new XivoUserServices(servicesManager, app.injector.instanceOf[Secured])
    test(servicesManager, controller)
    ()
  }

  "XivoUserServices controller" should {

    "answer 404 when getting user services for non existing user" in withMocks {
      (manager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer
          val userId                              = 1
          stub(manager.getServices(userId))
            .toReturn(Failure(new AnormException("not found")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.get(userId), rq)

          val expected: Long = 1
          status(res) shouldEqual NOT_FOUND
        }
    }

    "get user services" in withMocks { (manager, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val userId                              = 1
        val services = UserServices(
          true,
          UserForward(false, "1234"),
          UserForward(true, "5678"),
          UserForward(false, "7890")
        )

        stub(manager.getServices(userId)).toReturn(Success(services))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.get(userId), rq)

        val expected: Long = 1
        status(res) shouldEqual OK
        contentAsJson(res).as[UserServices] shouldEqual services
      }
    }

    "update all user services" in withMocks { (manager, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val userId                              = 1

        val allServices = PartialUserServices(
          Some(true),
          Some(UserForward(true, "01234")),
          Some(UserForward(false, "15678")),
          Some(UserForward(true, "27890"))
        )

        stub(manager.updatePartialServices(userId, allServices))
          .toReturn(Success(allServices))

        val rq = FakeRequest(PUT, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(Json.toJson(allServices))

        val res = call(controller.update(userId), rq)

        status(res) shouldEqual OK
        contentAsJson(res).as[PartialUserServices] shouldEqual allServices
        verify(manager).updatePartialServices(userId, allServices)
      }
    }

    "partial update user services" in withMocks { (manager, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val userId                              = 1

        val update = PartialUserServices(
          None,
          None,
          Some(UserForward(false, "05678")),
          None
        )

        stub(manager.updatePartialServices(userId, update))
          .toReturn(Success(update))

        val rq = FakeRequest(PUT, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(Json.toJson(update))

        val res = call(controller.update(userId), rq)

        status(res) shouldEqual OK
        contentAsJson(res).as[PartialUserServices] shouldEqual update
        verify(manager).updatePartialServices(userId, update)
      }
    }

    "update fails if no json" in withMocks { (manager, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val userId                              = 1

        val rq = FakeRequest(PUT, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.update(userId), rq)

        val expected: Long = 1
        status(res) shouldEqual BAD_REQUEST
      }
    }
  }

}
