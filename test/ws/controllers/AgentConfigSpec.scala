package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import docker.DockerPlaySpec
import org.mockito.Mockito._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import xivo.model.{Agent, QueueCategory, QueueMember, QueueMemberUserType}
import xivo.service.AgentConfigManager

import scala.util.{Failure, Success}

class AgentConfigSpec extends DockerPlaySpec {

  def withMocks(test: (AgentConfigManager, AgentConfig) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer

    val agentManager  = mock[AgentConfigManager]
    val configuration = mock[Configuration]

    val controller = new AgentConfig(
      agentManager,
      configuration,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(agentManager, controller)
    ()
  }

  "AgentConfig" should {

    "convert to json" in {
      val member = List(
        QueueMember(
          "queue1",
          1L,
          "Agent/1001",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1,
          "Agent",
          QueueCategory.Queue,
          1
        ),
        QueueMember(
          "queue2",
          1L,
          "Agent/1001",
          penalty = 1,
          0,
          QueueMemberUserType.Agent,
          1,
          "Agent",
          QueueCategory.Queue,
          1
        )
      )
      val agent =
        Agent(1L, "Agent", "One", "1001", "default", member, 1L, Some(1L))

      Json.toJson(agent) shouldEqual Json.obj(
        "id"        -> agent.id,
        "firstname" -> agent.firstname,
        "lastname"  -> agent.lastname,
        "number"    -> agent.number,
        "context"   -> agent.context,
        "member"    -> agent.member,
        "numgroup"  -> agent.numgroup,
        "userid"    -> agent.userid
      )
    }

    "return agent config for an agentId" in withMocks {
      (agentManager: AgentConfigManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val member = QueueMember(
            "queue1",
            1L,
            "Agent/1001",
            penalty = 1,
            0,
            QueueMemberUserType.Agent,
            1,
            "Agent",
            QueueCategory.Queue,
            1
          )
          val agent = Agent(
            1L,
            "Agent",
            "One",
            "1001",
            "default",
            List(member),
            1L,
            Some(1L)
          )

          val agentId: Long = 1
          stub(agentManager.getById(agentId)).toReturn(Success(agent))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAgentConfig(agentId), rq)

          status(res) shouldEqual OK
          verify(agentManager).getById(1)
          contentAsJson(res) shouldEqual Json.toJson(agent)
        }
    }

    "return agent config for all agents" in withMocks {
      (agentManager: AgentConfigManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val member = QueueMember(
            "queue1",
            1L,
            "Agent/1001",
            penalty = 1,
            0,
            QueueMemberUserType.Agent,
            1,
            "Agent",
            QueueCategory.Queue,
            1
          )
          val a1 = Agent(
            1L,
            "Agent",
            "One",
            "1001",
            "default",
            List(member),
            1L,
            Some(1L)
          )
          val a2 = Agent(
            2L,
            "Agent",
            "Two",
            "1002",
            "default",
            List(member),
            2L,
            Some(2L)
          )

          val agentConfigList = List(a1, a2)

          stub(agentManager.all()).toReturn(Success(agentConfigList))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAgentConfigAll, rq)

          status(res) shouldEqual OK
          verify(agentManager).all()
        }
    }

    "handle exception NoSuchElementException" in withMocks {
      (agentManager: AgentConfigManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val agentId: Long = 1
          stub(agentManager.getById(agentId))
            .toReturn(Failure(AnormException("not found")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAgentConfig(agentId), rq)

          val expected: Long = 1

          status(res) shouldEqual NOT_FOUND
          verify(agentManager).getById(expected)
        }
    }

    "handle exception generic" in withMocks {
      (agentManager: AgentConfigManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1
          stub(agentManager.getById(queueId))
            .toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAgentConfig(queueId), rq)

          val expected: Long = 1

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(agentManager).getById(expected)
        }
    }
  }
}
