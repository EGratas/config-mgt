package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import docker.DockerPlaySpec
import model.RecordingModeType
import org.mockito.Mockito._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import xivo.model.QueueFeature
import xivo.service.QueueFeatureManager

import scala.util.{Failure, Success}

class QueueSpec extends DockerPlaySpec {

  def withMocks(test: (QueueFeatureManager, Queue) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer
    val queueFeatureManager                 = mock[QueueFeatureManager]
    val configuration                       = mock[Configuration]

    val controller = new Queue(
      queueFeatureManager,
      configuration,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(queueFeatureManager, controller)
    ()
  }

  "Queue" should {
    "convert to json" in {
      val queueFeature = QueueFeature(
        Some(1L),
        "queue",
        "queue",
        "1010",
        Some("context"),
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        "url",
        "announce",
        Some(1),
        Some("fs"),
        1,
        Some(1),
        Some(1),
        1,
        RecordingModeType.Recorded,
        1
      )

      Json.toJson(queueFeature) shouldEqual Json.obj(
        "id"                    -> 1L,
        "name"                  -> queueFeature.name,
        "displayName"           -> queueFeature.displayname,
        "number"                -> queueFeature.number,
        "context"               -> queueFeature.context,
        "data_quality"          -> queueFeature.data_quality,
        "hitting_callee"        -> queueFeature.hitting_callee,
        "hitting_caller"        -> queueFeature.hitting_caller,
        "retries"               -> queueFeature.retries,
        "ring"                  -> queueFeature.ring,
        "transfer_user"         -> queueFeature.transfer_user,
        "transfer_call"         -> queueFeature.transfer_call,
        "write_caller"          -> queueFeature.write_caller,
        "write_calling"         -> queueFeature.write_calling,
        "url"                   -> queueFeature.url,
        "announceoverride"      -> queueFeature.announceoverride,
        "timeout"               -> queueFeature.timeout,
        "preprocess_subroutine" -> queueFeature.preprocess_subroutine,
        "announce_holdtime"     -> queueFeature.announce_holdtime,
        "waittime"              -> queueFeature.waittime,
        "waitratio"             -> queueFeature.waitratio,
        "ignore_forward"        -> queueFeature.ignore_forward,
        "recording_mode"        -> "recorded",
        "recording_activated"   -> 1
      )
    }

    "return queue config for a queueId" in withMocks {
      (queueFeatureManager: QueueFeatureManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueFeature = QueueFeature(
            Some(1L),
            "queue",
            "queue",
            "1010",
            Some("context"),
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            "url",
            "announce",
            Some(1),
            Some("fs"),
            1,
            Some(1),
            Some(1),
            1,
            RecordingModeType.Recorded,
            1
          )

          val queueId: Long = 1
          stub(queueFeatureManager.get(queueId)).toReturn(Success(queueFeature))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueConfig(queueId), rq)

          val expected: Long = 1

          status(res) shouldEqual OK
          verify(queueFeatureManager).get(expected)

          contentAsJson(res) shouldEqual Json.toJson(queueFeature)
        }
    }

    "return queue config for all queues" in withMocks {
      (queueFeatureManager: QueueFeatureManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val qf1 = QueueFeature(
            Some(1L),
            "queue1",
            "queue1",
            "111",
            Some("context"),
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            "url",
            "announce",
            Some(1),
            Some("fs"),
            1,
            Some(1),
            Some(1),
            1,
            RecordingModeType.Recorded,
            1
          )
          val qf2 = QueueFeature(
            Some(2L),
            "queue1",
            "queue1",
            "333",
            Some("context"),
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            "url",
            "announce",
            Some(1),
            Some("fs"),
            1,
            Some(1),
            Some(1),
            1,
            RecordingModeType.Recorded,
            1
          )

          val queueFeatureList = List(qf1, qf2)

          stub(queueFeatureManager.all()).toReturn(Success(queueFeatureList))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueConfigAll, rq)

          status(res) shouldEqual OK
          verify(queueFeatureManager).all()

          contentAsJson(res) shouldEqual Json.toJson(queueFeatureList)
        }
    }

    "handle exception NoSuchElementException" in withMocks {
      (queueFeatureManager: QueueFeatureManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1
          stub(queueFeatureManager.get(queueId))
            .toReturn(Failure(AnormException("not found")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueConfig(queueId), rq)

          val expected: Long = 1

          status(res) shouldEqual NOT_FOUND
          verify(queueFeatureManager).get(expected)
        }
    }

    "handle exception generic" in withMocks {
      (queueFeatureManager: QueueFeatureManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1
          stub(queueFeatureManager.get(queueId))
            .toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueConfig(queueId), rq)

          val expected: Long = 1

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(queueFeatureManager).get(expected)
        }
    }
  }
}
