package ws.controllers

import akka.stream.Materializer
import configuration.AuthConfig
import docker.DockerPlaySpec
import org.mockito.Matchers.{any, anyString}
import org.mockito.Mockito.{stub, verify, when}
import play.api.Configuration
import play.api.http.Status
import play.api.libs.json.Json
import play.api.libs.ws.{DefaultWSCookie, WSClient, WSRequest, WSResponse}
import play.api.mvc.{ControllerComponents, Cookie, DefaultActionBuilder}
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.model._
import xivo.service.{MediaServerManager, XivoCookieResponse, XivoWebService}

import java.sql.SQLException
import scala.concurrent.Future
import scala.util.{Failure, Success}

class MediaServerSpec extends DockerPlaySpec {

  class Helper() {
    implicit lazy val materializer: Materializer = app.materializer
    val cc: ControllerComponents                 = app.injector.instanceOf[ControllerComponents]
    val mediaServerManager: MediaServerManager   = mock[MediaServerManager]
    val configuration: Configuration             = mock[Configuration]
    val authConfig                               = app.injector.instanceOf[AuthConfig]
    val action                                   = app.injector.instanceOf[DefaultActionBuilder]
    val xivows                                   = mock[XivoWebService]
    val secured                                  = new controllers.Secured(authConfig, xivows, action)

    val wSClient: WSClient     = mock[WSClient]
    val wsRequest: WSRequest   = mock[WSRequest]
    val wsResponse: WSResponse = mock[WSResponse]

    stub(wsResponse.status).toReturn(Status.OK)
    stub(wsRequest.addCookies(any[DefaultWSCookie])).toReturn(wsRequest)
    stub(wsRequest.get()).toReturn(Future.successful(wsResponse))
    stub(wSClient.url(anyString())).toReturn(wsRequest)
    stub(xivows.validateCookieAgainstXiVO(Cookie("_eid", "xxxx")))
      .toReturn(Future.successful(XivoCookieResponse))

    def getController(): MediaServer =
      new MediaServer(mediaServerManager, cc, wSClient, configuration, secured)
  }

  "MediaServer" should {

    "return '400 Bad Request' if no json send when adding a media server" in new Helper() {
      val controller = getController()
      val rq         = FakeRequest(POST, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.createMediaServer, rq)
      status(res) shouldEqual BAD_REQUEST
    }

    "return Bad_Request when adding a media server with bad Json" in new Helper() {
      val controller = getController()
      val value      = Json.parse("""{"id": 8}""")
      val rq = FakeRequest(POST, "/")
        .withJsonBody(value)
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.createMediaServer, rq)

      status(res) shouldEqual BAD_REQUEST

    }

    "return Bad_Request when adding a media server with special characters in its name" in new Helper() {
      val controller = getController()
      val json       = Json.parse("""
                              |{
                              | "name": "mds_1",
                              | "display_name": "media server",
                              | "voip_ip": "10.52.0.10"
                              |}
        """.stripMargin)

      val rq = FakeRequest(POST, "/")
        .withJsonBody(json)
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.createMediaServer, rq)

      status(res) shouldEqual BAD_REQUEST

    }

    "return OK when adding a media server with correct JSON" in new Helper() {
      val json = Json.parse("""
          |{
          | "name": "mds1",
          | "display_name": "media server",
          | "voip_ip": "10.52.0.10"
          |}
        """.stripMargin)

      val jsonValidate =
        MediaServerConfigNoId("mds1", "media server", Some("10.52.0.10"))

      val returnedValue =
        MediaServerConfig(42, "mds1", "media server", Some("10.52.0.10"), false)

      when(mediaServerManager.create(jsonValidate))
        .thenReturn(Success(returnedValue))

      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withJsonBody(json)
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.createMediaServer, rq)

      status(res) shouldEqual OK
      verify(mediaServerManager).create(jsonValidate)
    }

    "return Internal_Server_Error when adding a media server with Failure" in new Helper() {
      val json = Json.parse("""
          |{
          | "name": "mds1",
          | "display_name": "media server",
          | "voip_ip": "10.52.0.10"
          |}
        """.stripMargin)

      val jsonValidate = MediaServerConfigNoId(
        "mds1",
        "media server",
        Some("10.52.0.10")
      )

      when(mediaServerManager.create(jsonValidate))
        .thenReturn(Failure(new Exception("BDD unreachable")))
      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withJsonBody(json)
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.createMediaServer, rq)

      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(mediaServerManager).create(jsonValidate)
    }

    "return Internal_Server_Error when deleting a media server with Failure" in new Helper() {
      val id: Long = 5
      when(mediaServerManager.delete(id))
        .thenReturn(Failure(new Exception("Media Server not exist")))
      val controller = getController()
      val rq         = FakeRequest(DELETE, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.deleteMediaServer(id), rq)

      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(mediaServerManager).delete(id)
    }

    "return OK when deleting a media server with Success" in new Helper() {
      val id: Long = 5
      val msc: MediaServerConfig = new MediaServerConfig(
        5,
        "mds1",
        "media server",
        Some("10.52.0.10"),
        false
      )

      when(mediaServerManager.delete(id)).thenReturn(Success(msc))
      val controller = getController()
      val rq         = FakeRequest(DELETE, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.deleteMediaServer(id), rq)

      status(res) shouldEqual OK
      verify(mediaServerManager).delete(id)
    }

    "return Internal_Server_Error when request a media server with Success" in new Helper() {
      val id: Long = 5
      when(mediaServerManager.get(id))
        .thenReturn(Failure(new Exception("can't get media server")))
      val controller = getController()
      val rq         = FakeRequest(PUT, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getMediaServer(id), rq)

      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(mediaServerManager).get(id)
    }

    "return OK when request a media server with Success" in new Helper() {
      val id: Long = 5
      val msc: MediaServerConfig = new MediaServerConfig(
        5,
        "mds1",
        "media server",
        Some("10.52.0.10"),
        false
      )

      when(mediaServerManager.get(id)).thenReturn(Success(msc))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getMediaServer(id), rq)

      status(res) shouldEqual OK
      verify(mediaServerManager).get(id)
    }

    "return Internal_Server_Error when request all media server with Failure" in new Helper() {
      when(mediaServerManager.all())
        .thenReturn(Failure(new Exception("can't get all media server")))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.allMediaServer(), rq)

      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(mediaServerManager).all()
    }

    "return OK when request all media server with Success" in new Helper() {
      when(mediaServerManager.all()).thenReturn(
        Success(
          MediaServerListConfig(
            List(MediaServerConfig(1, "mds", "mds", None, read_only = true))
          )
        )
      )
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.allMediaServer(), rq)

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.arr(
        Json.obj(
          "id"           -> 1,
          "name"         -> "mds",
          "display_name" -> "mds",
          "read_only"    -> true
        )
      )
      verify(mediaServerManager).all()
    }

    "return Internal_Server_Error when update a media server with Failure" in new Helper() {
      val mds = MediaServerConfig(
        1,
        "mds1",
        "media server",
        Some("10.52.0.10"),
        false
      )

      when(mediaServerManager.update(mds))
        .thenReturn(Failure(new Exception("can't update media server")))
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withJsonBody(Json.toJson(mds))
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.updateMediaServer(), rq)

      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(mediaServerManager).update(mds)
    }

    "return OK when request all media server and their line counts with Success" in new Helper() {
      when(mediaServerManager.linesCount()).thenReturn(
        Success(
          MediaServerListWithLineCount(
            List(
              MediaServerWithLineCount(
                MediaServerConfig(
                  1,
                  "mds1",
                  "mediaserver1",
                  Some("10.51.0.4"),
                  true
                ),
                12
              )
            )
          )
        )
      )
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.mediaServerLinesCount(), rq)

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.arr(
        Json.obj(
          "mediaServer" -> Json.obj(
            "id"           -> 1,
            "name"         -> "mds1",
            "display_name" -> "mediaserver1",
            "voip_ip"      -> "10.51.0.4",
            "read_only"    -> true
          ),
          "line_count" -> 12
        )
      )
      verify(mediaServerManager).linesCount()
    }

    "return Internal_Server_Error when request all media server and their line counts with Failure" in new Helper() {
      when(mediaServerManager.linesCount()).thenReturn(
        Failure(
          new Exception("can't get all media server with their lines count")
        )
      )
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.mediaServerLinesCount(), rq)

      status(res) shouldEqual INTERNAL_SERVER_ERROR
      verify(mediaServerManager).linesCount()
    }

    "return OK when update a media server with Success" in new Helper() {
      val mds = MediaServerConfig(
        1,
        "mds1",
        "media server",
        Some("10.52.0.10"),
        false
      )

      when(mediaServerManager.update(mds)).thenReturn(Success(mds))
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withJsonBody(Json.toJson(mds))
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.updateMediaServer(), rq)

      status(res) shouldEqual OK
      verify(mediaServerManager).update(mds)
    }

    "return '400 Bad Request' if no json send when updating a media server" in new Helper() {
      val controller = getController()
      val rq         = FakeRequest(PUT, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.updateMediaServer(), rq)

      status(res) shouldEqual BAD_REQUEST
    }

    "return Bad_Request when updating a media server with bad Json" in new Helper() {
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withJsonBody(Json.parse("""{"id": 8}"""))
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.updateMediaServer(), rq)

      status(res) shouldEqual BAD_REQUEST
    }

    "return duplicate error when creating media server with same name" in new Helper() {
      val json = Json.parse("""
          |{
          | "name": "mds1",
          | "display_name": "media server",
          | "voip_ip": "10.52.0.10"
          |}
        """.stripMargin)

      val mds =
        MediaServerConfigNoId("mds1", "media server", Some("10.52.0.10"))

      when(mediaServerManager.create(mds)).thenReturn(
        Failure(
          new SQLException("duplicated", "23505")
        )
      )

      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withJsonBody(json)
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.createMediaServer, rq)

      status(res) shouldEqual BAD_REQUEST
      verify(mediaServerManager).create(mds)
      contentAsString(
        res
      ) shouldEqual "{\"error\":\"Duplicate\",\"message\":\"duplicated\"}"
    }
  }

}
