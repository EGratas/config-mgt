package ws.controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import org.mockito.Mockito._
import play.api.Configuration
import play.api.http.Status.BAD_REQUEST
import play.api.libs.json.{JsValue, Json}
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import xc.model.CallQualificationAnswer
import xc.service.CallQualificationAnswerManager

import scala.util.{Failure, Success}

class CallQualificationAnswersSpec extends DockerPlaySpec {

  def withMocks(
      test: (
          CallQualificationAnswerManager,
          CallQualificationAnswers,
          WSClient
      ) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer

    val callQualificationAnswerManager = mock[CallQualificationAnswerManager]
    val wsClient                       = mock[WSClient]
    val configuration                  = mock[Configuration]

    val controller = new CallQualificationAnswers(
      callQualificationAnswerManager,
      wsClient,
      configuration,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(callQualificationAnswerManager, controller, wsClient)
    ()
  }

  "CallQualificationAnswers" should {

    "get qualification answer by queue within time range" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val queueId             = 1L
          val fromRefTime: String = "2016-01-01"
          val toRefTime: String   = "2018-12-12"

          val answer = CallQualificationAnswer(
            sub_qualification_id = 1,
            time = "2017-05-26 17:25:38",
            callid = "callid1",
            agent = 1,
            queue = 1,
            firstName = "first",
            lastName = "last",
            comment = "some comment",
            customData = "{\"opt1\":\"1000\"}"
          )

          val answers = List(
            answer,
            answer.copy(time = "2010-05-26 17:25:38"),
            answer.copy(time = "2018-05-26 17:25:38.197679")
          )

          val answersCsv: String = CallQualificationAnswer.toCsv(answers)

          stub(
            callQualificationAnswerManager.allByQueue(
              queueId,
              fromRefTime,
              toRefTime
            )
          ).toReturn(Success(answers))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res =
            call(controller.exportTickets(queueId, fromRefTime, toRefTime), rq)

          status(res) shouldEqual OK
          verify(callQualificationAnswerManager).allByQueue(
            queueId,
            fromRefTime,
            toRefTime
          )

          contentAsString(res) shouldEqual answersCsv
        }
    }

    "create qualification answer" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val answer = CallQualificationAnswer(
            sub_qualification_id = 1,
            time = "2018-03-21 17:00:00",
            callid = "callid1",
            agent = 1,
            queue = 1,
            firstName = "first",
            lastName = "last",
            comment = "some comment",
            customData = "some custom data"
          )

          val json: JsValue = Json.parse(
            """
            | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
            | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
            | """.stripMargin
          )

          stub(callQualificationAnswerManager.create(answer))
            .toReturn(Success(Some(1.toLong)))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.create, rq)

          status(res) shouldEqual OK
          verify(callQualificationAnswerManager).create(answer)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "update qualification answer" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val answer = CallQualificationAnswer(
            sub_qualification_id = 1,
            time = "2018-03-21 17:00:00",
            callid = "callid1",
            agent = 1,
            queue = 1,
            firstName = "first",
            lastName = "last",
            comment = "some comment",
            customData = "some custom data"
          )

          val json: JsValue = Json.parse(
            """
            | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
            | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
            | """.stripMargin
          )

          stub(callQualificationAnswerManager.update(1, answer))
            .toReturn(Success(1))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.update(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationAnswerManager).update(1, answer)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "delete qualification answer" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          stub(callQualificationAnswerManager.delete(1)).toReturn(Success(1))

          val rq = FakeRequest(POST, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.delete(1), rq)

          status(res) shouldEqual OK
          verify(callQualificationAnswerManager).delete(1)

          contentAsJson(res) shouldEqual Json.toJson(1)
        }
    }

    "return InternalServerError if failed to create qualification answer" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val answer = CallQualificationAnswer(
            sub_qualification_id = 1,
            time = "2018-03-21 17:00:00",
            callid = "callid1",
            agent = 1,
            queue = 1,
            firstName = "first",
            lastName = "last",
            comment = "some comment",
            customData = "some custom data"
          )

          val json: JsValue = Json.parse(
            """
            | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
            | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
            | """.stripMargin
          )

          stub(callQualificationAnswerManager.create(answer))
            .toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.create, rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(callQualificationAnswerManager).create(answer)
        }
    }

    "return BadRequest if failed to create qualification answer with wrong json payload" in withMocks {
      (callQualificationAnswerManager, controller, wsClient) =>
        {

          implicit val materializer: Materializer = app.materializer
          val wsRequest: WSRequest                = mock[WSRequest]

          val json: JsValue = Json.parse(
            """
            | {"error": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
            | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
            | """.stripMargin
          )

          stub(wsClient.url(org.mockito.Matchers.any[String]))
            .toReturn(wsRequest)

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.create, rq)

          status(res) shouldEqual BAD_REQUEST
          verifyZeroInteractions(callQualificationAnswerManager)
        }
    }

    "return BadRequest if failed to create qualification answer with missing json payload" in withMocks {
      (callQualificationAnswerManager, controller, wsClient) =>
        {

          implicit val materializer: Materializer = app.materializer
          val wsRequest: WSRequest                = mock[WSRequest]

          stub(wsClient.url(org.mockito.Matchers.any[String]))
            .toReturn(wsRequest)

          val rq = FakeRequest(POST, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.create, rq)

          status(res) shouldEqual BAD_REQUEST
          verifyZeroInteractions(callQualificationAnswerManager)
        }
    }

    "return InternalServerError if failed to update qualification answer" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val answer = CallQualificationAnswer(
            sub_qualification_id = 1,
            time = "2018-03-21 17:00:00",
            callid = "callid1",
            agent = 1,
            queue = 1,
            firstName = "first",
            lastName = "last",
            comment = "some comment",
            customData = "some custom data"
          )

          val json: JsValue = Json.parse(
            """
            | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
            | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "some custom data"}
            | """.stripMargin
          )

          stub(callQualificationAnswerManager.update(1, answer))
            .toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.update(1), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(callQualificationAnswerManager).update(1, answer)
        }
    }

    "return InternalServerError if failed to delete qualification answer" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          stub(callQualificationAnswerManager.delete(1))
            .toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(POST, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.delete(1), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(callQualificationAnswerManager).delete(1)
        }
    }

    "fail if csv request has incorrect date format" in withMocks {
      (callQualificationAnswerManager, controller, _) =>
        {

          implicit val materializer: Materializer = app.materializer

          val queueId             = 1L
          val fromRefTime: String = "2016/01/01"
          val toRefTime: String   = "2018-12-12"

          stub(
            callQualificationAnswerManager.allByQueue(
              queueId,
              fromRefTime,
              toRefTime
            )
          ).toReturn(Failure(new Exception("failed")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res =
            call(controller.exportTickets(queueId, fromRefTime, toRefTime), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(callQualificationAnswerManager).allByQueue(
            queueId,
            fromRefTime,
            toRefTime
          )
        }
    }
  }
}
