package ws.controllers.meetingrooms

import akka.stream.Materializer
import anorm.AnormException
import configuration.AuthConfig
import docker.DockerPlaySpec
import model.{DynamicFilter, OperatorLike}
import org.mockito.Matchers.{any, anyString}
import org.mockito.Mockito.{verify, verifyZeroInteractions, when}
import org.postgresql.util.PSQLException
import org.postgresql.util.PSQLState.UNIQUE_VIOLATION
import play.api.http.Status
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.{DefaultWSCookie, WSClient, WSRequest, WSResponse}
import play.api.mvc.{Cookie, DefaultActionBuilder, Result}
import play.api.test.Helpers.{GET, route, _}
import play.api.test.{FakeRequest, Helpers}
import xivo.model.{
  FindMeetingRoom,
  FindMeetingRoomResult,
  MeetingRoomConfig,
  RoomTypes
}
import xivo.service.MeetingRoomManager.createGetFilter
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import xivo.service.{
  MeetingRoomAuthentication,
  MeetingRoomManager,
  XivoCookieResponse,
  XivoWebService
}

import java.sql.Timestamp
import java.time.Instant
import java.util.UUID
import scala.concurrent.Future
import scala.util.{Failure, Success}

class StaticMeetingRoomSpec extends DockerPlaySpec {

  class Helper() {

    implicit lazy val materializer: Materializer = app.materializer
    val meetingRoomMananger: MeetingRoomManager  = mock[MeetingRoomManager]
    val rabbitPublisher                          = mock[XivoRabbitEventsPublisher]
    val authConfig                               = app.injector.instanceOf[AuthConfig]
    val action                                   = app.injector.instanceOf[DefaultActionBuilder]
    val xivows                                   = mock[XivoWebService]
    val meetingRoomAuth                          = app.injector.instanceOf[MeetingRoomAuthentication]
    val secured                                  = new controllers.Secured(authConfig, xivows, action)
    val meetingRoomCommon = new MeetingRoomCommon(
      meetingRoomMananger,
      secured,
      Helpers.stubControllerComponents()
    )

    val FILTER = MeetingRoomManager.createRoomFilter(RoomTypes.Static)

    val wSClient: WSClient     = mock[WSClient]
    val wsRequest: WSRequest   = mock[WSRequest]
    val wsResponse: WSResponse = mock[WSResponse]

    val id: Long = 1
    val ts       = Timestamp.from(Instant.now())
    val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
      Some(id),
      "My Conf Room 1",
      Some("1050"),
      Some("1234"),
      Some(UUID.randomUUID()),
      RoomTypes.Static,
      None,
      Some(ts)
    )
    val meetingRoomNoId: MeetingRoomConfig = new MeetingRoomConfig(
      None,
      "My Conf Room 1",
      Some("1050"),
      Some("1234"),
      None,
      RoomTypes.Static,
      None
    )

    when(wsResponse.status).thenReturn(Status.OK)
    when(wsRequest.addCookies(any[DefaultWSCookie])).thenReturn(wsRequest)
    when(wsRequest.get()).thenReturn(Future.successful(wsResponse))
    when(wSClient.url(anyString())).thenReturn(wsRequest)
    when(xivows.validateCookieAgainstXiVO(Cookie("_eid", "xxxx")))
      .thenReturn(Future.successful(XivoCookieResponse))

    def getController() =
      new StaticMeetingRoom(
        meetingRoomCommon,
        Helpers.stubControllerComponents()
      )
  }

  "StaticMeetingRoom Controller" should {
    "get a static meeting room and return 200" in new Helper() {
      val filter = FILTER ++ createGetFilter("id", id.toString)

      when(meetingRoomMananger.get(filter)).thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.get(id), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(meetingRoom)

      verify(meetingRoomMananger).get(filter)
    }

    "get a non-existent meeting room and return 500" in new Helper() {
      val filter = FILTER ++ createGetFilter("id", id.toString)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Failure(AnormException("not found")))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.get(id), rq)

      status(res) shouldBe NOT_FOUND
      contentAsString(res) should include("not found")

      verify(meetingRoomMananger).get(filter)
    }

    "create a meeting room and return 200" in new Helper() {
      val json = Json.parse("""
                              |{
                              |"displayName":"My Conf Room 1",
                              |"number":"1050",
                              |"userPin":"1234",
                              |"roomType":"static"
                              |}
        """.stripMargin)

      val expected =
        meetingRoomNoId.copy(id = Some(id), uuid = Some(UUID.randomUUID()))

      when(meetingRoomMananger.create(meetingRoomNoId))
        .thenReturn(Success(expected))
      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(json)
      val res = call(controller.create(), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).create(meetingRoomNoId)
    }

    "not create a personal meeting room" in new Helper() {
      val json = Json.parse("""
                              |{
                              |"displayName":"My Conf Room 1",
                              |"number":"1050",
                              |"userPin":"1234",
                              |"roomType":"personal"
                              |}
        """.stripMargin)

      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(json)
      val res = call(controller.create(), rq)

      status(res) shouldBe BAD_REQUEST
      contentAsString(res) should include(
        "Not allowed room type for this endpoint"
      )

      verifyZeroInteractions(meetingRoomMananger)
    }

    "create a meeting room ignoring uuid and return 200" in new Helper() {
      val json = Json.parse("""
                              |{
                              |"displayName":"My Conf Room 1",
                              |"number":"1050",
                              |"userPin":"1234",
                              |"uuid":"123-456",
                              |"roomType":"static"
                              |}
        """.stripMargin)

      val expected =
        meetingRoomNoId.copy(id = Some(id), uuid = Some(UUID.randomUUID()))

      when(meetingRoomMananger.create(meetingRoomNoId))
        .thenReturn(Success(expected))
      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(json)
      val res = call(controller.create(), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).create(meetingRoomNoId)
    }

    "create a meeting room without pin and return 200" in new Helper() {
      val room = meetingRoomNoId.copy(userPin = None)
      val json = Json.parse("""
                              |{
                              |"displayName":"My Conf Room 1",
                              |"number":"1050",
                              |"roomType":"static"
                              |}
        """.stripMargin)

      val expected =
        room.copy(id = Some(id), uuid = Some(UUID.randomUUID()))

      when(meetingRoomMananger.create(room))
        .thenReturn(Success(expected))
      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(json)
      val res = call(controller.create(), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).create(room)
    }

    "create a invalid meeting room and return 400" in new Helper() {
      val json = Json.parse("""
                              |{
                              |"number":"1050",
                              |"userPin":"1234",
                              |"roomType":"static"
                              |}
        """.stripMargin)

      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(json)
      val res = call(controller.create(), rq)

      status(res) shouldBe BAD_REQUEST
      contentAsString(res) should include("Invalid")

      verifyZeroInteractions(meetingRoomMananger)
    }

    "create a duplicate meeting room and return 400" in new Helper() {
      val json = Json.parse("""
                              |{
                              |"displayName":"My Conf Room 1",
                              |"number":"1050",
                              |"userPin":"1234",
                              |"roomType":"static"
                              |}
        """.stripMargin)

      when(meetingRoomMananger.create(meetingRoomNoId)).thenReturn(
        Failure(
          new PSQLException("violates unique constraint", UNIQUE_VIOLATION)
        )
      )
      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(json)
      val res = call(controller.create(), rq)

      status(res) shouldBe BAD_REQUEST
      contentAsString(res) should include("Duplicate")

      verify(meetingRoomMananger).create(meetingRoomNoId)
    }

    "update a meeting room and return 200" in new Helper() {
      val room = meetingRoom.copy(uuid = None, pinTimestamp = None)
      when(meetingRoomMananger.update(room))
        .thenReturn(Success(room))
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(room))
      val res = call(controller.update(), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(room)

      verify(meetingRoomMananger).update(room)
    }

    "update a meeting room without id and return 500" in new Helper() {
      when(meetingRoomMananger.update(meetingRoomNoId))
        .thenReturn(Failure(new Exception("Cannot update a meeting room")))
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(meetingRoomNoId))
      val res = call(controller.update(), rq)

      status(res) shouldBe INTERNAL_SERVER_ERROR
      contentAsString(res) should include("Unable to update meeting room")

      verify(meetingRoomMananger).update(meetingRoomNoId)
    }

    "update a meeting room violating constraints and return 500" in new Helper() {
      when(meetingRoomMananger.update(meetingRoomNoId)).thenReturn(
        Failure(
          new PSQLException("violates unique constraint", UNIQUE_VIOLATION)
        )
      )
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(meetingRoomNoId))
      val res = call(controller.update(), rq)

      status(res) shouldBe BAD_REQUEST
      contentAsString(res) should include("violates unique constraint")

      verify(meetingRoomMananger).update(meetingRoomNoId)
    }

    "update a non-existing meeting room and return 500" in new Helper() {
      when(meetingRoomMananger.update(meetingRoomNoId))
        .thenReturn(Failure(new Exception("unable to update")))
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(meetingRoomNoId))
      val res = call(controller.update(), rq)

      status(res) shouldBe INTERNAL_SERVER_ERROR
      contentAsString(res) should include("unable to update")

      verify(meetingRoomMananger).update(meetingRoomNoId)
    }

    "delete a meeting room and return 200" in new Helper() {
      val filter = FILTER ++ createGetFilter("id", id.toString)

      when(meetingRoomMananger.delete(filter)).thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq         = FakeRequest(DELETE, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.delete(id), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(meetingRoom)

      verify(meetingRoomMananger).delete(filter)
    }

    "delete a non-existing meeting room and return 500" in new Helper() {
      val filter = FILTER ++ createGetFilter("id", id.toString)

      when(meetingRoomMananger.delete(filter))
        .thenReturn(Failure(new Exception("unable to delete")))
      val controller = getController()
      val rq = FakeRequest(PUT, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(meetingRoom))
      val res = call(controller.delete(id), rq)

      status(res) shouldBe INTERNAL_SERVER_ERROR
      contentAsString(res) should include("unable to delete")

      verify(meetingRoomMananger).delete(filter)
    }
  }

  "StaticMeetingRoom Routes" should {
    "get a list of meeting rooms" in new Helper() {
      when(meetingRoomMananger.getAll(FILTER))
        .thenReturn(Success(List(meetingRoom)))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(GET, "/configmgt/api/2.0/meetingrooms/static")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).getAll(FILTER)

      mockedApp.stop()
    }

    "get a meeting room" in new Helper() {
      val filter = FILTER ++ createGetFilter("id", "1")

      when(meetingRoomMananger.get(filter)).thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(GET, "/configmgt/api/2.0/meetingrooms/static/1")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "create a meeting room" in new Helper() {
      when(meetingRoomMananger.create(meetingRoomNoId))
        .thenReturn(
          Success(
            meetingRoomNoId.copy(id = Some(1), uuid = Some(UUID.randomUUID()))
          )
        )

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(POST, "/configmgt/api/2.0/meetingrooms/static")
          .withCookies(Cookie("_eid", "xxxx"))
          .withJsonBody(Json.toJson(meetingRoomNoId))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).create(meetingRoomNoId)

      mockedApp.stop()
    }

    "not create a personal meeting room" in new Helper() {
      val room = meetingRoomNoId.copy(roomType = RoomTypes.Personal)
      when(meetingRoomMananger.create(room))
        .thenReturn(
          Success(
            room.copy(id = Some(1), uuid = Some(UUID.randomUUID()))
          )
        )

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(POST, "/configmgt/api/2.0/meetingrooms/static")
          .withCookies(Cookie("_eid", "xxxx"))
          .withJsonBody(Json.toJson(room))
      )

      status(res) shouldBe BAD_REQUEST
      verifyZeroInteractions(meetingRoomMananger)

      mockedApp.stop()
    }

    "update a meeting room" in new Helper() {
      val room = meetingRoom.copy(uuid = None, pinTimestamp = None)

      when(meetingRoomMananger.update(room))
        .thenReturn(Success(room))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(PUT, "/configmgt/api/2.0/meetingrooms/static")
          .withCookies(Cookie("_eid", "xxxx"))
          .withJsonBody(Json.toJson(room))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).update(room)

      mockedApp.stop()
    }

    "delete a meeting room" in new Helper() {
      val filter = FILTER ++ createGetFilter("id", "1")

      when(meetingRoomMananger.delete(filter)).thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(DELETE, "/configmgt/api/2.0/meetingrooms/static/1")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).delete(filter)

      mockedApp.stop()
    }

    "find a meeting room" in new Helper() {
      val jsonFilters = List(
        DynamicFilter(
          "displayName",
          Some(OperatorLike),
          Some("My Conf"),
          None,
          None
        )
      )

      val allFilters = jsonFilters ++ FILTER

      when(meetingRoomMananger.find(allFilters, 0, 10))
        .thenReturn(Success(FindMeetingRoomResult(1, List(meetingRoom))))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(POST, "/configmgt/api/2.0/meetingrooms/static/find")
          .withCookies(Cookie("_eid", "xxxx"))
          .withJsonBody(Json.toJson(FindMeetingRoom(jsonFilters, 0, 10)))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).find(allFilters, 0, 10)

      mockedApp.stop()
    }
  }
}
