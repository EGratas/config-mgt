package ws.controllers.meetingrooms

import akka.stream.Materializer
import anorm.AnormException
import configuration.AuthConfig
import docker.DockerPlaySpec
import model.{DynamicFilter, OperatorEq, OperatorLike}
import org.mockito.Matchers.{any, anyString}
import org.mockito.Mockito.{verify, verifyZeroInteractions, when}
import play.api.http.Status
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.libs.ws.{DefaultWSCookie, WSClient, WSRequest, WSResponse}
import play.api.mvc.Cookie.SameSite
import play.api.mvc.{Cookie, DefaultActionBuilder, Result}
import play.api.test.Helpers.{GET, route, _}
import play.api.test.{FakeRequest, Helpers}
import xivo.model._
import xivo.service.MeetingRoomManager.createGetFilter
import xivo.service._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import java.sql.Timestamp
import java.time.Instant
import java.util.UUID
import scala.concurrent.Future
import scala.util.{Failure, Success}

class MeetingRoomSpec extends DockerPlaySpec {

  class Helper() {

    val rabbitPublisher      = mock[XivoRabbitEventsPublisher]
    val currentTime: Instant = Instant.now()

    val clockMock = new MeetingRoomInstantTime {
      override def now(): Instant = {
        currentTime
      }
    }

    val uuidGeneratorMock = new MeetingRoomUUIDGenerator {
      override def generateUUID(): UUID = new UUID(0, 0)
    }

    implicit lazy val materializer: Materializer = app.materializer
    val meetingRoomMananger: MeetingRoomManager  = mock[MeetingRoomManager]
    val authConfig                               = app.injector.instanceOf[AuthConfig]
    val action                                   = app.injector.instanceOf[DefaultActionBuilder]
    val xivows                                   = mock[XivoWebService]
    val meetingRoomAuth                          = app.injector.instanceOf[MeetingRoomAuthentication]
    val secured                                  = new controllers.Secured(authConfig, xivows, action)
    val meetingRoomCommon = new MeetingRoomCommon(
      meetingRoomMananger,
      secured,
      Helpers.stubControllerComponents()
    )
    val meetingConfig = app.injector.instanceOf[MeetingRoomConfiguration]

    val ts = Timestamp.from(Instant.now())
    ts.setNanos(0)

    val wSClient: WSClient     = mock[WSClient]
    val wsRequest: WSRequest   = mock[WSRequest]
    val wsResponse: WSResponse = mock[WSResponse]

    val NO_FILTERS: List[DynamicFilter] = List()

    when(wsResponse.status).thenReturn(Status.OK)
    when(wsRequest.addCookies(any[DefaultWSCookie])).thenReturn(wsRequest)
    when(wsRequest.get()).thenReturn(Future.successful(wsResponse))
    when(wSClient.url(anyString())).thenReturn(wsRequest)
    when(xivows.validateCookieAgainstXiVO(Cookie("_eid", "xxxx")))
      .thenReturn(Future.successful(XivoCookieResponse))

    def getController() =
      new MeetingRoom(
        meetingRoomMananger,
        meetingRoomAuth,
        meetingRoomCommon,
        uuidGeneratorMock,
        secured,
        Helpers.stubControllerComponents()
      )(clockMock)
  }

  "MeetingRoom Controller" should {
    "get a meeting room by number and return 200" in new Helper() {
      val id: Long       = 1
      val number: String = "1050"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        "My Conf Room 1",
        Some(number),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("number", number)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getByNumber(number), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(meetingRoom)

      verify(meetingRoomMananger).get(filter)
    }

    "retrieve meeting room token by meeting room alias and redirect to PIN authorization page if required" in new Helper() {
      val id: Long    = 1
      val displayName = "My Conf Room 1"
      val uuid: UUID  = UUID.randomUUID()
      val number      = "1050"
      val pin         = "1234"
      val alias       = "abcd-1234"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts),
        Some(alias)
      )

      val filter = createGetFilter("alias", alias)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/alias/validate/$alias"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe FOUND
      headers(res)
        .get("Location")
        .get shouldBe s"""/authorize/video?token=${tokenContent.token}#config.subject="My%20Conf%20Room%201 (**1050)""""
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
      mockedApp.stop()
    }

    "retrieve meeting room token by meeting room alias and redirect to video page if no PIN required" in new Helper() {
      val id: Long    = 1
      val displayName = "My Conf Room 1"
      val uuid: UUID  = UUID.randomUUID()
      val number      = "1050"
      val alias       = "abcd-1234"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        None,
        Some(alias)
      )

      val filter = createGetFilter("alias", alias)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            timestamp = None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/alias/validate/$alias"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe FOUND
      headers(res)
        .get("Location")
        .get shouldBe s"""/video/$uuid?jwt=${tokenContent.token}#config.subject="My%20Conf%20Room%201 (**1050)""""
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
      mockedApp.stop()
    }

    "get a meeting room token with requirepin=false for room without pin" in new Helper() {
      val id          = "1"
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("id", id)
      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = false,
          timestamp = None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getToken(id), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "get a meeting room token by room id and with user id and return 200" in new Helper() {
      val id          = "1"
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some("1234"),
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

      val filter = createGetFilter("id", id)
      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq =
        FakeRequest(GET, "/?userId=2").withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.getToken(id), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "get a meeting room token with requirepin=true for room with pin" in new Helper() {
      val id          = "1"
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some("1234"),
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

      val filter = createGetFilter("id", id)
      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq =
        FakeRequest(GET, "/?userId=2").withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.getToken(id), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "get a meeting room token with requirepin=false for room with empty pin" in new Helper() {
      val id          = "1"
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some(""),
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

      val filter = createGetFilter("id", id)
      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = false,
          None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq =
        FakeRequest(GET, "/?userId=2").withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.getToken(id), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "get a meeting room token by room id with wrong user id and return 404" in new Helper() {
      val id   = "1"
      val uuid = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

      val filter = createGetFilter("id", id)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq =
        FakeRequest(GET, "/?userId=3").withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.getToken(id), rq)

      status(res) shouldBe NOT_FOUND
      contentAsString(res) should include(s"not match with requested id")

      verify(meetingRoomMananger).get(filter)
    }

    "not get a meeting room token by room id with user id and if room is static and return 200" in new Helper() {
      val id          = "1"
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some("1234"),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("id", id)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq =
        FakeRequest(GET, "/?userId=3").withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.getToken(id), rq)

      status(res) shouldBe NOT_FOUND
      contentAsString(res) should include(s"not match with requested id")

      verify(meetingRoomMananger).get(filter)
    }

    "not get a meeting room token by room id without user id and if room is personal and return 200" in new Helper() {
      val id          = "1"
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some("1234"),
        Some(uuid),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

      val filter = createGetFilter("id", id)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getToken(id), rq)

      status(res) shouldBe NOT_FOUND
      contentAsString(res) should include(s"not match with requested id")

      verify(meetingRoomMananger).get(filter)
    }

    "get a non-existent meeting room token and return 404" in new Helper() {
      val id: String = "1"
      val filter     = createGetFilter("id", id)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Failure(AnormException("not found")))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getToken(id), rq)

      status(res) shouldBe NOT_FOUND
      contentAsString(res) should include("not found")

      verify(meetingRoomMananger).get(filter)
    }

    "get a list of meeting rooms and return 200" in new Helper() {
      val id: Long = 1
      val staticMeetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val personalMeetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Personal,
        Some(2),
        Some(ts)
      )

      when(meetingRoomMananger.getAll(NO_FILTERS))
        .thenReturn(Success(List(staticMeetingRoom, personalMeetingRoom)))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getAll(), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(staticMeetingRoom, personalMeetingRoom)
      )

      verify(meetingRoomMananger).getAll(NO_FILTERS)
    }

    "get an empty list of meeting rooms and return 500" in new Helper() {
      when(meetingRoomMananger.getAll(NO_FILTERS)).thenReturn(Success(List()))
      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getAll(), rq)

      status(res) shouldBe OK
      contentAsString(res) shouldEqual "[]"

      verify(meetingRoomMananger).getAll(NO_FILTERS)
    }

    "find a meeting room with predicate and return 200" in new Helper() {
      val id: Long = 1
      val uuid     = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filters = List(
        DynamicFilter(
          "displayName",
          Some(OperatorLike),
          Some("My Conf"),
          None,
          None
        )
      )

      val expected = FindMeetingRoomResult(1, List(meetingRoom))

      when(meetingRoomMananger.find(filters, 0, 10))
        .thenReturn(Success(FindMeetingRoomResult(1, List(meetingRoom))))

      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(FindMeetingRoom(filters, 0, 10)))
      val res = call(controller.find(), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)
      contentAsString(
        res
      ) shouldEqual s"""{\"total\":1,\"list\":[{\"id\":1,\"displayName\":\"My Conf Room 1\",\"number\":\"1050\",\"userPin\":\"1234\",\"uuid\":\"$uuid\",\"roomType\":\"static\",\"pinTimestamp\":${ts.getTime}}]}"""

      verify(meetingRoomMananger).find(filters, 0, 10)
    }

    "find a meeting room with wrong predicate and return 400" in new Helper() {
      val id: Long = 1
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filters = List(
        DynamicFilter(
          "displayName",
          Some(OperatorEq),
          None,
          None,
          Some(List("Room"))
        )
      )

      when(meetingRoomMananger.find(filters, 0, 10))
        .thenReturn(Success(FindMeetingRoomResult(1, List(meetingRoom))))

      val controller = getController()
      val rq = FakeRequest(POST, "/")
        .withCookies(Cookie("_eid", "xxxx"))
        .withJsonBody(Json.toJson(FindMeetingRoom(filters, 0, 10)))
      val res = call(controller.find(), rq)

      status(res) shouldBe BAD_REQUEST
    }

    "validate a token without room pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            timestamp = None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(tokenContent)

      verify(meetingRoomMananger).get(filter)
    }

    "validate a token with empty room pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(""),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            timestamp = None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(tokenContent)

      verify(meetingRoomMananger).get(filter)
    }

    "validate token with room pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = Some(ts),
          expiry = Some(futureTs)
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?pin=$pin")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "challenge the token with room pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )
      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "challenge the token with room pin and redirect" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?redirect=true")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe FOUND
      headers(res)
        .get("Location")
        .get shouldBe s"""/authorize/video?token=${expected.token}#config.subject="My%20Conf%20Room%201 (**1050)""""
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
    }

    "authenticate token with timestamp" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val futureTs    = Timestamp.from(Instant.now().plusSeconds(100))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = Some(ts),
            expiry = Some(futureTs)
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )
      contentAsJson(res) shouldEqual Json.toJson(tokenContent)

      verify(meetingRoomMananger).get(filter)
    }

    "not authenticate token with expired PIN timestamp" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val oldTs       = Timestamp.from(Instant.now().minusSeconds(100))
      val futureTs    = Timestamp.from(Instant.now().plusSeconds(100))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = Some(oldTs),
            expiry = Some(futureTs)
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe UNAUTHORIZED
      cookies(res).get("xivoMeetingRoomToken").isEmpty shouldBe true
      contentAsString(res) should include("PIN expired")

      verify(meetingRoomMananger).get(filter)
    }

    "not authenticate token with expired TOKEN timestamp" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val oldTs       = Timestamp.from(Instant.now().minusSeconds(100))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            timestamp = Some(ts),
            expiry = Some(oldTs)
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe UNAUTHORIZED
      cookies(res).get("xivoMeetingRoomToken").isEmpty shouldBe true
      contentAsString(res) should include("Token expired")

      verifyZeroInteractions(meetingRoomMananger)
    }

    "not validate token with wrong pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?pin=WRONGPIN")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe UNAUTHORIZED
      cookies(res).get("xivoMeetingRoomToken").isEmpty shouldBe true
      contentAsString(res) should include("pin not validated")

      verify(meetingRoomMananger).get(filter)
    }

    "validate token with room pin and redirect" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          Some(ts),
          expiry = Some(futureTs)
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?pin=$pin&redirect=true")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe FOUND
      headers(res)
        .get("Location")
        .get shouldBe s"""/video/$uuid?jwt=${expected.token}#config.subject="My%20Conf%20Room%201 (**1050)""""
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
    }

    "normalize room name with accents and special characters" in new Helper() {
      val id          = 1
      val displayName = "Général & ùsèr"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          Some(ts),
          expiry = Some(futureTs)
        )
      )
      val titleEncoded = "G%C3%A9n%C3%A9ral%20%26%20%C3%B9s%C3%A8r"

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?pin=$pin&redirect=true")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe FOUND
      headers(res)
        .get("Location")
        .get shouldBe s"""/video/$uuid?jwt=${expected.token}#config.subject="$titleEncoded (**1050)""""
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
    }

    "validate token without pin and redirect" in new Helper() {
      //todo: duplicate test
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          None
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?redirect=true")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe FOUND
      headers(res)
        .get("Location")
        .get shouldBe s"""/authorize/video?token=${tokenContent.token}#config.subject="My%20Conf%20Room%201 (**1050)""""
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
    }

    "validate a pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = Some(ts),
          expiry = Some(futureTs)
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?pin=$pin")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK

      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      contentAsJson(res) shouldEqual Json.toJson(expected)

      verify(meetingRoomMananger).get(filter)
    }

    "validate a wrong pin" in new Helper() {
      val id          = 1
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(id),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))
      val controller = getController()
      val rq = FakeRequest(GET, s"/?pin=11111")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe UNAUTHORIZED
      contentAsString(res) should include("pin not validated")

      verify(meetingRoomMananger).get(filter)
    }

    "get a temporary meeting room token" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = None
      val uuid        = new UUID(0, 0)
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))

      val controller = getController()
      val rq         = FakeRequest(GET, "/").withCookies(Cookie("_eid", "xxxx"))
      val res        = call(controller.getTemporaryToken(displayName), rq)

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          number,
          uuid,
          requirepin = false,
          timestamp = None,
          temporary = true,
          expiry = Some(futureTs)
        )
      )

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(expected)
    }

    "validate a valid temporary token" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = ""
      val uuid        = UUID.randomUUID()
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))

      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            timestamp = None,
            temporary = true,
            expiry = Some(futureTs)
          )
        )

      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(tokenContent)
    }

    "not authenticate expired temporary token" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = ""
      val uuid        = UUID.randomUUID()
      val oldTs       = Timestamp.from(Instant.now().minusSeconds(100))

      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            timestamp = None,
            expiry = Some(oldTs),
            temporary = true
          )
        )

      val controller = getController()
      val rq = FakeRequest(GET, s"/")
        .withCookies(Cookie("_eid", "xxxx"))
      val res = call(controller.validateToken(tokenContent.token), rq)

      status(res) shouldBe UNAUTHORIZED
      cookies(res).get("xivoMeetingRoomToken").isEmpty shouldBe true
      contentAsString(res) should include("Token expired")
    }
  }

  "MeetingRoom Routes" should {
    "get a list of meeting rooms" in new Helper() {
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      when(meetingRoomMananger.getAll(NO_FILTERS))
        .thenReturn(Success(List(meetingRoom)))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res) = route(
        mockedApp,
        FakeRequest(GET, "/configmgt/api/2.0/meetingrooms")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).getAll(NO_FILTERS)

      mockedApp.stop()
    }

    "get a meeting room by number" in new Helper() {
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("number", "1050")

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(GET, "/configmgt/api/2.0/meetingrooms/number/1050")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "get a meeting room token" in new Helper() {
      val id = "1"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("id", id)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(GET, s"/configmgt/api/2.0/meetingrooms/token/$id")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "get a meeting room temporary token" in new Helper() {
      val id = "1"

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(GET, s"/configmgt/api/2.0/meetingrooms/temporary/token/$id")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK

      mockedApp.stop()
    }

    "get a meeting room alias" in new Helper() {
      val id    = "1"
      val alias = "abcd-1234"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts),
        Some(alias)
      )

      val filter = createGetFilter("id", id)

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(GET, s"/configmgt/api/2.0/meetingrooms/alias/$id")
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      contentAsJson(res) shouldEqual Json.toJson(
        MeetingRoomAliasResponse(Some(alias))
      )
      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "validate a token" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/token/validate/${tokenContent.token}"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).get(filter)
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      contentAsJson(res) shouldEqual Json.toJson(tokenContent)

      mockedApp.stop()
    }

    "validate a temporary token" in new Helper() {
      val displayName = "My Conf Room 1"
      val uuid        = UUID.randomUUID()

      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            None,
            uuid,
            requirepin = false,
            timestamp = None,
            expiry = None,
            temporary = true
          )
        )

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/token/validate/${tokenContent.token}"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      contentAsJson(res) shouldEqual Json.toJson(tokenContent)

      mockedApp.stop()
    }

    "validate a token and redirect with flag" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/token/validate/${tokenContent.token}?redirect=true"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe FOUND
      verify(meetingRoomMananger).get(filter)
      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "validate a token and not redirect with flag" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        None,
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = false,
            None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/token/validate/${tokenContent.token}?redirect=false"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK

      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        tokenContent.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      contentAsJson(res) shouldEqual Json.toJson(tokenContent)

      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "validate a pin" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val futureTs =
        Timestamp.from(currentTime.plusSeconds(meetingConfig.expiration()))
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      val expected = meetingRoomAuth.encode(
        MeetingRoomTokenContent(
          displayName,
          Some(number),
          uuid,
          requirepin = true,
          timestamp = Some(ts),
          expiry = Some(futureTs)
        )
      )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .overrides(bind[MeetingRoomInstantTime].to(clockMock))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/token/validate/${tokenContent.token}?pin=$pin"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe OK

      cookies(res).get("xivoMeetingRoomToken").get shouldBe Cookie(
        "xivoMeetingRoomToken",
        expected.token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )

      verify(meetingRoomMananger).get(filter)
      contentAsJson(res) shouldEqual Json.toJson(expected)

      mockedApp.stop()
    }

    "not validate a wrong pin" in new Helper() {
      val displayName = "My Conf Room 1"
      val number      = "1050"
      val uuid        = UUID.randomUUID()
      val pin         = "123456"
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        displayName,
        Some(number),
        Some(pin),
        Some(uuid),
        RoomTypes.Static,
        None,
        Some(ts)
      )

      val filter = createGetFilter("uuid", uuid.toString)
      val tokenContent =
        meetingRoomAuth.encode(
          MeetingRoomTokenContent(
            displayName,
            Some(number),
            uuid,
            requirepin = true,
            None
          )
        )

      when(meetingRoomMananger.get(filter))
        .thenReturn(Success(meetingRoom))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[MeetingRoomAuthentication].to(meetingRoomAuth))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(
          GET,
          s"/configmgt/api/2.0/meetingrooms/token/validate/${tokenContent.token}?pin=WRONGPING"
        )
          .withCookies(Cookie("_eid", "xxxx"))
      )

      status(res) shouldBe UNAUTHORIZED

      cookies(res).get("xivoMeetingRoomToken").isEmpty shouldBe true

      verify(meetingRoomMananger).get(filter)

      mockedApp.stop()
    }

    "find a meeting room" in new Helper() {
      val meetingRoom: MeetingRoomConfig = new MeetingRoomConfig(
        Some(1),
        "My Conf Room 1",
        Some("1050"),
        Some("1234"),
        Some(UUID.randomUUID()),
        RoomTypes.Static,
        None,
        Some(ts)
      )
      val filters = List(
        DynamicFilter(
          "displayName",
          Some(OperatorLike),
          Some("My Conf"),
          None,
          None
        )
      )

      when(meetingRoomMananger.find(filters, 0, 10))
        .thenReturn(Success(FindMeetingRoomResult(1, List(meetingRoom))))

      val mockedApp = new GuiceApplicationBuilder()
        .configure(playConfig)
        .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
        .overrides(bind[MeetingRoomManager].to(meetingRoomMananger))
        .overrides(bind[XivoWebService].to(xivows))
        .build()

      val Some(res: Future[Result]) = route(
        mockedApp,
        FakeRequest(POST, "/configmgt/api/2.0/meetingrooms/find")
          .withCookies(Cookie("_eid", "xxxx"))
          .withJsonBody(Json.toJson(FindMeetingRoom(filters, 0, 10)))
      )

      status(res) shouldBe OK
      verify(meetingRoomMananger).find(filters, 0, 10)

      mockedApp.stop()
    }
  }
}
