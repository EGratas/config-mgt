package ws.controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import eu.timepit.refined._
import eu.timepit.refined.collection.NonEmpty
import org.mockito.Mockito.{never, times, verify, when}
import play.api.Configuration
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import play.api.libs.ws.WSClient
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import ws.controllers.route.RouteHelper
import xivo.model._
import xivo.service.{MediaServerManager, RouteManager}

import scala.util.{Failure, Success}

class RouteSpec extends DockerPlaySpec {

  def withMocks(
      test: (RouteManager, Route, MediaServerManager, WSClient) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer
    val routeManager                        = mock[RouteManager]
    val mdsManager                          = mock[MediaServerManager]
    val helper                              = new RouteHelper(routeManager, mdsManager)

    val configuration: Configuration = mock[Configuration]
    val wSClient: WSClient           = mock[WSClient]

    val controller = new Route(
      routeManager,
      mdsManager,
      helper,
      wSClient,
      configuration,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(routeManager, controller, mdsManager, wSClient)
    ()
  }

  "Route" should {
    "convert to json" in {
      val dp1 = DialPattern("""\d\d\d\d""", None, None, None)
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule = List(RichSchedule(1, "evening", true))
      val richRouteNoId = RichRouteNoId(
        List(dp1),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        List(RichRight(1, "deny")),
        Some("sub"),
        internal = false
      )

      Json.toJson(richRouteNoId) shouldEqual Json.obj(
        "dialpattern" -> Json.parse("""[{"pattern":"XXXX"}]"""),
        "priority"    -> 1,
        "context"     -> Json.toJson(richContextList),
        "mediaserver" -> Json.toJson(richMdsList),
        "trunk" -> Json.parse(
          """[{"id":1,"name":"trunk1","used":true},{"id":2,"name":"trunk2","used":false}]"""
        ),
        "schedule"   -> Json.toJson(richSchedule),
        "right"      -> Json.parse("""[{"id":1,"name":"deny","used":false}]"""),
        "subroutine" -> "sub",
        "internal"   -> false
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse("""{"dialpattern":[{"pattern":"XXXX"}],
          "priority":1,
          "context":[{"name":"default","used":true},{"name":"remote","used":false}],
          "mediaserver":[{"id":1,"name":"mds0","display_name":"MDS 0","used":true},{"id":2,"name":"mds1","display_name":"MDS 1","used":false}],
          "trunk":[{"id":1,"name":"trunk1","used":true},{"id":2,"name":"trunk2","used":false}],
          "schedule":[{"id":1,"name":"evening","used":true}],
          "right":[{"id":1,"name":"deny","used":false}],
          "subroutine":"sub",
          "internal":false}""".stripMargin)

      val dp1 = DialPattern("""\d\d\d\d""", None, None, None)
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule = List(RichSchedule(1, "evening", true))
      val richRouteNoId = RichRouteNoId(
        List(dp1),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        List(RichRight(1, "deny")),
        Some("sub"),
        internal = false
      )

      json.validate[RichRouteNoId] match {
        case JsSuccess(r, _) => r shouldEqual richRouteNoId
        case JsError(errors) => fail()
      }
    }

    "be created from json without internal option" in {
      val json: JsValue = Json.parse("""{"dialpattern":[{"pattern":"XXXX"}],
          "priority":1,
          "context":[{"name":"default","used":true},{"name":"remote","used":false}],
          "mediaserver":[{"id":1,"name":"mds0","display_name":"MDS 0","used":true},{"id":2,"name":"mds1","display_name":"MDS 1","used":false}],
          "trunk":[{"id":1,"name":"trunk1","used":true},{"id":2,"name":"trunk2","used":false}],
          "schedule":[{"id":1,"name":"evening","used":true}],
          "right":[{"id":1,"name":"deny","used":false}],
          "subroutine":"sub"}""".stripMargin)

      val dp1 = DialPattern("""\d\d\d\d""", None, None, None)
      val richContextList =
        List(RichContext("default", true), RichContext("remote", false))
      val richMdsList = List(
        RichMediaserverConfig(1L, "mds0", "MDS 0", true),
        RichMediaserverConfig(2L, "mds1", "MDS 1", false)
      )
      val richTrunkList =
        List(TrunkGeneric(1, "trunk1", true), TrunkGeneric(2, "trunk2", false))
      val richSchedule = List(RichSchedule(1, "evening", true))
      val richRouteNoId = RichRouteNoId(
        List(dp1),
        None,
        1,
        richContextList,
        richMdsList,
        richTrunkList,
        richSchedule,
        List(RichRight(1, "deny")),
        Some("sub"),
        internal = false
      )

      json.validate[RichRouteNoId] match {
        case JsSuccess(r, _) => r shouldEqual richRouteNoId
        case JsError(errors) => fail()
      }
    }

    "be rejected from json with empty strings in dialpattern fields" in {
      val json: JsValue = Json.parse("""{"dialpattern":[{"pattern":"XXXX", "regexp":"",  "callerid": null}],
          "priority":1,
          "context":[{"name":"default","used":true},{"name":"remote","used":false}],
          "mediaserver":[{"id":1,"name":"mds0","display_name":"MDS 0","used":true},{"id":2,"name":"mds1","display_name":"MDS 1","used":false}],
          "trunk":[{"id":1,"name":"trunk1","used":true},{"id":2,"name":"trunk2","used":false}],
          "schedule":[{"id":1,"name":"evening","used":true}],
          "right":[{"id":1,"name":"deny","used":false}],
          "subroutine":"sub"}""".stripMargin)

      json.validate[RichRouteNoId] match {
        case JsSuccess(_, _) => fail()
        case JsError(errors) => errors should have length 1
      }
    }

    "get list of all routes" in withMocks {
      (routeManager, controller, mdsManager, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val dp1 = DialPattern(
            """\+33\d[0-9#\*]+""",
            Some(refineMV[NonEmpty]("""123\1""")),
            Some(refineMV[NonEmpty](""".{3}(.*)""")),
            Some(refineMV[NonEmpty]("123456789"))
          )
          val dp2 = DialPattern("""\+32\d[0-9#\*]+""", None, None, None)
          val dp3 = DialPattern("""\+420\d[0-9#\*]+""", None, None, None)

          val simpleRoute1 = PlainRoute(
            1,
            List(dp1, dp2),
            Some("sub"),
            Some("desc1"),
            1,
            List("default"),
            List(1),
            List(SortedTrunkIds(1, 1), SortedTrunkIds(2, 2)),
            List(1),
            List(),
            internal = false
          )
          val simpleRoute2 = PlainRoute(
            2,
            List(dp3),
            None,
            None,
            1,
            List("remote1"),
            List(2),
            List(SortedTrunkIds(2, 1)),
            List(),
            List(1),
            internal = true
          )

          val trunk = List(
            TrunkSip(2, "trunk2-prio2"),
            TrunkSip(1, "trunk1-prio1"),
            TrunkSip(3, "trunk3")
          )
          val context = List(Context("default"), Context("remote1"))
          val mds = MediaServerListConfig(
            List(
              MediaServerConfig(1, "mds0", "MDS 0", Some("1.1.1.1"), false),
              MediaServerConfig(2, "mds1", "MDS 1", Some("1.1.1.1"), false)
            )
          )
          val schedule = List(Schedule(1, "evening"))
          val right    = List(Right(1, "deny"))

          when(routeManager.getAll).thenReturn(
            Success(List(simpleRoute1, simpleRoute2))
          )
          when(routeManager.getContextAll).thenReturn(Success(context))
          when(routeManager.getTrunkAll).thenReturn(Success(trunk))
          when(mdsManager.all()).thenReturn(Success(mds))
          when(routeManager.getScheduleAll).thenReturn(Success(schedule))
          when(routeManager.getRightAll).thenReturn(Success(right))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getAll, rq)

          status(res) shouldEqual OK
          contentAsJson(res) shouldEqual Json.arr(
            Json.obj(
              "id" -> 1,
              "dialpattern" -> Json.arr(
                Json.obj(
                  "pattern"  -> "+33X.",
                  "regexp"   -> ".{3}(.*)",
                  "target"   -> "123\\1",
                  "callerid" -> "123456789"
                ),
                Json.obj(
                  "pattern" -> "+32X."
                )
              ),
              "subroutine"  -> "sub",
              "description" -> "desc1",
              "priority"    -> 1,
              "internal"    -> false,
              "context" -> Json.arr(
                Json.obj(
                  "name" -> "default",
                  "used" -> true
                ),
                Json.obj(
                  "name" -> "remote1",
                  "used" -> false
                )
              ),
              "mediaserver" -> Json.arr(
                Json.obj(
                  "id"           -> 1,
                  "name"         -> "mds0",
                  "display_name" -> "MDS 0",
                  "used"         -> true
                ),
                Json.obj(
                  "id"           -> 2,
                  "name"         -> "mds1",
                  "display_name" -> "MDS 1",
                  "used"         -> false
                )
              ),
              "trunk" -> Json.arr(
                Json.obj(
                  "id"   -> 1,
                  "name" -> "trunk1-prio1",
                  "used" -> true
                ),
                Json.obj(
                  "id"   -> 2,
                  "name" -> "trunk2-prio2",
                  "used" -> true
                ),
                Json.obj(
                  "id"   -> 3,
                  "name" -> "trunk3",
                  "used" -> false
                )
              ),
              "schedule" -> Json.arr(
                Json.obj(
                  "id"   -> 1,
                  "name" -> "evening",
                  "used" -> true
                )
              ),
              "right" -> Json.arr(
                Json.obj(
                  "id"   -> 1,
                  "name" -> "deny",
                  "used" -> false
                )
              )
            ),
            Json.obj(
              "id" -> 2,
              "dialpattern" -> Json.arr(
                Json.obj(
                  "pattern" -> "+420X."
                )
              ),
              "priority" -> 1,
              "internal" -> true,
              "context" -> Json.arr(
                Json.obj(
                  "name" -> "default",
                  "used" -> false
                ),
                Json.obj(
                  "name" -> "remote1",
                  "used" -> true
                )
              ),
              "mediaserver" -> Json.arr(
                Json.obj(
                  "id"           -> 1,
                  "name"         -> "mds0",
                  "display_name" -> "MDS 0",
                  "used"         -> false
                ),
                Json.obj(
                  "id"           -> 2,
                  "name"         -> "mds1",
                  "display_name" -> "MDS 1",
                  "used"         -> true
                )
              ),
              "trunk" -> Json.arr(
                Json.obj(
                  "id"   -> 2,
                  "name" -> "trunk2-prio2",
                  "used" -> true
                ),
                Json.obj(
                  "id"   -> 1,
                  "name" -> "trunk1-prio1",
                  "used" -> false
                ),
                Json.obj(
                  "id"   -> 3,
                  "name" -> "trunk3",
                  "used" -> false
                )
              ),
              "schedule" -> Json.arr(
                Json.obj(
                  "id"   -> 1,
                  "name" -> "evening",
                  "used" -> false
                )
              ),
              "right" -> Json.arr(
                Json.obj(
                  "id"   -> 1,
                  "name" -> "deny",
                  "used" -> true
                )
              )
            )
          )
          verify(routeManager).getAll
        }
    }

    "get route by id" in withMocks {
      (routeManager, controller, mdsManager, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val dp1                                 = DialPattern("""\d\d\d\d""", None, None, None)

          val simpleRoute = PlainRoute(
            1,
            List(dp1),
            Some("sub"),
            Some("desc1"),
            1,
            List("default"),
            List(1),
            List(SortedTrunkIds(1, 1)),
            List(1),
            List(1),
            internal = false
          )
          val trunk   = List(TrunkSip(1, "trunk1"), TrunkSip(2, "trunk2"))
          val context = List(Context("default"), Context("remote1"))
          val mds = MediaServerListConfig(
            List(
              MediaServerConfig(1, "mds0", "MDS 0", Some("1.1.1.1"), false),
              MediaServerConfig(2, "mds1", "MDS 1", Some("1.1.1.1"), false)
            )
          )
          val schedule = List(Schedule(1, "evening"))
          val right    = List(Right(1, "deny"))

          when(routeManager.get(1)).thenReturn(Success(simpleRoute))
          when(routeManager.getContextAll).thenReturn(Success(context))
          when(routeManager.getTrunkAll).thenReturn(Success(trunk))
          when(mdsManager.all()).thenReturn(Success(mds))
          when(routeManager.getScheduleAll).thenReturn(Success(schedule))
          when(routeManager.getRightAll).thenReturn(Success(right))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.get(1), rq)

          status(res) shouldEqual OK
          contentAsJson(res) shouldEqual Json.obj(
            "id"          -> 1,
            "dialpattern" -> Json.arr(Json.obj("pattern" -> "XXXX")),
            "subroutine"  -> "sub",
            "description" -> "desc1",
            "priority"    -> 1,
            "internal"    -> false,
            "context" -> Json.arr(
              Json.obj(
                "name" -> "default",
                "used" -> true
              ),
              Json.obj(
                "name" -> "remote1",
                "used" -> false
              )
            ),
            "mediaserver" -> Json.arr(
              Json.obj(
                "id"           -> 1,
                "name"         -> "mds0",
                "display_name" -> "MDS 0",
                "used"         -> true
              ),
              Json.obj(
                "id"           -> 2,
                "name"         -> "mds1",
                "display_name" -> "MDS 1",
                "used"         -> false
              )
            ),
            "trunk" -> Json.arr(
              Json.obj(
                "id"   -> 1,
                "name" -> "trunk1",
                "used" -> true
              ),
              Json.obj(
                "id"   -> 2,
                "name" -> "trunk2",
                "used" -> false
              )
            ),
            "schedule" -> Json.arr(
              Json.obj(
                "id"   -> 1,
                "name" -> "evening",
                "used" -> true
              )
            ),
            "right" -> Json.arr(
              Json.obj(
                "id"   -> 1,
                "name" -> "deny",
                "used" -> true
              )
            )
          )
          verify(routeManager).get(1)
        }
    }

    "get new route" in withMocks { (routeManager, controller, mdsManager, _) =>
      {
        implicit val materializer: Materializer = app.materializer
        val trunk                               = List(TrunkSip(1, "trunk1"), TrunkSip(2, "trunk2"))
        val context                             = List(Context("default"), Context("remote1"))
        val mds = MediaServerListConfig(
          List(
            MediaServerConfig(1, "mds0", "MDS 0", Some("1.1.1.1"), false),
            MediaServerConfig(2, "mds1", "MDS 1", Some("1.1.1.1"), false)
          )
        )
        val schedule = List(Schedule(1, "evening"))
        val right    = List(Right(1, "deny"))

        when(routeManager.getLastPriority).thenReturn(Success(1L))
        when(routeManager.getContextAll).thenReturn(Success(context))
        when(routeManager.getTrunkAll).thenReturn(Success(trunk))
        when(mdsManager.all()).thenReturn(Success(mds))
        when(routeManager.getScheduleAll).thenReturn(Success(schedule))
        when(routeManager.getRightAll).thenReturn(Success(right))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.getNew, rq)

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.obj(
          "priority" -> 1,
          "context" -> Json.arr(
            Json.obj(
              "name" -> "default",
              "used" -> false
            ),
            Json.obj(
              "name" -> "remote1",
              "used" -> false
            )
          ),
          "mediaserver" -> Json.arr(
            Json.obj(
              "id"           -> 1,
              "name"         -> "mds0",
              "display_name" -> "MDS 0",
              "used"         -> false
            ),
            Json.obj(
              "id"           -> 2,
              "name"         -> "mds1",
              "display_name" -> "MDS 1",
              "used"         -> false
            )
          ),
          "trunk" -> Json.arr(
            Json.obj(
              "id"   -> 1,
              "name" -> "trunk1",
              "used" -> false
            ),
            Json.obj(
              "id"   -> 2,
              "name" -> "trunk2",
              "used" -> false
            )
          ),
          "schedule" -> Json.arr(
            Json.obj(
              "id"   -> 1,
              "name" -> "evening",
              "used" -> false
            )
          ),
          "right" -> Json.arr(
            Json.obj(
              "id"   -> 1,
              "name" -> "deny",
              "used" -> false
            )
          )
        )
        verify(routeManager).getLastPriority
      }
    }

    "delete route by id" in withMocks { (routeManager, controller, _, _) =>
      {
        implicit val materializer: Materializer = app.materializer
        val dp1                                 = DialPattern("""\d\d\d\d""", None, None, None)
        val simpleRoute = PlainRoute(
          1,
          List(dp1),
          None,
          None,
          1,
          List("default"),
          List(1),
          List(SortedTrunkIds(1, 1)),
          List(1),
          List(1),
          internal = false
        )

        when(routeManager.deleteRoutePattern(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteContext(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteMediaserver(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteTrunk(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteSchedule(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteRight(1)).thenReturn(Success(1L))
        when(routeManager.delete(1)).thenReturn(Success(simpleRoute))

        val rq = FakeRequest(DELETE, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.delete(1), rq)

        status(res) shouldEqual NO_CONTENT
        verify(routeManager).deleteRouteContext(1)
        verify(routeManager).deleteRouteMediaserver(1)
        verify(routeManager).deleteRouteTrunk(1)
        verify(routeManager).deleteRouteSchedule(1)
        verify(routeManager).deleteRouteRight(1)
        verify(routeManager).delete(1)
      }
    }

    "not delete route by id" in withMocks { (routeManager, controller, _, _) =>
      {
        implicit val materializer: Materializer = app.materializer
        when(routeManager.deleteRoutePattern(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteContext(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteMediaserver(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteTrunk(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteSchedule(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteRight(1)).thenReturn(Success(1L))
        when(routeManager.delete(1))
          .thenReturn(Failure(new Exception("failed")))

        val rq = FakeRequest(DELETE, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.delete(1), rq)

        status(res) shouldEqual INTERNAL_SERVER_ERROR
        verify(routeManager).deleteRouteContext(1)
        verify(routeManager).deleteRouteMediaserver(1)
        verify(routeManager).deleteRouteTrunk(1)
        verify(routeManager).deleteRouteSchedule(1)
        verify(routeManager).deleteRouteRight(1)
        verify(routeManager).delete(1)
      }
    }

    "not delete route by id (failed fk)" in withMocks {
      (routeManager, controller, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          when(routeManager.deleteRoutePattern(1)).thenReturn(Success(1L))
          when(routeManager.deleteRouteContext(1)).thenReturn(Success(1L))
          when(routeManager.deleteRouteMediaserver(1))
            .thenReturn(Failure(new Exception("failed")))
          when(routeManager.deleteRouteTrunk(1)).thenReturn(Success(1L))

          val rq = FakeRequest(DELETE, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.delete(1), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(routeManager).deleteRouteContext(1)
          verify(routeManager).deleteRouteMediaserver(1)
        }
    }

    "create route" in withMocks { (routeManager, controller, _, _) =>
      {
        implicit val materializer: Materializer = app.materializer
        val dp1                                 = DialPattern("""\d\d\d\d""", None, None, None)
        val richContextList =
          List(RichContext("default", true), RichContext("remote", false))
        val richMdsList = List(
          RichMediaserverConfig(1L, "mds0", "MDS 0", true),
          RichMediaserverConfig(2L, "mds1", "MDS 1", false)
        )
        val richTrunkList = List(
          TrunkGeneric(1, "trunk1", true),
          TrunkGeneric(2, "trunk2", false)
        )
        val richSchedule = List(RichSchedule(1, "evening", true))
        val richRouteNoId = RichRouteNoId(
          List(dp1),
          None,
          1,
          richContextList,
          richMdsList,
          richTrunkList,
          richSchedule,
          List(),
          Some("sub"),
          internal = false
        )

        when(routeManager.createRoute(richRouteNoId))
          .thenReturn(Success(richRouteNoId.withId(1L)))
        when(routeManager.createRoutePattern(1, dp1))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteContext(1, "default"))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteMediaserver(1, 1L))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteTrunk(1, 1, 0))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteSchedule(1, 1))
          .thenReturn(Success(Some(1L)))

        val json = Json.toJson(richRouteNoId)

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)
        val res = call(controller.create, rq)

        status(res) shouldEqual CREATED
        verify(routeManager, times(1)).createRoute(richRouteNoId)
        verify(routeManager, times(1)).createRoutePattern(1, dp1)
        verify(routeManager, times(1)).createRouteContext(1, "default")
        verify(routeManager, times(1)).createRouteMediaserver(1, 1)
        verify(routeManager, times(1)).createRouteTrunk(1, 1, 0)
        verify(routeManager, times(1)).createRouteSchedule(1, 1)
        verify(routeManager, never()).createRouteRight(1, 1)
      }
    }

    "not create route (failed in routecontext)" in withMocks {
      (routeManager, controller, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val dp1                                 = DialPattern("""\d\d\d\d""", None, None, None)
          val richContextList =
            List(RichContext("default", true), RichContext("remote", false))
          val richMdsList = List(
            RichMediaserverConfig(1L, "mds0", "MDS 0", true),
            RichMediaserverConfig(2L, "mds1", "MDS 1", false)
          )
          val richTrunkList = List(
            TrunkGeneric(1, "trunk1", true),
            TrunkGeneric(2, "trunk2", false)
          )
          val richSchedule  = List(RichSchedule(1, "evening", true))
          val richRightList = List(RichRight(1, "deny", false))
          val richRouteNoId = RichRouteNoId(
            List(dp1),
            None,
            1,
            richContextList,
            richMdsList,
            richTrunkList,
            richSchedule,
            richRightList,
            None,
            internal = false
          )

          when(routeManager.createRoute(richRouteNoId))
            .thenReturn(Success(richRouteNoId.withId(1L)))
          when(routeManager.createRoutePattern(1, dp1))
            .thenReturn(Success(Some(1L)))
          when(routeManager.createRouteContext(1, "default"))
            .thenReturn(Failure(new Exception("failed")))
          when(routeManager.createRouteMediaserver(1, 1L))
            .thenReturn(Success(Some(1L)))
          when(routeManager.createRouteTrunk(1, 1, 0))
            .thenReturn(Success(Some(1L)))
          when(routeManager.createRouteSchedule(1, 1))
            .thenReturn(Success(Some(1L)))

          val json = Json.toJson(richRouteNoId)

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.create, rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(routeManager, times(1)).createRoute(richRouteNoId)
          verify(routeManager, times(1)).createRoutePattern(1, dp1)
          verify(routeManager).createRouteContext(1, "default")
          verify(routeManager).createRouteMediaserver(1, 1)
          verify(routeManager).createRouteTrunk(1, 1, 0)
          verify(routeManager).createRouteSchedule(1, 1)
          verify(routeManager, never()).createRouteRight(1, 1)
        }
    }

    "update the route" in withMocks { (routeManager, controller, _, _) =>
      {
        implicit val materializer: Materializer = app.materializer
        val dp1                                 = DialPattern("+33\\d*", None, None, None)
        val dp2                                 = DialPattern("""\+33\d[0-9#\*]+""", None, None, None)
        val simpleRoute = PlainRoute(
          1,
          List(dp1),
          None,
          Some("desc1"),
          1,
          List("default"),
          List(1),
          List(SortedTrunkIds(1, 1)),
          List(),
          List(1),
          internal = false
        )
        val richContextList =
          List(RichContext("default", true), RichContext("remote", false))
        val richMdsList = List(
          RichMediaserverConfig(1L, "mds0", "MDS 0", true),
          RichMediaserverConfig(2L, "mds1", "MDS 1", false)
        )
        val richTrunkList = List(
          TrunkGeneric(1, "trunk1", true),
          TrunkGeneric(2, "trunk2", false)
        )
        val richRightList = List(RichRight(1, "deny", true))
        val richRoute = RichRoute(
          1,
          List(dp2),
          Some("desc1"),
          1,
          richContextList,
          richMdsList,
          richTrunkList,
          List(),
          richRightList,
          Some("sub"),
          internal = false
        )
        val richRouteNoId = RichRouteNoId(
          List(dp2),
          Some("desc1"),
          1,
          richContextList,
          richMdsList,
          richTrunkList,
          List(),
          richRightList,
          Some("sub"),
          internal = false
        )

        when(routeManager.createRoute(richRouteNoId))
          .thenReturn(Success(richRouteNoId.withId(1)))
        when(routeManager.createRouteContext(1, "default"))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteMediaserver(1, 1L))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteTrunk(1, 1, 0))
          .thenReturn(Success(Some(1L)))
        when(routeManager.createRouteRight(1, 1)).thenReturn(Success(Some(1L)))

        when(routeManager.deleteRoutePattern(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteContext(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteMediaserver(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteTrunk(1)).thenReturn(Success(1L))
        when(routeManager.deleteRouteSchedule(1)).thenReturn(Success(0L))
        when(routeManager.deleteRouteRight(1)).thenReturn(Success(1L))
        when(routeManager.delete(1)).thenReturn(Success(simpleRoute))

        val json = Json.toJson(richRoute)

        val rq = FakeRequest(PUT, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)
        val res = call(controller.update, rq)

        status(res) shouldEqual CREATED
        contentAsJson(res) shouldEqual Json.obj(
          "id" -> 1,
          "dialpattern" -> Json.arr(
            Json.obj(
              "pattern" -> "+33X."
            )
          ),
          "description" -> "desc1",
          "priority"    -> 1,
          "internal"    -> false,
          "context" -> Json.arr(
            Json.obj(
              "name" -> "default",
              "used" -> true
            ),
            Json.obj(
              "name" -> "remote",
              "used" -> false
            )
          ),
          "mediaserver" -> Json.arr(
            Json.obj(
              "id"           -> 1,
              "name"         -> "mds0",
              "display_name" -> "MDS 0",
              "used"         -> true
            ),
            Json.obj(
              "id"           -> 2,
              "name"         -> "mds1",
              "display_name" -> "MDS 1",
              "used"         -> false
            )
          ),
          "trunk" -> Json.arr(
            Json.obj(
              "id"   -> 1,
              "name" -> "trunk1",
              "used" -> true
            ),
            Json.obj(
              "id"   -> 2,
              "name" -> "trunk2",
              "used" -> false
            )
          ),
          "schedule" -> Json.arr(),
          "right" -> Json.arr(
            Json.obj(
              "id"   -> 1,
              "name" -> "deny",
              "used" -> true
            )
          ),
          "subroutine" -> "sub"
        )
        verify(routeManager).deleteRouteContext(1)
        verify(routeManager).deleteRouteMediaserver(1)
        verify(routeManager).deleteRouteTrunk(1)
        verify(routeManager).deleteRouteRight(1)
        verify(routeManager).delete(1)
        verify(routeManager).createRoute(richRouteNoId)
        verify(routeManager).createRouteContext(1, "default")
        verify(routeManager).createRouteMediaserver(1, 1)
        verify(routeManager, times(1)).createRouteTrunk(1, 1, 0)
        verify(routeManager, never()).createRouteSchedule(1, 1)
        verify(routeManager, times(1)).createRouteRight(1, 1)
      }
    }

    "transform asterisk pattern to regexp" in {
      RouteHelper.fromAsteriskPattern("+33X.") shouldBe """\+33\d[0-9#\*]+"""
      RouteHelper.fromAsteriskPattern("+420X.") shouldBe """\+420\d[0-9#\*]+"""
      RouteHelper.fromAsteriskPattern("XXXX") shouldBe """\d\d\d\d"""
      RouteHelper.fromAsteriskPattern("X.") shouldBe """\d[0-9#\*]+"""
      RouteHelper.fromAsteriskPattern(
        "+33[1-5]XXXXXXXX"
      ) shouldBe """\+33[1-5]\d\d\d\d\d\d\d\d"""
      RouteHelper.fromAsteriskPattern("00ZX.") shouldBe """00[1-9]\d[0-9#\*]+"""
      RouteHelper.fromAsteriskPattern("2Z!") shouldBe """2[1-9][0-9#\*]*"""
      RouteHelper.fromAsteriskPattern(
        "+2XNZ!."
      ) shouldBe """\+2\d[2-9][1-9][0-9#\*]*[0-9#\*]+"""
    }

    "transform regexp to asterisk pattern" in {
      RouteHelper.toAsteriskPattern("""\+33\d[0-9#\*]+""") shouldBe "+33X."
      RouteHelper.toAsteriskPattern("""\+420\d[0-9#\*]+""") shouldBe "+420X."
      RouteHelper.toAsteriskPattern("""\d\d\d\d""") shouldBe "XXXX"
      RouteHelper.toAsteriskPattern("""\d[0-9#\*]+""") shouldBe "X."
      RouteHelper.toAsteriskPattern(
        """\+33[1-5]\d\d\d\d\d\d\d\d"""
      ) shouldBe "+33[1-5]XXXXXXXX"
      RouteHelper.toAsteriskPattern("""00[1-9]\d[0-9#\*]+""") shouldBe "00ZX."
      RouteHelper.toAsteriskPattern("""2[1-9][0-9#\*]*""") shouldBe "2Z!"
      RouteHelper.toAsteriskPattern(
        """\+2\d[2-9][1-9][0-9#\*]*[0-9#\*]+"""
      ) shouldBe "+2XNZ!."
    }
  }
}
