package ws.controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import org.mockito.Mockito._
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}
import ws.controllers.{UserQueueMembership => UserQueueMembershipCtrl}
import ws.model.{
  QueueMembership,
  UsersQueueMembership,
  UserQueueMembership => UserQueueMembershipModel
}
import xc.model.QueueMemberDefault
import xc.service.QueueMemberDefaultManager

import scala.util.Success

class XivoCtiUserWithProfileQueueMembershipSpec extends DockerPlaySpec {

  def withMocks(
      test: (QueueMemberDefaultManager, UserQueueMembership) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer
    val queueMemberDefaultManager           = mock[QueueMemberDefaultManager]
    val controller = new UserQueueMembershipCtrl(
      queueMemberDefaultManager,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(queueMemberDefaultManager, controller)
    ()
  }

  "UserQueueMembership" should {
    "Get default queue membership of a given user" in withMocks {
      (queueMemberDefaultManager, controller) =>
        {
          import ws.model.QueueMembershipJson._

          implicit val materializer: Materializer = app.materializer
          val userId                              = 42L

          val queueMembershipList = List(
            QueueMembership(1, 1),
            QueueMembership(5, 12),
            QueueMembership(7, 5)
          )

          val queueMemberDefaultList = queueMembershipList.map {
            case QueueMembership(queueId, penalty) =>
              QueueMemberDefault(queueId, userId, penalty)
          }

          stub(queueMemberDefaultManager.getByUserId(userId))
            .toReturn(Success(queueMemberDefaultList))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getUserDefaultMembership(userId), rq)

          status(res) shouldEqual OK
          verify(queueMemberDefaultManager).getByUserId(userId)

          contentAsJson(res) shouldEqual Json.toJson(queueMembershipList)
        }
    }

    "Set default queue membership of a given user" in withMocks {
      (queueMemberDefaultManager, controller) =>
        {
          import ws.model.QueueMembershipJson._

          implicit val materializer: Materializer = app.materializer
          val userId                              = 42L

          val queueMembershipList = List(
            QueueMembership(1, 1),
            QueueMembership(5, 12),
            QueueMembership(7, 5)
          )

          val queueMemberDefaultList = queueMembershipList.map {
            case QueueMembership(queueId, penalty) =>
              QueueMemberDefault(queueId, userId, penalty)
          }

          stub(queueMemberDefaultManager.getByUserId(userId))
            .toReturn(Success(queueMemberDefaultList))
          stub(
            queueMemberDefaultManager.setForUserId(
              userId,
              queueMemberDefaultList
            )
          ).toReturn(Success(queueMemberDefaultList))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withBody(Json.toJson(queueMembershipList))

          val res = call(controller.setUserDefaultMembership(userId), rq)

          status(res) shouldEqual OK
          verify(queueMemberDefaultManager).setForUserId(
            userId,
            queueMemberDefaultList
          )

        }
    }

    "Get all default queue membership" in withMocks {
      (queueMemberDefaultManager, controller) =>
        {
          import ws.model.UserQueueMembershipJson._

          implicit val materializer: Materializer = app.materializer

          val memberships = List(
            QueueMemberDefault(1, 10, 7),
            QueueMemberDefault(3, 10, 4),
            QueueMemberDefault(2, 12, 5),
            QueueMemberDefault(3, 12, 8)
          )

          val usersMembership = List(
            UserQueueMembershipModel(
              10,
              List(QueueMembership(1, 7), QueueMembership(3, 4))
            ),
            UserQueueMembershipModel(
              12,
              List(QueueMembership(2, 5), QueueMembership(3, 8))
            )
          )

          stub(queueMemberDefaultManager.all()).toReturn(Success(memberships))

          val rq = FakeRequest(GET, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")

          val res = call(controller.getAllDefaultMembership(), rq)
          status(res) shouldEqual OK

          verify(queueMemberDefaultManager).all()

          contentAsJson(res) shouldEqual Json.toJson(usersMembership)
        }
    }

    "Set default queue membership of a set of users" in withMocks {
      (queueMemberDefaultManager, controller) =>
        {
          import ws.model.UsersQueueMembershipJson._

          implicit val materializer: Materializer = app.materializer
          val userIds                             = List(10L, 12L, 42L)

          val uqm = UsersQueueMembership(
            userIds,
            List(
              QueueMembership(1, 1),
              QueueMembership(5, 12),
              QueueMembership(7, 5)
            )
          )

          userIds.foreach(u => {
            val qList = uqm.membership.collect { case QueueMembership(q, p) =>
              QueueMemberDefault(q, u, p)
            }
            stub(queueMemberDefaultManager.getByUserId(u))
              .toReturn(Success(List.empty[QueueMemberDefault]))
            stub(queueMemberDefaultManager.setForUserId(u, qList))
              .toReturn(Success(qList))
          })

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withBody(Json.toJson(uqm))

          val res = call(controller.setUsersDefaultMembership(), rq)

          status(res) shouldEqual OK
          userIds.foreach(u => {
            val qList = uqm.membership.collect { case QueueMembership(q, p) =>
              QueueMemberDefault(q, u, p)
            }
            verify(queueMemberDefaultManager).setForUserId(u, qList)
          })

        }
    }
  }
}
