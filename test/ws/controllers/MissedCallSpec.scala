package ws.controllers

import akka.stream.Materializer
import controllers.Secured
import docker.DockerPlaySpec
import org.mockito.Mockito.{stub, verify}
import play.api.http.Status.OK
import play.api.test.FakeRequest
import play.api.test.Helpers.{
  call,
  contentAsString,
  defaultAwaitTimeout,
  status,
  writeableOf_AnyContentAsEmpty,
  GET
}
import xivo.service.{UserPreference, UserPreferenceKey, UserPreferenceManager}

import scala.util.{Failure, Success}

class MissedCallSpec extends DockerPlaySpec {

  def withMocks(
      test: (UserPreferenceManager, MissedCall) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer

    val userPreferenceManager = mock[UserPreferenceManager]

    val controller = new MissedCall(
      userPreferenceManager,
      app.injector.instanceOf[Secured]
    )
    test(userPreferenceManager, controller)
    ()
  }

  "MissedCall controller" should {
    "Increment the number of missed call for the user" in withMocks {
      (userPreferenceManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer
          val userId                              = 42
          val nbMissedCall                        = "4"
          val expectedMissedCall                  = 5
          val expected =
            s"""{"userId":${userId},"nbMissedCall":$expectedMissedCall}"""

          stub(
            userPreferenceManager.get(userId, UserPreferenceKey.NbMissedCall)
          ).toReturn(
            Success(
              UserPreference(
                userId,
                UserPreferenceKey.NbMissedCall,
                nbMissedCall,
                "String"
              )
            )
          )

          stub(
            userPreferenceManager.createOrUpdate(
              UserPreference(
                userId,
                UserPreferenceKey.NbMissedCall,
                s"$expectedMissedCall",
                "String"
              )
            )
          ).toReturn(
            Success(
              1
            )
          )

          val rq =
            FakeRequest(GET, s"/api/2.0/users/$userId/new_missed_call")
              .withSession(
                "username"     -> "test",
                "isSuperAdmin" -> "true"
              )
          val res = call(controller.newMissedCall(userId), rq)

          status(res) shouldEqual OK
          verify(userPreferenceManager).get(
            userId,
            UserPreferenceKey.NbMissedCall
          )
          verify(userPreferenceManager).createOrUpdate(
            UserPreference(
              userId,
              UserPreferenceKey.NbMissedCall,
              s"$expectedMissedCall",
              "String"
            )
          )
          contentAsString(
            res
          ) shouldBe expected
        }
    }

    "Increment correctly the number if the user preference is not set" in withMocks {
      (userPreferenceManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer
          val userId                              = 42
          val expectedMissedCall                  = 1
          val expected =
            s"""{"userId":${userId},"nbMissedCall":$expectedMissedCall}"""

          stub(
            userPreferenceManager.get(userId, UserPreferenceKey.NbMissedCall)
          ).toReturn(
            Failure(
              new Exception("Preference does not exist")
            )
          )

          stub(
            userPreferenceManager.createOrUpdate(
              UserPreference(
                userId,
                UserPreferenceKey.NbMissedCall,
                s"$expectedMissedCall",
                "String"
              )
            )
          ).toReturn(
            Success(
              1
            )
          )

          val rq =
            FakeRequest(GET, s"/api/2.0/users/$userId/new_missed_call")
              .withSession(
                "username"     -> "test",
                "isSuperAdmin" -> "true"
              )
          val res = call(controller.newMissedCall(userId), rq)

          status(res) shouldEqual OK
          contentAsString(
            res
          ) shouldBe expected
        }
    }
  }
}
