package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import controllers.Secured
import docker.DockerPlaySpec
import eu.timepit.refined.refineMV
import org.mockito.Mockito._
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import ws.model.{Line, LineType}
import xivo.service.LineManager

import scala.util.{Failure, Success}

class XivoUserLineSpec extends DockerPlaySpec {

  def withMocks(test: (LineManager, XivoUserLine) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer

    val lineManager = mock[LineManager]

    val controller =
      new XivoUserLine(lineManager, app.injector.instanceOf[Secured])
    test(lineManager, controller)
    ()
  }

  "XivoUserLine controller" should {

    "get should answer 404 when no line found" in withMocks {
      (lineManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val userId = 1
          stub(lineManager.get(userId))
            .toReturn(Failure(AnormException("not found")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.get(userId), rq)

          val expected: Long = 1
          status(res) shouldEqual NOT_FOUND
          verify(lineManager).get(expected)
        }
    }

    "get user's line" in withMocks { (lineManager, controller) =>
      {

        implicit val materializer: Materializer = app.materializer

        val userId = 1
        val line = Line(
          Some(2),
          LineType.PHONE,
          refineMV("default"),
          refineMV("1000"),
          refineMV("default"),
          Some(refineMV("AAA")),
          Some(refineMV("1hlnzr8t")),
          Some(1),
          Some(1234)
        )

        stub(lineManager.get(userId)).toReturn(Success(line))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.get(userId), rq)

        status(res) shouldEqual OK
        verify(lineManager).get(userId)

        contentAsString(res) shouldEqual
          "{\"id\":2,\"lineType\":\"phone\",\"context\":\"default\",\"extension\":\"1000\",\"site\":\"default\",\"device\":\"AAA\",\"name\":\"1hlnzr8t\",\"lineNum\":1,\"provisioningId\":1234}"
      }
    }

    "create user's line" in withMocks { (lineManager, controller) =>
      {

        implicit val materializer: Materializer = app.materializer

        val userId = 1
        val line = Line(
          None,
          LineType.PHONE,
          refineMV("default"),
          refineMV("1000"),
          refineMV("default"),
          Some(refineMV("AAA")),
          None,
          Some(1),
          Some(1234)
        )
        val expectedLine =
          line.copy(id = Some(2), name = Some(refineMV("1hlnzr8t")))

        stub(lineManager.create(userId, line)).toReturn(Success(expectedLine))

        val json = Json.toJson(line)
        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)
        val res = call(controller.create(userId), rq)

        status(res) shouldEqual OK
        verify(lineManager).create(userId, line)

        contentAsString(res) shouldEqual
          "{\"id\":2,\"lineType\":\"phone\",\"context\":\"default\",\"extension\":\"1000\",\"site\":\"default\",\"device\":\"AAA\",\"name\":\"1hlnzr8t\",\"lineNum\":1,\"provisioningId\":1234}"
      }
    }

    "update user's line" in withMocks { (lineManager, controller) =>
      {

        implicit val materializer: Materializer = app.materializer

        val userId = 1
        val line = Line(
          Some(2),
          LineType.PHONE,
          refineMV("default"),
          refineMV("1000"),
          refineMV("default"),
          Some(refineMV("AAA")),
          Some(refineMV("1hlnzr8t")),
          Some(1),
          Some(1234)
        )
        val expectedLine = line.copy(extension = refineMV("1002"))

        stub(lineManager.update(userId, line)).toReturn(Success(expectedLine))

        val json = Json.toJson(line)
        val rq = FakeRequest(PUT, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)
        val res = call(controller.update(userId), rq)

        status(res) shouldEqual OK
        verify(lineManager).update(userId, line)

        contentAsString(res) shouldEqual
          "{\"id\":2,\"lineType\":\"phone\",\"context\":\"default\",\"extension\":\"1002\",\"site\":\"default\",\"device\":\"AAA\",\"name\":\"1hlnzr8t\",\"lineNum\":1,\"provisioningId\":1234}"
      }
    }

    "delete user's line" in withMocks { (lineManager, controller) =>
      {

        implicit val materializer: Materializer = app.materializer

        val userId = 1
        val line = Line(
          Some(2),
          LineType.PHONE,
          refineMV("default"),
          refineMV("1000"),
          refineMV("default"),
          Some(refineMV("AAA")),
          Some(refineMV("1hlnzr8t")),
          Some(1),
          Some(1234)
        )

        stub(lineManager.delete(userId)).toReturn(Success(line))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.delete(userId), rq)

        status(res) shouldEqual NO_CONTENT
        verify(lineManager).delete(userId)

        contentAsString(res) shouldBe empty
      }
    }
  }
}
