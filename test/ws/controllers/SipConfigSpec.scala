package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import controllers.Secured
import docker.DockerPlaySpec
import org.mockito.Mockito.{stub, verify}
import play.api.http.Status.{INTERNAL_SERVER_ERROR, OK}
import play.api.test.FakeRequest
import play.api.test.Helpers.{
  call,
  contentAsString,
  defaultAwaitTimeout,
  status,
  writeableOf_AnyContentAsEmpty,
  GET
}
import ws.controllers.{SipConfig => SIPConfigController}
import xivo.service.{SipConfig, StaticSipManager}

import scala.util.{Failure, Success}

class SipConfigSpec extends DockerPlaySpec {

  def withMocks(test: (StaticSipManager, SIPConfigController) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer

    val staticSipManager = mock[StaticSipManager]

    val controller = new SIPConfigController(
      staticSipManager,
      app.injector.instanceOf[Secured]
    )
    test(staticSipManager, controller)
    ()
  }

  "StaticSip controller" should {

    "get stun address" in withMocks { (staticSipManager, controller) =>
      {
        implicit val materializer: Materializer = app.materializer

        stub(staticSipManager.getStunAddress())
          .toReturn(Success(SipConfig(Some("stun:stun.l.google.com:19302"))))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.getStunAddress(), rq)

        status(res) shouldEqual OK
        verify(staticSipManager).getStunAddress()

        contentAsString(
          res
        ) shouldBe "{\"stunAddress\":\"stun:stun.l.google.com:19302\"}"
      }
    }

    "return null body if no stun address is retrieved" in withMocks {
      (staticSipManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          stub(staticSipManager.getStunAddress())
            .toReturn(Success(SipConfig(None)))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getStunAddress(), rq)

          status(res) shouldEqual OK
          verify(staticSipManager).getStunAddress()

          contentAsString(res) shouldBe "{\"stunAddress\":null}"
        }
    }

    "return Internal server error if database request fail" in withMocks {
      (staticSipManager, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          stub(staticSipManager.getStunAddress())
            .toReturn(Failure(AnormException("Internal error")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getStunAddress(), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
          verify(staticSipManager).getStunAddress()
        }
    }

  }
}
