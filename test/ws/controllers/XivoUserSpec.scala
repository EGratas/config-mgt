package ws.controllers

import akka.stream.Materializer
import controllers.Secured
import docker.DockerPlaySpec
import model.{DynamicFilter, OperatorContainsAll, OperatorEq}
import org.mockito.Mockito._
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import ws.model.LineType
import xivo.model.{
  FindXivoUserRequest,
  FindXivoUserResponse,
  XivoUserContact,
  XivoUser => XivoUserModel
}
import xivo.service.XivoUserManager

import scala.util.{Failure, Success}

class XivoUserSpec extends DockerPlaySpec {

  def withMocks(test: (XivoUserManager, XivoUser) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer

    val xivoUserManager = mock[XivoUserManager]

    val controller = new ws.controllers.XivoUser(
      xivoUserManager,
      app.injector.instanceOf[Secured]
    )
    test(xivoUserManager, controller)
    ()
  }

  "XivoUser controller" should {

    "get users list as contacts" in withMocks { (xivoUserManager, controller) =>
      {

        implicit val materializer: Materializer = app.materializer

        val users = List(
          XivoUserContact(
            1,
            "Jean-Pierre",
            "Thomasset",
            Some("jpthomasset@xivo.solutions"),
            Some("0612345678"),
            Some("0678945612"),
            List("blue", "green", "red"),
            List("0123220009", "0154632897")
          ),
          XivoUserContact(
            2,
            "Alvin",
            "Sevil",
            Some("asevil@xivo.solutions"),
            Some(""),
            None,
            List(),
            List()
          )
        )

        stub(xivoUserManager.getAllAsContact).toReturn(Success(users))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.getAllAsContact(), rq)

        status(res) shouldEqual OK

        contentAsString(
          res
        ) shouldEqual """[{"id":1,"firstname":"Jean-Pierre","lastname":"Thomasset","email":"jpthomasset@xivo.solutions","mobilephonenumber":"0612345678","internalphonenumber":"0678945612","labels":["blue","green","red"],"externalphonenumber":["0123220009","0154632897"]},{"id":2,"firstname":"Alvin","lastname":"Sevil","email":"asevil@xivo.solutions","mobilephonenumber":"","internalphonenumber":null,"labels":[],"externalphonenumber":[]}]""".stripMargin
      }
    }

    "get users list" in withMocks { (xivoUserManager, controller) =>
      {

        implicit val materializer: Materializer = app.materializer

        val users = List(
          XivoUserModel(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.PHONE),
            Some("1000"),
            "XiVO test",
            Some("test"),
            Some("Mds Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          ),
          XivoUserModel(
            2,
            "John Wayne",
            Some(5678),
            Some(LineType.PHONE),
            Some("1010"),
            "XiVO test",
            Some("test"),
            Some("Mds Main"),
            disabled = false,
            Some(List("blue", "red"))
          )
        )

        stub(xivoUserManager.getAll).toReturn(Success(users))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.getAll(), rq)

        status(res) shouldEqual OK
        verify(xivoUserManager).getAll

        contentAsString(res) shouldEqual "[" +
          "{\"id\":1,\"fullName\":\"James Bond\",\"provisioning\":1234,\"lineType\":\"phone\",\"phoneNumber\":\"1000\",\"entity\":\"XiVO test\",\"mdsName\":\"test\",\"mdsDisplayName\":\"Mds Main\",\"disabled\":false,\"labels\":[\"blue\",\"green\",\"red\"]}," +
          "{\"id\":2,\"fullName\":\"John Wayne\",\"provisioning\":5678,\"lineType\":\"phone\",\"phoneNumber\":\"1010\",\"entity\":\"XiVO test\",\"mdsName\":\"test\",\"mdsDisplayName\":\"Mds Main\",\"disabled\":false,\"labels\":[\"blue\",\"red\"]}" +
          "]"
      }
    }

    "find users with a predicate" in withMocks {
      (xivoUserManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val users = List(
            XivoUserModel(
              1,
              "James Bond",
              Some(1234),
              Some(LineType.PHONE),
              Some("1000"),
              "XiVO test",
              Some("test"),
              Some("Mds Main"),
              disabled = false,
              Some(List("blue", "green", "red"))
            )
          )
          val filters = List(
            DynamicFilter(
              "fullName",
              Some(OperatorEq),
              Some("James Bond"),
              None,
              None
            )
          )

          stub(xivoUserManager.find(filters, 0, 10))
            .toReturn(Success(FindXivoUserResponse(1, users)))

          val rq = FakeRequest(GET, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(FindXivoUserRequest(filters, 0, 10)))

          val res = call(controller.find, rq)

          status(res) shouldEqual OK
          verify(xivoUserManager).find(filters, 0, 10)

          contentAsString(res) shouldEqual "{\"total\":1,\"list\":[" +
            "{\"id\":1,\"fullName\":\"James Bond\",\"provisioning\":1234,\"lineType\":\"phone\",\"phoneNumber\":\"1000\",\"entity\":\"XiVO test\",\"mdsName\":\"test\",\"mdsDisplayName\":\"Mds Main\",\"disabled\":false,\"labels\":[\"blue\",\"green\",\"red\"]}" +
            "]}"
        }
    }

    "throw error if an operator different from in is provided without a value" in withMocks {
      (_, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val filters = List(
            DynamicFilter(
              "fullName",
              Some(OperatorEq),
              None,
              None,
              Some(List("James Bond"))
            )
          )

          val rq = FakeRequest(GET, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(FindXivoUserRequest(filters, 0, 10)))

          val res = call(controller.find, rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "throw error if an operator in is provided without a list" in withMocks {
      (_, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val filters = List(
            DynamicFilter(
              "labels",
              Some(OperatorContainsAll),
              Some("James Bond"),
              None,
              None
            )
          )

          val rq = FakeRequest(GET, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.toJson(FindXivoUserRequest(filters, 0, 10)))

          val res = call(controller.find, rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "validate user with username and password" in withMocks {
      (xivoUserManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          val user = XivoUserModel(
            1,
            "James Bond",
            Some(1234),
            Some(LineType.PHONE),
            Some("1000"),
            "XiVO test",
            Some("test"),
            Some("Mds Main"),
            disabled = false,
            Some(List("blue", "green", "red"))
          )
          stub(xivoUserManager.validateUserPassword("jbond", "jbond"))
            .toReturn(Success(user))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(
              Json.obj("username" -> "jbond", "password" -> "jbond")
            )

          val res = call(controller.validateUser(), rq)

          status(res) shouldEqual NO_CONTENT
          verify(xivoUserManager).validateUserPassword("jbond", "jbond")
        }
    }

    "not validate user with wrong username and password" in withMocks {
      (xivoUserManager, controller) =>
        {

          implicit val materializer: Materializer = app.materializer

          stub(xivoUserManager.validateUserPassword("jbond", "notjbond"))
            .toReturn(Failure(new Exception("not found")))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(
              Json.obj("username" -> "jbond", "password" -> "notjbond")
            )

          val res = call(controller.validateUser(), rq)

          status(res) shouldEqual NOT_FOUND
          verify(xivoUserManager).validateUserPassword("jbond", "notjbond")
        }
    }
  }
}
