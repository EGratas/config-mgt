package ws.controllers

import akka.stream.Materializer
import anorm.AnormException
import docker.DockerPlaySpec
import model.RecordingModeType
import org.mockito.Mockito._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.ControllerComponents
import play.api.test.FakeRequest
import play.api.test.Helpers.{GET, call, _}
import xivo.model.{QueueDissuasionQueue, QueueDissuasionSoundFile, QueueFeature}
import xivo.service.{
  FileSystemManager,
  QueueDissuasionManager,
  QueueFeatureManager
}

import scala.util.{Failure, Success}

class QueueDissuasionSpec extends DockerPlaySpec {

  val soundFileDirectoryPath = "/var/lib/xivo/sounds/playback/"

  def createQueueFeature(id: Long, name: String): QueueFeature = {
    QueueFeature(
      Some(id),
      name,
      "Queue created by the method createQueueFeature()",
      "3000",
      None,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      0,
      "",
      "",
      None,
      None,
      0,
      None,
      None,
      0,
      RecordingModeType.NotRecorded,
      0
    )
  }

  def withMocks(
      test: (
          QueueDissuasion,
          QueueDissuasionManager,
          QueueFeatureManager,
          FileSystemManager,
          Configuration
      ) => Any
  ): Unit = {
    val queueDissuasionManager = mock[QueueDissuasionManager]
    val queueFeatureManager    = mock[QueueFeatureManager]
    val fileSystemManager      = mock[FileSystemManager]
    val cc                     = app.injector.instanceOf[ControllerComponents]
    val configuration          = mock[Configuration]

    val controller = new QueueDissuasion(
      app.injector.instanceOf[controllers.Secured],
      cc,
      queueDissuasionManager,
      queueFeatureManager,
      fileSystemManager,
      configuration
    )
    test(
      controller,
      queueDissuasionManager,
      queueFeatureManager,
      fileSystemManager,
      configuration
    )
    ()
  }

  "QueueDissuasion" should {
    "return the sound file's name when querying a dissuasion" in withMocks {
      (controller, queueDissuasionManager, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 2
          val queueName     = "queueTest"
          val fileName      = "some_audio_file"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          val expectedQDFile: QueueDissuasionSoundFile =
            QueueDissuasionSoundFile(None, fileName)
          when(queueDissuasionManager.get(queueId))
            .thenReturn(Success(Some(expectedQDFile)))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueDissuasion(queueId), rq)

          val expected = Json.obj(
            "id"   -> queueId,
            "name" -> queueName,
            "dissuasion" -> Json.obj(
              "type" -> "soundFile",
              "value" -> Json.obj(
                "soundFile" -> fileName
              )
            )
          )

          status(res) shouldEqual OK

          contentAsJson(res) shouldEqual expected
        }
    }

    "return the queue to redirect calls to when querying a dissuasion" in withMocks {
      (controller, queueDissuasionManager, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long  = 2
          val queueName      = "queueTest"
          val otherQueueName = "otherQueueTest"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          val expectedQDQueue: QueueDissuasionQueue =
            QueueDissuasionQueue(None, otherQueueName)
          when(queueDissuasionManager.get(queueId))
            .thenReturn(Success(Some(expectedQDQueue)))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueDissuasion(queueId), rq)

          val expected = Json.obj(
            "id"   -> queueId,
            "name" -> queueName,
            "dissuasion" -> Json.obj(
              "type" -> "queue",
              "value" -> Json.obj(
                "queueName" -> otherQueueName
              )
            )
          )

          status(res) shouldEqual OK

          contentAsJson(res) shouldEqual expected
        }
    }

    "return 'other' if the queue has its dissuasion configured neither towards a sound file nor a queue: " in withMocks {
      (controller, queueDissuasionManager, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 2
          val queueName     = "queueTest"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(queueDissuasionManager.get(queueId)).thenReturn(Success(None))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueDissuasion(queueId), rq)

          val expected = Json.obj(
            "id"   -> queueId,
            "name" -> queueName,
            "dissuasion" -> Json.obj(
              "type" -> "other"
            )
          )

          status(res) shouldEqual OK

          contentAsJson(res) shouldEqual expected
        }
    }

    "throw a queueFeatureNotFound exception when querying a queue that does not exist" in withMocks {
      (controller, _, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 4

          when(queueFeatureManager.get(queueId))
            .thenReturn(Failure(AnormException("This queue does not exist")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )

          val res = call(controller.getQueueDissuasion(queueId), rq)

          status(res) shouldEqual NOT_FOUND
        }
    }

    "throw an exception when querying the database failed" in withMocks {
      (controller, queueDissuasionManager, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 42
          val queueName     = "support"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(queueDissuasionManager.get(queueId)).thenReturn(
            Failure(AnormException("Querying the database failed!"))
          )

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueDissuasion(queueId), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
        }
    }

    "get the list of dissuasions for a queue" in withMocks {
      (controller, _, queueFeatureManager, fileSystemManager, configuration) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 6
          val queueName     = "queue1"

          val expected = Json.obj(
            "id"   -> queueId,
            "name" -> queueName,
            "dissuasions" -> Json.obj(
              "soundFiles" -> List(
                Json.obj(
                  "soundFile" -> "queue1_audio_file"
                ),
                Json.obj(
                  "soundFile" -> "queue1_audio_file_2"
                )
              ),
              "queues" -> List(
                Json.obj(
                  "queueName" -> "default_queue"
                )
              )
            )
          )

          when(fileSystemManager.getListOfFiles(soundFileDirectoryPath))
            .thenReturn(
              List[String](
                "queue1_audio_file.wav",
                "queue1_audio_file_2.wav",
                "queue2_audio_file_3.wav"
              )
            )

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(configuration.getOptional[String]("defaultQueueDissuasion"))
            .thenReturn(Some("default_queue"))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueDissuasionList(queueId), rq)

          status(res) shouldEqual OK

          contentAsJson(res) shouldEqual Json.toJson(expected)
        }
    }

    "get a not found error when the queue does not exist" in withMocks {
      (controller, _, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 1

          when(queueFeatureManager.get(queueId))
            .thenReturn(Failure(AnormException("This queue does not exist")))

          val rq = FakeRequest(GET, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.getQueueDissuasionList(queueId), rq)

          status(res) shouldEqual NOT_FOUND
        }
    }

    "update a queue dissuasion sound file" in withMocks {
      (
          controller,
          queueDissuasionManager,
          queueFeatureManager,
          fileSystemManager,
          _
      ) =>
        {
          implicit val materializer: Materializer = app.materializer
          val queueId: Long                       = 8
          val queueName                           = "promotion"
          val newFileName                         = "promotion_newFileName"
          val expected                            = QueueDissuasionSoundFile(Some(queueId), newFileName)
          val json                                = Json.parse(s"""{"soundFile": "$newFileName"}""")

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(
            fileSystemManager.doesFileExist(soundFileDirectoryPath, newFileName)
          ).thenReturn(true)
          when(
            queueDissuasionManager.dissuasionAccessToSoundFile(
              queueId,
              soundFileDirectoryPath + newFileName
            )
          ).thenReturn(Success(expected))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.setQueueDissuasion(queueId), rq)

          status(res) shouldEqual OK
          contentAsJson(res) shouldEqual Json.toJson(expected)
        }
    }

    "return '400 Bad Request' if no json sent when set dissuasion audio file" in withMocks {
      (controller, _, _, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val queueId: Long                       = 17

          val rq = FakeRequest(PUT, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.setQueueDissuasion(queueId), rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "return Bad_Request when set dissuasion audio file with bad json" in withMocks {
      (controller, _, _, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val queueId: Long                       = 17
          val newFileName                         = "promotion_fileName"

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.parse(s"""{"wrong_property": "$newFileName"}"""))
          val res = call(controller.setQueueDissuasion(queueId), rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "throw a queueFeatureNotFound exception when updating sound file from a non-existent queue" in withMocks {
      (controller, _, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 12
          val fileName      = "promotion_newFile"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Failure(AnormException("This queue does not exist")))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.parse(s"""{"soundFile": "$fileName"}"""))
          val res = call(controller.setQueueDissuasion(queueId), rq)

          status(res) shouldEqual NOT_FOUND
        }
    }

    "throw a fileNotFound exception when updating sound file with a non-existent soundFile" in withMocks {
      (controller, _, queueFeatureManager, fileSystemManager, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 12
          val queueName     = "support"
          val newFileName   = "support_newFile"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(
            fileSystemManager.doesFileExist(soundFileDirectoryPath, newFileName)
          ).thenReturn(false)

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.parse(s"""{"soundFile": "$newFileName"}"""))
          val res = call(controller.setQueueDissuasion(queueId), rq)

          status(res) shouldEqual NOT_FOUND
        }
    }

    "set a queue to redirect calls to another queue in case of dissuasion" in withMocks {
      (controller, queueDissuasionManager, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val queueId: Long                       = 8
          val queueName                           = "promotion"
          val otherQueueId: Long                  = 56
          val otherQueueName                      = "other_queue"
          val expected                            = QueueDissuasionQueue(Some(queueId), otherQueueName)
          val json                                = Json.parse(s"""{"queueName": "$otherQueueName"}""")

          createQueueFeature(otherQueueId, otherQueueName)

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(queueFeatureManager.getQueueIdByName(otherQueueName))
            .thenReturn(Some(otherQueueId))
          when(
            queueDissuasionManager.dissuasionAccessToOtherQueue(
              queueId,
              otherQueueId,
              otherQueueName
            )
          ).thenReturn(Success(expected))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)
          val res = call(controller.setQueueDissuasionToOtherQueue(queueId), rq)

          status(res) shouldEqual OK
          contentAsJson(res) shouldEqual Json.toJson(expected)
        }
    }

    "return '400 Bad Request' if no json sent when setting a queue to return calls to in case of dissuasion" in withMocks {
      (controller, _, _, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val queueId: Long                       = 17

          val rq = FakeRequest(PUT, "/").withSession(
            "username"     -> "test",
            "isSuperAdmin" -> "true"
          )
          val res = call(controller.setQueueDissuasionToOtherQueue(queueId), rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "return Bad_Request when setting a queue to return calls to in case of dissuasion with bad json" in withMocks {
      (controller, _, _, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer
          val queueId: Long                       = 17
          val otherQueueName                      = "other_queue"

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(
              Json.parse(s"""{"wrong_property": "$otherQueueName"}""")
            )
          val res = call(controller.setQueueDissuasionToOtherQueue(queueId), rq)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "throw a queueFeatureNotFound exception when updating the dissuasion of a non-existent queue to another queue" in withMocks {
      (controller, _, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long  = 12
          val otherQueueName = "other_queue"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Failure(AnormException("This queue does not exist")))

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.parse(s"""{"queueName": "$otherQueueName"}"""))
          val res = call(controller.setQueueDissuasionToOtherQueue(queueId), rq)

          status(res) shouldEqual NOT_FOUND
        }
    }

    "throw a queueFeatureNotFound exception when updating a queue to return calls to in case of dissuasion with a non-existent queue" in withMocks {
      (controller, _, queueFeatureManager, _, _) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long  = 12
          val queueName      = "support"
          val otherQueueName = "other_queue"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(queueFeatureManager.getQueueIdByName(otherQueueName))
            .thenReturn(None)

          val rq = FakeRequest(PUT, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.parse(s"""{"queueName": "$otherQueueName"}"""))
          val res = call(controller.setQueueDissuasionToOtherQueue(queueId), rq)

          status(res) shouldEqual NOT_FOUND
        }
    }

    "throw internal error exception if there is an error while querying the database to set dissuasion " in withMocks {
      (
          controller,
          queueDissuasionManager,
          queueFeatureManager,
          fileSystemManager,
          _
      ) =>
        {
          implicit val materializer: Materializer = app.materializer

          val queueId: Long = 38
          val queueName     = "urgence"
          val newFileName   = "urgence_soundFile"

          when(queueFeatureManager.get(queueId))
            .thenReturn(Success(createQueueFeature(queueId, queueName)))
          when(
            fileSystemManager.doesFileExist(soundFileDirectoryPath, newFileName)
          ).thenReturn(true)
          when(
            queueDissuasionManager.dissuasionAccessToSoundFile(
              queueId,
              soundFileDirectoryPath + newFileName
            )
          ).thenReturn(Failure(new Exception("Querying the database failed!")))

          val rq = FakeRequest(GET, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(Json.parse(s"""{"soundFile": "$newFileName"}"""))
          val res = call(controller.setQueueDissuasion(queueId), rq)

          status(res) shouldEqual INTERNAL_SERVER_ERROR
        }

    }

    "filter sound file who is prefixed by the queue name" in {
      val fileName = "queueName_test.wav"
      QueueDissuasion.isQueueSoundFile(fileName, "queueName") shouldBe true
    }

    "not filter sound file who is prefixed by the queue name" in {
      val fileName = "queueName2_test.wav"
      QueueDissuasion.isQueueSoundFile(fileName, "queueName") shouldBe false
    }

    "filter a list of sound file" in {

      val list = List[String](
        "queue1_audio_file.wav",
        "queue1_audio_file_2.wav",
        "queue2_audio_file_3.wav"
      )

      val expected = List[String](
        "queue1_audio_file.wav",
        "queue1_audio_file_2.wav"
      )

      list.filter(
        QueueDissuasion.isQueueSoundFile(_, "queue1")
      ) shouldEqual expected
    }

    "remove the extension from a file name" in {
      QueueDissuasion.removeExtension("someFile.wav") shouldEqual "someFile"
    }

    "return the file name if the file name has no extension" in {
      QueueDissuasion.removeExtension("someFile") shouldEqual "someFile"
    }

    "remove the path in front of the file name" in {
      QueueDissuasion.getFileName(
        "/var/lib/xivo/sounds/playback/someQueue_someFile"
      ) shouldEqual "someQueue_someFile"
    }

    "return the queueName if no path" in {
      QueueDissuasion.getFileName(
        "someQueue_someFile"
      ) shouldEqual "someQueue_someFile"
    }
  }
}
