package ws.model

import play.api.libs.json._
import testutils.PlayShouldSpec

/**
  */
class QueueMembershipSpec extends PlayShouldSpec {
  import QueueMembershipJson._

  "QueueMembership" should {
    "convert to json" in {
      val qm = QueueMembership(10, 5)
      Json.toJson(qm) shouldEqual (Json.obj(
        "queueId" -> qm.queueId,
        "penalty" -> qm.penalty
      ))
    }

    "be created from json" in {
      val json: JsValue = Json.parse("""{"queueId": 8, "penalty": 2}""")
      json.validate[QueueMembership] match {
        case JsSuccess(qm, _) => qm shouldEqual (QueueMembership(8, 2))
        case JsError(errors)  => fail()
      }
    }
  }

}
