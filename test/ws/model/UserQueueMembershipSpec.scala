package ws.model

import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import testutils.PlayShouldSpec
import xc.model.QueueMemberDefault

class UserQueueMembershipSpec extends PlayShouldSpec {
  import UserQueueMembershipJson._
  import QueueMembershipJson._

  "UserQueueMembership" should {
    "convert to json" in {
      val o = UserQueueMembership(
        10,
        List(QueueMembership(1, 2), QueueMembership(3, 5))
      )

      Json.toJson(o) should equal(
        Json.obj(
          "userId"     -> o.userId,
          "membership" -> o.membership
        )
      )
    }

    "be created from json" in {
      val json: JsValue = Json.parse(
        """{"userId": 10, "membership": [{"queueId":1, "penalty":2}, {"queueId":3, "penalty":5}]}"""
      )
      val res = UserQueueMembership(
        10,
        List(QueueMembership(1, 2), QueueMembership(3, 5))
      )

      json.validate[UserQueueMembership] match {
        case JsSuccess(qm, _) => qm shouldEqual (res)
        case JsError(errors)  => fail()
      }
    }

    "be created from List of QueueMemberDefault" in {
      val input = List(
        QueueMemberDefault(1, 10, 2),
        QueueMemberDefault(3, 10, 5),
        QueueMemberDefault(4, 12, 3),
        QueueMemberDefault(7, 12, 8)
      )
      val res = List(
        UserQueueMembership(
          10,
          List(QueueMembership(1, 2), QueueMembership(3, 5))
        ),
        UserQueueMembership(
          12,
          List(QueueMembership(4, 3), QueueMembership(7, 8))
        )
      )

      UserQueueMembership.convert(input) should equal(res)
    }
  }

}
