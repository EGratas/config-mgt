package ws.model

import docker.DockerPlaySpec
import play.api.libs.json.{JsError, JsSuccess, JsValue, Json}
import xc.model.CallQualificationAnswer

class CallQualificationAnswerSpec extends DockerPlaySpec {

  "CallQualificationAnswer" should {
    import xc.model.CallQualificationAnswer._

    "be created from json" in {
      val json: JsValue = Json.parse(
        """
          | {"sub_qualification_id": 1, "time": "2018-03-21 17:00:00", "callid": "callid1", "agent": 1, "queue": 1,
          | "first_name": "first", "last_name": "last", "comment": "some comment", "custom_data": "{\"opt1\":\"1000\"}"}
          | """.stripMargin
      )

      val res = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"opt1\":\"1000\"}"
      )

      json.validate[CallQualificationAnswer] match {
        case JsSuccess(qm, _) => qm shouldEqual res
        case JsError(_)       => fail()
      }
    }

    "be converted to json" in {
      val q = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"opt1\":\"1000\"}"
      )

      Json.toJson(q) shouldEqual Json.obj(
        "sub_qualification_id" -> q.sub_qualification_id,
        "time"                 -> q.time,
        "callid"               -> q.callid,
        "agent"                -> q.agent,
        "queue"                -> q.queue,
        "first_name"           -> q.firstName,
        "last_name"            -> q.lastName,
        "comment"              -> q.comment,
        "custom_data"          -> q.customData
      )
    }

    "export a list of qualification answers to CSV" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"custom\":\"1000\"}"
      )
      val qualification2 = CallQualificationAnswer(
        sub_qualification_id = 2,
        time = "2018-03-21 17:00:00",
        callid = "callid2",
        agent = 2,
        queue = 2,
        firstName = "second",
        lastName = "last",
        comment = "some comment",
        customData = "{\"custom\":\"1001\"}"
      )

      CallQualificationAnswer.toCsv(
        List(qualification1, qualification2)
      ) shouldEqual (s"""sub_qualification_id,time,callid,agent,queue,first_name,last_name,comment,custom
           |"${qualification1.sub_qualification_id}","${qualification1.time.toString}","${qualification1.callid}","${qualification1.agent}","${qualification1.queue}","${qualification1.firstName}","${qualification1.lastName}","${qualification1.comment}","1000"
           |"${qualification2.sub_qualification_id}","${qualification2.time.toString}","${qualification2.callid}","${qualification2.agent}","${qualification2.queue}","${qualification2.firstName}","${qualification2.lastName}","${qualification2.comment}","1001"""".stripMargin)
    }

    "export a list of qualification answers to CSV with parsed custom data header" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"callee\":\"2002\",\"opt2\":\"1\"}"
      )
      val qualification2 = CallQualificationAnswer(
        sub_qualification_id = 2,
        time = "2018-03-21 17:00:00",
        callid = "callid2",
        agent = 2,
        queue = 2,
        firstName = "second",
        lastName = "last",
        comment = "some comment",
        customData =
          "{\"caller\":\"1002\",\"contract\":\"contract1\",\"optField1\":\"opt1\",\"optField3\":\"opt3\",\"problemDescription\":\"problem desc1\"}"
      )

      CallQualificationAnswer.toCsv(
        List(qualification1, qualification2)
      ) shouldEqual (s"""sub_qualification_id,time,callid,agent,queue,first_name,last_name,comment,callee,opt2,caller,contract,optField1,optField3,problemDescription
           |"${qualification1.sub_qualification_id}","${qualification1.time.toString}","${qualification1.callid}","${qualification1.agent}","${qualification1.queue}","${qualification1.firstName}","${qualification1.lastName}","${qualification1.comment}","2002","1","","","","",""
           |"${qualification2.sub_qualification_id}","${qualification2.time.toString}","${qualification2.callid}","${qualification2.agent}","${qualification2.queue}","${qualification2.firstName}","${qualification2.lastName}","${qualification2.comment}","","","1002","contract1","opt1","opt3","problem desc1"""".stripMargin)
    }

    "export a list of qualification answers to CSV with existing and non-existing custom data" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = ""
      )
      val qualification2 = CallQualificationAnswer(
        sub_qualification_id = 2,
        time = "2018-03-21 17:00:00",
        callid = "callid2",
        agent = 2,
        queue = 2,
        firstName = "second",
        lastName = "last",
        comment = "some comment",
        customData =
          "{\"caller\":\"1002\",\"contract\":\"contract1\",\"optField1\":\"opt1\",\"optField3\":\"opt3\",\"problemDescription\":\"problem desc1\"}"
      )

      CallQualificationAnswer.toCsv(
        List(qualification1, qualification2)
      ) shouldEqual (s"""sub_qualification_id,time,callid,agent,queue,first_name,last_name,comment,caller,contract,optField1,optField3,problemDescription
             |"${qualification1.sub_qualification_id}","${qualification1.time.toString}","${qualification1.callid}","${qualification1.agent}","${qualification1.queue}","${qualification1.firstName}","${qualification1.lastName}","${qualification1.comment}",""
             |"${qualification2.sub_qualification_id}","${qualification2.time.toString}","${qualification2.callid}","${qualification2.agent}","${qualification2.queue}","${qualification2.firstName}","${qualification2.lastName}","${qualification2.comment}","1002","contract1","opt1","opt3","problem desc1"""".stripMargin)
    }

    "parse custom data keys to header" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"callee\":\"2002\",\"opt2\":\"1\"}"
      )
      val qualification2 = CallQualificationAnswer(
        sub_qualification_id = 2,
        time = "2018-03-21 17:00:00",
        callid = "callid2",
        agent = 2,
        queue = 2,
        firstName = "second",
        lastName = "last",
        comment = "some comment",
        customData =
          "{\"caller\":\"1002\",\"contract\":\"contract1\",\"optField1\":\"opt1\",\"optField3\":\"opt3\",\"problemDescription\":\"problem desc1\"}"
      )

      val qualificationAnswers = List(qualification1, qualification2)

      CallQualificationAnswer.getCustomDataKeys(
        qualificationAnswers
      ) shouldEqual List(
        "callee",
        "opt2",
        "caller",
        "contract",
        "optField1",
        "optField3",
        "problemDescription"
      )
    }

    "parse malformed json in custom data keys " in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"callee\"\"2002\",\"opt2\":\"1\"}"
      )
      val qualification2 = CallQualificationAnswer(
        sub_qualification_id = 2,
        time = "2018-03-21 17:00:00",
        callid = "callid2",
        agent = 2,
        queue = 2,
        firstName = "second",
        lastName = "last",
        comment = "some comment",
        customData =
          "{\"caller\":\"1002\",\"contract\":\"contract1\",\"optField1\":\"opt1\",\"optField3\":\"opt3\",\"problemDescription\":\"problem desc1\"}"
      )

      val qualificationAnswers = List(qualification1, qualification2)

      CallQualificationAnswer.getCustomDataKeys(
        qualificationAnswers
      ) shouldEqual List(
        "caller",
        "contract",
        "optField1",
        "optField3",
        "problemDescription"
      )
    }

    "parse custom data value to row" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"callee\":\"2002\",\"opt2\":\"1\"}"
      )
      val qualification2 = CallQualificationAnswer(
        sub_qualification_id = 2,
        time = "2018-03-21 17:00:00",
        callid = "callid2",
        agent = 2,
        queue = 2,
        firstName = "second",
        lastName = "last",
        comment = "some comment",
        customData =
          "{\"caller\":\"1002\",\"contract\":\"contract1\",\"optField1\":\"opt1\",\"optField3\":\"opt3\",\"problemDescription\":\"problem desc1\"}"
      )

      val qualificationAnswers = List(qualification1, qualification2)
      val keys                 = CallQualificationAnswer.getCustomDataKeys(qualificationAnswers)

      CallQualificationAnswer.getCustomDataValues(
        qualification1.customData,
        keys
      ) shouldEqual List("2002", "1", "", "", "", "", "")
      CallQualificationAnswer.getCustomDataValues(
        qualification2.customData,
        keys
      ) shouldEqual List(
        "",
        "",
        "1002",
        "contract1",
        "opt1",
        "opt3",
        "problem desc1"
      )
    }

    "parse empty custom data" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = ""
      )

      val qualificationAnswers = List(qualification1)
      val keys                 = CallQualificationAnswer.getCustomDataKeys(qualificationAnswers)

      CallQualificationAnswer.getCustomDataKeys(
        qualificationAnswers
      ) shouldEqual List()
      CallQualificationAnswer.getCustomDataValues(
        qualification1.customData,
        keys
      ) shouldEqual List("")
    }

    "parse malformed json in custom data" in {
      val qualification1 = CallQualificationAnswer(
        sub_qualification_id = 1,
        time = "2018-03-21 17:00:00",
        callid = "callid1",
        agent = 1,
        queue = 1,
        firstName = "first",
        lastName = "last",
        comment = "some comment",
        customData = "{\"callee\"\"2002\",\"opt2\":\"1\"}"
      )

      val qualificationAnswers = List(qualification1)
      val keys                 = CallQualificationAnswer.getCustomDataKeys(qualificationAnswers)

      CallQualificationAnswer.getCustomDataKeys(
        qualificationAnswers
      ) shouldEqual List()
      CallQualificationAnswer.getCustomDataValues(
        qualification1.customData,
        keys
      ) shouldEqual List("")
    }
  }
}
