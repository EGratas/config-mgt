package integration

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.Utils
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class XivoObjectsSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE userfeatures, users, rights, queuefeatures, agentgroup, incall CASCADE"
      ).execute()
      super.beforeEach()
    }

  val context: String = playConfig("play.http.context")
  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  "The XivoObjects Controller" should {
    "return all queues for a superadmin" in withConnection() { implicit c =>
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")
      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/queues")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj("id" -> 1, "name" -> "queue1", "displayName" -> "Queue 1"),
          Json.obj("id" -> 2, "name" -> "queue2", "displayName" -> "Queue 2")
        )
      )
    }

    "return only the user's queues for a supervisor" in withConnection() {
      implicit c =>
        val userid = Utils.insertUser("login1", "supervisor")
        Utils.insertQueueRight(userid, 2)
        Utils.insertQueueFeature(2, "thequeue")
        Utils.insertQueueFeature(3, "anotherqueue")

        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/queues")
            .withSession("username" -> "login1")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(Json.obj("id" -> 2, "name" -> "thequeue", "displayName" -> ""))
        )
    }

    "return all agent groups for a superadmin" in withConnection() {
      implicit c =>
        Utils.insertAgentGroup(1, "group1")
        Utils.insertAgentGroup(2, "group2")
        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/groups")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(
            Json.obj("id" -> 1, "name" -> "group1"),
            Json.obj("id" -> 2, "name" -> "group2")
          )
        )
    }

    "return only the user's groups for a supervisor" in withConnection() {
      implicit c =>
        val userid = Utils.insertUser("login1", "supervisor")
        Utils.insertGroupRight(userid, 2)
        Utils.insertAgentGroup(2, "thegroup")
        Utils.insertAgentGroup(3, "anothergroup")

        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/groups")
            .withSession("username" -> "login1")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(Json.obj("id" -> 2, "name" -> "thegroup"))
        )
    }

    "return all xivo users" in withConnection() { implicit c =>
      Utils.insertXivoUser(
        1,
        "Pierre",
        "Martin",
        Some("pmartin"),
        Some(1),
        Some(1)
      )

      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/xivo_users")
          .withSession("username" -> "test")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj(
            "firstname" -> "Pierre",
            "lastname"  -> "Martin",
            "login"     -> "pmartin"
          )
        )
      )
    }

    "return all incalls for a superadmin" in withConnection() { implicit c =>
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/incalls")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj("id" -> 1, "number" -> "5301"),
          Json.obj("id" -> 2, "number" -> "5302")
        )
      )
    }

    "return only the user's incalls for a supervisor" in withConnection() {
      implicit c =>
        val userid = Utils.insertUser("login1", "supervisor")
        Utils.insertIncallRight(userid, 2)
        Utils.insertIncall(2, "5003")
        Utils.insertIncall(3, "5004")

        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/incalls")
            .withSession("username" -> "login1")
        ).get

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(Json.obj("id" -> 2, "number" -> "5003"))
        )
    }
  }
}
