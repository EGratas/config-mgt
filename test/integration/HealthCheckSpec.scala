package integration

import docker.DockerPlaySpec
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class HealthCheckSpec extends DockerPlaySpec {

  val context: String = playConfig("play.http.context")
  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfig)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  "The HealthCheck Controller" should {

    "return 204 if configmgt is running" in {
      val res = route(
        app,
        FakeRequest(GET, context + "/api/2.0/healthcheck/running")
          .withSession()
      ).get

      status(res) shouldEqual NO_CONTENT
    }
  }
}
