package integration

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model.{Utils, XivoCtiUserWithProfile}
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class UsersSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE userfeatures, users, rights CASCADE").execute()
      super.beforeEach()
    }

  val context: String = playConfig("play.http.context")
  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  "The Users Controller" should {
    "list users" in withConnection() { implicit c =>
      Utils.insertUser(
        XivoCtiUserWithProfile("Dupond", "Pierre", "pdupond", "admin")
      )
      Utils.insertUser(
        XivoCtiUserWithProfile("Dupont", "Jean", "jdupont", "supervisor")
      )

      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/users")
          .withSession("username" -> "test")
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.toJson(
        List(
          Json.obj(
            "name"      -> "Dupond",
            "firstname" -> "Pierre",
            "login"     -> "pdupond",
            "profile"   -> "admin"
          ),
          Json.obj(
            "name"      -> "Dupont",
            "firstname" -> "Jean",
            "login"     -> "jdupont",
            "profile"   -> "supervisor"
          )
        )
      )
    }
  }
}
