package integration

import anorm.SQL
import docker.DockerPlayWithDbSpec
import model._
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class RightsSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE users, rights").execute()
      super.beforeEach()
    }

  val formatter: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  val context: String = playConfig("play.http.context")
  val rabbitPublisher: XivoRabbitEventsPublisher =
    mock[XivoRabbitEventsPublisher]

  override implicit lazy val app: Application = new GuiceApplicationBuilder()
    .configure(playConfigWithDatabase)
    .overrides(bind[XivoRabbitEventsPublisher].to(rabbitPublisher))
    .build()

  "The Rights Controller" should {
    "return the rights for a user" in withConnection() { implicit c =>
      Utils.insertUser("login1", "admin")

      val res = route(
        app,
        FakeRequest(GET, context + "/api/1.0/rights/user/login1")
          .withSession()
      ).get

      status(res) shouldEqual OK
      contentAsJson(res) shouldEqual Json.obj(
        "type" -> "admin",
        "data" -> Json.obj()
      )
    }

    "return 404 if there is no right for this user" in withConnection() {
      implicit c =>
        val res = route(
          app,
          FakeRequest(GET, context + "/api/1.0/rights/user/login1")
            .withSession()
        ).get

        status(res) shouldEqual NOT_FOUND
    }

    "create a new right" in withConnection() { implicit c =>
      val res = route(
        app,
        FakeRequest(POST, context + "/api/1.0/rights/user/login1")
          .withJsonBody(Json.obj("type" -> "admin", "data" -> Json.obj()))
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
      ).get

      status(res) shouldEqual OK
      val rightManager = app.injector.instanceOf(classOf[RightManagerImpl])
      rightManager.forUser("login1") should be(Some(AdminRight()))
    }

    "delete a right" in withConnection() { implicit c =>
      Utils.insertUser("login1", "admin")

      val res = route(
        app,
        FakeRequest(DELETE, context + "/api/1.0/rights/user/login1")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
      ).get

      status(res) shouldEqual OK
    }

    "check the user's right before doing modifications" in withConnection() {
      implicit c =>
        Utils.insertUser(
          "login1",
          "teacher",
          Some(new DateTime()),
          Some(new DateTime())
        )
        Utils.insertUser("login2", "supervisor")

        status(
          route(
            app,
            FakeRequest(POST, context + "/api/1.0/rights/user/login1")
              .withJsonBody(Json.obj("type" -> "admin"))
              .withSession("username" -> "login2")
          ).get
        ) shouldEqual UNAUTHORIZED

        status(
          route(
            app,
            FakeRequest(POST, context + "/api/1.0/rights/user/login1")
              .withJsonBody(Json.obj("type" -> "admin"))
              .withSession("username" -> "test", "isSuperAdmin" -> "true")
          ).get
        ) shouldEqual OK
    }

    "check the user's right before deleting" in withConnection() { implicit c =>
      Utils.insertUser(
        "login1",
        "teacher",
        Some(new DateTime()),
        Some(new DateTime())
      )
      Utils.insertUser("login2", "supervisor")

      status(
        route(
          app,
          FakeRequest(DELETE, context + "/api/1.0/rights/user/login1")
            .withSession("username" -> "login2")
        ).get
      ) shouldEqual OK
      status(
        route(
          app,
          FakeRequest(DELETE, context + "/api/1.0/rights/user/login2")
            .withSession("username" -> "login2")
        ).get
      ) shouldEqual UNAUTHORIZED
      status(
        route(
          app,
          FakeRequest(DELETE, context + "/api/1.0/rights/user/login2")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
        ).get
      ) shouldEqual OK
    }

    "modify teacher right by supervisor" in withConnection() { implicit c =>
      Utils.insertUser("test", "supervisor")
      Utils.insertUser(
        "login1",
        "teacher",
        Some(formatter.parseDateTime("2014-06-02 00:00:00")),
        Some(formatter.parseDateTime("2014-09-12 23:59:59"))
      )

      val res = route(
        app,
        FakeRequest(POST, context + "/api/1.0/rights/user/login1")
          .withJsonBody(
            Json.obj(
              "type" -> "teacher",
              "data" ->
                Json.obj(
                  "queueIds"  -> "[]",
                  "groupIds"  -> "[]]",
                  "incallIds" -> "[]",
                  "start"     -> "2014-06-02",
                  "end"       -> "2014-09-12"
                )
            )
          )
          .withSession("username" -> "test", "isSuperAdmin" -> "false")
      ).get

      status(res) shouldEqual OK
      val rightManager = app.injector.instanceOf(classOf[RightManagerImpl])
      rightManager.forUser("login1") should be(
        Some(TeacherRight(List(), List(), List(), "2014-06-02", "2014-09-12"))
      )
    }
  }
}
