CREATE SCHEMA xc;

CREATE TABLE xc.users (
  id INTEGER NOT NULL PRIMARY KEY,
  login VARCHAR(64) NOT NULL,
  first_name VARCHAR(128) NOT NULL,
  last_name VARCHAR(128) NOT NULL
);

CREATE TABLE xc.queues (
  id INTEGER NOT NULL PRIMARY KEY,
  name VARCHAR(128) NOT NULL,
  display_name VARCHAR(128) NOT NULL,
  number VARCHAR(128) NOT NULL
);

CREATE TABLE xc.queue_members_default (
  user_id INTEGER REFERENCES xc.users(id),
  queue_id INTEGER REFERENCES xc.queues(id),
  penalty INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY(user_id, queue_id)
);