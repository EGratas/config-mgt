package controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import model.{AdminRight, RightManager}
import models.AuthenticatedUser
import models.authentication.AuthenticationProvider
import org.mockito.Mockito.stub
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.http.Status.SEE_OTHER
import play.api.mvc.{AnyContentAsFormUrlEncoded, Request}
import play.api.test.Helpers.{POST, _}
import play.api.test._

class LoginSpec
    extends DockerPlaySpec
    with BeforeAndAfterEach
    with MockitoSugar {

  def withMocks(
      test: (RightManager, AuthenticationProvider, Login) => Any
  ): Unit = {
    implicit val materializer: Materializer = app.materializer
    val rightManagerMock                    = mock[RightManager]
    val authProviderMock                    = mock[AuthenticationProvider]

    val controller = new Login(rightManagerMock, authProviderMock)
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(rightManagerMock, authProviderMock, controller)
    ()
  }

  "The Login controller" should {
    "should authenticate user" in withMocks {
      (rightManagerMock, authProviderMock, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val user = AuthenticatedUser("test", false)
          stub(rightManagerMock.forUser("john"))
            .toReturn(Some(mock[AdminRight]))
          stub(authProviderMock.authenticate("john", "doe"))
            .toReturn(Some(user))

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "doe")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual SEE_OTHER
          session(res).get("isSuperAdmin") shouldBe None
        }
    }

    "should authenticate super admin user" in withMocks {
      (rightManagerMock, authProviderMock, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val user = AuthenticatedUser("test", true)
          stub(rightManagerMock.forUser("john"))
            .toReturn(Some(mock[AdminRight]))
          stub(authProviderMock.authenticate("john", "doe"))
            .toReturn(Some(user))

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "doe")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual SEE_OTHER
          session(res).get("isSuperAdmin").get shouldEqual "true"
        }
    }

    "should not authenticate user" in withMocks {
      (rightManagerMock, authProviderMock, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          stub(rightManagerMock.forUser("john"))
            .toReturn(Some(mock[AdminRight]))
          stub(authProviderMock.authenticate("john", "wrongPass"))
            .toReturn(None)

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "wrongPass")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual BAD_REQUEST
        }
    }

    "should not authenticate user without rights" in withMocks {
      (rightManagerMock, authProviderMock, controller) =>
        {
          implicit val materializer: Materializer = app.materializer

          val user = AuthenticatedUser("test", false)
          stub(rightManagerMock.forUser("john")).toReturn(None)
          stub(authProviderMock.authenticate("john", "doe"))
            .toReturn(Some(user))

          val req: Request[AnyContentAsFormUrlEncoded] =
            FakeRequest(POST, "/login")
              .withFormUrlEncodedBody("login" -> "john", "mdp" -> "doe")

          val res = call(controller.authenticate, req)

          status(res) shouldEqual BAD_REQUEST
        }
    }
  }
}
