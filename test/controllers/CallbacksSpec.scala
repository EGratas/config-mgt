package controllers

import akka.stream.Materializer
import docker.DockerPlaySpec
import model._
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, LocalTime}
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.libs.json.Json
import play.api.test.Helpers._
import play.api.test.{FakeRequest, Helpers}

import java.security.InvalidParameterException
import java.util.UUID

class CallbacksSpec
    extends DockerPlaySpec
    with BeforeAndAfterEach
    with MockitoSugar {

  def withMocks(test: (CallbackMocks, Callbacks) => Any): Unit = {
    implicit val materializer: Materializer = app.materializer
    val mocks                               = new CallbackMocks
    val controller = new Callbacks(
      mocks.callbackListManager,
      mocks.callbackRequestManager,
      mocks.preferredCallbackPeriodManager,
      app.injector.instanceOf[controllers.Secured]
    )
    controller.setControllerComponents(
      Helpers.stubControllerComponents(bodyParser =
        stubPlayBodyParsers.anyContent
      )
    )
    test(mocks, controller)
    ()
  }

  class CallbackMocks {
    val callbackListManager            = mock[CallbackListManager]
    val callbackRequestManager         = mock[CallbackRequestManager]
    val preferredCallbackPeriodManager = mock[PreferredCallbackPeriodManager]
  }

  "The Callbacks controller" should {
    "list callbacks" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid1                               = UUID.randomUUID()
        val uuid2                               = UUID.randomUUID()
        val cb1 = CallbackRequest(
          Some(UUID.randomUUID()),
          uuid1,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None
        )
        val cb2 = CallbackRequest(
          Some(UUID.randomUUID()),
          uuid1,
          None,
          Some("7200"),
          None,
          None,
          Some("Comp"),
          Some("foo bar")
        )
        val l1 = CallbackList(Some(uuid1), "first list", 1, List(cb1, cb2))
        val l2 = CallbackList(Some(uuid2), "second list", 2, List(cb1, cb2))

        stub(mocks.callbackListManager.all(true)).toReturn(List(l1, l2))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.lists(true), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackListManager).all(true)
        contentAsJson(res) shouldEqual Json.toJson(List(l1, l2))
      }
    }

    "get lists without their requests" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid1                               = UUID.randomUUID()
        val uuid2                               = UUID.randomUUID()
        val l1                                  = CallbackList(Some(uuid1), "first list", 1, List())
        val l2                                  = CallbackList(Some(uuid2), "second list", 2, List())

        stub(mocks.callbackListManager.all(false)).toReturn(List(l1, l2))

        val rq = FakeRequest(GET, "/").withSession(
          "username"     -> "test",
          "isSuperAdmin" -> "true"
        )
        val res = call(controller.lists(false), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackListManager).all(false)
        contentAsJson(res) shouldEqual Json.toJson(List(l1, l2))
      }
    }

    "create callbacks from CSV" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID().toString
        val csv                                 = "the csv"

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withHeaders("Content-Type" -> "text/plain;charset=utf-8")
          .withBody(csv)

        val res = call(controller.importCsv(uuid), rq)

        status(res) shouldEqual CREATED
        verify(mocks.callbackRequestManager).createFromCsv(
          mocks.callbackListManager,
          uuid,
          csv
        )
      }
    }

    "give a callback request to an agent" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID().toString
        val agentId                             = 12L

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withBody(Json.obj("agentId" -> agentId))

        val res = call(controller.takeCallback(uuid), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager)
          .takeWithAgent(uuid.toString, agentId)
      }
    }

    "release a callback" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID().toString

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.releaseCallback(uuid), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager).release(uuid)
      }
    }

    "get all pending callbacks for an agent" in withMocks {
      (mocks, controller) =>
        {
          implicit val materializer: Materializer = app.materializer
          val agentId: Long                       = 12
          val uuid                                = UUID.randomUUID()
          val ids                                 = List(uuid)

          stub(mocks.callbackRequestManager.takenCallbacks(agentId))
            .toReturn(ids)

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")

          val res = call(controller.getTakenCallbacks(agentId), rq)

          status(res) shouldEqual OK
          verify(mocks.callbackRequestManager).takenCallbacks(agentId)
          contentAsJson(res) shouldEqual Json.obj("ids" -> Json.toJson(ids))
        }
    }

    "retrieve a callback" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()
        val listUuid                            = UUID.randomUUID()
        val cb = CallbackRequest(
          Some(uuid),
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12),
          queueId = Some(3)
        )

        stub(mocks.callbackRequestManager.getCallback(uuid.toString))
          .toReturn(cb)

        val rq = FakeRequest(GET, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.getCallback(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager).getCallback(uuid.toString)
        contentAsJson(res) shouldEqual Json.toJson(cb)
      }
    }

    "find a callback" in withMocks { (mocks, controller) =>
      {
        import CallbackRequest.{writesFind, writesResp}
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()
        val listUuid                            = UUID.randomUUID()
        val cb = CallbackRequest(
          Some(uuid),
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12),
          queueId = Some(3)
        )
        val resp = FindCallbackResponse(1, List(cb))
        val f = FindCallbackRequest(
          List(DynamicFilter("uuid", Some(OperatorEq), Some(uuid.toString))),
          0,
          10
        )
        stub(mocks.callbackRequestManager.find(f.filters, f.offset, f.limit))
          .toReturn(resp)

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(Json.toJson(f))

        val res = call(controller.findCallback(), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager).find(f.filters, f.offset, f.limit)
        contentAsJson(res) shouldEqual Json.toJson(resp)
      }
    }

    "create a callback list" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val decodedList                         = CallbackList(None, "Test", 12, List())
        val newList = CallbackList(
          Some(UUID.randomUUID()),
          decodedList.name,
          decodedList.queueId,
          List()
        )
        stub(mocks.callbackListManager.create(decodedList)).toReturn(newList)

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(
            Json
              .obj("name" -> decodedList.name, "queueId" -> decodedList.queueId)
          )

        val res = call(controller.createList(), rq)

        status(res) shouldEqual CREATED
        contentAsJson(res) shouldEqual Json.toJson(newList)
      }
    }

    "delete a callback list" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()

        val rq = FakeRequest(DELETE, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.deleteList(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackListManager).delete(uuid.toString)
      }
    }

    "cloture a callback request" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.clotureCallback(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager).cloture(uuid.toString)
      }
    }

    "uncloture a callback request" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.unclotureCallback(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager).uncloture(uuid.toString)
      }
    }

    "reschedule a callback request" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val dateFormat                          = DateTimeFormat.forPattern("yyyy-MM-dd")
        val uuid                                = UUID.randomUUID()
        val periodUuid                          = UUID.randomUUID().toString

        val json = Json.obj(
          "dueDate"    -> "2016-08-09",
          "periodUuid" -> periodUuid
        )

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)

        val res = call(controller.rescheduleCallback(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.callbackRequestManager).reschedule(
          uuid.toString,
          DateTime.parse("2016-08-09", dateFormat),
          periodUuid
        )
      }
    }

    "create a single callback request in a list" in withMocks {
      (mocks, controller) =>
        {
          implicit val materializer: Materializer = app.materializer
          val listUuid                            = UUID.randomUUID()
          val uuid                                = UUID.randomUUID()
          val json = Json.obj(
            "phoneNumber" -> "0236547896",
            "firstName"   -> "John"
          )

          val request = CallbackRequest(
            None,
            listUuid,
            Some("0236547896"),
            None,
            Some("John"),
            None,
            None,
            None
          )
          stub(mocks.callbackRequestManager.fromJson(listUuid.toString, json))
            .toReturn(request)
          stub(mocks.callbackRequestManager.create(request))
            .toReturn(request.copy(uuid = Some(uuid)))

          val rq = FakeRequest(POST, "/")
            .withSession("username" -> "test", "isSuperAdmin" -> "true")
            .withJsonBody(json)

          val res =
            call(controller.createCallbackRequest(listUuid.toString), rq)

          status(res) shouldEqual CREATED
          contentAsJson(res).validate[CallbackRequest].get shouldEqual request
            .copy(uuid = Some(uuid))
        }
    }

    "list the existing callback periods" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()
        val period = PreferredCallbackPeriod(
          Some(uuid),
          "My Period",
          new LocalTime(9, 0, 0),
          new LocalTime(12, 0, 0),
          true
        )
        stub(mocks.preferredCallbackPeriodManager.getPeriods)
          .toReturn(List(period))

        val rq = FakeRequest(GET, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.getCallbackPeriods(), rq)

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(
            Json.obj(
              "uuid"        -> uuid.toString,
              "name"        -> "My Period",
              "periodStart" -> "09:00:00",
              "periodEnd"   -> "12:00:00",
              "default"     -> true
            )
          )
        )
      }
    }

    "filter callback periods by name" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()
        val periodName                          = "My Period"
        val period = PreferredCallbackPeriod(
          Some(uuid),
          "My Period",
          new LocalTime(9, 0, 0),
          new LocalTime(12, 0, 0),
          true
        )
        stub(mocks.preferredCallbackPeriodManager.getPeriodsByName(periodName))
          .toReturn(List(period))

        val rq = FakeRequest(GET, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.getCallbackPeriods(Some(periodName)), rq)

        status(res) shouldEqual OK
        contentAsJson(res) shouldEqual Json.toJson(
          List(
            Json.obj(
              "uuid"        -> uuid.toString,
              "name"        -> "My Period",
              "periodStart" -> "09:00:00",
              "periodEnd"   -> "12:00:00",
              "default"     -> true
            )
          )
        )
      }
    }

    "create a new callback period" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val period = PreferredCallbackPeriod(
          None,
          "the period",
          new LocalTime(8, 0, 0),
          new LocalTime(12, 30, 0),
          true
        )
        val json = Json.obj(
          "name"        -> period.name,
          "periodStart" -> period.periodStart.toString("hh:mm:ss"),
          "periodEnd"   -> period.periodEnd.toString("hh:mm:ss"),
          "default"     -> period.default
        )

        val uuid = UUID.randomUUID()
        stub(mocks.preferredCallbackPeriodManager.createPeriod(period))
          .toReturn(period.copy(uuid = Some(uuid)))

        val rq = FakeRequest(POST, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)

        val res = call(controller.createCallbackPeriod, rq)

        status(res) shouldEqual CREATED
        contentAsJson(res) shouldEqual Json.obj(
          "uuid"        -> uuid.toString,
          "name"        -> period.name,
          "periodStart" -> period.periodStart.toString("hh:mm:ss"),
          "periodEnd"   -> period.periodEnd.toString("hh:mm:ss"),
          "default"     -> period.default
        )
      }
    }

    "delete a callback period" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()

        val rq = FakeRequest(DELETE, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")

        val res = call(controller.deleteCallbackPeriod(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.preferredCallbackPeriodManager).deletePeriod(uuid.toString)
      }
    }

    "edit a callback period" in withMocks { (mocks, controller) =>
      {
        implicit val materializer: Materializer = app.materializer
        val uuid                                = UUID.randomUUID()
        val period = PreferredCallbackPeriod(
          Some(uuid),
          "the period",
          new LocalTime(8, 0, 0),
          new LocalTime(12, 30, 0),
          true
        )
        val json = Json.obj(
          "uuid"        -> period.uuid,
          "name"        -> period.name,
          "periodStart" -> period.periodStart.toString("hh:mm:ss"),
          "periodEnd"   -> period.periodEnd.toString("hh:mm:ss"),
          "default"     -> period.default
        )

        val rq = FakeRequest(PUT, "/")
          .withSession("username" -> "test", "isSuperAdmin" -> "true")
          .withJsonBody(json)

        val res = call(controller.editCallbackPeriod(uuid.toString), rq)

        status(res) shouldEqual OK
        verify(mocks.preferredCallbackPeriodManager).edit(period)
      }
    }

    "return BadRequest when InvalidParameterException is thrown" in withMocks {
      (mocks, controller) =>
        {
          controller
            .WithExceptionCatching(
              throw new InvalidParameterException("My message")
            )
            .header
            .status shouldEqual BAD_REQUEST
        }
    }

    "return NotFound when NoSuchElementException is thrown" in withMocks {
      (mocks, controller) =>
        {
          controller
            .WithExceptionCatching(
              throw new NoSuchElementException("My message")
            )
            .header
            .status shouldEqual NOT_FOUND
        }
    }

    "return InternalServerError when NoSuchElementException is thrown" in withMocks {
      (mocks, controller) =>
        {
          controller
            .WithExceptionCatching(throw new Exception("My message"))
            .header
            .status shouldEqual INTERNAL_SERVER_ERROR
        }
    }
  }

}
