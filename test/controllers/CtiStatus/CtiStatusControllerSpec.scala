package controllers.CtiStatus
import akka.stream.Materializer
import controllers.Secured
import cti.controllers.CtiStatusController
import cti.models.{CtiStatus, CtiStatusActionLegacy, CtiStatusLegacy}
import docker.DockerPlaySpec
import org.mockito.Mockito.{verify, when}
import play.api.http.Status._
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers.{
  call,
  contentAsJson,
  defaultAwaitTimeout,
  status,
  writeableOf_AnyContentAsEmpty,
  GET
}
import xivo.service.CtiStatusManager

import scala.util.Try

class CtiStatusControllerSpec extends DockerPlaySpec {

  class Helper {
    implicit lazy val materializer: Materializer = app.materializer
    val ctiStatusManager: CtiStatusManager       = mock[CtiStatusManager]

    val result: Map[String, List[CtiStatus]] = Map[String, List[CtiStatus]](
      "1" -> List(
        CtiStatus(Option("toto"), Option("toto"), Option(1)),
        CtiStatus(Option("test"), Option("test"), Option(0))
      ),
      "2" -> List(
        CtiStatus(Option("toto"), Option("toto"), Option(1)),
        CtiStatus(Option("test"), Option("test"), Option(0))
      ),
      "3" -> List(
        CtiStatus(Option("toto"), Option("toto"), Option(1)),
        CtiStatus(Option("test"), Option("test"), Option(0))
      ),
      "4" -> List(
        CtiStatus(Option("tata"), Option("tata"), Option(1)),
        CtiStatus(Option("titi"), Option("titi"), Option(0))
      )
    )
    val resultLegacy: Map[String, List[CtiStatusLegacy]] =
      Map[String, List[CtiStatusLegacy]](
        "1" -> List(
          CtiStatusLegacy(
            Option("test"),
            Option("test"),
            Option("red"),
            List(CtiStatusActionLegacy("queueunpause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("toto"),
            Option("toto"),
            Option("red"),
            List(CtiStatusActionLegacy("queuepause_all", Option("")))
          )
        ),
        "4" -> List(
          CtiStatusLegacy(
            Option("titi"),
            Option("titi"),
            Option("red"),
            List()
          ),
          CtiStatusLegacy(
            Option("tata"),
            Option("tata"),
            Option("red"),
            List(
              CtiStatusActionLegacy("enablednd", Option("true")),
              CtiStatusActionLegacy("queuepause_all", Option("true"))
            )
          )
        ),
        "2" -> List(
          CtiStatusLegacy(
            Option("test"),
            Option("test"),
            Option("red"),
            List(CtiStatusActionLegacy("queueunpause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("toto"),
            Option("toto"),
            Option("red"),
            List(CtiStatusActionLegacy("queuepause_all", Option("")))
          )
        ),
        "3" -> List(
          CtiStatusLegacy(
            Option("test"),
            Option("test"),
            Option("red"),
            List(CtiStatusActionLegacy("queueunpause_all", Option("")))
          ),
          CtiStatusLegacy(
            Option("toto"),
            Option("toto"),
            Option("red"),
            List(CtiStatusActionLegacy("queuepause_all", Option("")))
          )
        )
      )

    val ctiStatusJson: String =
      """
        {
        "1": [
          {
            "name": "toto",
            "displayName": "toto",
            "status": 1
          },
          {
            "name": "test",
            "displayName": "test",
            "status": 0
          }
        ],
        "2": [
          {
            "name": "toto",
            "displayName": "toto",
            "status": 1
          },
          {
            "name": "test",
            "displayName": "test",
            "status": 0
          }
        ],
        "3": [
          {
            "name": "toto",
            "displayName": "toto",
            "status": 1
          },
          {
            "name": "test",
            "displayName": "test",
            "status": 0
          }
        ],
        "4": [
          {
            "name": "tata",
            "displayName": "tata",
            "status": 1
          },
          {
            "name": "titi",
            "displayName": "titi",
            "status": 0
          }
        ]
      }
        """.stripMargin
    val ctiStatusLegacyJson: String =
      """
        {
        "1": [
          {
            "name": "test",
            "longName": "test",
            "color": "red",
            "actions": [
              {
                "name": "queueunpause_all",
                "parameters": ""
              }
            ]
          },
          {
            "name": "toto",
            "longName": "toto",
            "color": "red",
            "actions": [
              {
                "name": "queuepause_all",
                "parameters": ""
              }
            ]
          }
        ],
        "4": [
          {
            "name": "titi",
            "longName": "titi",
            "color": "red",
            "actions": []
          },
          {
            "name": "tata",
            "longName": "tata",
            "color": "red",
            "actions": [
              {
                "name": "enablednd",
                "parameters": "true"
              },
              {
                "name": "queuepause_all",
                "parameters": "true"
              }
            ]
          }
        ],
        "2": [
          {
            "name": "test",
            "longName": "test",
            "color": "red",
            "actions": [
              {
                "name": "queueunpause_all",
                "parameters": ""
              }
            ]
          },
          {
            "name": "toto",
            "longName": "toto",
            "color": "red",
            "actions": [
              {
                "name": "queuepause_all",
                "parameters": ""
              }
            ]
          }
        ],
        "3": [
          {
            "name": "test",
            "longName": "test",
            "color": "red",
            "actions": [
              {
                "name": "queueunpause_all",
                "parameters": ""
              }
            ]
          },
          {
            "name": "toto",
            "longName": "toto",
            "color": "red",
            "actions": [
              {
                "name": "queuepause_all",
                "parameters": ""
              }
            ]
          }
        ]
      }
        """.stripMargin

    def getController: CtiStatusController = {
      new CtiStatusController(
        ctiStatusManager = ctiStatusManager,
        secured = app.injector.instanceOf[Secured]
      )
    }
  }

  "CtiStatusController" should {

    "return 200 OK when retrieving CtiStatuses" in new Helper {
      val controller: CtiStatusController = getController

      when(ctiStatusManager.get).thenReturn(Try(result))

      val rq = FakeRequest(GET, s"/api/2.0/internal/cti/ctistatus").withSession(
        "username"     -> "bwillis",
        "isSuperAdmin" -> "true"
      )
      val res = call(controller.get(), rq)
      status(res) shouldEqual OK
      verify(ctiStatusManager).get
      contentAsJson(res) shouldEqual Json.parse(s"$ctiStatusJson")
    }
    "return 200 OK when retrieving CtiStatusesLegacy" in new Helper {
      val controller: CtiStatusController = getController

      when(ctiStatusManager.getLegacy).thenReturn(Try(resultLegacy))

      val rq =
        FakeRequest(GET, s"/api/2.0/internal/cti/ctistatus/legacy").withSession(
          "username"     -> "bwillis",
          "isSuperAdmin" -> "true"
        )
      val res = call(controller.getLegacy(), rq)
      status(res) shouldEqual OK
      verify(ctiStatusManager).getLegacy
      contentAsJson(res) shouldEqual Json.parse(s"$ctiStatusLegacyJson")
    }
  }

}
