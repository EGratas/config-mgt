package testutils

import org.scalatest.OptionValues
import org.scalatestplus.play.WsScalaTestClient
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

abstract class PlayShouldSpec
    extends AnyWordSpec
    with Matchers
    with OptionValues
    with WsScalaTestClient {}
