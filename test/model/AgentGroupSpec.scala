package model

import anorm.SQL
import docker.DockerPlayWithDbSpec

class AgentGroupSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE agentgroup").execute()
      super.beforeEach()
    }

  "The AgentGroup singleton" should {

    "retrieve all agent groups" in {
      withConnection() { implicit c =>
        Utils.insertAgentGroup(1, "default")
        Utils.insertAgentGroup(2, "autre")

        val agentGroupManager =
          app.injector.instanceOf(classOf[AgentGroupManagerImpl])
        agentGroupManager.all() shouldEqual List(
          AgentGroup(Some(1), "default"),
          AgentGroup(Some(2), "autre")
        )
      }
    }

    "do not retrieve deleted agent groups" in {
      withConnection() { implicit c =>
        Utils.insertAgentGroup(1, "default")
        Utils.insertAgentGroup(2, "autre", deleted = true)

        val agentGroupManager =
          app.injector.instanceOf(classOf[AgentGroupManagerImpl])
        agentGroupManager.all() shouldEqual List(AgentGroup(Some(1), "default"))
      }
    }

    "retrieve only a few groups" in {
      withConnection() { implicit c =>
        Utils.insertAgentGroup(1, "default")
        Utils.insertAgentGroup(2, "autre")

        val agentGroupManager =
          app.injector.instanceOf(classOf[AgentGroupManagerImpl])
        agentGroupManager.withIds(List(1)) shouldEqual List(
          AgentGroup(Some(1), "default")
        )
      }
    }

    "not fail with an empty list" in {
      val agentGroupManager =
        app.injector.instanceOf(classOf[AgentGroupManagerImpl])
      agentGroupManager.withIds(List()) shouldEqual List()
    }
  }
}
