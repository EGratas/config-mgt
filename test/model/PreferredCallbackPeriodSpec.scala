package model

import anorm.SQL
import docker.DockerPlayWithDbSpec
import org.joda.time.LocalTime

import java.security.InvalidParameterException
import java.util.UUID

class PreferredCallbackPeriodSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE preferred_callback_period, callback_request"
      ).execute()

      super.beforeEach()
    }

  "The PreferredCallbackPeriod singleton" should {

    "list periods" in withConnection() { implicit c =>
      val p1 = PreferredCallbackPeriod(
        None,
        "period 1",
        new LocalTime(9, 30, 0),
        new LocalTime(12, 0, 0),
        true
      )
      val p2 = PreferredCallbackPeriod(
        None,
        "period 2",
        new LocalTime(14, 0, 0),
        new LocalTime(17, 0, 0),
        false
      )
      val uuid1 = Utils.insertPeriod(p1)
      val uuid2 = Utils.insertPeriod(p2)

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      preferredCallbackPeriodManager.getPeriods should contain.only(
        p1.copy(uuid = Some(uuid1)),
        p2.copy(uuid = Some(uuid2))
      )
    }

    "filter periods by name" in withConnection() { implicit c =>
      val p1 = PreferredCallbackPeriod(
        None,
        "period 1",
        new LocalTime(9, 30, 0),
        new LocalTime(12, 0, 0),
        true
      )
      val uuid1 = Utils.insertPeriod(p1)

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      preferredCallbackPeriodManager.getPeriodsByName(
        "period 1"
      ) should contain only (p1.copy(uuid = Some(uuid1)))
    }

    "return true if the period exists" in withConnection() { implicit c =>
      val uuid = Utils.insertPeriod(
        PreferredCallbackPeriod(
          None,
          "Test",
          new LocalTime(9, 0, 0),
          new LocalTime(12, 0, 0),
          true
        )
      )

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      preferredCallbackPeriodManager.periodExists(uuid) should be(true)
    }

    "return false if the period does not exist" in {

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      preferredCallbackPeriodManager.periodExists(UUID.randomUUID()) should be(
        false
      )
    }

    "return the default period" in withConnection() { implicit c =>
      val p1 = PreferredCallbackPeriod(
        None,
        "period 1",
        new LocalTime(9, 30, 0),
        new LocalTime(12, 0, 0),
        true
      )
      val uuid1 = Utils.insertPeriod(p1)

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      preferredCallbackPeriodManager.getDefault shouldEqual Some(
        p1.copy(uuid = Some(uuid1))
      )
    }

    "return None if there is no default period" in withConnection() {
      implicit c =>
        Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "period 1",
            new LocalTime(9, 30, 0),
            new LocalTime(12, 0, 0),
            false
          )
        )
        Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "period 2",
            new LocalTime(14, 0, 0),
            new LocalTime(17, 0, 0),
            false
          )
        )

        val preferredCallbackPeriodManager =
          app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
        preferredCallbackPeriodManager.getDefault shouldEqual None
    }

    "get a period by uuid" in withConnection() { implicit c =>
      val p2 = PreferredCallbackPeriod(
        None,
        "period 2",
        new LocalTime(14, 0, 0),
        new LocalTime(17, 0, 0),
        false
      )
      val uuid2 = Utils.insertPeriod(p2)

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      preferredCallbackPeriodManager.getPeriod(uuid2) shouldEqual Some(
        p2.copy(uuid = Some(uuid2))
      )
    }

    "create a period" in {
      val p = PreferredCallbackPeriod(
        None,
        "test",
        new LocalTime(8, 0, 0),
        new LocalTime(10, 0, 0),
        true
      )

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      val res = preferredCallbackPeriodManager.createPeriod(p)

      res.uuid.isEmpty should be(false)
      res.copy(uuid = None) shouldEqual p
      preferredCallbackPeriodManager.getPeriod(res.uuid.get) shouldEqual Some(
        res
      )
    }

    "disable default for the other periods if the created one is the default" in withConnection() {
      implicit c =>
        val uuid1 = Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "test1",
            new LocalTime(),
            new LocalTime(),
            true
          )
        )

        val preferredCallbackPeriodManager =
          app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

        val created = preferredCallbackPeriodManager.createPeriod(
          PreferredCallbackPeriod(
            None,
            "test2",
            new LocalTime(),
            new LocalTime(),
            true
          )
        )

        preferredCallbackPeriodManager.getPeriod(uuid1).get.default should be(
          false
        )
        preferredCallbackPeriodManager.getDefault shouldEqual Some(created)
    }

    "delete a callback period" in withConnection() { implicit c =>
      Utils.insertPeriod(
        PreferredCallbackPeriod(
          None,
          "default",
          new LocalTime(),
          new LocalTime(),
          true
        )
      )
      val uuid = Utils.insertPeriod(
        PreferredCallbackPeriod(
          None,
          "test1",
          new LocalTime(),
          new LocalTime(),
          false
        )
      )

      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      preferredCallbackPeriodManager.deletePeriod(uuid.toString)

      preferredCallbackPeriodManager.getPeriod(uuid) shouldEqual None
    }

    "refuse to delete the default callback period" in withConnection() {
      implicit c =>
        val uuid = Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "test1",
            new LocalTime(),
            new LocalTime(),
            true
          )
        )
        val preferredCallbackPeriodManager =
          app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

        an[
          InvalidParameterException
        ] should be thrownBy preferredCallbackPeriodManager.deletePeriod(
          uuid.toString
        )
    }

    "throw NoSuchElementException if the deleted period does not exist" in {
      val uuid = UUID.randomUUID()
      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

      an[
        NoSuchElementException
      ] should be thrownBy preferredCallbackPeriodManager
        .deletePeriod(uuid.toString)
    }

    "move the requests associated to the provided period to the default callback period" in {
      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackListManager])
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])

      val list =
        callbackListManager.create(CallbackList(None, "test list", 3, List()))
      val default = preferredCallbackPeriodManager.createPeriod(
        PreferredCallbackPeriod(
          None,
          "test1",
          new LocalTime(8, 0, 0),
          new LocalTime(18, 0, 0),
          true
        )
      )
      val p = preferredCallbackPeriodManager.createPeriod(
        PreferredCallbackPeriod(
          None,
          "test2",
          new LocalTime(8, 0, 0),
          new LocalTime(12, 0, 0),
          false
        )
      )
      val rq = callbackRequestManager.create(
        CallbackRequest(
          None,
          list.uuid.get,
          Some("2000"),
          None,
          None,
          None,
          None,
          None,
          preferredPeriodUuid = p.uuid
        )
      )

      preferredCallbackPeriodManager.deletePeriod(p.uuid.get.toString)

      callbackRequestManager
        .getCallback(rq.uuid.get.toString) shouldEqual rq.copy(
        preferredPeriodUuid = default.uuid,
        preferredPeriod = Some(default),
        queueId = Some(3)
      )
      preferredCallbackPeriodManager.getPeriod(p.uuid.get) shouldEqual None
    }

    "edit a callback period" in {
      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      val p = preferredCallbackPeriodManager.createPeriod(
        PreferredCallbackPeriod(
          None,
          "test2",
          new LocalTime(8, 0, 0),
          new LocalTime(12, 0, 0),
          false
        )
      )
      val p2 = PreferredCallbackPeriod(
        p.uuid,
        "test3",
        new LocalTime(9, 0, 0),
        new LocalTime(12, 30, 0),
        false
      )

      preferredCallbackPeriodManager.edit(p2)

      preferredCallbackPeriodManager.getPeriod(p.uuid.get) shouldEqual Some(p2)
    }

    "throw a NoSuchElementException if the edited period does not exist" in {
      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      val p = PreferredCallbackPeriod(
        Some(UUID.randomUUID()),
        "test3",
        new LocalTime(9, 0, 0),
        new LocalTime(12, 30, 0),
        false
      )

      an[
        NoSuchElementException
      ] should be thrownBy preferredCallbackPeriodManager
        .edit(p)
    }

    "unset default for the other periods if the update sets the default flag" in {
      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      val default = preferredCallbackPeriodManager.createPeriod(
        PreferredCallbackPeriod(
          None,
          "test1",
          new LocalTime(8, 0, 0),
          new LocalTime(18, 0, 0),
          true
        )
      )
      val p = preferredCallbackPeriodManager.createPeriod(
        PreferredCallbackPeriod(
          None,
          "test2",
          new LocalTime(8, 0, 0),
          new LocalTime(12, 0, 0),
          false
        )
      )
      val p2 = PreferredCallbackPeriod(
        p.uuid,
        "test3",
        new LocalTime(8, 0, 0),
        new LocalTime(12, 0, 0),
        true
      )

      preferredCallbackPeriodManager.edit(p2)

      preferredCallbackPeriodManager
        .getPeriod(default.uuid.get)
        .get
        .default should be(false)
      preferredCallbackPeriodManager
        .getPeriod(p.uuid.get)
        .get
        .default should be(
        true
      )
    }

    "refuse to unset the default flag for the current default period" in {
      val preferredCallbackPeriodManager =
        app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])
      val p = preferredCallbackPeriodManager.createPeriod(
        PreferredCallbackPeriod(
          None,
          "test2",
          new LocalTime(8, 0, 0),
          new LocalTime(12, 0, 0),
          true
        )
      )
      val p2 = PreferredCallbackPeriod(
        p.uuid,
        "test3",
        new LocalTime(8, 0, 0),
        new LocalTime(12, 0, 0),
        false
      )

      an[
        InvalidParameterException
      ] should be thrownBy preferredCallbackPeriodManager.edit(p2)
    }
  }
}
