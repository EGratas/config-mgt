package model

import anorm.SQL
import docker.DockerPlayWithDbSpec

class QueueSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE queuefeatures"
      ).execute()

      super.beforeEach()
    }

  "The Queue singleton" should {
    "retrieve all queues" in withConnection() { implicit c =>
      Utils.insertQueueFeature(1, "queue1", "Queue 1")
      Utils.insertQueueFeature(2, "queue2", "Queue 2")

      val queueManager = app.injector.instanceOf(classOf[QueueManager])

      queueManager.all() should contain.only(
        Queue(Some(1), "queue1", "Queue 1"),
        Queue(Some(2), "queue2", "Queue 2")
      )
    }

    "retrieve only a few queues" in withConnection() { implicit c =>
      Utils.insertQueueFeature(1, "queue1")
      Utils.insertQueueFeature(2, "queue2")
      val queueManager = app.injector.instanceOf(classOf[QueueManager])

      queueManager.withIds(List(1)) should contain.only(
        Queue(Some(1), "queue1", "")
      )
    }

    "not fail with an empty list" in {

      val queueManager = app.injector.instanceOf(classOf[QueueManager])

      queueManager.withIds(List()) shouldBe empty
    }
  }
}
