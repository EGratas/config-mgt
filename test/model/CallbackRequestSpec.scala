package model

import anorm.SQL
import docker.DockerPlayWithDbSpec
import org.joda.time.{DateTime, LocalDate, LocalTime}
import play.api.libs.json.Json

import java.security.InvalidParameterException
import java.util.UUID

class CallbackRequestSpec extends DockerPlayWithDbSpec {

  val queueId: Long  = 3L
  var listUuid: UUID = UUID.randomUUID()
  var period: PreferredCallbackPeriod = PreferredCallbackPeriod(
    None,
    "The period",
    new LocalTime(9, 0, 0),
    new LocalTime(12, 0, 0),
    default = true
  )

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE queuefeatures, callback_list, callback_request, preferred_callback_period"
      ).execute()

      val _list = CallbackList(None, "The list", queueId, List())
      listUuid = Utils.insertCallbackList(_list)
      period = PreferredCallbackPeriod(
        None,
        "The period",
        new LocalTime(9, 0, 0),
        new LocalTime(12, 0, 0),
        true
      )
      period = period.copy(uuid = Some(Utils.insertPeriod(period)))
      super.beforeEach()
    }

  def findUncloturedByList(
      cbr: CallbackRequestManager,
      uuid: UUID
  ): List[CallbackRequest] = {
    val filters = List(
      DynamicFilter("listUuid", Some(OperatorEq), Some(uuid.toString)),
      DynamicFilter("clotured", Some(OperatorEq), Some("false"))
    )
    cbr.find(filters).list
  }

  "The CallbackRequest singleton" should {
    "write callback requests to JSON" in {
      val uuid  = UUID.randomUUID()
      val lUuid = UUID.randomUUID()
      val period = PreferredCallbackPeriod(
        Some(UUID.randomUUID()),
        "the period",
        new LocalTime(9, 0, 0),
        new LocalTime(12, 0, 0),
        true
      )
      val cb1 = CallbackRequest(
        Some(uuid),
        lUuid,
        Some("1000"),
        Some("7000"),
        Some("John"),
        Some("Doe"),
        Some("Company"),
        Some("The description"),
        agentId = Some(12),
        queueId = Some(3),
        preferredPeriod = Some(period),
        dueDate = new LocalDate(2015, 1, 10),
        voiceMessageRef = Some("655463464.52")
      )

      val cb2 = CallbackRequest(
        Some(uuid),
        lUuid,
        Some("1001"),
        Some("7001"),
        Some("Alice"),
        Some("Joy"),
        Some("Company"),
        Some("The description"),
        agentId = Some(12),
        queueId = Some(3),
        preferredPeriod = Some(period),
        dueDate = new LocalDate(2015, 1, 10)
      )

      Json.toJson(cb1) shouldEqual Json.obj(
        "uuid"              -> uuid.toString,
        "listUuid"          -> lUuid.toString,
        "phoneNumber"       -> "1000",
        "mobilePhoneNumber" -> "7000",
        "firstName"         -> "John",
        "lastName"          -> "Doe",
        "company"           -> "Company",
        "description"       -> "The description",
        "agentId"           -> 12,
        "queueId"           -> 3,
        "clotured"          -> false,
        "preferredPeriod"   -> period,
        "dueDate"           -> "2015-01-10",
        "voiceMessageRef"   -> "655463464.52"
      )

      Json.toJson(cb2) shouldEqual Json.obj(
        "uuid"              -> uuid.toString,
        "listUuid"          -> lUuid.toString,
        "phoneNumber"       -> "1001",
        "mobilePhoneNumber" -> "7001",
        "firstName"         -> "Alice",
        "lastName"          -> "Joy",
        "company"           -> "Company",
        "description"       -> "The description",
        "agentId"           -> 12,
        "queueId"           -> 3,
        "clotured"          -> false,
        "preferredPeriod"   -> period,
        "dueDate"           -> "2015-01-10"
      )
    }

    "extract requests from CSV and insert them" in {
      val csv =
        """phoneNumber|mobilePhoneNumber|firstName|lastName|company|description
          |1000||John|Doe||
          ||7200|||The Company|Test""".stripMargin

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])
      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackListManager])
      callbackRequestManager.createFromCsv(
        callbackListManager,
        listUuid.toString,
        csv
      )

      findUncloturedByList(callbackRequestManager, listUuid).map(
        _.copy(uuid = None)
      ) should contain.allOf(
        CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          queueId = Some(queueId),
          preferredPeriodUuid = period.uuid,
          preferredPeriod = Some(period)
        ),
        CallbackRequest(
          None,
          listUuid,
          None,
          Some("7200"),
          None,
          None,
          Some("The Company"),
          Some("Test"),
          queueId = Some(queueId),
          preferredPeriodUuid = period.uuid,
          preferredPeriod = Some(period)
        )
      )
    }

    "extract requests from CSV using comma as separator and insert them" in {
      val csv =
        """phoneNumber,mobilePhoneNumber,firstName,lastName,company,description
          |"1000","","John","Doe","",""
          |"","7200","","","The Company","Test""".stripMargin

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])
      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackListManager])
      callbackRequestManager.createFromCsv(
        callbackListManager,
        listUuid.toString,
        csv
      )

      findUncloturedByList(callbackRequestManager, listUuid).map(
        _.copy(uuid = None)
      ) should contain.allOf(
        CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          queueId = Some(queueId),
          preferredPeriodUuid = period.uuid,
          preferredPeriod = Some(period)
        ),
        CallbackRequest(
          None,
          listUuid,
          None,
          Some("7200"),
          None,
          None,
          Some("The Company"),
          Some("Test"),
          queueId = Some(queueId),
          preferredPeriodUuid = period.uuid,
          preferredPeriod = Some(period)
        )
      )
    }

    "extract requests from CSV and insert them with defined period" in withConnection() {
      implicit c =>
        val csv =
          """phoneNumber|mobilePhoneNumber|firstName|lastName|company|description|period
          |1000||John|Doe|||Morning period
          |1001||James|Bond|||Unhandled period
          |1002||Jason|Bourne|||
          ||7200|||The Company|Test|Afternoon period""".stripMargin

        Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "Default period",
            new LocalTime(9, 0, 0),
            new LocalTime(18, 0, 0),
            true
          )
        )
        val morningPeriodUuid = Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "Morning period",
            new LocalTime(9, 0, 0),
            new LocalTime(12, 0, 0),
            false
          )
        )
        val afternoonPeriodUuid = Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "Afternoon period",
            new LocalTime(14, 0, 0),
            new LocalTime(18, 0, 0),
            false
          )
        )

        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManager])
        val callbackListManager =
          app.injector.instanceOf(classOf[CallbackListManager])
        val pcbManager =
          app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

        val defaultPeriod   = pcbManager.getDefault;
        val morningPeriod   = pcbManager.getPeriod(morningPeriodUuid)
        val afternoonPeriod = pcbManager.getPeriod(afternoonPeriodUuid)

        callbackRequestManager.createFromCsv(
          callbackListManager,
          listUuid.toString,
          csv
        )

        findUncloturedByList(callbackRequestManager, listUuid).map(
          _.copy(uuid = None)
        ) should contain.allOf(
          CallbackRequest(
            None,
            listUuid,
            Some("1000"),
            None,
            Some("John"),
            Some("Doe"),
            None,
            None,
            queueId = Some(queueId),
            preferredPeriodUuid = morningPeriod.flatMap(_.uuid),
            preferredPeriod = morningPeriod
          ),
          CallbackRequest(
            None,
            listUuid,
            Some("1001"),
            None,
            Some("James"),
            Some("Bond"),
            None,
            None,
            queueId = Some(queueId),
            preferredPeriodUuid = defaultPeriod.flatMap(_.uuid),
            preferredPeriod = defaultPeriod
          ),
          CallbackRequest(
            None,
            listUuid,
            Some("1002"),
            None,
            Some("Jason"),
            Some("Bourne"),
            None,
            None,
            queueId = Some(queueId),
            preferredPeriodUuid = defaultPeriod.flatMap(_.uuid),
            preferredPeriod = defaultPeriod
          ),
          CallbackRequest(
            None,
            listUuid,
            None,
            Some("7200"),
            None,
            None,
            Some("The Company"),
            Some("Test"),
            queueId = Some(queueId),
            preferredPeriodUuid = afternoonPeriod.flatMap(_.uuid),
            preferredPeriod = afternoonPeriod
          )
        )
    }

    "extract requests from CSV and insert them with defined period and due date" in withConnection() {
      implicit c =>
        val csv =
          """phoneNumber|mobilePhoneNumber|firstName|lastName|company|description|period|dueDate|
          |1000||John|Doe|||Morning period|invalid date
          |1001||James|Bond|||Unhandled period|2016-09-09
          |1002||Jason|Bourne||||2016-10-12
          ||7200|||The Company|Test|Afternoon period|2017-01-01""".stripMargin

        Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "Default period",
            new LocalTime(9, 0, 0),
            new LocalTime(18, 0, 0),
            true
          )
        )
        val morningPeriodUuid = Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "Morning period",
            new LocalTime(9, 0, 0),
            new LocalTime(12, 0, 0),
            false
          )
        )
        val afternoonPeriodUuid = Utils.insertPeriod(
          PreferredCallbackPeriod(
            None,
            "Afternoon period",
            new LocalTime(14, 0, 0),
            new LocalTime(18, 0, 0),
            false
          )
        )

        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManager])
        val callbackListManager =
          app.injector.instanceOf(classOf[CallbackListManager])
        val pcbManager =
          app.injector.instanceOf(classOf[PreferredCallbackPeriodManager])

        val defaultPeriod   = pcbManager.getDefault;
        val morningPeriod   = pcbManager.getPeriod(morningPeriodUuid)
        val afternoonPeriod = pcbManager.getPeriod(afternoonPeriodUuid)

        callbackRequestManager.createFromCsv(
          callbackListManager,
          listUuid.toString,
          csv
        )

        findUncloturedByList(callbackRequestManager, listUuid).map(
          _.copy(uuid = None)
        ) should contain.allOf(
          CallbackRequest(
            None,
            listUuid,
            Some("1000"),
            None,
            Some("John"),
            Some("Doe"),
            None,
            None,
            preferredPeriodUuid = morningPeriod.flatMap(_.uuid),
            preferredPeriod = morningPeriod,
            queueId = Some(queueId)
          ),
          CallbackRequest(
            None,
            listUuid,
            Some("1001"),
            None,
            Some("James"),
            Some("Bond"),
            None,
            None,
            preferredPeriodUuid = defaultPeriod.flatMap(_.uuid),
            new LocalDate(2016, 9, 9),
            preferredPeriod = defaultPeriod,
            queueId = Some(queueId)
          ),
          CallbackRequest(
            None,
            listUuid,
            Some("1002"),
            None,
            Some("Jason"),
            Some("Bourne"),
            None,
            None,
            preferredPeriodUuid = defaultPeriod.flatMap(_.uuid),
            new LocalDate(2016, 10, 12),
            preferredPeriod = defaultPeriod,
            queueId = Some(queueId)
          ),
          CallbackRequest(
            None,
            listUuid,
            None,
            Some("7200"),
            None,
            None,
            Some("The Company"),
            Some("Test"),
            preferredPeriodUuid = afternoonPeriod.flatMap(_.uuid),
            new LocalDate(2017, 1, 1),
            preferredPeriod = afternoonPeriod,
            queueId = Some(queueId)
          )
        )
    }

    "throw an InvalidParameterException if the uuid is invalid" in {
      val csv         = "the csv"
      val invalidUuid = "123"

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackListManager])

      an[InvalidParameterException] should be thrownBy callbackRequestManager
        .createFromCsv(callbackListManager, invalidUuid, csv)
    }

    "throw an InvalidParameterException if there is no list with the provided uuid" in {
      val csv         = "the csv"
      val invalidUuid = UUID.randomUUID()

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackListManager])

      an[InvalidParameterException] should be thrownBy callbackRequestManager
        .createFromCsv(callbackListManager, invalidUuid.toString, csv)
    }

    "associate a callback request to an agent" in withConnection() {
      implicit c =>
        val request = CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None
        )
        val uuid    = Utils.insertCallbackRequest(request)
        val agentId = 12L

        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManagerImpl])

        callbackRequestManager.takeWithAgent(uuid.toString, agentId)

        Utils.agentForCallbackRequest(uuid) shouldEqual Some(12)
    }

    "throw an InvalidParameterException if the provided UUID is invalid" in {
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      an[InvalidParameterException] should be thrownBy callbackRequestManager
        .takeWithAgent("123", 12)
    }

    "throw an InvalidParameterException if the request is already taken by another agent" in withConnection() {
      implicit c =>
        val request = CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12)
        )
        val uuid = Utils.insertCallbackRequest(request)

        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManagerImpl])

        an[InvalidParameterException] should be thrownBy callbackRequestManager
          .takeWithAgent(uuid.toString, 13)
        Utils.agentForCallbackRequest(uuid) shouldEqual Some(12)
    }

    "throw a NoSuchElementException if there is no CallbackRequest with this id" in {
      val uuid = UUID.randomUUID()
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      an[NoSuchElementException] should be thrownBy callbackRequestManager
        .takeWithAgent(uuid.toString, 12)
    }

    "remove association between callback request and agent" in withConnection() {
      implicit c =>
        val request = CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12)
        )
        val uuid = Utils.insertCallbackRequest(request)
        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
        callbackRequestManager.release(uuid.toString)

        Utils.agentForCallbackRequest(uuid) shouldEqual None
    }

    "get all associations between pending callback request and agent" in withConnection() {
      implicit c =>
        val r1 = CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12)
        )
        val r2 = CallbackRequest(
          None,
          listUuid,
          Some("1001"),
          None,
          Some("Jane"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12)
        )
        val uuid1 = Utils.insertCallbackRequest(r1)
        val uuid2 = Utils.insertCallbackRequest(r2)
        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
        val result = callbackRequestManager.takenCallbacks(12)

        result shouldEqual List(uuid1, uuid2)
    }

    "return a callback request with its queueRef and its period" in withConnection() {
      implicit c =>
        val request = CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          agentId = Some(12),
          clotured = true,
          preferredPeriodUuid = period.uuid,
          dueDate = new LocalDate(2015, 1, 10)
        )
        val uuid = Utils.insertCallbackRequest(request)

        val callbackRequestManager =
          app.injector.instanceOf(classOf[CallbackRequestManagerImpl])

        callbackRequestManager
          .getCallback(uuid.toString) shouldEqual request.copy(
          uuid = Some(uuid),
          queueId = Some(queueId),
          preferredPeriod = Some(period)
        )
    }

    "throw an exception if the callback request does not exist" in {
      val uuid = UUID.randomUUID()
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])

      an[NoSuchElementException] should be thrownBy callbackRequestManager
        .getCallback(uuid.toString)
    }

    "cloture a callback request" in withConnection() { implicit c =>
      val request = CallbackRequest(
        None,
        listUuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None,
        agentId = Some(12)
      )
      val uuid = Utils.insertCallbackRequest(request)

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      callbackRequestManager.cloture(uuid.toString)

      callbackRequestManager.getCallback(uuid.toString).clotured should be(true)
    }

    "uncloture a callback request" in withConnection() { implicit c =>
      val request = CallbackRequest(
        None,
        listUuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None,
        agentId = Some(12),
        clotured = true
      )
      val uuid = Utils.insertCallbackRequest(request)

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      callbackRequestManager.uncloture(uuid.toString)

      val cb = callbackRequestManager.getCallback(uuid.toString)
      cb.clotured should be(false)
      cb.agentId shouldEqual None
    }

    "reschedule a callback request" in withConnection() { implicit c =>
      val request = CallbackRequest(
        None,
        listUuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None,
        agentId = Some(12),
        clotured = false
      )
      val uuid = Utils.insertCallbackRequest(request)

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManagerImpl])
      callbackRequestManager.reschedule(
        uuid.toString,
        DateTime.parse("2016-08-09"),
        period.uuid.get.toString
      )

      val cb = callbackRequestManager.getCallback(uuid.toString)
      cb.dueDate should be(LocalDate.parse("2016-08-09"))
    }

    "parse a CallbackRequest from its JSON representation and a separated listUuid" in {
      val periodUuid = UUID.randomUUID()
      val json = Json.obj(
        "phoneNumber"         -> "0145789630",
        "mobilePhoneNumber"   -> "0789632145",
        "firstName"           -> "John",
        "lastName"            -> "Doe",
        "company"             -> "Avencall",
        "description"         -> "the description",
        "preferredPeriodUuid" -> periodUuid.toString,
        "dueDate"             -> "2015-01-10",
        "voiceMessageRef"     -> "56546546543.56"
      )
      val listUuid = UUID.randomUUID()

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])

      callbackRequestManager.fromJson(
        listUuid.toString,
        json
      ) shouldEqual CallbackRequest(
        None,
        listUuid,
        Some("0145789630"),
        Some("0789632145"),
        Some("John"),
        Some("Doe"),
        Some("Avencall"),
        Some("the description"),
        preferredPeriodUuid = Some(periodUuid),
        dueDate = new LocalDate(2015, 1, 10),
        Some("56546546543.56")
      )
    }

    "insert a CallbackRequest" in {
      val request = CallbackRequest(
        None,
        listUuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None,
        preferredPeriodUuid = period.uuid,
        preferredPeriod = Some(period),
        dueDate = new LocalDate(2015, 1, 10),
        voiceMessageRef = Some("65454654654.12")
      )

      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])
      val res =
        callbackRequestManager.create(request).copy(queueId = Some(queueId))

      res.uuid.isEmpty should be(false)
      res.copy(uuid = None, queueId = None) shouldEqual request
      findUncloturedByList(
        callbackRequestManager,
        listUuid
      ) should contain only (res)
    }

    "throw an InvalidParameterException if the provided period uuid does not exist" in {
      val uuid = UUID.randomUUID()
      val request = CallbackRequest(
        None,
        listUuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None,
        preferredPeriodUuid = Some(uuid)
      )
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])

      an[InvalidParameterException] should be thrownBy callbackRequestManager
        .create(request)
    }

    "use the default period if none is provided" in {
      val request = CallbackRequest(
        None,
        listUuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None
      )
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])

      val res =
        callbackRequestManager.create(request).copy(queueId = Some(queueId))

      res.uuid.isEmpty should be(false)
      res.preferredPeriodUuid shouldEqual period.uuid
      findUncloturedByList(
        callbackRequestManager,
        listUuid
      ) should contain only (res.copy(preferredPeriod = Some(period)))
    }

    "find a list of CallbackRequest based on filter" in {
      val callbackRequestManager =
        app.injector.instanceOf(classOf[CallbackRequestManager])
      val cloturedRequest = callbackRequestManager.create(
        CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          queueId = Some(queueId),
          preferredPeriodUuid = period.uuid,
          preferredPeriod = Some(period),
          dueDate = new LocalDate(2015, 1, 10),
          clotured = true,
          voiceMessageRef = Some("89879879.18")
        )
      )

      callbackRequestManager.cloture(cloturedRequest.uuid.map(_.toString).get)

      callbackRequestManager.create(
        CallbackRequest(
          None,
          listUuid,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          queueId = Some(queueId),
          preferredPeriodUuid = period.uuid,
          preferredPeriod = Some(period),
          dueDate = new LocalDate(2015, 1, 10)
        )
      )

      val filters = List(
        DynamicFilter("listUuid", Some(OperatorEq), Some(listUuid.toString)),
        DynamicFilter("clotured", Some(OperatorEq), Some("true"))
      )
      callbackRequestManager
        .find(filters)
        .list should contain only (cloturedRequest)
    }
  }
}
