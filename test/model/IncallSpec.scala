package model

import anorm.SQL
import docker.DockerPlayWithDbSpec

class IncallSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE incall"
      ).execute()

      super.beforeEach()
    }

  "The Incall singleton" should {
    "retrieve all incalls" in withConnection() { implicit c =>
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val incallManager = app.injector.instanceOf(classOf[IncallManager])

      incallManager.all() should contain.allOf(
        Incall(Some(1), "5301"),
        Incall(Some(2), "5302")
      )
    }

    "retrieve only some incalls" in withConnection() { implicit c =>
      Utils.insertIncall(1, "5301")
      Utils.insertIncall(2, "5302")
      val incallManager = app.injector.instanceOf(classOf[IncallManager])

      incallManager
        .withIds(List(1)) should contain only Incall(Some(1), "5301")
    }

    "not fail with an empty list" in withConnection() { implicit c =>
      val incallManager = app.injector.instanceOf(classOf[IncallManager])
      incallManager.withIds(List()) shouldBe empty
    }
  }
}
