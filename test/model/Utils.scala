package model

import java.sql.{Connection, Timestamp}
import java.util.UUID
import anorm.SQL
import anorm.SqlParser._
import org.joda.time.DateTime
import UUIDUtils._
import model.Utils.TrunkProtocol.TrunkProtocol
import model.Utils.UserCustomCategory.UserCustomCategory
import model.Utils.UserIaxCategory.UserIaxCategory
import xivo.model.RoomTypes.RoomType
import xivo.model.{Agent, QueueMember}

import java.time.Instant
import scala.util.Try

object Utils {
  object UserIaxCategory extends Enumeration {
    type UserIaxCategory = Value

    val trunk, user = Value
  }

  object TrunkProtocol extends Enumeration {
    type TrunkProtocol = Value

    val custom, sip = Value
  }

  object UserCustomCategory extends Enumeration {
    type UserCustomCategory = Value

    val trunk, user = Value
  }

  def bool2int(value: Boolean) = if (value) 1 else 0

  def insertIncallRight(userId: Int, incallId: Int)(implicit c: Connection) =
    insertGenericRight(userId, "incall", incallId)

  def insertRecordingAccessRight(userId: Int, hasAccess: Int)(implicit
      c: Connection
  ) = insertGenericRight(userId, "recording_access", hasAccess)

  def insertDissuasionAccessRight(userId: Int, hasAccess: Int)(implicit
      c: Connection
  ) = insertGenericRight(userId, "dissuasion_access", hasAccess)

  def insertIncall(
      id: Int,
      exten: String,
      context: String = "default",
      description: String = ""
  )(implicit c: Connection) =
    SQL(
      "INSERT INTO incall(id,exten,context, description) VALUES ({id}, {exten},{context},{description})"
    )
      .on(
        Symbol("id")          -> id,
        Symbol("exten")       -> exten,
        Symbol("context")     -> context,
        Symbol("description") -> description
      )
      .executeUpdate()

  def insertXivoUser(
      id: Int,
      firstName: String,
      lastName: String,
      login: Option[String],
      agentid: Option[Int] = None,
      entityid: Option[Int],
      dnd: Boolean = false,
      enableUnc: Boolean = false,
      destUnc: String = "",
      enableNA: Boolean = false,
      destNA: String = "",
      enableBusy: Boolean = false,
      destBusy: String = ",",
      password: Option[String] = Some("passwd"),
      callerId: Option[String] = None,
      email: Option[String] = None,
      mobilePhoneNumber: Option[String] = None
  )(implicit c: Connection) = {
    SQL(
      """INSERT INTO userfeatures
      (id, loginclient, lastname, firstname, agentid, entityid, callerid, enablednd, enableunc, destunc, enablerna, destrna, enablebusy, destbusy, passwdclient, email,mobilephonenumber) 
      VALUES 
      ({id}, {login}, {lastname}, {firstname}, {agentid}, {entityid}, {callerid}, {enablednd}, {enableunc}, {destunc}, {enablerna}, {destrna}, {enablebusy}, {destbusy}, {password} , {email}, {mobilephonenumber});"""
    )
      .on(
        Symbol("id")        -> id,
        Symbol("login")     -> login,
        Symbol("lastname")  -> lastName,
        Symbol("firstname") -> firstName,
        Symbol("agentid")   -> agentid,
        Symbol("entityid")  -> entityid,
        Symbol("callerid") -> callerId.getOrElse(
          s""""$firstName $lastName""""
        ),
        Symbol("enablednd")         -> bool2int(dnd),
        Symbol("enableunc")         -> bool2int(enableUnc),
        Symbol("destunc")           -> destUnc,
        Symbol("enablerna")         -> bool2int(enableNA),
        Symbol("destrna")           -> destNA,
        Symbol("enablebusy")        -> bool2int(enableBusy),
        Symbol("destbusy")          -> destBusy,
        Symbol("password")          -> password,
        Symbol("email")             -> email,
        Symbol("mobilephonenumber") -> mobilePhoneNumber.getOrElse("")
      )
      .executeInsert(scalar[Long].single)
  }

  def insertLineFeature(
      id: Int,
      protocol: String,
      configregistrar: String,
      number: String,
      provisioningid: Int,
      protocolId: Option[Long] = None,
      device: Option[String] = None,
      name: Option[String] = None,
      context: String = "default",
      num: Long = 1
  )(implicit c: Connection) =
    SQL(
      "INSERT INTO linefeatures(id, protocol, configregistrar, number, provisioningid, protocolid, device, name, context, num) VALUES ({id}, {protocol}, {configregistrar}, {number}, {provisioningid}, {protocolid}, {device}, {name}, {context}, {num})"
    )
      .on(
        Symbol("id")              -> id,
        Symbol("protocol")        -> protocol,
        Symbol("configregistrar") -> configregistrar,
        Symbol("number")          -> number,
        Symbol("provisioningid")  -> provisioningid,
        Symbol("protocolid")      -> protocolId,
        Symbol("device")          -> device,
        Symbol("name")            -> name,
        Symbol("context")         -> context,
        Symbol("num")             -> num
      )
      .executeInsert(scalar[Long].single)

  def insertMediaServer(id: Int, name: String, display_name: String)(implicit
      c: Connection
  ) =
    SQL(
      "INSERT INTO mediaserver(id, name, display_name) VALUES ({id}, {name}, {display_name})"
    )
      .on(
        Symbol("id")           -> id,
        Symbol("name")         -> name,
        Symbol("display_name") -> display_name
      )
      .executeInsert(scalar[Long].single)

  def insertEntity(id: Int, name: String, displayName: String)(implicit
      c: Connection
  ) =
    SQL(
      "INSERT INTO entity(id, name, displayname) VALUES ({id}, {name}, {displayname})"
    )
      .on(
        Symbol("id")          -> id,
        Symbol("name")        -> name,
        Symbol("displayname") -> displayName
      )
      .executeInsert(scalar[Long].single)

  def insertQueueFeature(id: Int, name: String, displayName: String = "")(
      implicit c: Connection
  ) =
    SQL(
      "INSERT INTO queuefeatures(id, name, displayname) VALUES ({id}, {name}, {displayname})"
    ).on(
      Symbol("id")          -> id,
      Symbol("name")        -> name,
      Symbol("displayname") -> displayName
    ).executeUpdate()

  def insertQueue(
      name: String,
      musicClass: String,
      strategy: String,
      retry: Int,
      ringInUse: Int,
      category: String = "group"
  )(implicit c: Connection) =
    SQL(
      "INSERT INTO queue (name, musicclass, strategy, retry, ringinuse, category) VALUES ({name}, {musicClass}, {strategy}, {retry}, {ringInUse}, {category}::queue_category)"
    ).on(
      Symbol("name")       -> name,
      Symbol("musicClass") -> musicClass,
      Symbol("strategy")   -> strategy,
      Symbol("retry")      -> retry,
      Symbol("ringInUse")  -> ringInUse,
      Symbol("category")   -> category
    ).executeUpdate()

  def insertCallerId(
      mode: Option[String],
      callerDisplay: String,
      typeCaller: String,
      typeVal: Int
  )(implicit c: Connection) =
    SQL(
      "INSERT INTO callerid (mode, callerdisplay, type, typeval) VALUES ({mode}::callerid_mode, {callerDisplay}, {typeCaller}::callerid_type, {typeVal})"
    ).on(
      Symbol("mode")          -> mode,
      Symbol("callerDisplay") -> callerDisplay,
      Symbol("typeCaller")    -> typeCaller,
      Symbol("typeVal")       -> typeVal
    ).executeUpdate()

  def insertAgentGroup(id: Int, name: String, deleted: Boolean = false)(implicit
      c: Connection
  ) =
    SQL(
      "INSERT INTO agentgroup(id, name, deleted) VALUES ({id}, {name}, {deleted})"
    ).on(
      Symbol("id")      -> id,
      Symbol("name")    -> name,
      Symbol("deleted") -> (if (deleted) 1 else 0)
    ).executeUpdate()

  def insertGroupRight(userId: Int, groupId: Int)(implicit c: Connection) =
    insertGenericRight(userId, "agentgroup", groupId)

  def insertQueueRight(userId: Int, queueId: Int)(implicit c: Connection) =
    insertGenericRight(userId, "queue", queueId)

  def insertUser(
      login: String,
      userType: String,
      start: Option[DateTime] = None,
      end: Option[DateTime] = None
  )(implicit c: Connection): Int =
    SQL(
      "INSERT INTO users(login, type, validity_start, validity_end) VALUES ({login}, {type}::user_type, {start}, {end})"
    )
      .on(
        Symbol("login") -> login,
        Symbol("type")  -> userType,
        Symbol("start") -> start.map(_.toDate),
        Symbol("end")   -> end.map(_.toDate)
      )
      .executeInsert[Option[Long]]()
      .get
      .toInt

  def insertUser(user: XivoCtiUserWithProfile)(implicit c: Connection): Int = {
    SQL(
      "INSERT INTO userfeatures(loginclient, lastname, firstname) VALUES ({login}, {name}, {firstname})"
    )
      .on(
        Symbol("login")     -> user.login,
        Symbol("name")      -> user.name,
        Symbol("firstname") -> user.firstName
      )
      .executeUpdate()
    SQL("INSERT INTO users(login, type) VALUES ({login}, {type}::user_type)")
      .on(Symbol("login") -> user.login, Symbol("type") -> user.profile)
      .executeInsert[Option[Long]]()
      .get
      .toInt
  }

  private def insertGenericRight(
      userId: Int,
      category: String,
      categoryId: Int
  )(implicit c: Connection) =
    SQL(
      "INSERT INTO rights(user_id, category, category_id) VALUES ({userId}, {category}::right_type, {categoryId})"
    )
      .on(
        Symbol("userId")     -> userId,
        Symbol("category")   -> category,
        Symbol("categoryId") -> categoryId
      )
      .executeUpdate()

  def insertCallbackList(cbList: CallbackList)(implicit c: Connection): UUID = {
    SQL("INSERT INTO callback_list(name, queue_id) VALUES ({name}, {queueId})")
      .on(Symbol("name") -> cbList.name, Symbol("queueId") -> cbList.queueId)
      .executeInsert(get[UUID]("uuid").*)
      .head
  }

  def insertCallbackRequest(
      cb: CallbackRequest
  )(implicit c: Connection): UUID = {
    SQL(
      "INSERT INTO callback_request(" +
        "list_uuid, phone_number, mobile_phone_number, firstname, lastname, company, description, agent_id, clotured, preferred_callback_period_uuid, due_date) VALUES " +
        "({lUuid}, {phone}, {mobile}, {firstname}, {lastname}, {company}, {description}, {agentId}, {clotured}, {periodUuid}::uuid, {dueDate})"
    )
      .on(
        Symbol("lUuid")       -> cb.listUuid,
        Symbol("phone")       -> cb.phoneNumber,
        Symbol("mobile")      -> cb.mobilePhoneNumber,
        Symbol("firstname")   -> cb.firstName,
        Symbol("lastname")    -> cb.lastName,
        Symbol("company")     -> cb.company,
        Symbol("description") -> cb.description,
        Symbol("agentId")     -> cb.agentId,
        Symbol("clotured")    -> cb.clotured,
        Symbol("periodUuid")  -> cb.preferredPeriodUuid,
        Symbol("dueDate")     -> cb.dueDate.toDate
      )
      .executeInsert(get[UUID]("uuid").*)
      .head
  }

  def agentForCallbackRequest(
      uuid: UUID
  )(implicit c: Connection): Option[Long] = {
    SQL("SELECT agent_id FROM callback_request WHERE uuid = {uuid}")
      .on(Symbol("uuid") -> uuid)
      .as(get[Option[Long]]("agent_id").*)
      .head
  }

  def insertPeriod(p: PreferredCallbackPeriod)(implicit c: Connection): UUID =
    SQL(
      "INSERT INTO preferred_callback_period(name, period_start, period_end, \"default\") VALUES " +
        "({name}, {start}, {end}, {default})"
    )
      .on(
        Symbol("name")    -> p.name,
        Symbol("start")   -> p.periodStart.toDateTimeToday.toDate,
        Symbol("end")     -> p.periodEnd.toDateTimeToday.toDate,
        Symbol("default") -> p.default
      )
      .executeInsert(get[UUID]("uuid").*)
      .head

  def insertUserType(userType: String)(implicit c: Connection): Unit = {
    SQL(s"ALTER TYPE user_type ADD VALUE '$userType'").execute()
    ()
  }

  def insertAgentFeatures(agent: Agent)(implicit c: Connection) = {
    SQL(
      "INSERT INTO agentfeatures(id, numgroup, firstname, lastname, number, context, passwd, language, description) " +
        "VALUES ({agentId}, {numgroup}, {firstname}, {lastname}, {number}, {context}, {passwd}, {language}, {description})"
    )
      .on(
        Symbol("agentId")     -> agent.id,
        Symbol("numgroup")    -> agent.numgroup,
        Symbol("firstname")   -> agent.firstname,
        Symbol("lastname")    -> agent.lastname,
        Symbol("number")      -> agent.number,
        Symbol("context")     -> agent.context,
        Symbol("passwd")      -> "",
        Symbol("language")    -> "",
        Symbol("description") -> ""
      )
      .executeUpdate()
  }

  def insertQueueMember(
      queueMember: QueueMember,
      userType: String,
      category: String
  )(implicit c: Connection) = {
    SQL(
      "INSERT INTO queuemember(queue_name, interface, penalty, commented, usertype, userid, channel, position, category) " +
        "VALUES ({queue_name}, {interface}, {penalty}, {commented}, {usertype}::queuemember_usertype, {userid}, {channel}, {position}, {category}::queue_category)"
    )
      .on(
        Symbol("queue_name") -> queueMember.queue_name,
        Symbol("interface")  -> queueMember.interface,
        Symbol("penalty")    -> queueMember.penalty,
        Symbol("commented")  -> queueMember.commented,
        Symbol("usertype")   -> userType,
        Symbol("userid")     -> queueMember.userid,
        Symbol("channel")    -> queueMember.channel,
        Symbol("position")   -> queueMember.position,
        Symbol("category")   -> category
      )
      .executeUpdate()
  }

  def insertTrunkfeatures(
      protocol: String,
      protocolid: Int,
      registerid: Int = 0,
      registercommented: Int = 0,
      description: String,
      mediaserverid: Int
  )(implicit c: Connection): Int = {
    SQL(
      "INSERT INTO trunkfeatures(protocol, protocolid, registerid, registercommented, description, mediaserverid) VALUES ({protocol}, {protocolid}, {registerid}, {registercommented}, {description}, {mediaserverid})"
    )
      .on(
        Symbol("protocol")          -> protocol,
        Symbol("protocolid")        -> protocolid,
        Symbol("registerid")        -> registerid,
        Symbol("registercommented") -> registercommented,
        Symbol("description")       -> description,
        Symbol("mediaserverid")     -> mediaserverid
      )
      .executeInsert(get[Int]("id").*)
      .head
  }

  def insertUserSip(
      name: String,
      category: UserIaxCategory,
      options: String = "{}",
      context: String = "default"
  )(implicit c: Connection): Long = {
    SQL(
      "INSERT INTO usersip(name, category, options, context, type, secret) VALUES ({name}, {category}::useriax_category, {options}::character varying[], {context}, 'friend'::useriax_type, 'abcdef')"
    )
      .on(
        Symbol("name")     -> name,
        Symbol("category") -> category.toString,
        Symbol("options")  -> options,
        Symbol("context")  -> context
      )
      .executeInsert(get[Long]("id").*)
      .head
  }

  def insertUserCustom(
      name: String,
      category: UserCustomCategory,
      interface: String,
      protocol: TrunkProtocol,
      context: Option[String] = None
  )(implicit c: Connection): Long = {
    SQL(
      "INSERT INTO usercustom (name, category, interface, protocol, context) " +
        "VALUES ({name}, {category}::usercustom_category, {interface}, {protocol}::trunk_protocol, {context})"
    )
      .on(
        Symbol("name")      -> name,
        Symbol("category")  -> category.toString,
        Symbol("interface") -> interface,
        Symbol("protocol")  -> protocol.toString,
        Symbol("context")   -> context
      )
      .executeInsert(scalar[Long].single)
  }

  def insertSccpLine(
      name: String,
      context: String,
      cid_name: String,
      cid_num: String
  )(implicit c: Connection): Long = {
    SQL(
      "INSERT INTO sccpline (name, context, cid_name, cid_num) " +
        "VALUES ({name}, {context}, {cid_name}, {cid_num})"
    )
      .on(
        Symbol("name")     -> name,
        Symbol("context")  -> context,
        Symbol("cid_name") -> cid_name,
        Symbol("cid_num")  -> cid_num
      )
      .executeInsert(scalar[Long].single)
  }

  def insertSccpDevice(name: String, device: String, line: String)(implicit
      c: Connection
  ): Long = {
    (SQL("""INSERT INTO sccpdevice(name, device, line)
               VALUES({name}, {device}, {line});""")
      .on(
        Symbol("name")   -> name,
        Symbol("device") -> device,
        Symbol("line")   -> line
      )
      .executeInsert(scalar[Long].single))
  }

  def insertDialAction(
      event: String,
      category: String,
      categoryval: Long,
      action: String,
      actionarg1: Option[String] = None,
      actionarg2: Option[String] = None
  )(implicit c: Connection): Long = {
    SQL(
      "INSERT INTO dialaction (event, category, categoryval, action, actionarg1, actionarg2) " +
        "VALUES ({event}::dialaction_event, {category}::dialaction_category, {categoryval}, {action}::dialaction_action, {actionarg1}, {actionarg2})"
    )
      .on(
        Symbol("event")       -> event,
        Symbol("category")    -> category,
        Symbol("categoryval") -> categoryval,
        Symbol("action")      -> action,
        Symbol("actionarg1")  -> actionarg1.orNull,
        Symbol("actionarg2")  -> actionarg2.orNull
      )
      .executeInsert(get[String]("categoryval").*)
      .head
      .toLong
  }

  def insertLabel(id: Int, display_name: String, description: String)(implicit
      c: Connection
  ) = {
    SQL(
      "INSERT INTO labels(id, display_name, description) VALUES ({id}, {display_name}, {description})"
    )
      .on(
        Symbol("id")           -> id,
        Symbol("display_name") -> display_name,
        Symbol("description")  -> description
      )
      .executeInsert(scalar[Long].single)
  }

  def insertUserLabel(id: Int, user_id: Long, label_id: Long)(implicit
      c: Connection
  ) = {
    SQL(
      "INSERT INTO userlabels(id, user_id, label_id) VALUES ({id}, {user_id}, {label_id})"
    )
      .on(
        Symbol("id")       -> id,
        Symbol("user_id")  -> user_id,
        Symbol("label_id") -> label_id
      )
      .executeInsert(scalar[Long].single)
  }

  def insertUserLine(id: Int, user_id: Long, line_id: Long)(implicit
      c: Connection
  ): Long = {
    SQL(
      "INSERT INTO user_line(id, user_id, line_id, main_line, main_user) VALUES ({id}, {user_id}, {line_id}, 't', 't')"
    )
      .on(
        Symbol("id")      -> id,
        Symbol("user_id") -> user_id,
        Symbol("line_id") -> line_id
      )
      .executeInsert(scalar[Long].single)
  }

  def insertExtension(
      context: String,
      exten: String,
      extType: String,
      extTypeVal: String
  )(implicit c: Connection): Long = {
    SQL(
      "INSERT INTO extensions(context, exten, type, typeval) VALUES ({context}, {exten}, {type}::extenumbers_type, {typeval})"
    )
      .on(
        Symbol("context") -> context,
        Symbol("exten")   -> exten,
        Symbol("type")    -> extType,
        Symbol("typeval") -> extTypeVal
      )
      .executeInsert(scalar[Long].single)
  }

  def insertXivoUUID(uuid: UUID)(implicit c: Connection) = {
    SQL("INSERT INTO infos(uuid) values({uuid});")
      .on(Symbol("uuid") -> uuid)
      .executeInsert(scalar[String].single)
  }

  def insertUserPreference(
      id: Long,
      key: String,
      value: String,
      valueType: String
  )(implicit c: Connection) = {
    SQL(
      "INSERT INTO userpreferences values({id}, {key}, {value}, {valueType});"
    )
      .on(
        Symbol("id")        -> id,
        Symbol("key")       -> key,
        Symbol("value")     -> value,
        Symbol("valueType") -> valueType
      )
      .executeInsert(scalar[Long].single)
  }

  def insertStunAddress(id: Long, address: Option[String])(implicit
      c: Connection
  ) = {
    SQL(
      "INSERT INTO staticsip (id, filename, category, var_name, var_val) VALUES ({id}, 'sip.conf', 'general', 'stunaddr', {address})"
    )
      .on(Symbol("id") -> id, Symbol("address") -> address)
      .executeInsert(scalar[Long].single)
  }

  def insertMeetingRooms(
      id: Long,
      displayName: String,
      number: Int,
      userPin: Option[String],
      uuid: UUID = UUID.randomUUID(),
      roomType: RoomType,
      userId: Option[Long],
      pinTimestamp: Timestamp = Timestamp.from(Instant.now()),
      alias: Option[String] = None
  )(implicit c: Connection): Long = {
    SQL(
      "INSERT INTO meetingroom (id, display_name, number, user_pin, uuid, room_type, user_id, pin_timestamp, alias) VALUES ({id}, {display_name}, {number}, {user_pin}, {uuid}, {room_type}, {user_id}, {pin_timestamp}, {alias})"
    )
      .on(
        Symbol("id")            -> id,
        Symbol("display_name")  -> displayName,
        Symbol("number")        -> number,
        Symbol("user_pin")      -> userPin,
        Symbol("uuid")          -> uuid,
        Symbol("room_type")     -> roomType.toString,
        Symbol("user_id")       -> userId,
        Symbol("pin_timestamp") -> pinTimestamp,
        Symbol("alias")         -> alias
      )
      .executeInsert(scalar[Long].single)
  }

  def insertGroupFeature(
      id: Int,
      groupName: String,
      groupNumber: String,
      preprocessSubroutine: Option[String] = None
  )(implicit
      c: Connection
  ): Long =
    SQL(
      "INSERT INTO groupfeatures (id, name, number, context, transfer_user, transfer_call, write_caller, write_calling, ignore_forward, timeout, preprocess_subroutine, deleted, mediaserverid) VALUES" +
        "({id}, {name}, {number}, 'default', 1, 0, 0, 0, 1, 10, {preprocessSubroutine}, 0, 1)"
    ).on(
      "id"                   -> id,
      "name"                 -> groupName,
      "number"               -> groupNumber,
      "preprocessSubroutine" -> preprocessSubroutine
    ).executeInsert(scalar[Long].single)

  def getQueueMember(groupId: Long, userId: Long)(implicit
      c: Connection
  ): Try[QueueMember] =
    Try {
      SQL(
        s"SELECT queue_name, id, interface, penalty, commented, usertype, userid, channel, category, position " +
          s"FROM queuemember " +
          s"INNER JOIN groupfeatures ON groupfeatures.name = queuemember.queue_name " +
          s"WHERE userid = $userId and queue_name = (" +
          s"SELECT name FROM groupfeatures WHERE id = $groupId" +
          s")"
      ).executeQuery().as(QueueMember.parser.single)
    }

  def insertFuncKey(typeId: Int, destTypeId: Int)(implicit
      c: Connection
  ): Long =
    SQL(
      "INSERT INTO func_key (type_id, destination_type_id) VALUES" +
        "({type_id}, {destination_type_id})"
    ).on(
      "type_id"             -> typeId,
      "destination_type_id" -> destTypeId
    ).executeInsert(scalar[Long].single)

  def insertFuncKeyDestGroup(funcKeyId: Int, destTypeId: Int, groupId: Int)(
      implicit c: Connection
  ): Long =
    SQL(
      "INSERT INTO func_key_dest_group (func_key_id, destination_type_id, group_id) VALUES" +
        "({func_key_id}, {destination_type_id}, {group_id})"
    ).on(
      "func_key_id"         -> funcKeyId,
      "destination_type_id" -> destTypeId,
      "group_id"            -> groupId
    ).executeInsert(scalar[Long].single)

  def insertFuncKeyDestinationType(id: Long, name: String)(implicit
      c: Connection
  ): Long =
    SQL(
      "INSERT INTO func_key_destination_type (id, name) VALUES ({id}, {name})"
    )
      .on(
        "id"   -> id,
        "name" -> name
      )
      .executeInsert(scalar[Long].single)

  def insertFuncKeyType(id: Long, name: String)(implicit c: Connection): Long =
    SQL(
      "INSERT INTO func_key_type (id, name) VALUES ({id}, {name})"
    )
      .on(
        "id"   -> id,
        "name" -> name
      )
      .executeInsert(scalar[Long].single)

  def insertContext(
      name: String,
      displayName: String = "",
      entity: Option[String] = None,
      contextType: String = "internal",
      commented: Int = 0,
      description: String = ""
  )(implicit c: Connection): String = SQL(
    """INSERT INTO context
      |VALUES ({name}, {displayName}, {entity}, {contextType}, {commented}, {description})""".stripMargin
  )
    .on(
      "name"        -> name,
      "displayName" -> displayName,
      "entity"      -> entity,
      "contextType" -> contextType,
      "commented"   -> commented,
      "description" -> description
    )
    .executeInsert(scalar[String].single)

  def insertCtiStatus(
   presence_id: Int, name: String, display_name: String, actions:String, color:String, access_status:String, deletable:Int
 )(implicit c: Connection): Long = {
    SQL("""
        | INSERT INTO ctistatus (presence_id, name, display_name, actions, color, access_status, deletable)
        | VALUES  ({presence_id}, {name}, {display_name}, {actions}, {color}, {access_status}, {deletable})
        |
        |""".stripMargin)
      .on(
        "presence_id" -> presence_id,
        "name" -> name,
        "display_name" -> display_name,
        "actions" -> actions,
        "color" -> color,
        "access_status" -> access_status,
        "deletable" -> deletable
      )
      .executeInsert(scalar[Long].single)
  }

  def insertCtiPresences(
      id: Int, name:String, description:String, deletable:Int
    )(implicit c: Connection): Long = {
    SQL(
      """
        | INSERT INTO ctipresences (id, name, description, deletable)
        | VALUES  ({id},{name},{description},{deletable})
        |
        |""".stripMargin)
      .on(
        "id" -> id,
        "name" -> name,
        "description" -> description,
        "deletable" -> deletable
      ).executeInsert(scalar[Long].single)
  }

  def insertCtiProfiles(
      id: Int, name:String, presence_id: Int, phonehints_id: Int
    )(implicit c: Connection): Long = {
    SQL(
      """
        | INSERT INTO cti_profile (id, name, presence_id, phonehints_id)
        | VALUES  ({id},{name},{presence_id},{phonehints_id})
        |
        |""".stripMargin)
      .on(
        "id" -> id,
        "name" -> name,
        "presence_id" -> presence_id,
        "phonehints_id" -> phonehints_id
      ).executeInsert(scalar[Long].single)
  }

}
