package model

import anorm.SQL
import docker.DockerPlayWithDbSpec
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.json.Json

class RightSpec extends DockerPlayWithDbSpec {

  var userid1 = 0
  var userid2 = 0
  var userid3 = 0
  val formatter: DateTimeFormatter =
    DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE rights, users"
      ).execute()
      userid1 = Utils.insertUser("login1", "admin")
      userid2 = Utils.insertUser("login2", "supervisor")
      userid3 = Utils.insertUser(
        "login3",
        "teacher",
        Some(formatter.parseDateTime("2014-06-02 00:00:00")),
        Some(formatter.parseDateTime("2014-09-12 23:59:59"))
      )
      super.beforeEach()
    }

  "The Right singleton" should {
    "return an Admin right" in {

      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.forUser("login1") should be(Some(AdminRight()))
    }

    "return a Supervisor right with recording access by default" in withConnection() {
      implicit c =>
        Utils.insertGroupRight(userid2, 2)
        Utils.insertGroupRight(userid2, 3)
        Utils.insertQueueRight(userid2, 4)
        Utils.insertQueueRight(userid2, 5)
        Utils.insertIncallRight(userid2, 6)
        Utils.insertIncallRight(userid2, 7)

        val rightManager = app.injector.instanceOf(classOf[RightManager])

        rightManager.forUser("login2") should be(
          Some(SupervisorRight(List(4, 5), List(2, 3), List(6, 7)))
        )
    }

    "return a Supervisor right without recording access" in withConnection() {
      implicit c =>
        Utils.insertGroupRight(userid2, 2)
        Utils.insertGroupRight(userid2, 3)
        Utils.insertQueueRight(userid2, 4)
        Utils.insertQueueRight(userid2, 5)
        Utils.insertIncallRight(userid2, 6)
        Utils.insertIncallRight(userid2, 7)
        Utils.insertRecordingAccessRight(userid2, 0)

        val rightManager = app.injector.instanceOf(classOf[RightManager])

        rightManager.forUser("login2") should be(
          Some(
            SupervisorRight(
              List(4, 5),
              List(2, 3),
              List(6, 7),
              recordingAccess = false
            )
          )
        )
    }

    "return a Supervisor right without the right to update dissuasions by default" in withConnection() {
      implicit c =>
        Utils.insertGroupRight(userid2, 2)
        Utils.insertGroupRight(userid2, 3)
        Utils.insertQueueRight(userid2, 4)
        Utils.insertQueueRight(userid2, 5)
        Utils.insertIncallRight(userid2, 6)
        Utils.insertIncallRight(userid2, 7)

        val rightManager = app.injector.instanceOf(classOf[RightManager])

        rightManager.forUser("login2") should be(
          Some(SupervisorRight(List(4, 5), List(2, 3), List(6, 7)))
        )
    }

    "return a Supervisor right with the right to update dissuasions" in withConnection() {
      implicit c =>
        Utils.insertGroupRight(userid2, 2)
        Utils.insertGroupRight(userid2, 3)
        Utils.insertQueueRight(userid2, 4)
        Utils.insertQueueRight(userid2, 5)
        Utils.insertIncallRight(userid2, 6)
        Utils.insertIncallRight(userid2, 7)
        Utils.insertDissuasionAccessRight(userid2, 1)

        val rightManager = app.injector.instanceOf(classOf[RightManager])

        rightManager.forUser("login2") should be(
          Some(
            SupervisorRight(
              List(4, 5),
              List(2, 3),
              List(6, 7),
              dissuasionAccess = true
            )
          )
        )
    }

    "return a Teacher right" in withConnection() { implicit c =>
      Utils.insertGroupRight(userid3, 2)
      Utils.insertGroupRight(userid3, 3)
      Utils.insertQueueRight(userid3, 4)
      Utils.insertQueueRight(userid3, 5)
      Utils.insertIncallRight(userid3, 6)
      Utils.insertIncallRight(userid3, 7)

      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.forUser("login3") should be(
        Some(
          TeacherRight(
            List(4, 5),
            List(2, 3),
            List(6, 7),
            "2014-06-02",
            "2014-09-12"
          )
        )
      )
    }

    "return None for a non existing user" in {

      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.forUser("loginXXX") should be(None)
    }

    "return None when user type is not managed" in withConnection() {
      implicit c =>
        Utils.insertUserType("unknowntype")
        Utils.insertUser("loginUnknownType", "unknowntype")

        val rightManager = app.injector.instanceOf(classOf[RightManager])

        rightManager.forUser("loginUnknownType") should be(None)

    }

    "parse an AdminRight from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(Json.obj("type" -> "admin", "data" -> Json.obj()))
      ) should be(AdminRight())
    }

    "parse a Supervisor Right without recording access information from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(
          Json.obj(
            "type" -> "supervisor",
            "data" -> Json.obj(
              "queueIds"  -> List(1, 2),
              "groupIds"  -> List(3, 4),
              "incallIds" -> List(5, 6)
            )
          )
        )
      ) shouldEqual
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
    }

    "parse a Supervisor Right with recording access enabled from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(
          Json.obj(
            "type" -> "supervisor",
            "data" -> Json.obj(
              "queueIds"        -> List(1, 2),
              "groupIds"        -> List(3, 4),
              "incallIds"       -> List(5, 6),
              "recordingAccess" -> true
            )
          )
        )
      ) shouldEqual
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
    }

    "parse a Supervisor Right with recording access disabled from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(
          Json.obj(
            "type" -> "supervisor",
            "data" -> Json.obj(
              "queueIds"        -> List(1, 2),
              "groupIds"        -> List(3, 4),
              "incallIds"       -> List(5, 6),
              "recordingAccess" -> false
            )
          )
        )
      ) shouldEqual
        SupervisorRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          recordingAccess = false
        )
    }

    "parse a Supervisor Right with update dissuasion right enabled from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(
          Json.obj(
            "type" -> "supervisor",
            "data" -> Json.obj(
              "queueIds"         -> List(1, 2),
              "groupIds"         -> List(3, 4),
              "incallIds"        -> List(5, 6),
              "dissuasionAccess" -> true
            )
          )
        )
      ) shouldEqual
        SupervisorRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          dissuasionAccess = true
        )
    }

    "parse a Supervisor Right with update dissuasion right disabled from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(
          Json.obj(
            "type" -> "supervisor",
            "data" -> Json.obj(
              "queueIds"         -> List(1, 2),
              "groupIds"         -> List(3, 4),
              "incallIds"        -> List(5, 6),
              "dissuasionAccess" -> false
            )
          )
        )
      ) shouldEqual
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
    }

    "parse a TeacherRight from its JSON representation" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])

      rightManager.fromJson(
        Json.toJson(
          Json.obj(
            "type" -> "teacher",
            "data" -> Json.obj(
              "queueIds"  -> List(1, 2),
              "groupIds"  -> List(3, 4),
              "incallIds" -> List(5, 6),
              "start"     -> "2014-01-08",
              "end"       -> "2014-08-07"
            )
          )
        )
      ) shouldEqual
        TeacherRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          formatter.parseDateTime("2014-01-08 00:00:00"),
          formatter.parseDateTime("2014-08-07 23:59:59")
        )
    }

    "create a new user with admin rights" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace("login3", AdminRight())
      rightManager.forUser("login3") should be(Some(AdminRight()))
    }

    "create a new user with supervisor rights" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login3",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )
      rightManager.forUser("login3") should be(
        Some(SupervisorRight(List(1, 2), List(3, 4), List(5, 6)))
      )
    }

    "create a new user with teacher rights" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        TeacherRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          "2014-02-01",
          "2014-06-12"
        )
      )
      rightManager.forUser("login2") should be(
        Some(
          TeacherRight(
            List(1, 2),
            List(3, 4),
            List(5, 6),
            "2014-02-01",
            "2014-06-12"
          )
        )
      )
    }

    "replace an existing supervisor with admin rights" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace("login2", AdminRight())
      rightManager.forUser("login2") should be(Some(AdminRight()))
    }

    "delete an admin right" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace("login2", AdminRight())

      rightManager.deleteByLogin("login2")

      rightManager.forUser("login2") should be(None)
    }

    "delete a supervisor right" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.deleteByLogin("login2")

      rightManager.forUser("login2") should be(None)
    }

    "allow a Supervisor to create a Teacher with the same rights" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.userCanCreateRight(
        "login2",
        TeacherRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          new DateTime(),
          new DateTime()
        )
      ) should be(true)
    }

    "allow a Supervisor to create a Teacher with more restrictive rights" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.userCanCreateRight(
        "login2",
        TeacherRight(List(1), List(3), List(5), new DateTime(), new DateTime())
      ) should be(true)
    }

    "forbid a Supervisor to create a Supervisor or Admin right" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.userCanCreateRight(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      ) should be(false)
      rightManager.userCanCreateRight("login2", AdminRight()) should be(false)
    }

    "forbid a Supervisor to create a Teacher with rights on other objects" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.userCanCreateRight(
        "login2",
        TeacherRight(
          List(1, 3),
          List(3, 4),
          List(5, 6),
          new DateTime(),
          new DateTime()
        )
      ) should be(false)
    }

    "forbid a Supervisor to create a Teacher if it doesn't have access to recording" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          recordingAccess = false
        )
      )

      rightManager.userCanCreateRight(
        "login2",
        TeacherRight(
          List(1, 2),
          List(3, 4),
          List(5, 6),
          new DateTime(),
          new DateTime()
        )
      ) should be(false)
    }

    "forbid an Admin or a Teacher to create a right" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.userCanModifyUser("login1", "login4") should be(false)
      rightManager.userCanModifyUser("login3", "login4") should be(false)
    }

    "forbid a Supervisor to downgrade a Supervisor or an Admin to Teacher" in {
      val rightManager = app.injector.instanceOf(classOf[RightManager])
      rightManager.createOrReplace(
        "login2",
        SupervisorRight(List(1, 2), List(3, 4), List(5, 6))
      )

      rightManager.userCanModifyUser("login2", "login1") should be(false)
      rightManager.userCanModifyUser("login2", "login2") should be(false)
    }

  }

  "The AdminRight class" should {
    "jsonify itself" in {
      AdminRight().toJson should be(
        Json.obj("type" -> "admin", "data" -> Json.obj())
      )
    }
  }

  "The SupervisorRight class" should {
    "jsonify itself" in {
      SupervisorRight(List(1, 2), List(3, 4), List(5, 6)).toJson should be(
        Json.obj(
          "type" -> "supervisor",
          "data" -> Json.obj(
            "queueIds"         -> List(1, 2),
            "groupIds"         -> List(3, 4),
            "incallIds"        -> List(5, 6),
            "recordingAccess"  -> true,
            "dissuasionAccess" -> false
          )
        )
      )
    }
  }

  "The TeacherRight class" should {
    "jsonify itself" in {
      val start = formatter.parseDateTime("2014-01-01 00:00:00")
      val end   = formatter.parseDateTime("2014-02-01 23:59:59")
      TeacherRight(
        List(1, 2),
        List(3, 4),
        List(5, 6),
        start,
        end
      ).toJson should be(
        Json.obj(
          "type" -> "teacher",
          "data" -> Json.obj(
            "queueIds"  -> List(1, 2),
            "groupIds"  -> List(3, 4),
            "incallIds" -> List(5, 6),
            "start"     -> "2014-01-01",
            "end"       -> "2014-02-01"
          )
        )
      )
    }
  }

}
