package model

import anorm.SQL
import docker.DockerPlayWithDbSpec
import play.api.libs.json.Json

import java.util.UUID

class CallbackListSpec extends DockerPlayWithDbSpec {

  val queue: Queue = Queue(Some(2), "thequeue", "The Queue")

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL("TRUNCATE queuefeatures, callback_list, callback_request").execute()
      Utils.insertQueueFeature(queue.id.get.toInt, queue.name, queue.displayName)
      super.beforeEach()
    }

  "The CallbackList singleton" should {
    "list callback lists with their unclotured requests" in withConnection() {
      implicit c =>
        val l1        = CallbackList(None, "the list", queue.id.get, List())
        val l2        = CallbackList(None, "other list", queue.id.get, List())
        val listUuid1 = Utils.insertCallbackList(l1)
        val listUuid2 = Utils.insertCallbackList(l2)
        val cb1 = CallbackRequest(
          None,
          listUuid1,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None,
          queueId = queue.id
        )
        val cb2 = CallbackRequest(
          None,
          listUuid1,
          None,
          Some("7000"),
          None,
          None,
          Some("Company"),
          Some("Test"),
          agentId = Some(12),
          queueId = queue.id
        )
        val cb3 = CallbackRequest(
          None,
          listUuid1,
          None,
          Some("7000"),
          None,
          None,
          Some("Company"),
          Some("Test"),
          agentId = Some(12),
          queueId = queue.id,
          clotured = true
        )
        val cbUuid1 = Utils.insertCallbackRequest(cb1)
        val cbUuid2 = Utils.insertCallbackRequest(cb2)
        Utils.insertCallbackRequest(cb3)

        val callbackListManager =
          app.injector.instanceOf(classOf[CallbackListManager])

        callbackListManager.all(true) should contain.allOf(
          l1.copy(
            uuid = Some(listUuid1),
            callbacks = List(
              cb1.copy(uuid = Some(cbUuid1)),
              cb2.copy(uuid = Some(cbUuid2))
            )
          ),
          l2.copy(uuid = Some(listUuid2))
        )
    }

    "list callback lists without requests" in withConnection() { implicit c =>
      val l1        = CallbackList(None, "the list", queue.id.get, List())
      val l2        = CallbackList(None, "other list", queue.id.get, List())
      val listUuid1 = Utils.insertCallbackList(l1)
      val listUuid2 = Utils.insertCallbackList(l2)
      val cb1 = CallbackRequest(
        None,
        listUuid1,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None
      )
      val cb2 = CallbackRequest(
        None,
        listUuid1,
        None,
        Some("7000"),
        None,
        None,
        Some("Company"),
        Some("Test"),
        agentId = Some(12)
      )
      val cb3 = CallbackRequest(
        None,
        listUuid1,
        None,
        Some("7000"),
        None,
        None,
        Some("Company"),
        Some("Test"),
        agentId = Some(12),
        clotured = true
      )
      Utils.insertCallbackRequest(cb1)
      Utils.insertCallbackRequest(cb2)
      Utils.insertCallbackRequest(cb3)

      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackListManager])

      callbackListManager.all(false) should contain.allOf(
        l1.copy(uuid = Some(listUuid1)),
        l2.copy(uuid = Some(listUuid2))
      )
    }

    "write callback lists to JSON" in {
      val uuid = UUID.randomUUID()
      val cb1 = CallbackRequest(
        Some(UUID.randomUUID()),
        uuid,
        Some("1000"),
        None,
        Some("John"),
        Some("Doe"),
        None,
        None
      )
      val cb2 = CallbackRequest(
        Some(UUID.randomUUID()),
        uuid,
        None,
        Some("7000"),
        None,
        None,
        Some("Company"),
        Some("Test")
      )
      val l1 =
        CallbackList(Some(uuid), "the list", queue.id.get, List(cb1, cb2))

      Json.toJson(l1) shouldEqual Json.obj(
        "uuid"      -> uuid.toString,
        "name"      -> "the list",
        "queueId"   -> 2,
        "callbacks" -> List(cb1, cb2)
      )
    }

    "create a callback list" in {
      val list = CallbackList(None, "The List", 7, List())

      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackManagerImpl])
      val res = callbackListManager.create(list)

      res.uuid.isEmpty shouldBe false
      res.copy(uuid = None) shouldEqual list
      callbackListManager.all(false) should contain(res)
    }

    "delete a callback list" in withConnection() { implicit c =>
      val l1        = CallbackList(None, "the list", queue.id.get, List())
      val l2        = CallbackList(None, "other list", queue.id.get, List())
      val listUuid1 = Utils.insertCallbackList(l1)
      val listUuid2 = Utils.insertCallbackList(l2)
      Utils.insertCallbackRequest(
        CallbackRequest(
          None,
          listUuid1,
          Some("1000"),
          None,
          Some("John"),
          Some("Doe"),
          None,
          None
        )
      )
      Utils.insertCallbackRequest(
        CallbackRequest(
          None,
          listUuid1,
          None,
          Some("7000"),
          None,
          None,
          Some("Company"),
          Some("Test"),
          queueId = Some(12)
        )
      )

      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackManagerImpl])

      callbackListManager.delete(listUuid1.toString)

      callbackListManager.all(false) should contain only l2.copy(uuid =
        Some(listUuid2)
      )
    }

    "throw a NoSuchElementException if the uuid provided for deletion does not exist" in {
      val uuid = UUID.randomUUID()
      val callbackListManager =
        app.injector.instanceOf(classOf[CallbackManagerImpl])
      an[NoSuchElementException] must be thrownBy callbackListManager.delete(
        uuid.toString
      )
    }
  }
}
