package model

import anorm.{NamedParameter, ToParameterValue}
import play.api.libs.json._
import testutils.PlayShouldSpec

class DynamicFilterSpec extends PlayShouldSpec {

  "NoCasing" should {
    "preserve camel case" in {
      NoCasing.transform("firstNameOfUser") should be("firstNameOfUser")
    }

    "preserve snake case" in {
      NoCasing.transform("first_name_of_user") should be("first_name_of_user")
    }
  }

  "ToSnakeCase" should {
    "convert camel case to snake case" in {
      ToSnakeCase.transform("firstNameOfUser") should be("first_name_of_user")
    }

    "preserve snake case" in {
      ToSnakeCase.transform("first_name_of_user") should be(
        "first_name_of_user"
      )
    }
  }

  "LowerCasing" should {
    "convert camel case to lower case" in {
      LowerCasing.transform("firstNameOfUser") should be("firstnameofuser")
    }

    "preserve lower case" in {
      LowerCasing.transform("firstnameofuser") should be("firstnameofuser")
    }
  }

  "DynamicFilterOperator" should {
    "convert string repr to object" in {
      DynamicFilterOperator.toObject(Some("=")) should be(Some(OperatorEq))
      DynamicFilterOperator.toObject(Some("!=")) should be(Some(OperatorNeq))
      DynamicFilterOperator.toObject(Some(">")) should be(Some(OperatorGt))
      DynamicFilterOperator.toObject(Some(">=")) should be(Some(OperatorGte))
      DynamicFilterOperator.toObject(Some("<")) should be(Some(OperatorLt))
      DynamicFilterOperator.toObject(Some("<=")) should be(Some(OperatorLte))
      DynamicFilterOperator.toObject(Some("like")) should be(Some(OperatorLike))
      DynamicFilterOperator.toObject(Some("ilike")) should be(
        Some(OperatorIlike)
      )
      DynamicFilterOperator.toObject(Some("is null")) should be(
        Some(OperatorIsNull)
      )
      DynamicFilterOperator.toObject(Some("is not null")) should be(
        Some(OperatorIsNotNull)
      )
      DynamicFilterOperator.toObject(Some("contains_all")) should be(
        Some(OperatorContainsAll)
      )
    }
  }

  "DynamicFilter" should {
    "convert DynamicFilter to sql condition" in {
      implicit val casing = NoCasing
      val f               = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))
      DynamicFilter.toSqlWhereCondition(f) should be("myField = {myField}")
    }

    "convert DynamicFilter with is null operator to sql condition" in {
      implicit val casing = NoCasing
      val f               = DynamicFilter("myField", Some(OperatorIsNull), None)
      DynamicFilter.toSqlWhereCondition(f) should be("myField is null")
    }

    "convert DynamicFilter with is not null operator to sql condition" in {
      implicit val casing = NoCasing
      val f               = DynamicFilter("myField", Some(OperatorIsNotNull), None)
      DynamicFilter.toSqlWhereCondition(f) should be("myField is not null")
    }

    "convert list of DynamicFilter to sql condition" in {
      implicit val casing = NoCasing
      val f = List(
        DynamicFilter("myField", Some(OperatorEq), Some("myvalue")),
        DynamicFilter("myOtherField", Some(OperatorEq), Some("myOthervalue"))
      )
      DynamicFilter.toSqlWhereCondition(f) should be(
        " WHERE myField = {myField} AND myOtherField = {myOtherField}\n"
      )
    }

    "convert list of DynamicFilter, including nullity test, to sql condition" in {
      implicit val casing = NoCasing
      val f = List(
        DynamicFilter("myField", Some(OperatorEq), Some("myvalue")),
        DynamicFilter("myOtherField", Some(OperatorIsNull), None)
      )
      DynamicFilter.toSqlWhereCondition(f) should be(
        " WHERE myField = {myField} AND myOtherField is null\n"
      )
    }

    "convert DynamicFilter to NamedParameter" in {
      implicit val typer = AllFieldsString
      val f              = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))
      val t              = ToParameterValue.apply[String]
      DynamicFilter.toNamedParameter(f) should be(
        NamedParameter(f.field, t(f.value.get))
      )
    }

    "convert list of DynamicFilter to list of NamedParameter" in {
      implicit val typer = AllFieldsString
      val f = List(
        DynamicFilter("myField", Some(OperatorEq), Some("myvalue")),
        DynamicFilter("myOtherField", Some(OperatorEq), Some("myOthervalue"))
      )
      val t        = ToParameterValue.apply[String]
      val expected = f map (i => NamedParameter(i.field, t(i.value.get)))
      DynamicFilter.toNamedParameterList(f) should be(expected)
    }

    "convert DynamicFilter to json" in {
      val f = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))

      Json.toJson(f) should be(
        Json.parse(
          """{"field": "myField", "operator": "=", "value": "myvalue", "order": null}"""
        )
      )
    }

    "convert json to DynamicFilter" in {
      val expected = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"))

      val result = Json
        .parse("""{"field": "myField", "operator": "=", "value": "myvalue"}""")
        .validate[DynamicFilter]
      result should be(JsSuccess(expected))
    }

    "convert DynamicFilter to Order clause" in {
      implicit val casing = NoCasing
      val a = DynamicFilter(
        "myField",
        Some(OperatorEq),
        Some("myvalue"),
        Some(OrderAsc)
      )
      val d = DynamicFilter(
        "myField",
        Some(OperatorEq),
        Some("myvalue"),
        Some(OrderDesc)
      )
      val no = DynamicFilter("myField", Some(OperatorEq), Some("myvalue"), None)

      DynamicFilter.toOrderClause(a) should be(Some("myField ASC"))
      DynamicFilter.toOrderClause(d) should be(Some("myField DESC"))
      DynamicFilter.toOrderClause(no) should be(None)
    }

    "convert List of DynamicFilter to Order clause" in {
      implicit val casing = NoCasing
      val l = List(
        DynamicFilter(
          "myField1",
          Some(OperatorEq),
          Some("myvalue"),
          Some(OrderAsc)
        ),
        DynamicFilter(
          "myField2",
          Some(OperatorEq),
          Some("myvalue"),
          Some(OrderDesc)
        ),
        DynamicFilter("myField3", Some(OperatorEq), Some("myvalue"), None)
      )

      DynamicFilter.toOrderClause(l) should be(
        " ORDER BY myField1 ASC, myField2 DESC"
      )
    }

    "convert json with list of values to DynamicFilter" in {
      val expected = DynamicFilter(
        "myField",
        Some(OperatorContainsAll),
        None,
        None,
        Some(List("Red", "Blue"))
      )

      val result = Json
        .parse(
          """{"field": "myField", "operator": "contains_all", "list": ["Red", "Blue"]}"""
        )
        .validate[DynamicFilter]
      result should be(JsSuccess(expected))
    }
  }
}
