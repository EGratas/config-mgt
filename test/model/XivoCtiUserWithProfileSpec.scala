package model

import anorm.SQL
import docker.DockerPlayWithDbSpec

class XivoCtiUserWithProfileSpec extends DockerPlayWithDbSpec {

  override protected def beforeEach(): Unit =
    withConnection() { implicit c =>
      SQL(
        "TRUNCATE users, userfeatures CASCADE"
      ).execute()

      super.beforeEach()
    }

  "The User singleton" should {
    "retrieve a full User" in withConnection() { implicit c =>
      val user1 = XivoCtiUserWithProfile("Dupond", "Pierre", "pdupond", "admin")
      val user2 =
        XivoCtiUserWithProfile("Dupont", "Jean", "jdupont", "supervisor")
      Utils.insertUser(user1)
      Utils.insertUser(user2)

      val userManager = app.injector.instanceOf(classOf[UserProfileManager])

      userManager.list should contain.only(user1, user2)
    }
  }
}
