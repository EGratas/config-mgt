package xc.model

import play.api.libs.functional.syntax._
import play.api.libs.json._

case class SubQualification(id: Option[Long], name: String) {
  def toJson: JsValue =
    Json.obj(
      "id"                   -> id,
      "subQualificationName" -> name
    )
}

object SubQualification {

  implicit val subQualificationWrites: Writes[SubQualification] {
    def writes(subQualification: SubQualification): JsObject
  } = new Writes[SubQualification] {
    def writes(subQualification: SubQualification) =
      Json.obj(
        "id"   -> subQualification.id,
        "name" -> subQualification.name
      )
  }

  implicit val subQualificationReads: Reads[SubQualification] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "name").read[String]
  )(SubQualification.apply _)
}

case class CallQualification(
    id: Option[Long],
    name: String,
    subQualification: List[SubQualification]
) {
  def toJson: JsValue =
    Json.obj(
      "id"                -> id,
      "qualificationName" -> name
    )
}

object CallQualification {

  implicit val callQualificationWrites: Writes[CallQualification] {
    def writes(qualification: CallQualification): JsObject
  } = new Writes[CallQualification] {
    def writes(qualification: CallQualification) =
      Json.obj(
        "id"                -> qualification.id,
        "name"              -> qualification.name,
        "subQualifications" -> qualification.subQualification
      )
  }

  implicit val callQualificationReads: Reads[CallQualification] = (
    (JsPath \ "id").readNullable[Long] and
      (JsPath \ "name").read[String] and
      (JsPath \ "subQualifications").read[List[SubQualification]]
  )(CallQualification.apply _)
}

case class SimpleCallQualification(id: Long, name: Option[String]) {
  def toJson: JsValue =
    Json.obj(
      "id"   -> id,
      "name" -> name
    )
}

object SimpleCallQualification {
  implicit val simpleCallQualificationReads: Reads[SimpleCallQualification] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "name").readNullable[String]
  )(SimpleCallQualification.apply _)
}
