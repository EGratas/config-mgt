package xc.model

import play.api.libs.functional.syntax._
import play.api.libs.json._

import scala.util.{Failure, Success, Try}

case class CallQualificationAnswer(
    sub_qualification_id: Int,
    time: String,
    callid: String,
    agent: Int,
    queue: Int,
    firstName: String,
    lastName: String,
    comment: String,
    customData: String
)

object CallQualificationAnswer {

  implicit val writes: Writes[CallQualificationAnswer] {
    def writes(u: CallQualificationAnswer): JsObject
  } = new Writes[CallQualificationAnswer] {
    def writes(u: CallQualificationAnswer) =
      Json.obj(
        "sub_qualification_id" -> u.sub_qualification_id,
        "time"                 -> u.time,
        "callid"               -> u.callid,
        "agent"                -> u.agent,
        "queue"                -> u.queue,
        "first_name"           -> u.firstName,
        "last_name"            -> u.lastName,
        "comment"              -> u.comment,
        "custom_data"          -> u.customData
      )
  }

  implicit val reads: Reads[CallQualificationAnswer] = (
    (JsPath \ "sub_qualification_id").read[Int] and
      (JsPath \ "time").read[String] and
      (JsPath \ "callid").read[String] and
      (JsPath \ "agent").read[Int] and
      (JsPath \ "queue").read[Int] and
      (JsPath \ "first_name").read[String] and
      (JsPath \ "last_name").read[String] and
      (JsPath \ "comment").read[String] and
      (JsPath \ "custom_data").read[String]
  )(CallQualificationAnswer.apply _)

  def toCsv(qualificationAnswers: List[CallQualificationAnswer]): String = {
    val staticColumnHeaders = List(
      "sub_qualification_id",
      "time",
      "callid",
      "agent",
      "queue",
      "first_name",
      "last_name",
      "comment"
    )
    val dynamicColumnHeaders = getCustomDataKeys(qualificationAnswers)

    val header =
      (staticColumnHeaders ::: dynamicColumnHeaders).mkString(",") + "\n"

    val lines = qualificationAnswers.map(
      toList(_, dynamicColumnHeaders).map(quote).mkString(",")
    )
    header + lines.mkString("\n")
  }

  def getCustomDataKeys(
      qualificationAnswers: List[CallQualificationAnswer]
  ): List[String] = {
    def parseKeys(customData: String): Option[List[String]] = {
      val customDataJson: Try[JsValue] = Try(Json.parse(customData))
      customDataJson match {
        case Success(customDataParsed) =>
          customDataParsed.validate[JsObject] match {
            case JsSuccess(customDataObj, _) =>
              Some(customDataObj.keys.toList)
            case JsError(e) => None
          }
        case Failure(f) => None
      }
    }
    qualificationAnswers.flatMap(q => parseKeys(q.customData)).flatten.distinct
  }

  def getCustomDataValues(
      customData: String,
      columnHeaders: List[String]
  ): List[String] = {
    def parseValues(customData: String): List[String] = {
      val customDataJson: Try[JsValue] = Try(Json.parse(customData))
      customDataJson match {
        case Success(customDataParsed) =>
          columnHeaders.map { k =>
            (customDataParsed \ k).asOpt[String] match {
              case Some(value) => value
              case None        => ""
            }
          }
        case Failure(f) => List("")
      }
    }
    parseValues(customData)
  }

  def quote(s: String): String = s""""${s.replace("\"", "\"\"")}""""

  def toList(
      q: CallQualificationAnswer,
      dynamicColumnHeaders: List[String]
  ): List[String] = {
    (q.sub_qualification_id.toString ::
      q.time.toString ::
      q.callid.toString ::
      q.agent.toString ::
      q.queue.toString ::
      q.firstName ::
      q.lastName ::
      q.comment :: Nil) :::
      getCustomDataValues(q.customData, dynamicColumnHeaders)
  }
}
