package xc.service

import anorm._
import com.google.inject.{ImplementedBy, Inject}
import common.{AnormMacro, Crud, CrudMacro}
import org.slf4j.LoggerFactory
import play.api.db._
import xc.model.UserQueueMember
import xivo.service.UserFeatureManager

import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[UserQueueManagerImpl])
trait UserQueueManager extends Crud[UserQueueMember, Long] {}

class UserQueueManagerImpl @Inject() (
    @NamedDatabase("xc") dbXc: Database,
    xivoUserMgr: UserFeatureManager
) extends UserQueueManager {
  val logger = LoggerFactory.getLogger(getClass)
  val crudParser: RowParser[UserQueueMember] =
    AnormMacro.namedParser[UserQueueMember](AnormMacro.ColumnNaming.SnakeCase)

  private def xivoUserToXcUser(u: xivo.model.UserFeature): UserQueueMember = {
    u.id match {
      case Some(id) =>
        UserQueueMember(id, u.firstname, u.lastname, u.loginclient)
      case None =>
        throw new Exception("Received invalid Xivo UserFeature without id")
    }
  }

  override def create(t: UserQueueMember): Try[UserQueueMember] =
    dbXc.withConnection { implicit c =>
      Try({
        CrudMacro
          .query[UserQueueMember](
            t,
            "xc.users",
            Set("id"),
            Set(),
            false,
            CrudMacro.SnakeCase,
            CrudMacro.SQLInsert
          )
          .executeInsert()
        t
      })
    }

  override def update(t: UserQueueMember): Try[UserQueueMember] =
    dbXc.withConnection { implicit c =>
      val updateCount = CrudMacro
        .query[UserQueueMember](
          t,
          "xc.users",
          Set("id"),
          Set(),
          false,
          CrudMacro.SnakeCase,
          CrudMacro.SQLUpdate
        )
        .executeUpdate()

      if (updateCount == 1)
        Success(t)
      else
        Failure(new Exception("Cannot update User"))
    }

  override def get(id: Long): Try[UserQueueMember] =
    dbXc.withConnection { implicit c =>
      Try(
        SQL("select  * from xc.users where id={id}")
          .on("id" -> id)
          .as(crudParser.single)
      ).recoverWith { case t =>
        logger.info("xc.User not found locally, try from xivo", t)
        xivoUserMgr
          .get(id)
          .recoverWith({ case t =>
            println(t.toString); Failure(t)
          })
          .map(xivoUserToXcUser)
          .flatMap(create)
      }
    }

  override def all(): Try[List[UserQueueMember]] =
    dbXc.withConnection { implicit c =>
      Try(SQL("select  * from xc.users").as(crudParser.*))
    }

  override def delete(id: Long): Try[UserQueueMember] =
    dbXc.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure) entry // already deleted
      else {
        val deletedCount = SQL("delete from xc.users where id={id}")
          .on("id" -> id)
          .executeUpdate()
        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete User"))
      }
    }
}
