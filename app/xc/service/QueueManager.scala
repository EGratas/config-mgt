package xc.service

import anorm._
import com.google.inject.{ImplementedBy, Inject}
import common.{AnormMacro, Crud, CrudMacro}
import org.slf4j.LoggerFactory
import play.api.db.{Database, _}
import xc.model.Queue
import xivo.service.QueueFeatureManager

import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[QueueManagerImpl])
trait QueueManager extends Crud[Queue, Long] {}

class QueueManagerImpl @Inject() (
    @NamedDatabase("xc") dbXc: Database,
    xivoQueueFeatureMgr: QueueFeatureManager
) extends QueueManager {

  val logger = LoggerFactory.getLogger(getClass)
  val crudParser: RowParser[Queue] =
    AnormMacro.namedParser[Queue](AnormMacro.ColumnNaming.SnakeCase)

  private def xivoQueueFeatureToXcQueue(q: xivo.model.QueueFeature): Queue = {
    q.id match {
      case Some(id) => Queue(id, q.name, q.displayname, q.number)
      case None =>
        throw new Exception("Received invalid Xivo QueueFeature without id")
    }
  }

  override def create(t: Queue): Try[Queue] =
    dbXc.withConnection { implicit c =>
      Try({
        CrudMacro
          .query[Queue](
            t,
            "xc.queues",
            Set("id"),
            Set(),
            false,
            CrudMacro.SnakeCase,
            CrudMacro.SQLInsert
          )
          .executeInsert()
        t
      })
    }

  override def update(t: Queue): Try[Queue] =
    dbXc.withConnection { implicit c =>
      val updateCount = CrudMacro
        .query[Queue](
          t,
          "xc.queues",
          Set("id"),
          Set(),
          false,
          CrudMacro.SnakeCase,
          CrudMacro.SQLUpdate
        )
        .executeUpdate()

      if (updateCount == 1)
        Success(t)
      else
        Failure(new Exception("Cannot update Queue"))
    }

  override def get(id: Long): Try[Queue] =
    dbXc.withConnection { implicit c =>
      Try(
        SQL("select  * from xc.queues where id={id}")
          .on("id" -> id)
          .as(crudParser.single)
      ).recoverWith { case t =>
        logger.info("xc.Queue not found locally, try from xivo", t)
        xivoQueueFeatureMgr
          .get(id)
          .map(xivoQueueFeatureToXcQueue)
          .flatMap(create)
      }
    }

  override def all(): Try[List[Queue]] =
    dbXc.withConnection { implicit c =>
      Try(SQL("select  * from xc.queues").as(crudParser.*))
    }

  override def delete(id: Long): Try[Queue] =
    dbXc.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure) entry // already deleted
      else {
        val deletedCount = SQL("delete from xc.queues where id={id}")
          .on("id" -> id)
          .executeUpdate()
        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete Queue"))
      }
    }
}
