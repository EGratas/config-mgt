package xc.service

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import javax.inject.Inject
import play.api.db.{Database, _}
import xc.model.{CallQualification, SimpleCallQualification, SubQualification}

import scala.util.Try

@ImplementedBy(classOf[CallQualificationManagerImpl])
trait CallQualificationManager {
  def all(): Try[List[CallQualification]]
  def all(id: Long): Try[List[CallQualification]]
  def allByQueue(id: Long): Try[List[CallQualification]]
  def create(callQualification: CallQualification): Try[Option[Long]]
  def deleteQualification(id: Long): Try[Int]
  def deleteSubQualification(id: Long): Try[Int]
  def update(id: Long, callQualification: CallQualification): Try[Int]
  def updateSubQualification(s: SubQualification): Try[Int]
  def assign(
      id: Long,
      qualification: SimpleCallQualification
  ): Try[Option[Long]]
  def unassign(id: Long, qualification: SimpleCallQualification): Try[Int]
}

class CallQualificationManagerImpl @Inject() (
    @NamedDatabase("xivo") dbXivo: Database
) extends CallQualificationManager {

  val simple: RowParser[CallQualification] =
    get[Long]("qId") ~
      get[String]("qualificationName") map { case id ~ qualificationName =>
        CallQualification(
          id = Some(id),
          name = qualificationName,
          subQualification = getSubQualification(id)
        )
      }

  def simpleSubQualification: RowParser[SubQualification] = {
    get[Option[Long]]("id") ~
      get[String]("name") map { case id ~ name =>
        SubQualification(id = id, name = name)
      }
  }

  private def getSubQualification(
      qualificationId: Long
  ): List[SubQualification] =
    dbXivo.withConnection { implicit c =>
      SQL(s"""SELECT
         |        id, name
         | FROM
         |        subqualifications
         | WHERE
         |        qualification_id={qualificationId} AND active=1
         |
     """.stripMargin)
        .on(Symbol("qualificationId") -> qualificationId)
        .as(simpleSubQualification.*)
    }

  override def all(): Try[List[CallQualification]] =
    dbXivo.withConnection { implicit c =>
      Try(SQL(s"""SELECT
         |        qualifications.id as qId, qualifications.name as qualificationName
         | FROM
         |        qualifications
         | WHERE
         |        qualifications.active=1""".stripMargin).as(simple.*))
    }

  override def all(id: Long): Try[List[CallQualification]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""SELECT
         |        qualifications.id as qId, qualifications.name as qualificationName
         | FROM
         |        qualifications
         | WHERE
         |        qualifications.id = {id}""".stripMargin)
          .on(Symbol("id") -> id)
          .as(simple.*)
      )
    }

  override def allByQueue(queueId: Long): Try[List[CallQualification]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""SELECT
         |        DISTINCT(qualifications.id) as qId, qualifications.name as qualificationName
         | FROM
         |        qualifications
         | LEFT JOIN
         |        queue_qualification
         | ON
         |        qualifications.id=queue_qualification.qualification_id
         | WHERE
         |        queue_qualification.queue_id={queueId}
       """.stripMargin)
          .on(Symbol("queueId") -> queueId)
          .as(simple.*)
      )
    }

  override def create(callQualification: CallQualification): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try {
        val qualificationId: Option[Long] = SQL(s"""INSERT INTO
           |        qualifications (name)
           | VALUES
           |        ({name})""".stripMargin)
          .on(Symbol("name") -> callQualification.name)
          .executeInsert()

        qualificationId match {
          case Some(id) =>
            for (s <- callQualification.subQualification) {
              Try(
                SQL("""INSERT INTO
                |        subqualifications(name, qualification_id)
                | VALUES
                |         ({name}, {qualificationId})""".stripMargin)
                  .on(
                    Symbol("name")            -> s.name,
                    Symbol("qualificationId") -> qualificationId
                  )
                  .executeInsert()
              )
            }
          case None => throw new Exception("Failed to create qualification.")
        }
        qualificationId
      }
    }

  override def deleteQualification(id: Long): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" UPDATE
        |        qualifications
        | SET
        |        active=0
        | WHERE
        |        id={id};
        | UPDATE
        |        subqualifications
        | SET
        |        active=0
        | WHERE
        |         qualification_id={id};
        |  """.stripMargin)
          .on(
            Symbol("id") -> id
          )
          .executeUpdate()
      )
    }

  override def deleteSubQualification(id: Long): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" UPDATE
        |        subqualifications
        | SET
        |        active=0
        | WHERE
        |         id={id};
        |  """.stripMargin)
          .on(
            Symbol("id") -> id
          )
          .executeUpdate()
      )
    }

  override def update(id: Long, qualification: CallQualification): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" UPDATE
        |        qualifications
        | SET
        |        name={name}
        | WHERE
        |        id={id};
        |  """.stripMargin)
          .on(
            Symbol("id")   -> id,
            Symbol("name") -> qualification.name
          )
          .executeUpdate()
      )
    }

  override def updateSubQualification(s: SubQualification): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" UPDATE
        |        subqualifications
        | SET
        |        name={name}
        | WHERE
        |        id={id};
        |  """.stripMargin)
          .on(
            Symbol("id")   -> s.id,
            Symbol("name") -> s.name
          )
          .executeUpdate()
      )
    }

  override def assign(
      queueId: Long,
      simpleQualification: SimpleCallQualification
  ): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""INSERT INTO
         |        queue_qualification (queue_id, qualification_id)
         | VALUES
         |        ({queueId}, {qualificationId})""".stripMargin)
          .on(
            Symbol("queueId")         -> queueId,
            Symbol("qualificationId") -> simpleQualification.id
          )
          .executeInsert()
      )
    }

  override def unassign(
      queueId: Long,
      simpleQualification: SimpleCallQualification
  ): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(
          s"""DELETE FROM
         |        queue_qualification
         | WHERE
         |        queue_qualification.queue_id={queueId} AND queue_qualification.qualification_id={qualificationId}""".stripMargin
        )
          .on(
            Symbol("queueId")         -> queueId,
            Symbol("qualificationId") -> simpleQualification.id
          )
          .executeUpdate()
      )
    }
}
