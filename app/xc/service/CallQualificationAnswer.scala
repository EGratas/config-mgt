package xc.service

import java.sql.Timestamp

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import javax.inject.Inject
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import play.api.db.{Database, _}
import xc.model.CallQualificationAnswer

import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[CallQualificationAnswerManagerImpl])
trait CallQualificationAnswerManager {
  def all(): Try[List[CallQualificationAnswer]]
  def all(id: Long): Try[List[CallQualificationAnswer]]
  def allByQueue(
      queueId: Long,
      from: String,
      to: String
  ): Try[List[CallQualificationAnswer]]
  def create(answer: CallQualificationAnswer): Try[Option[Long]]
  def delete(id: Long): Try[Int]
  def update(id: Long, answer: CallQualificationAnswer): Try[Int]
}

class CallQualificationAnswerManagerImpl @Inject() (
    @NamedDatabase("xivo") dbXivo: Database
) extends CallQualificationAnswerManager {

  val simple: RowParser[CallQualificationAnswer] =
    get[Int]("sub_qualification_id") ~
      get[String]("time") ~
      get[String]("callid") ~
      get[Int]("agent") ~
      get[Int]("queue") ~
      get[String]("first_name") ~
      get[String]("last_name") ~
      get[String]("comment") ~
      get[String]("custom_data") map {
        case subQualifId ~ time ~ callId ~ agent ~ queue ~ firstName ~ lastName ~ comment ~ customData =>
          CallQualificationAnswer(
            subQualifId,
            time.toString,
            callId,
            agent,
            queue,
            firstName,
            lastName,
            comment,
            customData
          )
      }

  def checkDateFormat(stringTime: String): String = {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    Try[DateTime](formatter.parseDateTime(stringTime)) match {
      case Success(_) =>
        stringTime
      case Failure(_) =>
        Try[DateTime](
          DateTimeFormat.forPattern("yyyy-MM-dd").parseDateTime(stringTime)
        ) match {
          case Success(_) =>
            stringTime.concat(" 00:00:00")
          case Failure(_) =>
            throw new Exception(
              "Call qualification date range is in incorrect format."
            )
        }
    }
  }

  def toTimestamp(stringTime: String): Timestamp =
    Timestamp.valueOf(checkDateFormat(stringTime))

  override def all(): Try[List[CallQualificationAnswer]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""SELECT sub_qualification_id, time::text, callid, agent, queue, first_name, last_name, comment, custom_data
        | FROM qualification_answers""".stripMargin).as(simple.*)
      )
    }

  override def all(id: Long): Try[List[CallQualificationAnswer]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""SELECT sub_qualification_id, time::text, callid, agent, queue, first_name, last_name, comment, custom_data
        | FROM qualification_answers
        |  WHERE id={id}""".stripMargin)
          .on(Symbol("id") -> id)
          .as(simple.*)
      )
    }

  override def allByQueue(
      queueId: Long,
      from: String,
      to: String
  ): Try[List[CallQualificationAnswer]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""SELECT
        |       sub_qualification_id, time::text, callid, agent, queue,
        |       first_name, last_name, comment, custom_data
        |  FROM
        |       qualification_answers
        |  WHERE
        |       queue = {queueId}
        |         AND
        |       time <= {toRefTime}
        |         AND
        |       time >= {fromRefTime}
        |  """.stripMargin)
          .on(
            Symbol("queueId")     -> queueId,
            Symbol("fromRefTime") -> toTimestamp(from),
            Symbol("toRefTime")   -> toTimestamp(to)
          )
          .as(simple.*)
      )
    }

  override def create(answer: CallQualificationAnswer): Try[Option[Long]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" INSERT INTO qualification_answers
        | (sub_qualification_id, time, callid, agent, queue, first_name, last_name, comment, custom_data)
        | VALUES ({subQualifId}, {time}, {callid}, {agent}, {queue}, {first_name}, {last_name}, {comment}, {custom_data})
        |  """.stripMargin)
          .on(
            Symbol("subQualifId") -> answer.sub_qualification_id,
            Symbol("time")        -> toTimestamp(answer.time),
            Symbol("callid")      -> answer.callid,
            Symbol("agent")       -> answer.agent,
            Symbol("queue")       -> answer.queue,
            Symbol("first_name")  -> answer.firstName,
            Symbol("last_name")   -> answer.lastName,
            Symbol("comment")     -> answer.comment,
            Symbol("custom_data") -> answer.customData
          )
          .executeInsert()
      )
    }

  override def delete(id: Long): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" DELETE FROM qualification_answers
        | WHERE id={id};
        |  """.stripMargin)
          .on(
            Symbol("id") -> id
          )
          .executeUpdate()
      )
    }

  override def update(id: Long, answer: CallQualificationAnswer): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(""" UPDATE qualification_answers
        |  SET sub_qualification_id={sub_qualification_id}, time={time}, callid={callid},
        |  agent={agent}, queue={queue}, first_name={first_name}, last_name={last_name}, comment={comment}, custom_data={custom_data}
        | WHERE id={id};
        |  """.stripMargin)
          .on(
            Symbol("id")                   -> id,
            Symbol("sub_qualification_id") -> answer.sub_qualification_id,
            Symbol("time")                 -> toTimestamp(answer.time),
            Symbol("callid")               -> answer.callid,
            Symbol("agent")                -> answer.agent,
            Symbol("queue")                -> answer.queue,
            Symbol("first_name")           -> answer.firstName,
            Symbol("last_name")            -> answer.lastName,
            Symbol("comment")              -> answer.comment,
            Symbol("custom_data")          -> answer.customData
          )
          .executeUpdate()
      )
    }
}
