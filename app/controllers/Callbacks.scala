package controllers

import java.security.InvalidParameterException

import javax.inject.Inject
import ws.model.{RescheduleCallback => WsRescheduleCallback}
import model._
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{Call, InjectedController, Result}

class Callbacks @Inject() (
    callbackList: CallbackListManager,
    callbackRequest: CallbackRequestManager,
    callbackPeriods: PreferredCallbackPeriodManager,
    secured: Secured
) extends InjectedController {

  val logger = LoggerFactory.getLogger(getClass)

  val loginRoute: Call = routes.Login.login

  def lists(withRequests: Boolean) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <Lists> $withRequests")
            Ok(Json.toJson(callbackList.all(withRequests)))
          }
    )

  def importCsv(listUuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <importCsv> $listUuid")
            request.body.asText match {
              case None =>
                BadRequest(Json.toJson("Payload should not be empty"))
              case Some(csv) =>
                callbackRequest.createFromCsv(callbackList, listUuid, csv)
                Created("")
            }
          }
    )

  def takeCallback(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <takeCallback> $uuid")
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                (json \ "agentId").asOpt[Long] match {
                  case None => BadRequest("Parameter agentId is required")
                  case Some(agentId) =>
                    callbackRequest.takeWithAgent(
                      uuid,
                      (json \ "agentId").asOpt[Long].get
                    )
                    Ok("")
                }
            }
          }
    )

  def releaseCallback(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <releaseCallback> $uuid")
            callbackRequest.release(uuid)
            Ok("")
          }
    )

  def getTakenCallbacks(agentId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(
              s"$user - Callbaks - Req : <getTakenCallbacks> $agentId"
            )
            Ok(
              Json.obj(
                "ids" -> Json.toJson(callbackRequest.takenCallbacks(agentId))
              )
            )
          }
    )

  def getCallback(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <getCallback> $uuid")
            Ok(Json.toJson(callbackRequest.getCallback(uuid)))
          }
    )

  def findCallback =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <findCallback>")
            import CallbackRequest.{readsFind, writesResp}
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                json.validate[FindCallbackRequest] match {
                  case JsSuccess(f, _) =>
                    Ok(
                      Json.toJson(
                        callbackRequest.find(f.filters, f.offset, f.limit)
                      )
                    )
                  case JsError(e) => BadRequest(e.toString)
                }
            }
          }
    )

  def createList =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <createList>")
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                json.validate[CallbackList] match {
                  case JsSuccess(res, _) =>
                    Created(Json.toJson(callbackList.create(res)))
                  case JsError(e) => BadRequest(e.toString)
                }
            }
          }
    )

  def deleteList(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <deleteList> $uuid")
            callbackList.delete(uuid)
            Ok("")
          }
    )

  def clotureCallback(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <clotureCallback> $uuid")
            callbackRequest.cloture(uuid)
            Ok("")
          }
    )

  def unclotureCallback(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <unclotureCallback> $uuid")
            callbackRequest.uncloture(uuid)
            Ok("")
          }
    )

  def rescheduleCallback(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <rescheduleCallback> $uuid")
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                json.validate[WsRescheduleCallback] match {
                  case JsSuccess(o, _) =>
                    logger.info(s"Rescheduling callback $uuid to ${o.dueDate}")
                    callbackRequest.reschedule(uuid, o.dueDate, o.periodUuid)
                    Ok("")
                  case JsError(e) => BadRequest("Invalid JSON")
                }
            }
          }
    )

  def createCallbackRequest(listUuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(
              s"$user - Callbaks - Req : <createCallbackRequest> $listUuid"
            )
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                val res = callbackRequest.create(
                  callbackRequest.fromJson(listUuid, json)
                )
                Created(Json.toJson(res))
            }
          }
    )

  def getCallbackPeriods(name: Option[String] = None) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <getCallbackPeriods> $name")
            name match {
              case None => Ok(Json.toJson(callbackPeriods.getPeriods))
              case Some(s) =>
                Ok(Json.toJson(callbackPeriods.getPeriodsByName(s)))
            }
          }
    )

  def createCallbackPeriod =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <createCallbackPeriod>")
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                json.validate[PreferredCallbackPeriod] match {
                  case JsSuccess(res, _) =>
                    Created(Json.toJson(callbackPeriods.createPeriod(res)))
                  case JsError(e) => BadRequest(e.toString)
                }
            }
          }
    )

  def deleteCallbackPeriod(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(
              s"$user - Callbaks - Req : <deleteCallbackPeriod> $uuid"
            )
            callbackPeriods.deletePeriod(uuid)
            Ok("")
          }
    )

  def editCallbackPeriod(uuid: String) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          WithExceptionCatching {
            logger.info(s"$user - Callbaks - Req : <editCallbackPeriod> $uuid")
            request.body.asJson match {
              case None => BadRequest("No JSON found")
              case Some(json) =>
                json.validate[PreferredCallbackPeriod] match {
                  case JsSuccess(res, _) =>
                    callbackPeriods.edit(res)
                    Ok("")
                  case JsError(e) => BadRequest(e.toString)
                }
            }
          }
    )

  def WithExceptionCatching(f: => Result): Result =
    try {
      f
    } catch {
      case e: InvalidParameterException => BadRequest(Json.toJson(e.getMessage))
      case e: NoSuchElementException    => NotFound(Json.toJson(e.getMessage))
      case e: Exception                 => InternalServerError(Json.toJson(e.getMessage))
    }
}
