package controllers

import common.AuthUtil
import model.ws.JsonParsingError
import model._
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, EssentialAction, InjectedController}

import javax.inject.Inject

class Rights @Inject() (
    rightManager: RightManager,
    userProfileManager: UserProfileManager,
    secured: Secured
) extends InjectedController {

  def getAllUsers =
    secured.IsAuthenticated(
      None,
      request => Ok(Json.toJson(userProfileManager.list))
    )

  def getRights(login: String): Action[AnyContent] =
    Action {
      rightManager.forUser(login) match {
        case None    => NotFound
        case Some(r) => Ok(r.toJson)
      }
    }

  def createOrEditRight(login: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request => {
          val username = AuthUtil.getUsername(user)
          request.body.asJson match {
            case None =>
              RightError(
                JsonParsingError,
                s"Error for ${username} - request body cannot be empty"
              ).toResult
            case Some(value) =>
              try {
                val right = rightManager.fromJson(value)
                AuthUtil.WithAuthenticatedUser(
                  user,
                  {
                    case Some(_: AdminRight) =>
                      rightManager.createOrReplace(login, right)
                      Ok("")
                    case Some(_)
                        if username.isDefined && rightManager.userCanModifyUser(
                          username.get,
                          login
                        ) && rightManager
                          .userCanCreateRight(username.get, right) =>
                      rightManager.createOrReplace(login, right)
                      Ok("")
                    case _ =>
                      RightError(
                        InsufficientCreateRight,
                        s"${username} is not allowed to create right for user $login"
                      ).toResult
                  }
                )(rightManager)
              } catch {
                case _: IllegalArgumentException =>
                  RightError(
                    JsonParsingError,
                    s"Error for ${username} - invalid JSON"
                  ).toResult
              }
          }
        }
    )

  def deleteRight(login: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ => {
          val username = AuthUtil.getUsername(user)
          AuthUtil.WithAuthenticatedUser(
            user,
            {
              case Some(_: AdminRight) =>
                rightManager.deleteByLogin(login)
                Ok("")
              case Some(_)
                  if username.isDefined && rightManager
                    .userCanModifyUser(username.get, login) =>
                rightManager.deleteByLogin(login)
                Ok("")
              case _ =>
                RightError(
                  InsufficientDeleteRight,
                  s"${username} is not allowed to delete right for user $login"
                ).toResult
            }
          )(rightManager)
        }
    )
}
