package controllers

import common.AuthUtil
import model._
import play.api.libs.json.Json
import play.api.mvc.InjectedController

import javax.inject.Inject

class XivoObjects @Inject() (
    queueManager: QueueManager,
    rightManager: RightManager,
    agentGroupManager: AgentGroupManager,
    incallManager: IncallManager,
    xivoCtiUserManager: XivoCtiUserManager,
    secured: Secured
) extends InjectedController {

  def getQueues =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request => {
          import Queue._
          AuthUtil.WithAuthenticatedUser(
            user,
            {
              case Some(_: AdminRight) => Ok(Json.toJson(queueManager.all()))
              case Some(r: SupervisorRight) =>
                Ok(Json.toJson(queueManager.withIds(r.queueIds)))
              case _ => Forbidden
            }
          )(rightManager)
        }
    )

  def getAgentGroups =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request => {
          import AgentGroup._
          AuthUtil.WithAuthenticatedUser(
            user,
            {
              case Some(_: AdminRight) =>
                Ok(Json.toJson(agentGroupManager.all()))
              case Some(r: SupervisorRight) =>
                Ok(Json.toJson(agentGroupManager.withIds(r.groupIds)))
              case _ => Forbidden
            }
          )(rightManager)
        }
    )

  def getIncalls =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request => {
          import Incall._
          AuthUtil.WithAuthenticatedUser(
            user,
            {
              case Some(_: AdminRight) => Ok(Json.toJson(incallManager.all()))
              case Some(r: SupervisorRight) =>
                Ok(Json.toJson(incallManager.withIds(r.incallIds)))
              case _ => Forbidden
            }
          )(rightManager)
        }
    )

  def getXivoCtiUsers = {
    import XivoCtiUser._
    secured.IsAuthenticated(
      None,
      request => Ok(Json.toJson(xivoCtiUserManager.all))
    )
  }
}
