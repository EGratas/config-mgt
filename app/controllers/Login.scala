package controllers

import model.RightManager
import models.AuthenticatedUser
import models.authentication.AuthenticationProvider
import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.I18nSupport
import play.api.mvc._
import views.html

import javax.inject.Inject

class Login @Inject() (
    rightManager: RightManager,
    authProvider: AuthenticationProvider
) extends InjectedController
    with I18nSupport {

  val logger = Logger(getClass.getName)

  def login =
    Action { implicit request => Ok(html.login("Config-Mgt", loginForm)) }

  val loginForm = Form(tuple("login" -> text, "mdp" -> text))

  def authenticate =
    Action { implicit request =>
      loginForm
        .bindFromRequest()
        .fold(
          formWithErrors =>
            BadRequest(html.login("Config-Mgt", formWithErrors)),
          { case (login, pwd) =>
            authProvider.authenticate(login, pwd) match {
              case Some(AuthenticatedUser(username, superAdmin))
                  if superAdmin =>
                Redirect(routes.Pages.rights).withSession(
                  "username"     -> username,
                  "isSuperAdmin" -> "true"
                )
              case Some(AuthenticatedUser(_, superAdmin))
                  if !superAdmin && rightManager.forUser(login).isDefined =>
                Redirect(routes.Pages.rights).withSession("username" -> login)
              case Some(_) | None =>
                BadRequest(
                  html.login(
                    "Config-Mgt",
                    loginForm
                      .withGlobalError("Login ou mot de passe invalide")
                  )
                )
            }
          }
        )
    }

  def logout =
    Action { implicit request =>
      logger.info(s"user ${request.session.get("username")} logout")
      Redirect(routes.Login.login).withNewSession
    }
}
