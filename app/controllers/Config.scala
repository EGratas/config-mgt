package controllers

import play.api.libs.json.{JsObject, JsString, JsValue, Json}
import play.api.mvc.{EssentialAction, InjectedController}

import javax.inject.Inject
import ws.controllers.WithExceptionCatching
import play.api.Configuration

import java.io.{BufferedReader, FileReader}
import scala.util.{Try, Using}
import java.nio.file.{Files, Paths}
import org.slf4j.{Logger, LoggerFactory}

import java.io.File
import play.api.mvc.Result
import model.ws.GenericError
import model.ws.NotFoundError
import xivo.model.MobileAppError

trait FileCheckTyping {
  type FileCheckResult[A] = Either[Result, A]
}
trait MobileConfiguration extends FileCheckTyping {
  val pushConfigKey: String
  val configMessage: String
  val checkSetupMessage: String
  val onParsingErrorMsg: String
  val onFindingErrorMsg: String

  def parse(file: File, logger: Logger): Either[Throwable, JsValue]

  def readFile(file: File, lineSeparator: String = ""): Try[String] = {
    Using(new BufferedReader(new FileReader(file))) { reader =>
      val fileText =
        Iterator
          .continually(reader.readLine())
          .takeWhile(_ != null)
          .map(st => st + lineSeparator)
          .mkString
      if (fileText.isEmpty) throw new Exception(onParsingErrorMsg)
      fileText
    }
  }
}

object AndroidConfig extends MobileConfiguration {
  val pushConfigKey: String = "mobile.FirebaseKeyPath"
  val configMessage         = "<getFirebaseConfig>"
  val checkSetupMessage     = "<checkAndroidSetup>"
  val onParsingErrorMsg =
    "Failed to parse firebase configuration file content, check the json file"
  val onFindingErrorMsg =
    "Unable to find firebase configuration file"

  def parse(file: File, logger: Logger): Either[Throwable, JsValue] = {
    readFile(file).toEither.map(fileText => Json.parse(fileText))
  }
}

object IosConfig extends MobileConfiguration {
  val pushConfigKey: String = "mobile.ApnsKeyPath"
  val configMessage         = "<getApnsConfig>"
  val checkSetupMessage     = "<checkIosSetup>"
  val onParsingErrorMsg =
    "Failed to parse apns configuration file content, check the file"
  val onFindingErrorMsg =
    "Unable to find apns configuration file"

  def parse(file: File, logger: Logger): Either[Throwable, JsValue] = {
    readFile(file, "\n").toEither.map(fileText =>
      JsObject(Seq("config" -> JsString(fileText)))
    )
  }
}

class Config @Inject() (
    secured: Secured,
    configuration: Configuration
) extends InjectedController
    with WithExceptionCatching
    with FileCheckTyping {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def getFirebaseConfig: EssentialAction = getConfig(AndroidConfig)
  def getApnsConfig: EssentialAction     = getConfig(IosConfig)
  def getConfig(config: MobileConfiguration): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            logger.info(s"$user - Req : ${config.configMessage}")
            checkAndParseFile(config) match {
              case Right(configJson) => Ok(configJson)
              case Left(result)      => result
            }
          }
    )

  def checkAndroidSetup: EssentialAction = checkSetup(AndroidConfig)
  def checkIosSetup: EssentialAction     = checkSetup(IosConfig)
  def checkSetup(config: MobileConfiguration): EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            logger.info(s"$user - Req : ${config.checkSetupMessage}")
            checkAndParseFile(config) match {
              case Right(_)     => NoContent
              case Left(result) => result
            }
          }
    )
  }

  private def checkAndParseFile(
      config: MobileConfiguration
  ): FileCheckResult[JsValue] =
    readFileIfExists(config)
      .flatMap(
        config
          .parse(_, logger)
          .left
          .map(_ => {
            logger.warn(config.onParsingErrorMsg)
            MobileAppError(
              MobileAppError.ParsingError,
              config.onParsingErrorMsg
            ).toResult
          })
      )

  private def readFileIfExists(
      config: MobileConfiguration
  ): FileCheckResult[File] = {
    val filepath = configuration.get[String](config.pushConfigKey)
    if (Files.exists(Paths.get(filepath))) {
      Right(new File(filepath))
    } else {
      logger.warn(config.onFindingErrorMsg)
      Left(
        GenericError(
          NotFoundError,
          config.onFindingErrorMsg
        ).toResult
      )
    }
  }

}
