package controllers

import configuration.AuthConfig
import play.api.i18n.I18nSupport
import play.api.mvc.{Call, InjectedController}

import javax.inject.Inject

class Pages @Inject() (
    authConfig: AuthConfig,
    secured: Secured
) extends InjectedController
    with I18nSupport {

  private lazy val httpContext =
    authConfig.config.getOptional[String]("play.http.context")

  def redirect(page: String) =
    Action(Redirect(s"${httpContext.getOrElse("")}/$page"))

  def rights =
    secured.IsAuthenticated(
      loginRoute,
      implicit request => Ok(views.html.rights("Config-Mgt"))
    )

  def callbacks =
    secured.IsAuthenticated(
      loginRoute,
      implicit request => Ok(views.html.callbacks("Config-Mgt"))
    )

  def apiDocs =
    Action(
      Redirect(
        s"${httpContext.getOrElse("")}/api/swagger-ui/index.html?url=/configmgt/api/swagger.json"
      )
    )

  val loginRoute: Option[Call] = Some(
    Call("GET", s"${httpContext.getOrElse("")}${routes.Login.login}")
  )
}
