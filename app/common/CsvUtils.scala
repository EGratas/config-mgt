package common

import scala.annotation.tailrec

object CsvUtils {

  def detectCsvSeparator(line: String, candidates: List[Char]): Option[Char] = {
    @tailrec
    def parseChar(line: String, pos: Int, inquote: Boolean): Option[Char] = {
      if (pos >= line.length) None
      else
        line(pos) match {
          case '"'                                     => parseChar(line, pos + 1, !inquote)
          case c if !inquote && candidates.contains(c) => Some(c)
          case _                                       => parseChar(line, pos + 1, inquote)
        }
    }
    parseChar(line, 0, false)
  }

  def parseCsvLine(line: String, separator: Char): List[String] = {
    @tailrec
    def parseCol(
        pos: Int,
        inquote: Boolean,
        current: String,
        acc: List[String]
    ): List[String] = {
      if (pos >= line.length) acc ::: List(current)
      else
        line(pos) match {
          case '"' => parseCol(pos + 1, !inquote, current, acc)
          case c if !inquote && c == separator =>
            parseCol(pos + 1, inquote, "", acc ::: List(current))
          case c => parseCol(pos + 1, inquote, current + c, acc)
        }
    }
    parseCol(0, false, "", List.empty)
  }
}
