package common

import model.{AdminRight, RightManager}
import models.{
  AuthenticatedToken,
  AuthenticatedUser,
  CredentialsValidation,
  InvalidCredentials
}
import play.api.mvc.Result
import model._

object AuthUtil {
  def WithAuthenticatedUser(
      user: CredentialsValidation,
      f: Option[Right] => Result
  )(rightManager: RightManager): Result = {
    user match {
      case AuthenticatedUser(_, superAdmin) if superAdmin =>
        f(Some(AdminRight()))
      case AuthenticatedUser(username, _) => f(rightManager.forUser(username))
      case AuthenticatedToken             => f(Some(AdminRight()))
      case InvalidCredentials             => f(None)
    }
  }

  def getUsername(user: CredentialsValidation): Option[String] = {
    user match {
      case AuthenticatedUser(username, _) => Some(username)
      case AuthenticatedToken             => None
      case InvalidCredentials             => None
    }
  }
}
