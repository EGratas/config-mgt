package ws.controllers

import controllers.Secured

import javax.inject.Inject
import model.ws.{GenericError, JsonParsingError, NotFoundError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.InjectedController
import xivo.model.{FindXivoUserRequest, XivoAuthRequest}
import xivo.service.XivoUserManager

import scala.util.{Failure, Success}
import play.api.mvc.EssentialAction

class XivoUser @Inject() (xivoUserManager: XivoUserManager, secured: Secured)
    extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def validateUser =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Xivo user's Req : <validateUser>")
            request.body.asJson match {
              case Some(json) =>
                json.validate[XivoAuthRequest] match {
                  case JsSuccess(res, _) =>
                    xivoUserManager
                      .validateUserPassword(res.username, res.password) match {
                      case Success(_) => NoContent
                      case Failure(f) =>
                        GenericError(
                          NotFoundError,
                          s"User not found, $f"
                        ).toResult
                    }
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Error decoding JSON: $e"
                    ).toResult
                }
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON found in body"
                ).toResult
            }
          }
    )

  def getAllAsContact: EssentialAction = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Xivo user's Req : <getAllAsContact>")

            xivoUserManager.getAllAsContact match {
              case Success(xivoUsers) => Ok(Json.toJson(xivoUsers))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  "Unable to get Xivo users, $f"
                ).toResult
            }
          }
    )
  }

  def getAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Xivo user's Req : <getAll>")

            xivoUserManager.getAll match {
              case Success(xivoUsers) => Ok(Json.toJson(xivoUsers))
              case Failure(f) =>
                GenericError(
                  NotHandledError,
                  s"Unable to get Xivo users, $f"
                ).toResult
            }
          }
    )

  def find =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            request.body.asJson match {
              case None =>
                GenericError(
                  JsonParsingError,
                  s"No JSON found in body"
                ).toResult
              case Some(json) =>
                json.validate[FindXivoUserRequest] match {
                  case JsSuccess(f, _) =>
                    logger.info(
                      s"$user - Xivo user's Req : <find> with filters ${f.filters} offset ${f.offset} and limit ${f.limit}"
                    )
                    xivoUserManager.find(f.filters, f.offset, f.limit) match {
                      case Success(foundXivoUsers) =>
                        Ok(Json.toJson(foundXivoUsers))
                      case Failure(f) =>
                        GenericError(
                          NotHandledError,
                          s"Unable to find Xivo users, $f"
                        ).toResult
                    }
                  case JsError(e) =>
                    GenericError(
                      JsonParsingError,
                      s"Unable to parse find Xivo users request, $e"
                    ).toResult
                }
            }
          }
    )
}
