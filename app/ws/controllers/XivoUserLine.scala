package ws.controllers

import controllers.Secured
import javax.inject.Inject
import model.ws.{GenericError, JsonParsingError, NotFoundError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.{AnyContent, InjectedController, Request, Result}
import ws.model.Line
import xivo.service.LineManager

import scala.util.{Failure, Success}

class XivoUserLine @Inject() (lineManager: LineManager, secured: Secured)
    extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def get(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Line Req: <get> for user Id $id")

            lineManager.get(id) match {
              case Success(line) => Ok(Json.toJson(line))
              case Failure(f) =>
                GenericError(
                  NotFoundError,
                  s"Unable to get line for user $id, $f"
                ).toResult
            }
          }
    )

  private def createOrUpdate(
      request: Request[AnyContent],
      id: Long,
      action: (Long, Line) => Result
  ): Result = {
    request.body.asJson match {
      case Some(json) =>
        json.validate[Line] match {
          case JsSuccess(line, _) =>
            action(id, line)
          case JsError(e) =>
            GenericError(JsonParsingError, s"Error decoding JSON: $e").toResult
        }
      case None =>
        GenericError(JsonParsingError, s"No JSON could be decoded").toResult
    }
  }

  def create(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Line Req: <create> for user Id $id")
            def fnCreate(id: Long, line: Line) =
              lineManager.create(id, line) match {
                case Success(line) => Ok(Json.toJson(line))
                case Failure(f) =>
                  GenericError(
                    NotFoundError,
                    s"Unable to create line for user Id $id, $f"
                  ).toResult
              }

            createOrUpdate(request, id, fnCreate)
          }
    )

  def update(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Line Req: <update> for user Id $id")

            def fnUpdate(id: Long, line: Line) =
              lineManager.update(id, line) match {
                case Success(line) => Ok(Json.toJson(line))
                case Failure(f) =>
                  GenericError(
                    NotFoundError,
                    s"Unable to update line for user Id $id, $f"
                  ).toResult
              }

            createOrUpdate(request, id, fnUpdate)
          }
    )

  def delete(id: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Line Req: <delete> for user Id $id")

            lineManager.delete(id) match {
              case Success(_) => NoContent
              case Failure(f) =>
                GenericError(
                  NotFoundError,
                  s"Unable to delete line for user $user, $f"
                ).toResult
            }
          }
    )

}
