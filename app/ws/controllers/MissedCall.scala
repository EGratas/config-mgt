package ws.controllers

import controllers.Secured
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json
import play.api.mvc.{EssentialAction, InjectedController}
import ws.model.NbMissedCall
import xivo.service._

import javax.inject.Inject
import scala.util.Try

class MissedCall @Inject() (
    userPreferenceManager: UserPreferenceManager,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {
  val logger: Logger = LoggerFactory.getLogger(getClass)

  def newMissedCall(userId: Long): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger
              .info(
                s"$user - New missed call for user Id $userId"
              )
            val currentMissedCall = Try(
              userPreferenceManager
                .get(userId, UserPreferenceKey.NbMissedCall)
                .map(_.value.toInt)
                .getOrElse(
                  0
                )
            )

            val result = for {
              missedCallInt <- currentMissedCall
              _ <- userPreferenceManager
                .createOrUpdate(
                  UserPreference(
                    userId,
                    UserPreferenceKey.NbMissedCall,
                    s"${missedCallInt + 1}",
                    "String"
                  )
                )
            } yield Ok(
              Json.toJson(NbMissedCall(userId, missedCallInt + 1))
            )

            result.getOrElse(
              UserPreferenceError(
                UserNotFound,
                s"Unable to get missed call for user $userId"
              ).toResult
            )
          }
    )

}
