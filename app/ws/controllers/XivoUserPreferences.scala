package ws.controllers

import controllers.Secured
import model.ws.{GenericError, JsonParsingError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import xivo.service._

import javax.inject.Inject
import scala.util.{Failure, Success}

class XivoUserPreferences @Inject() (
    userPreferenceManager: UserPreferenceManager,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def getAll(userid: Long): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Preferences Req: <getAll> for user Id $userid"
            )

            userPreferenceManager.getAll(userid) match {
              case Success(userPrefs) => Ok(Json.toJson(userPrefs))
              case Failure(f) =>
                UserPreferenceError(
                  UserNotFound,
                  s"Unable to get missed call user $userid, $f"
                ).toResult
            }
          }
    )

  def get(userid: Long, key: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Preferences Req: <get> $key for user Id $userid"
            )

            userPreferenceManager.get(userid, key) match {
              case Success(pref) =>
                Ok(
                  Json.toJson(UserPreferencePayload(pref.value, pref.valueType))
                )
              case Failure(f) =>
                UserPreferenceError(
                  PreferenceNotFound,
                  s"Unable to get user $userid preference $key, $f"
                ).toResult
            }
          }
    )

  private def createOrUpdate(
      request: Request[AnyContent],
      id: Long,
      key: String,
      action: (UserPreference) => Result
  ): Result = {
    request.body.asJson match {
      case Some(json) =>
        json.validate[UserPreferencePayload] match {
          case JsSuccess(payload, _) =>
            action(UserPreference(id, key, payload.value, payload.valueType))
          case JsError(e) =>
            GenericError(JsonParsingError, s"Error decoding JSON: $e").toResult
        }
      case None =>
        GenericError(JsonParsingError, s"No JSON could be decoded").toResult
    }
  }

  def create(id: Long, key: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Preference Req: <create> for user Id $id and preference $key"
            )
            def createUsrPref(usrPref: UserPreference) =
              userPreferenceManager.create(usrPref) match {
                case Success(_) =>
                  Ok(
                    Json.toJson(
                      UserPreferencePayload(usrPref.value, usrPref.valueType)
                    )
                  )
                case Failure(f) =>
                  f match {
                    case PreferenceAlreadyExistException(m) =>
                      UserPreferenceError(PreferenceAlreadyExists, m).toResult
                    case _ =>
                      Results.InternalServerError(
                        s"Unable to create preference $key for user $id, $f"
                      )
                  }
              }
            if (!preferenceKeyExist(key))
              UserPreferenceError(
                PreferenceKeyNotExist,
                s"It's not possible to create preference $key : this key does not exist"
              ).toResult
            else
              createOrUpdate(request, id, key, createUsrPref)
          }
    )

  def update(id: Long, key: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Preference choice Req: <update> for user Id $id and preference $key"
            )
            def updateUsrPref(usrPref: UserPreference) =
              userPreferenceManager.update(usrPref) match {
                case Success(nbUpdated) =>
                  nbUpdated match {
                    case 0 =>
                      UserPreferenceError(
                        PreferenceNotFound,
                        s"Preference $key does not exist for user $id"
                      ).toResult
                    case _ =>
                      Ok(
                        Json.toJson(
                          UserPreferencePayload(
                            usrPref.value,
                            usrPref.valueType
                          )
                        )
                      )
                  }
                case Failure(f) =>
                  Results.InternalServerError(
                    s"Unable to update preference $key for user $id, $f"
                  )
              }

            createOrUpdate(request, id, key, updateUsrPref)
          }
    )

  def delete(id: Long, key: String): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(
              s"$user - Preference choice Req: <delete> for user Id $id and preference $key"
            )
            userPreferenceManager.delete(id, key) match {
              case Success(nbUpdated) =>
                nbUpdated match {
                  case 0 =>
                    UserPreferenceError(
                      PreferenceNotFound,
                      s"Preference $key does not exist for user $id"
                    ).toResult
                  case _ => NoContent
                }
              case Failure(f) =>
                UserPreferenceError(
                  PreferenceNotFound,
                  s"Unable to get user $id preference $key, $f"
                ).toResult
            }
          }
    )

  private def preferenceKeyExist(key: String): Boolean = key match {
    case UserPreferenceKey.PreferredDevice => true
    case UserPreferenceKey.MobileAppInfo   => true
    case _                                 => false
  }
}
