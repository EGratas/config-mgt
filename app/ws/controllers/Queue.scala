package ws.controllers

import anorm.AnormException
import controllers.Secured
import javax.inject.Inject
import model.ws.{GenericError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.libs.json.{Json, Writes}
import play.api.mvc.{InjectedController, Result}
import xivo.model.{QueueFeature, QueueFeatureError, QueueFeatureNotFound}
import xivo.service.QueueFeatureManager

import scala.util.{Failure, Success, Try}

class Queue @Inject() (
    queueManager: QueueFeatureManager,
    configuration: Configuration,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def handleQueueResult[T](
      resultTry: Try[T],
      action: String,
      errMsg: String = ""
  )(implicit rds: Writes[T]): Result = {
    resultTry match {
      case Success(queue) =>
        Ok(Json.toJson(queue))
      case Failure(_: AnormException) =>
        QueueFeatureError(
          QueueFeatureNotFound,
          s"Unable to perform $action $errMsg"
        ).toResult
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to perform $action $errMsg: ${f.getMessage}"
        ).toResult
    }
  }

  def getQueueConfig(queueId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "getQueueConfig"
            logger.info(s"$user - Req : <$action>")

            handleQueueResult[QueueFeature](queueManager.get(queueId), action)
          }
    )

  def getQueueConfigAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "getQueueConfigAll"
            logger.info(s"$user - Req : <$action>")

            handleQueueResult[List[QueueFeature]](queueManager.all(), action)
          }
    )

}
