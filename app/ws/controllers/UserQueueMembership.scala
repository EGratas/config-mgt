package ws.controllers

import javax.inject.Inject

import controllers.Secured
import org.slf4j.LoggerFactory
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc.InjectedController
import ws.model._
import xc.model.QueueMemberDefault
import xc.service.QueueMemberDefaultManager

import scala.util.{Failure, Success}

class UserQueueMembership @Inject() (
    queueMemberDefaultManager: QueueMemberDefaultManager,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger = LoggerFactory.getLogger(getClass)

  def getUserDefaultMembership(userId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import QueueMembershipJson._
            logger.info(s"$user - Req : <getUserDefaultMembership> $userId")
            queueMemberDefaultManager.getByUserId(userId) match {
              case Success(l) =>
                Ok(
                  Json.toJson(l.map(m => QueueMembership(m.queueId, m.penalty)))
                )
              case Failure(t) => throw t
            }
          }
    )

  def getAllDefaultMembership =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import UserQueueMembershipJson._
            logger.info(s"$user - Req : <getAllDefaultMembership>")
            queueMemberDefaultManager.all() match {
              case Success(l) =>
                Ok(Json.toJson(ws.model.UserQueueMembership.convert(l)))
              case Failure(t) => throw t
            }
          }
    )

  def setUsersDefaultMembership() =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import UsersQueueMembershipJson._
            logger.info(s"$user - Req : <setUsersDefaultMembership>")
            request.body.asJson match {
              case None =>
                BadRequest("No JSON found")
              case Some(json) =>
                json.validate[ws.model.UsersQueueMembership] match {
                  case JsSuccess(usersQueue, _) =>
                    val failures = usersQueue.userIds
                      .map(userId => {
                        val membership = usersQueue.membership.map(m =>
                          QueueMemberDefault(m.queueId, userId, m.penalty)
                        )
                        queueMemberDefaultManager
                          .setForUserId(userId, membership)
                      })
                      .collect { case Failure(t) => t }

                    if (failures.length > 0) {
                      InternalServerError(
                        failures.map(_.getMessage).mkString("\n")
                      )
                    } else {
                      Ok("")
                    }

                  case JsError(e) => BadRequest(e.toString)
                }
            }
          }
    )

  def setUserDefaultMembership(userId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            import QueueMembershipJson._
            logger.info(s"$user - Req : <setUserDefaultMembership> $userId")
            request.body.asJson match {
              case None =>
                BadRequest("No JSON found")
              case Some(json) =>
                json.validate[List[QueueMembership]] match {
                  case JsSuccess(res, _) =>
                    queueMemberDefaultManager
                      .setForUserId(
                        userId,
                        res.map(m =>
                          QueueMemberDefault(m.queueId, userId, m.penalty)
                        )
                      ) match {
                      case Success(_) => Ok("")
                      case Failure(t) => InternalServerError(t.getMessage)
                    }
                  case JsError(e) => BadRequest(e.toString)
                }
            }
          }
    )
}
