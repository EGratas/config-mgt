package ws.controllers.meetingrooms

import anorm.AnormException
import controllers.Secured
import model.ws.{GenericError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json
import play.api.mvc.Cookie.SameSite
import play.api.mvc.{
  AbstractController,
  AnyContent,
  ControllerComponents,
  Cookie,
  Request,
  Result
}
import ws.controllers.WithExceptionCatching
import xivo.model._
import xivo.service.MeetingRoomAuthentication.{
  MeetingRoomAuth,
  MeetingRoomAuthChallenge,
  MeetingRoomAuthValidated,
  MeetingRoomTitle
}
import xivo.service.MeetingRoomManager.{createGetFilter, NO_FILTER}
import xivo.service.{
  MeetingRoomAuthentication,
  MeetingRoomInstantTime,
  MeetingRoomManager,
  MeetingRoomUUIDGenerator
}

import java.util.{NoSuchElementException, UUID}
import javax.inject.Inject
import scala.util.{Failure, Success, Try}

class MeetingRoom @Inject() (
    meetingRoomManager: MeetingRoomManager,
    meetingRoomAuth: MeetingRoomAuthentication,
    common: MeetingRoomCommon,
    meetingRoomUUIDGenerator: MeetingRoomUUIDGenerator,
    secured: Secured,
    cc: ControllerComponents
)(implicit instantTime: MeetingRoomInstantTime)
    extends AbstractController(cc)
    with WithExceptionCatching {

  val logger: Logger                     = LoggerFactory.getLogger(getClass)
  implicit val roomType: RoomTypes.Value = RoomTypes.All

  private def handleGenericErrors(
      failure: Throwable,
      key: String,
      value: String
  ): Result = failure match {
    case f: AnormException =>
      MeetingRoomError(
        MeetingRoomError.NotFound,
        s"Meeting room with $key $value not found, ${f.getMessage()}"
      ).toResult
    case f =>
      GenericError(
        NotHandledError,
        s"Unable to get meeting room with $key $value, ${f.getMessage}"
      ).toResult
  }

  def getByNumber(number: String) = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            logger.info(s"$user - Meeting rooms Req : <getByNumber>")
            meetingRoomManager.get(createGetFilter("number", number)) match {
              case Success(room) =>
                Ok(Json.toJson(room))
              case Failure(f) => handleGenericErrors(f, "number", number)
            }
          }
    )
  }

  private def generateToken(room: MeetingRoomConfig) = {
    meetingRoomAuth
      .encode(
        MeetingRoomTokenContent(
          displayName = room.displayName,
          number = room.number,
          uuid = room.uuid.get,
          requirepin = !room.userPin.forall(_.isEmpty),
          timestamp = None,
          expiry = None
        )
      )
  }

  def getToken(id: String) = {
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Meeting rooms Req : <getToken>")
            val failure = MeetingRoomError(
              MeetingRoomError.Invalid,
              s"Unexpected error, this should not happen."
            )
            common.withUserId(
              request,
              false,
              failure,
              { userId =>
                meetingRoomManager
                  .get(createGetFilter("id", id))
                  .filter { room =>
                    userId match {
                      case Some(id) if room.userId.contains(id)      => true
                      case None if room.roomType == RoomTypes.Static => true
                      case _                                         => false
                    }
                  }
                  .map(room =>
                    (
                      room,
                      generateToken(room)
                    )
                  ) match {
                  case Success((_, token)) =>
                    Ok(Json.toJson(token))
                  case Failure(_: NoSuchElementException) =>
                    MeetingRoomError(
                      MeetingRoomError.NotFound,
                      s"Meeting room does not match with requested id $userId"
                    ).toResult
                  case Failure(f) => handleGenericErrors(f, "id", id)
                }
              }
            )
          }
    )
  }

  def getTemporaryToken(displayName: String) =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        _ =>
          Ok(
            Json.toJson(
              meetingRoomAuth
                .encode(
                  MeetingRoomTokenContent(
                    displayName = displayName,
                    number = None,
                    uuid = meetingRoomUUIDGenerator.generateUUID(),
                    requirepin = false,
                    timestamp = None,
                    expiry = Some(meetingRoomAuth.createExpireTimestamp()),
                    temporary = true
                  )
                )
            )
          )
    )

  def getAlias(id: String) =
    secured.WithAuthenticatedUser(
      None,
      _ =>
        _ =>
          meetingRoomManager
            .get(createGetFilter("id", id)) match {
            case Success(room) =>
              Ok(Json.toJson(MeetingRoomAliasResponse(room.alias)))
            case Failure(_: NoSuchElementException) =>
              MeetingRoomError(
                MeetingRoomError.NotFound,
                s"Meeting room does not match with requested id $id"
              ).toResult
            case Failure(f) => handleGenericErrors(f, "id", id)
          }
    )

  def validateAlias(alias: String) = {
    Action { implicit request =>
      withExceptionCatching {
        logger.info(s"Meeting rooms Req : <validateAlias>")
        meetingRoomManager.get(createGetFilter("alias", alias)) match {
          case Success(room) =>
            val token = generateToken(room).token
            val title = generateTitle(room.displayName, room.number)
            if (room.userPin.isDefined) {
              redirectToAuth(token, title)
            } else {
              redirectToRoom(room.uuid.get, token, title)
            }
          case Failure(f) => handleGenericErrors(f, "alias", alias)
        }
      }
    }
  }

  private def generateTitle(displayName: String, number: Option[String]) =
    MeetingRoomTitle(displayName, number.getOrElse("")).urlEncode

  private def redirectToAuth(token: String, title: MeetingRoomTitle) = {
    Found(
      s"""/authorize/video?token=$token#config.subject="${title.withNumber}""""
    ).withCookies(
      Cookie(
        "xivoMeetingRoomToken",
        token,
        httpOnly = true,
        sameSite = Some(SameSite.Lax)
      )
    )
  }

  private def redirectToRoom(
      uuid: UUID,
      token: String,
      title: MeetingRoomTitle
  ) = {
    Found(
      s"""/video/$uuid?jwt=$token#config.subject="${title.withNumber}""""
    )
      .withCookies(
        Cookie(
          "xivoMeetingRoomToken",
          token,
          httpOnly = true,
          sameSite = Some(SameSite.Lax)
        )
      )
  }

  def validateToken(token: String) =
    Action { implicit request =>
      withExceptionCatching {
        logger.info(s"Meeting rooms Req : <validateToken>")

        val triedTokenValidation = for {
          decoded <- meetingRoomAuth.decode(token)
          _       <- decoded.isExpired
          title = generateTitle(decoded.displayName, decoded.number)
          validated <- validateTemporaryToken(decoded, token, title).orElse(
            validatedRegularToken(request, decoded, token, title)
          )
        } yield validated

        val redirect = meetingRoomAuth.extractRedirectFlag(request)

        triedTokenValidation match {
          case Failure(f: MeetingRoomException) =>
            MeetingRoomError(f.error, f.message).toResult
          case Failure(f: AnormException) =>
            MeetingRoomError(
              MeetingRoomError.NotFound,
              s"Meeting room not found, ${f.getMessage()}"
            ).toResult
          case Failure(f: NoSuchElementException) =>
            MeetingRoomError(
              MeetingRoomError.Invalid,
              s"Access to meeting room is not allowed with this token, ${f.getMessage}"
            ).toResult
          case Failure(f) =>
            GenericError(
              NotHandledError,
              s"Unable to validate token for meeting room, $f"
            ).toResult
          case Success(MeetingRoomAuthChallenge(_, token, title)) if redirect =>
            redirectToAuth(token, title)

          case Success(MeetingRoomAuthChallenge(uuid, token, _)) =>
            Ok(Json.toJson(MeetingRoomTokenResponse(uuid, token)))
              .withCookies(
                Cookie(
                  "xivoMeetingRoomToken",
                  token,
                  httpOnly = true,
                  sameSite = Some(SameSite.Lax)
                )
              )
          case Success(MeetingRoomAuthValidated(uuid, token, title))
              if redirect =>
            redirectToRoom(uuid, token, title)

          case Success(MeetingRoomAuthValidated(uuid, token, _)) =>
            Ok(Json.toJson(MeetingRoomTokenResponse(uuid, token)))
              .withCookies(
                Cookie(
                  "xivoMeetingRoomToken",
                  token,
                  httpOnly = true,
                  sameSite = Some(SameSite.Lax)
                )
              )
        }
      }
    }

  def getAll = {
    val actionType = "getAll"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting rooms"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { _ => filters => _ =>
        common.meetingRoomGetAll(filters)
      },
      NO_FILTER
    )
  }

  def find = {
    val actionType = "find"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting room"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { request => filters => userId =>
        common.meetingRoomFind(request, filters, userId)
      },
      extraFilters = NO_FILTER
    )
  }

  private def validatedRegularToken(
      request: Request[AnyContent],
      decoded: MeetingRoomTokenContent,
      token: String,
      title: MeetingRoomTitle
  ): Try[MeetingRoomAuth] = {
    for {
      meetingRoom <- meetingRoomManager.get(
        createGetFilter("uuid", decoded.uuid.toString)
      )
      uuid <- Try(meetingRoom.uuid.get)
    } yield {
      meetingRoom.userPin
        .filterNot(_.isEmpty)
        .fold[MeetingRoomAuth](
          MeetingRoomAuthValidated(uuid, token, title)
        )({ meetingRoomPin =>
          meetingRoomAuth.extractPin(request) match {
            case Some(pin) if meetingRoomPin == pin =>
              val newToken = meetingRoomAuth.recreateToken(
                decoded.copy(
                  timestamp = meetingRoom.pinTimestamp,
                  expiry = Some(meetingRoomAuth.createExpireTimestamp())
                )
              )
              MeetingRoomAuthValidated(uuid, newToken, title)
            case Some(_) =>
              throw MeetingRoomException(
                MeetingRoomError.InvalidPin,
                "Unable to validate room access, pin not validated"
              )
            case None
                if meetingRoom.pinTimestamp
                  .exists(decoded.timestamp.contains) =>
              MeetingRoomAuthValidated(uuid, token, title)
            case None
                if decoded.timestamp
                  .exists(t => !meetingRoom.pinTimestamp.contains(t)) =>
              throw MeetingRoomException(
                MeetingRoomError.PinTimestampExpired,
                "Unable to validate room access, pin timestamp has expired"
              )
            case None =>
              val newToken: String = meetingRoomAuth.recreateToken(
                decoded.copy(requirepin = true, timestamp = None)
              )
              MeetingRoomAuthChallenge(uuid, newToken, title)
          }
        })
    }
  }

  private def validateTemporaryToken(
      decoded: MeetingRoomTokenContent,
      token: String,
      title: MeetingRoomTitle
  ): Try[MeetingRoomAuth] = {
    if (decoded.temporary)
      Success(MeetingRoomAuthValidated(decoded.uuid, token, title))
    else
      Failure(
        MeetingRoomException(
          MeetingRoomError.InvalidToken,
          "Not a temporary token."
        )
      )
  }
}
