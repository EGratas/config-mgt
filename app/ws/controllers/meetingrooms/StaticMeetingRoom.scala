package ws.controllers.meetingrooms

import org.slf4j.{Logger, LoggerFactory}
import play.api.mvc.{AbstractController, ControllerComponents, EssentialAction}
import ws.controllers.WithExceptionCatching
import xivo.model.{MeetingRoomError, RoomTypes}
import xivo.service.MeetingRoomManager.{createGetFilter, NO_FILTER}

import javax.inject.Inject

class StaticMeetingRoom @Inject() (
    common: MeetingRoomCommon,
    cc: ControllerComponents
) extends AbstractController(cc)
    with WithExceptionCatching {

  val logger: Logger                     = LoggerFactory.getLogger(getClass)
  implicit val roomType: RoomTypes.Value = RoomTypes.Static

  def getAll: EssentialAction = {
    val actionType = "getAll"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting rooms"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { _ => filters => _ =>
        common.meetingRoomGetAll(filters)
      },
      NO_FILTER
    )
  }

  def get(id: Long): EssentialAction = {
    val actionType = "get"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting rooms"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { _ => filters => _ =>
        common.meetingRoomGet(id, filters)
      },
      extraFilters = createGetFilter("id", id.toString)
    )
  }

  def create: EssentialAction = {
    val actionType = "create"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting room"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { request => _ => userId =>
        common.meetingRoomCreate(request, userId)
      },
      extraFilters = NO_FILTER
    )
  }

  def update: EssentialAction = {
    val actionType = "update"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting room"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { request => _ => userId =>
        common.meetingRoomUpdate(request, userId)
      },
      extraFilters = NO_FILTER
    )
  }

  def delete(id: Long): EssentialAction = {
    val actionType = "delete"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting room with id $id"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { _ => filters => _ =>
        common.meetingRoomDelete(id, filters)
      },
      extraFilters = createGetFilter("id", id.toString)
    )
  }

  def find: EssentialAction = {
    val actionType = "find"
    val failure = MeetingRoomError(
      MeetingRoomError.Invalid,
      s"Unable to $actionType meeting room"
    )
    common.genericSimple(
      userIdRequired = false,
      actionType = actionType,
      failure = failure,
      dbAction = { request => filters => userId =>
        common.meetingRoomFind(request, filters, userId)
      },
      extraFilters = NO_FILTER
    )
  }
}
