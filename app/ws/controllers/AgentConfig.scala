package ws.controllers

import anorm.AnormException
import controllers.Secured
import javax.inject.Inject
import model.ws.{GenericError, NotHandledError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.Configuration
import play.api.libs.json.{Json, Writes}
import play.api.mvc.{InjectedController, Result}
import xivo.model.{Agent, AgentError, AgentNotFound}
import xivo.service.AgentConfigManager

import scala.util.{Failure, Success, Try}

class AgentConfig @Inject() (
    agentManager: AgentConfigManager,
    configuration: Configuration,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def handleAgentResult[T](
      resultTry: Try[T],
      action: String,
      errMsg: String = ""
  )(implicit rds: Writes[T]): Result = {
    resultTry match {
      case Success(agent) =>
        Ok(Json.toJson(agent))
      case Failure(e: AnormException) =>
        AgentError(
          AgentNotFound,
          s"Unable to perform $action $errMsg ${e.getMessage()}"
        ).toResult
      case Failure(f) =>
        GenericError(
          NotHandledError,
          s"Unable to perform $action $errMsg: ${f.getMessage}"
        ).toResult
    }
  }

  def getAgentConfig(agentId: Long) =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "getAgentConfig"
            logger.info(s"$user - Req : <$action>")

            handleAgentResult[Agent](agentManager.getById(agentId), action)
          }
    )

  def getAgentConfigAll =
    secured.WithAuthenticatedUser(
      None,
      user =>
        _ =>
          withExceptionCatching {
            val action = "getAgentConfigAll"
            logger.info(s"$user - Req : <$action>")

            handleAgentResult[List[Agent]](agentManager.all(), action)
          }
    )
}
