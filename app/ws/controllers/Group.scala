package ws.controllers

import com.google.inject.Inject
import controllers.Secured
import model.ws.{ErrorResult, GenericError, JsonParsingError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.{Json, Reads}
import play.api.libs.ws.WSResponse
import play.api.mvc._
import xivo.model._
import xivo.service.{DialActionManager, GroupManager, SysConfdClient}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Left, Right, Success}

class Group @Inject() (
    secured: Secured,
    groupMgr: GroupManager,
    dialActionMgr: DialActionManager,
    sysConfd: SysConfdClient
) extends InjectedController
    with WithExceptionCatching {

  val log: Logger = LoggerFactory.getLogger(getClass)

  def getAllGroups: EssentialAction = handleCRUDGroupAction(GetAllGroupsAction)

  def getGroupById(groupId: Long): EssentialAction = handleCRUDGroupAction(
    GetGroupAction(groupId)
  )

  def createGroup(): EssentialAction = handleCRUDGroupAction(CreateGroupAction)

  def updateGroup(
      groupId: Long
  ): EssentialAction = handleCRUDGroupAction(UpdateGroupAction(groupId))

  def deleteGroup(groupId: Long): EssentialAction = handleCRUDGroupAction(
    DeleteGroupAction(groupId)
  )

  def addUserToGroup(groupId: Long, userId: Long): EssentialAction =
    addOrRemoveUserFromGroup(AddUser, groupId, userId)

  def removeUserFromGroup(groupId: Long, userId: Long): EssentialAction =
    addOrRemoveUserFromGroup(RemoveUser, groupId, userId)

  private def handleCRUDGroupAction(
      action: GroupAction
  ): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            log.info(
              s"Groups - $user - Req : $action"
            )
            (action match {
              case GetAllGroupsAction      => getGroupsWithDialActions
              case GetGroupAction(groupId) => getGroupWithDialAction(groupId)
              case CreateGroupAction =>
                createGroupAndDialAction(request)
              case UpdateGroupAction(groupId) =>
                updateGroupAndDialAction(groupId, request)
              case DeleteGroupAction(groupId) =>
                deleteGroupAndDialAction(groupId)
            }).fold(_.toResult, identity)
          }
    )

  private def getGroupsWithDialActions: Either[ErrorResult, Result] = {
    def updateGroupsWithDialActions(
        groups: List[xivo.model.Group],
        dialActions: Map[Long, Map[
          DialActionEventType.DialActionEventType,
          DialActionAction
        ]]
    ): List[GroupWithDialActions] =
      groups.map(group => {
        GroupWithDialActions(group, dialActions.getOrElse(group.id, Map.empty))
      })

    for {
      groups <- groupMgr.getAllGroups
      dialActions <- dialActionMgr
        .getAllDialActions("group")
      groupsWithDA = updateGroupsWithDialActions(groups, dialActions)
    } yield Ok(Json.toJson(groupsWithDA))
  }

  private def getGroupWithDialAction(
      groupId: Long
  ): Either[ErrorResult, Result] =
    for {
      group       <- groupMgr.getGroupById(groupId)
      dialActions <- dialActionMgr.getDialActions(groupId, "group")
      groupWithDA = GroupWithDialActions(group, dialActions)
    } yield Ok(Json.toJson(groupWithDA))

  private def createGroupAndDialAction(
      request: Request[AnyContent]
  ): Either[ErrorResult, Result] =
    for {
      createGroup <- validateBody[CreateGroup](request)
      group       <- groupMgr.createGroup(createGroup)
      dialActions <- dialActionMgr.createDialActions(
        group.id,
        "group",
        createGroup.dialactions
      )
      groupWithDA = GroupWithDialActions(group, dialActions)
    } yield Created(
      Json.toJson(groupWithDA)
    )

  private def updateGroupAndDialAction(
      groupId: Long,
      request: Request[AnyContent]
  ): Either[ErrorResult, Result] = {
    def updateGroupDialActions(
        maybeDAs: Option[
          Map[DialActionEventType.DialActionEventType, DialActionAction]
        ],
        group: xivo.model.Group
    ): Either[ErrorResult, GroupWithDialActions] =
      maybeDAs
        .map(dAs =>
          dialActionMgr
            .updateDialActions(groupId, "group", dAs)
        )
        .getOrElse(dialActionMgr.getDialActions(group.id, "group"))
        .map(dAs => GroupWithDialActions(group, dAs))

    for {
      updateGroup  <- validateBody[UpdateGroup](request)
      group        <- groupMgr.updateGroup(groupId, updateGroup)
      groupWithDAs <- updateGroupDialActions(updateGroup.dialactions, group)
    } yield Ok(Json.toJson(groupWithDAs))
  }

  private def deleteGroupAndDialAction(
      groupId: Long
  ): Either[ErrorResult, Result] =
    for {
      _ <- groupMgr.deleteGroup(groupId)
      _ <- dialActionMgr.deleteDialActions(groupId, "group")
    } yield NoContent

  private def addOrRemoveUserFromGroup(
      action: GroupMembershipAction,
      groupId: Long,
      userId: Long
  ): EssentialAction = {
    secured.WithAuthenticatedUserAsync(
      None,
      user =>
        _ =>
          withExceptionCatchingAsync {
            log.info(
              s"Groups - $user - Req : <$action> | for user $userId in group $groupId"
            )
            (action match {
              case AddUser    => groupMgr.addUserToGroup(groupId, userId)
              case RemoveUser => groupMgr.removeUserFromGroup(groupId, userId)
            }).map(_ => updateConfiguration())
              .fold(
                error => Future.successful(Left(error)),
                identity
              )
              .map {
                case Right(_) if action == AddUser => Created
                case Right(_)                      => NoContent
                case Left(groupError)
                    if groupError.error == ConfigurationReloadError =>
                  rollbackAction(action, groupId, userId)
                  groupError.toResult
                case Left(groupError) => groupError.toResult
              }
          }
    )
  }

  private def rollbackAction(
      action: GroupMembershipAction,
      groupId: Long,
      userId: Long
  ): Either[GroupError, Long] =
    if (action == AddUser) {
      groupMgr.removeUserFromGroup(groupId, userId)
    } else {
      groupMgr.addUserToGroup(groupId, userId)
    }

  private def updateConfiguration(): Future[Either[GroupError, WSResponse]] = {
    sysConfd
      .reloadAsteriskQueueConfiguration()
      .transform {
        case Failure(f) =>
          Success(
            Left(
              GroupError(
                ConfigurationReloadError,
                s"Unhandled error happen when reloading asterisk queue configuration : ${f.getMessage}"
              )
            )
          )
        case Success(value) => Success(Right(value))
      }
  }

  private def validateBody[A](
      request: Request[AnyContent]
  )(implicit r: Reads[A]): Either[ErrorResult, A] =
    request.body.asJson
      .toRight(
        GenericError(JsonParsingError, "No JSON could be decoded")
      )
      .flatMap(
        _.validate[A].asEither.left.map(error =>
          GenericError(
            JsonParsingError,
            s"Error decoding JSON : $error"
          )
        )
      )
}
