package ws.model

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class QueueMembership(queueId: Long, penalty: Int)

object QueueMembershipJson {
  implicit val queueMembershipWrites: Writes[QueueMembership] =
    new Writes[QueueMembership] {
      override def writes(o: QueueMembership): JsValue =
        Json.obj(
          "queueId" -> o.queueId,
          "penalty" -> o.penalty
        )
    }

  implicit val queueMembershipReads: Reads[QueueMembership] = (
    (JsPath \ "queueId").read[Long] and
      (JsPath \ "penalty").read[Int]
  )(QueueMembership.apply _)
}
