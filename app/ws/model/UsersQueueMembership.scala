package ws.model

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class UsersQueueMembership(
    userIds: List[Long],
    membership: List[QueueMembership]
)

object UsersQueueMembershipJson {
  import QueueMembershipJson._

  implicit val usersQueueMembershipWrites: Writes[UsersQueueMembership] =
    new Writes[UsersQueueMembership] {
      override def writes(o: UsersQueueMembership): JsValue =
        Json.obj(
          "userIds"    -> o.userIds,
          "membership" -> o.membership
        )
    }

  implicit val usersQueueMembershipReads: Reads[UsersQueueMembership] = (
    (JsPath \ "userIds").read[List[Long]] and
      (JsPath \ "membership").read[List[QueueMembership]]
  )(UsersQueueMembership.apply _)
}
