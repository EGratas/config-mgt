package ws.model

import play.api.libs.json.{Json, Writes}

case class NbMissedCall(userId: Long, nbMissedCall: Int)
object NbMissedCall {
  implicit val nbMissedCallWrites: Writes[NbMissedCall] =
    Json.writes[NbMissedCall]
}
