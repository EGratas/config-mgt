package ws.model

import anorm.{Error, RowParser, Success, TypeDoesNotMatch}
import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined.W
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import play.api.libs.json.{
  Format,
  JsResult,
  JsString,
  JsSuccess,
  JsValue,
  Json,
  Reads,
  Writes
}

object LineType extends Enumeration {
  type Type = Value

  val PHONE  = Value("phone")
  val UA     = Value("ua")
  val WEBRTC = Value("webrtc")
  val SCCP   = Value("sccp")
  val CUSTOM = Value("custom")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(LineType.withName(json.as[String]))
  }

  def protocolToType(protocol: String, webrtc: String) =
    RowParser[LineType.Type] { implicit row =>
      {
        protocol match {
          case "sip" =>
            webrtc match {
              case "yes" => Success(LineType.WEBRTC)
              case "ua"  => Success(LineType.UA)
              case _     => Success(LineType.PHONE)
            }

          case "sccp"   => Success(LineType.SCCP)
          case "custom" => Success(LineType.CUSTOM)
          case _        => Error(TypeDoesNotMatch("unexpected protocol"))
        }
      }
    }

  def typeToProtocol(lineType: LineType.Type): String =
    lineType match {
      case LineType.PHONE | LineType.UA | LineType.WEBRTC => "sip"
      case LineType.SCCP                                  => "sccp"
      case LineType.CUSTOM                                => "custom"
    }
}

case class Line(
    id: Option[Long],
    lineType: LineType.Type,
    context: Line.Context,
    extension: Line.Extension,
    site: Line.Site,
    device: Option[Line.Device],
    name: Option[Line.Name],
    lineNum: Option[Line.Num],
    provisioningId: Option[Line.ProvisioningId]
)

object Line {
  type Context        = String Refined MaxSize[W.`39`.T]
  type Extension      = String Refined MaxSize[W.`40`.T]
  type Site           = String Refined MaxSize[W.`128`.T]
  type Device         = String Refined MaxSize[W.`32`.T]
  type DeviceMac      = String Refined MaxSize[W.`18`.T]
  type Name           = String Refined MaxSize[W.`80`.T]
  type Num            = Long
  type ProvisioningId = Int

  implicit val reads: Reads[Line]   = Json.reads[Line]
  implicit val writes: Writes[Line] = Json.writes[Line]
}
