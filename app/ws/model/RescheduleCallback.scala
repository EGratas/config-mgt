package ws.model

import org.joda.time.DateTime
import play.api.libs.json._
import play.api.libs.json.JodaReads
import play.api.libs.functional.syntax._

case class RescheduleCallback(dueDate: DateTime, periodUuid: String)

object RescheduleCallback {
  val dateFormat = "yyyy-MM-dd"
  private val readJodaTime: Reads[DateTime] =
    JodaReads.jodaDateReads(dateFormat)

  implicit val rescheduleCallbackWrites: Writes[RescheduleCallback] =
    new Writes[RescheduleCallback] {
      override def writes(o: RescheduleCallback): JsValue =
        Json.obj(
          "dueDate"    -> o.dueDate.toString(dateFormat),
          "periodUuid" -> o.periodUuid
        )
    }

  implicit val rescheduleCallbackReads: Reads[RescheduleCallback] = (
    (JsPath \ "dueDate").read[DateTime](readJodaTime) and
      (JsPath \ "periodUuid").read[String]
  )(RescheduleCallback.apply _)
}
