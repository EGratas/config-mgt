package ws.model

import play.api.libs.json._
import play.api.libs.functional.syntax._
import xc.model.QueueMemberDefault

case class UserQueueMembership(userId: Long, membership: List[QueueMembership])

object UserQueueMembershipJson {
  import QueueMembershipJson._

  implicit val userQueueMembershipWrites: Writes[UserQueueMembership] =
    new Writes[UserQueueMembership] {
      override def writes(o: UserQueueMembership): JsValue =
        Json.obj(
          "userId"     -> o.userId,
          "membership" -> o.membership
        )
    }

  implicit val userQueueMembershipReads: Reads[UserQueueMembership] = (
    (JsPath \ "userId").read[Long] and
      (JsPath \ "membership").read[List[QueueMembership]]
  )(UserQueueMembership.apply _)
}

object UserQueueMembership {
  implicit def convert(
      qms: List[QueueMemberDefault]
  ): List[UserQueueMembership] =
    qms
      .groupBy(_.userId)
      .map(userMembership => {
        val membership =
          userMembership._2.map(m => QueueMembership(m.queueId, m.penalty))
        UserQueueMembership(userMembership._1, membership)
      })
      .toList
}
