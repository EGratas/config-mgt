package ws.model

import play.api.libs.json._

case class UserIdList(userIds: List[Long])

object UserIdListJson {
  implicit val userIdListWrites: Writes[UserIdList] = new Writes[UserIdList] {
    override def writes(o: UserIdList): JsValue =
      Json.obj(
        "userIds" -> o.userIds
      )
  }

  implicit val userIdListReads: Reads[UserIdList] =
    (JsPath \ "userIds").read[List[Long]].map(UserIdList.apply _)
}
