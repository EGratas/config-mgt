package xivo.model

import play.api.libs.functional.syntax.{toFunctionalBuilderOps, unlift}

import play.api.libs.json.{JsPath, Json, Reads, Writes}

case class XivoUserContact(
    id: Long,
    firstname: String,
    lastname: String,
    email: Option[String],
    mobilephonenumber: Option[String],
    internalphonenumber: Option[String],
    labels: List[String],
    externalphonenumber: List[String]
)

object XivoUserContact {

  implicit val reads: Reads[XivoUserContact] = Json.reads[XivoUserContact]

  implicit val writes: Writes[XivoUserContact] = (
    (JsPath \ "id").write[Long] and
      (JsPath \ "firstname").write[String] and
      (JsPath \ "lastname").write[String] and
      (JsPath \ "email").write(Writes.optionWithNull[String]) and
      (JsPath \ "mobilephonenumber").write(Writes.optionWithNull[String]) and
      (JsPath \ "internalphonenumber").write(Writes.optionWithNull[String]) and
      (JsPath \ "labels").write[List[String]] and
      (JsPath \ "externalphonenumber").write[List[String]]
  )(unlift(XivoUserContact.unapply))
}
