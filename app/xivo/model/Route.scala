package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.types.string.NonEmptyString
import play.api.libs.functional.syntax._
import play.api.libs.json._
import ws.controllers.route.RouteHelper

case class PlainRoute(
    id: Long,
    untransformedPattern: List[DialPattern],
    subroutine: Option[String],
    description: Option[String],
    priority: Long,
    contextNames: List[String],
    mediaserverIds: List[Long],
    trunkIds: List[SortedTrunkIds],
    scheduleIds: List[Long],
    rightIds: List[Long],
    internal: Boolean
)

case class GrouppedRawRoute(
    context: List[String],
    mdsId: List[Long],
    trunkWithPriority: List[(Option[Long], Option[Long])],
    schedule: List[Long],
    right: List[Long],
    patterns: List[DialPattern]
)

case class RawRoute(
    id: Long,
    pattern: Option[String],
    target: Option[String Refined NonEmpty],
    regexp: Option[String Refined NonEmpty],
    callerid: Option[String Refined NonEmpty],
    subroutine: Option[String],
    description: Option[String],
    priority: Long,
    contextname: Option[String],
    mdsid: Option[Long],
    trunkid: Option[Long],
    trunkpriority: Option[Long],
    schedule: Option[Long],
    right: Option[Long],
    internal: Boolean
)

case class RichEmptyRoute(
    priority: Long,
    context: List[RichContext],
    mediaserver: List[RichMediaserverConfig],
    trunk: List[Trunk],
    schedule: List[RichSchedule],
    right: List[RichRight]
)

case class RichRoute(
    id: Long,
    dialPattern: List[DialPattern],
    description: Option[String],
    priority: Long,
    context: List[RichContext],
    mediaserver: List[RichMediaserverConfig],
    trunk: List[Trunk],
    schedule: List[RichSchedule],
    right: List[RichRight],
    subroutine: Option[String],
    internal: Boolean
)

case class RichRouteNoId(
    dialpattern: List[DialPattern],
    description: Option[String],
    priority: Long,
    context: List[RichContext],
    mediaserver: List[RichMediaserverConfig],
    trunk: List[Trunk],
    schedule: List[RichSchedule],
    right: List[RichRight],
    subroutine: Option[String],
    internal: Boolean
) {
  def withId(id: Long): RichRoute =
    RichRoute(
      id,
      dialpattern,
      description,
      priority,
      context,
      mediaserver,
      trunk,
      schedule,
      right,
      subroutine,
      internal: Boolean
    )
}

case class DialPattern(
    pattern: String,
    target: Option[String Refined NonEmpty],
    regexp: Option[String Refined NonEmpty],
    callerid: Option[String Refined NonEmpty]
)
case class SortedTrunkIds(id: Long, priority: Long)
case class Context(name: String)
case class RichContext(name: String, used: Boolean = false)

trait Trunk {
  def withTrueId(id: Long): Trunk
  def getId: Long
  def isUsed: Boolean
}

case class TrunkGeneric(id: Long, name: String, used: Boolean) extends Trunk {
  def withTrueId(id: Long): TrunkGeneric = this.copy(id = id)
  def getId: Long                        = id
  def isUsed: Boolean                    = used
}

case class TrunkSip(id: Long, name: String) extends Trunk {
  def withTrueId(id: Long): TrunkSip = this.copy(id = id)
  def getId: Long                    = id
  def isUsed: Boolean                = false
}

case class TrunkCustom(id: Long, interface: String, intfsuffix: String)
    extends Trunk {
  def withTrueId(id: Long): TrunkCustom = this.copy(id = id)
  def getId: Long                       = id
  def isUsed: Boolean                   = false

}

case class TrunkFeature(id: Long, protocol: String, protocolId: Long) {
  def withTrueId(id: Long): TrunkFeature = this.copy(id = id)
  def getId: Long                        = id
}

case class RichTrunkSip(id: Long, name: String, used: Boolean = false)
    extends Trunk {
  def withTrueId(id: Long): RichTrunkSip = this.copy(id = id)
  def getId: Long                        = id
  def isUsed: Boolean                    = used
}

case class RichTrunkCustom(id: Long, name: String, used: Boolean = false)
    extends Trunk {
  def withTrueId(id: Long): RichTrunkCustom = this.copy(id = id)
  def getId: Long                           = id
  def isUsed: Boolean                       = used
}

case class RichMediaserverConfig(
    id: Long,
    name: String,
    display_name: String,
    used: Boolean = false
)

case class Schedule(id: Long, name: String)
case class RichSchedule(id: Long, name: String, used: Boolean = false)

case class Right(id: Long, name: String)
case class RichRight(id: Long, name: String, used: Boolean = false)

object RichTrunkCustom {
  implicit val reads: Reads[RichTrunkCustom]   = Json.reads[RichTrunkCustom]
  implicit val writes: Writes[RichTrunkCustom] = Json.writes[RichTrunkCustom]
}

object Context {
  implicit val reads: Reads[Context]   = Json.reads[Context]
  implicit val writes: Writes[Context] = Json.writes[Context]
}

object RichContext {
  implicit val reads: Reads[RichContext]   = Json.reads[RichContext]
  implicit val writes: Writes[RichContext] = Json.writes[RichContext]
}

object Trunk {
  implicit val trunkReads: Reads[Trunk] = Reads[Trunk] { json =>
    json.validate[TrunkGeneric]
  }

  implicit val trunkWrites: Writes[Trunk] = new Writes[Trunk] {
    override def writes(t: Trunk): JsValue =
      t match {
        case t: TrunkGeneric    => Json.toJson(t)
        case t: TrunkSip        => Json.toJson(t)
        case t: TrunkCustom     => Json.toJson(t)
        case t: RichTrunkSip    => Json.toJson(t)
        case t: RichTrunkCustom => Json.toJson(t)
      }
  }
}

object TrunkGeneric {
  implicit val reads: Reads[TrunkGeneric]   = Json.reads[TrunkGeneric]
  implicit val writes: Writes[TrunkGeneric] = Json.writes[TrunkGeneric]
}

object TrunkSip {
  implicit val reads: Reads[TrunkSip]   = Json.reads[TrunkSip]
  implicit val writes: Writes[TrunkSip] = Json.writes[TrunkSip]
}

object TrunkCustom {
  implicit val reads: Reads[TrunkCustom]   = Json.reads[TrunkCustom]
  implicit val writes: Writes[TrunkCustom] = Json.writes[TrunkCustom]
}

object RichTrunkSip {
  implicit val reads: Reads[RichTrunkSip]   = Json.reads[RichTrunkSip]
  implicit val writes: Writes[RichTrunkSip] = Json.writes[RichTrunkSip]
}

object DialPattern {
  implicit val reads: Reads[DialPattern] = (
    (JsPath \ "pattern").read[String].map(RouteHelper.fromAsteriskPattern) and
      (JsPath \ "target").readNullable[NonEmptyString] and
      (JsPath \ "regexp").readNullable[NonEmptyString] and
      (JsPath \ "callerid").readNullable[NonEmptyString]
  )(DialPattern.apply _)

  implicit val writes: Writes[DialPattern] = new Writes[DialPattern] {
    def writes(dialPattern: DialPattern): JsObject = {
      val default = Json.obj(
        "pattern" -> RouteHelper.toAsteriskPattern(dialPattern.pattern)
      )

      val withStripNum = dialPattern.regexp match {
        case Some(_) =>
          default deepMerge Json.obj(
            "regexp" -> dialPattern.regexp
          )
        case _ => default
      }

      val withPrefix = dialPattern.target match {
        case Some(_) =>
          withStripNum deepMerge Json.obj(
            "target" -> dialPattern.target
          )
        case _ => withStripNum
      }

      dialPattern.callerid match {
        case Some(_) =>
          withPrefix deepMerge Json.obj(
            "callerid" -> dialPattern.callerid
          )
        case _ => withPrefix
      }
    }
  }
}

object RichRoute {
  implicit val reads: Reads[RichRoute] = (
    (JsPath \ "id").read[Long] and
      (JsPath \ "dialpattern").read[List[DialPattern]] and
      (JsPath \ "description").readNullable[String] and
      (JsPath \ "priority").read[Long] and
      (JsPath \ "context").read[List[RichContext]] and
      (JsPath \ "mediaserver").read[List[RichMediaserverConfig]] and
      (JsPath \ "trunk").read[List[Trunk]] and
      (JsPath \ "schedule")
        .formatNullable[List[RichSchedule]]
        .inmap[List[RichSchedule]](
          o => o.getOrElse(List.empty[RichSchedule]),
          s => if (s.isEmpty) None else Some(s)
        ) and
      (JsPath \ "right")
        .formatNullable[List[RichRight]]
        .inmap[List[RichRight]](
          o => o.getOrElse(List.empty[RichRight]),
          s => if (s.isEmpty) None else Some(s)
        ) and
      (JsPath \ "subroutine").readNullable[String] and
      (JsPath \ "internal").read[Boolean].or(Reads.pure(false))
  )(RichRoute.apply _)

  implicit val writes: Writes[RichRoute] = new Writes[RichRoute] {
    def writes(route: RichRoute): JsObject = {

      val default = Json.obj(
        "id"          -> route.id,
        "priority"    -> route.priority,
        "internal"    -> route.internal,
        "dialpattern" -> route.dialPattern,
        "context"     -> route.context,
        "mediaserver" -> route.mediaserver,
        "trunk"       -> route.trunk,
        "schedule"    -> route.schedule,
        "right"       -> route.right
      )

      val withDesc = route.description match {
        case Some(_) =>
          default deepMerge Json.obj(
            "description" -> route.description
          )
        case _ => default
      }

      route.subroutine match {
        case Some(_) =>
          withDesc deepMerge Json.obj(
            "subroutine" -> route.subroutine
          )
        case _ => withDesc
      }
    }
  }
}

object RichRouteNoId {
  implicit val reads: Reads[RichRouteNoId] = (
    (JsPath \ "dialpattern").read[List[DialPattern]] and
      (JsPath \ "description").readNullable[String] and
      (JsPath \ "priority").read[Long] and
      (JsPath \ "context").read[List[RichContext]] and
      (JsPath \ "mediaserver").read[List[RichMediaserverConfig]] and
      (JsPath \ "trunk").read[List[Trunk]] and
      (JsPath \ "schedule")
        .formatNullable[List[RichSchedule]]
        .inmap[List[RichSchedule]](
          o => o.getOrElse(List.empty[RichSchedule]),
          s => if (s.isEmpty) None else Some(s)
        ) and
      (JsPath \ "right")
        .formatNullable[List[RichRight]]
        .inmap[List[RichRight]](
          o => o.getOrElse(List.empty[RichRight]),
          s => if (s.isEmpty) None else Some(s)
        ) and
      (JsPath \ "subroutine").readNullable[String] and
      (JsPath \ "internal").read[Boolean].or(Reads.pure(false))
  )(RichRouteNoId.apply _)
  implicit val writes: Writes[RichRouteNoId] = Json.writes[RichRouteNoId]
}

object RichMediaserverConfig {
  implicit val reads: Reads[RichMediaserverConfig] =
    Json.reads[RichMediaserverConfig]
  implicit val writes: Writes[RichMediaserverConfig] =
    Json.writes[RichMediaserverConfig]
}

object RichEmptyRoute {
  implicit val reads: Reads[RichEmptyRoute]   = Json.reads[RichEmptyRoute]
  implicit val writes: Writes[RichEmptyRoute] = Json.writes[RichEmptyRoute]
}

object Schedule {
  implicit val reads: Reads[Schedule]   = Json.reads[Schedule]
  implicit val writes: Writes[Schedule] = Json.writes[Schedule]
}

object RichSchedule {
  implicit val reads: Reads[RichSchedule]   = Json.reads[RichSchedule]
  implicit val writes: Writes[RichSchedule] = Json.writes[RichSchedule]
}

object Right {
  implicit val reads: Reads[Right]   = Json.reads[Right]
  implicit val writes: Writes[Right] = Json.writes[Right]
}

object RichRight {
  implicit val reads: Reads[RichRight]   = Json.reads[RichRight]
  implicit val writes: Writes[RichRight] = Json.writes[RichRight]
}
