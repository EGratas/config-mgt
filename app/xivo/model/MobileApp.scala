package xivo.model

import model.ws.{ErrorResult, ErrorType}
import play.api.libs.json.{Json, Reads, Writes}
import play.api.mvc.{Result, Results}

case class MobileAppPushToken(token: Option[String])

object MobileAppPushToken {
  implicit val reads: Reads[MobileAppPushToken] = Json.reads[MobileAppPushToken]
  implicit val writes: Writes[MobileAppPushToken] =
    Json.writes[MobileAppPushToken]
}

case class MobileAppError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case MobileAppError.UserNotFound | MobileAppError.KeyNotFound =>
        Results.NotFound(body)
      case _ => Results.InternalServerError(body)
    }
  }
}

case object UserDoesNotExist extends Exception

object MobileAppError {
  implicit val mobileAppErrorWrites: Writes[MobileAppError] =
    Json.writes[MobileAppError]

  trait MobileAppErrorType extends ErrorType

  case object UserNotFound extends MobileAppErrorType {
    val error = "UserNotFound"
  }

  case object KeyNotFound extends MobileAppErrorType {
    val error = "KeyNotFound"
  }

  case object UnhandledError extends MobileAppErrorType {
    val error = "UnhandledError"
  }

  case object ParsingError extends MobileAppErrorType {
    val error = "ParsingError"
  }
}
