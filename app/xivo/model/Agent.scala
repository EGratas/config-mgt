package xivo.model

import model.ws.{ErrorResult, ErrorType}
import play.api.libs.json.{JsObject, Json, Writes}
import play.api.mvc.{Result, Results}

case class Agent(
    id: Long,
    firstname: String,
    lastname: String,
    number: String,
    context: String,
    member: List[QueueMember],
    numgroup: Long,
    userid: Option[Long]
)

object Agent {
  implicit val agentWrites: Writes[Agent] {
    def writes(agent: Agent): JsObject
  } = new Writes[Agent] {
    def writes(agent: Agent) =
      Json.obj(
        "id"        -> agent.id,
        "firstname" -> agent.firstname,
        "lastname"  -> agent.lastname,
        "number"    -> agent.number,
        "context"   -> agent.context,
        "member"    -> agent.member,
        "numgroup"  -> agent.numgroup,
        "userid"    -> agent.userid
      )
  }
}

trait AgentErrorType      extends ErrorType
case object AgentNotFound extends AgentErrorType { val error = "AgentNotFound" }

case class AgentError(error: ErrorType, message: String) extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case AgentNotFound => Results.NotFound(body)
      case _             => Results.InternalServerError(body)
    }
  }
}

object AgentError {
  implicit val queueErrorWrites: Writes[AgentError] = Json.writes[AgentError]
}
