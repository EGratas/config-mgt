package xivo.model

import play.api.libs.json.{Format, JsResult, JsString, JsSuccess, JsValue}

object ProtocolType extends Enumeration {
  type Type = Value

  val SIP    = Value("sip")
  val SCCP   = Value("sccp")
  val CUSTOM = Value("custom")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(ProtocolType.withName(json.as[String]))
  }
}
