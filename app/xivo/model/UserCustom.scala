package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined.{refineMV, W}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import play.api.libs.json.{Json, Reads, Writes}

case class UserCustom(
    id: Option[Long],
    name: Option[String Refined MaxSize[W.`40`.T]] = None,
    context: String Refined MaxSize[W.`39`.T],
    interface: String Refined MaxSize[W.`128`.T],
    intfsuffix: String Refined MaxSize[W.`32`.T] = refineMV(""),
    protocol: ProtocolType.Type = ProtocolType.CUSTOM,
    category: UserIaxCategoryType.Type = UserIaxCategoryType.USER,
    commented: Int = 0
)

object UserCustom {
  implicit val reads: Reads[UserCustom]   = Json.reads[UserCustom]
  implicit val writes: Writes[UserCustom] = Json.writes[UserCustom]
}
