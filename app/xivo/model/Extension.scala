package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import anorm.SqlParser.get
import anorm.{~, RowParser, TypeDoesNotMatch}
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import eu.timepit.refined.{refineV, W}
import play.api.libs.json._

import scala.util.{Left, Right}

object ExtenNumberType extends Enumeration {
  type Type = Value

  val user, group = Value

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(ExtenNumberType.withName(json.as[String]))
  }

  implicit def rowToExtenNumberType: anorm.Column[ExtenNumberType.Type] = {
    anorm.Column[ExtenNumberType.Type] { (value, _) =>
      value match {
        case "user" =>
          Right(ExtenNumberType.user)
        case "group" =>
          Right(ExtenNumberType.group)
        case unknown =>
          Left(
            TypeDoesNotMatch(s"unknown ExtenNumberType from $unknown")
          )
      }
    }
  }

}

case class Extension(
    id: Option[Long],
    commented: Int = 0,
    context: String Refined MaxSize[W.`39`.T],
    exten: String Refined MaxSize[W.`40`.T],
    `type`: ExtenNumberType.Type = ExtenNumberType.user,
    typeval: String Refined MaxSize[W.`255`.T]
)

object Extension {
  implicit val reads: Reads[Extension]   = Json.reads[Extension]
  implicit val writes: Writes[Extension] = Json.writes[Extension]

  def createRefinedExtensionFromString(
      id: Option[Long],
      commented: Int = 0,
      context: String,
      exten: String,
      `type`: ExtenNumberType.Type = ExtenNumberType.user,
      typeVal: String
  ): Either[String, Extension] = for {
    refinedContext <- refineV[MaxSize[W.`39`.T]](context)
    refinedExten   <- refineV[MaxSize[W.`40`.T]](exten)
    refinedTypeVal <- refineV[MaxSize[W.`255`.T]](typeVal)
  } yield Extension(
    id,
    commented,
    refinedContext,
    refinedExten,
    `type`,
    refinedTypeVal
  )

  def extensionMapping: RowParser[Extension] = {
    get[Long]("id") ~
      get[Int]("commented") ~
      get[String]("context") ~
      get[String]("exten") ~
      get[ExtenNumberType.Type]("type") ~
      get[String]("typeval") map {
        case id ~ commented ~ context ~ exten ~ _type ~ typeval =>
          {
            Extension.createRefinedExtensionFromString(
              Some(id),
              commented,
              context,
              exten,
              _type,
              typeval
            )
          }.toOption.get
      }
  }

}
