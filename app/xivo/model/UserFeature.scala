package xivo.model

object GenericBsFilter extends Enumeration {
  type Type = Value

  val No, Boss, Secretary = Value
}

case class UserFeature(
    id: Option[Long],
    uuid: String,
    firstname: String,
    lastname: String,
    voicemailid: Option[Int],
    agentid: Option[Int],
    pictureid: Option[Int],
    entityid: Option[Int],
    callerid: Option[String],
    ringseconds: Int,
    simultcalls: Int,
    enableclient: Int,
    loginclient: String,
    passwdclient: String,
    cti_profile_id: Option[Int],
    enablehint: Int,
    enablevoicemail: Int,
    enablexfer: Int,
    callrecord: Int,
    incallfilter: Int,
    enablednd: Int,
    enableunc: Int,
    destunc: String,
    enablerna: Int,
    destrna: String,
    enablebusy: Int,
    destbusy: String,
    musiconhold: String,
    outcallerid: String,
    mobilephonenumber: String,
    userfield: String,
    bsfilter: GenericBsFilter.Type,
    preprocess_subroutine: Option[String],
    timezone: Option[String],
    language: Option[String],
    ringintern: Option[String],
    ringextern: Option[String],
    ringgroup: Option[String],
    ringforward: Option[String],
    rightcallcode: Option[String],
    commented: Int,
    description: String,
    func_key_template_id: Option[Int],
    func_key_private_template_id: Int,
    mobile_push_token: Option[String]
)
