package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import eu.timepit.refined.string.MatchesRegex
import play.api.libs.json.{
  Format,
  JsResult,
  JsString,
  JsSuccess,
  JsValue,
  Json,
  Reads,
  Writes
}

object UserIaxType extends Enumeration {
  type Type = Value

  val FRIEND = Value("friend")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(UserIaxType.withName(json.as[String]))
  }
}

object UserIaxAmaFlagType extends Enumeration {
  type Type = Value

  val DEFAULT = Value("default")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(UserIaxAmaFlagType.withName(json.as[String]))
  }
}

object UserIaxCategoryType extends Enumeration {
  type Type = Value

  val USER = Value("user")

  implicit val format: Format[Type] = new Format[Type] {
    def writes(r: Type): JsValue = JsString(r.toString)
    def reads(json: JsValue): JsResult[Type] =
      JsSuccess(UserIaxCategoryType.withName(json.as[String]))
  }
}

case class UserSip(
    id: Option[Long],
    name: String Refined MatchesRegex[W.`"^[a-z0-9]{8,}$"`.T],
    `type`: UserIaxType.Type,
    secret: String Refined MatchesRegex[W.`"^[a-z0-9]{8,}$"`.T],
    md5secret: String Refined MaxSize[W.`32`.T] = refineMV(""),
    context: String Refined MaxSize[W.`39`.T],
    amaflags: UserIaxAmaFlagType.Type,
    subscribemwi: Int = 0,
    `call-limit`: Int = 0,
    callerid: String Refined MaxSize[W.`160`.T],
    protocol: ProtocolType.Type = ProtocolType.SIP,
    host: String Refined MaxSize[W.`255`.T] = refineMV("dynamic"),
    ipaddr: String Refined MaxSize[W.`255`.T] = refineMV(""),
    regseconds: Int = 0,
    lastms: String Refined MaxSize[W.`15`.T] = refineMV(""),
    category: UserIaxCategoryType.Type,
    options: List[String] // Hope this mapping works with anorm... see https://github.com/playframework/playframework/pull/3062
)

object UserSip {
  implicit val reads: Reads[UserSip]   = Json.reads[UserSip]
  implicit val writes: Writes[UserSip] = Json.writes[UserSip]
}
