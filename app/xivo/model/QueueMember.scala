package xivo.model

import anorm.SqlParser.get
import anorm.{~, RowParser, TypeDoesNotMatch}
import play.api.libs.json.{Json, Writes}

import scala.util.Right

object QueueMemberUserType extends Enumeration {
  type Type = Value

  val Agent, User = Value

  implicit def rowToQueueMemberUserType
      : anorm.Column[xivo.model.QueueMemberUserType.Type] = {
    anorm.Column[xivo.model.QueueMemberUserType.Type] { (value, meta) =>
      value match {
        case "agent" =>
          Right(xivo.model.QueueMemberUserType.Agent)
        case "user" => Right(xivo.model.QueueMemberUserType.User)
        case unknown =>
          Left(
            TypeDoesNotMatch(s"unknown QueueMemberUserType from $unknown")
          )
      }
    }
  }
}

case class QueueMember(
    queue_name: String,
    queue_id: Long,
    interface: String,
    penalty: Int,
    commented: Int,
    usertype: QueueMemberUserType.Type,
    userid: Long,
    channel: String,
    category: QueueCategory.Type,
    position: Int
)

object QueueMember {
  implicit val QueueMemberWrites: Writes[QueueMember] = Json.writes[QueueMember]

  implicit val parser: RowParser[QueueMember] =
    get[String]("queue_name") ~
      get[Long]("groupfeatures.id") ~
      get[String]("interface") ~
      get[Int]("penalty") ~
      get[Int]("commented") ~
      get[QueueMemberUserType.Type]("usertype") ~
      get[Long]("userid") ~
      get[String]("channel") ~
      get[QueueCategory.Type]("category") ~
      get[Int]("position") map {
        case queue_name ~ queue_id ~ interface ~ penalty ~ commented ~ usertype ~ userid ~ channel ~ category ~ position =>
          QueueMember(
            queue_name,
            queue_id,
            interface,
            penalty,
            commented,
            usertype,
            userid,
            channel,
            category,
            position
          )
      }
}
