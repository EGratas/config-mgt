package xivo.model

import be.venneborg.refined.play.RefinedJsonFormats._
import eu.timepit.refined.W
import eu.timepit.refined.api.Refined
import eu.timepit.refined.collection.MaxSize
import play.api.libs.json.{Json, Reads, Writes}

case class SccpDevice(
    id: Option[Long],
    name: String Refined MaxSize[W.`80`.T],
    device: String Refined MaxSize[W.`80`.T],
    line: String Refined MaxSize[W.`80`.T]
)

object SccpDevice {
  implicit val reads: Reads[SccpDevice]   = Json.reads[SccpDevice]
  implicit val writes: Writes[SccpDevice] = Json.writes[SccpDevice]
}
