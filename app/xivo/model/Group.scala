package xivo.model

import anorm.{Column, Macro, MetaDataItem, RowParser, TypeDoesNotMatch}
import model.ws.{ErrorResult, ErrorType}
import play.api.libs.functional.syntax.{toFunctionalBuilderOps, unlift}
import play.api.libs.json.{__, JsPath, Json, Reads, Writes}
import play.api.mvc.{Result, Results}
import xivo.model.CallerIdModeType.CallerIdModeType
import xivo.model.DialAction.DialActionMap
import xivo.model.DialActionEventType.DialActionEventType
import xivo.model.RingingStrategyType.RingingStrategyType

import scala.util.Right
import scala.util.chaining.scalaUtilChainingOps
case class GroupWithDialActions(
    group: Group,
    dialActions: DialActionMap
)

object GroupWithDialActions {
  implicit val gWDAsWrite: Writes[GroupWithDialActions] =
    (Json.writes[Group] and
      (JsPath \ "dialactions").write[DialActionMap])(
      unlift(GroupWithDialActions.unapply)
    )

  implicit lazy val dialactionsWrites: Writes[DialActionMap] =
    (__).lazyWrite(
      Writes.keyMapWrites[DialActionEventType, DialActionAction, Map](
        dET => dET.toString,
        Json.writes[DialActionAction]
      )
    )
}

case class Group(
    id: Long,
    name: String,
    context: String,
    mds: String,
    number: String,
    allowTransferUser: Boolean,
    allowTransferCall: Boolean,
    allowCallerRecord: Boolean,
    allowCalleeRecord: Boolean,
    ignoreForward: Boolean,
    preprocessSubroutine: Option[String],
    onHoldMusic: String,
    timeout: Option[Int],
    retryTime: Int,
    ringingStrategy: RingingStrategyType,
    ringInUse: Boolean,
    callerId: CallerId
) {

  def updateModifiedProperties(ug: UpdateGroup): Group = {
    def merge[T](initial: T, maybeUpdated: Option[T]): T =
      maybeUpdated.getOrElse(initial)

    def mergeOpt[T](
        maybeInitial: Option[T],
        maybeUpdated: Option[T]
    ): Option[T] =
      maybeUpdated.orElse(maybeInitial)

    def noneIfEmptyString(maybeStr: Option[String]): Option[String] =
      maybeStr.flatMap(str => if (str.equals("")) None else Some(str))

    def noneIfZero(maybeInt: Option[Int]): Option[Int] =
      maybeInt.flatMap(int => if (int == 0) None else Some(int))

    Group(
      id,
      merge(name, ug.name),
      merge(context, ug.context),
      merge(mds, ug.mds),
      merge(number, ug.number),
      merge(allowTransferUser, ug.allowTransferUser),
      merge(allowTransferCall, ug.allowTransferCall),
      merge(allowCallerRecord, ug.allowCallerRecord),
      merge(allowCalleeRecord, ug.allowCalleeRecord),
      merge(ignoreForward, ug.ignoreForward),
      mergeOpt(preprocessSubroutine, ug.preprocessSubroutine)
        .pipe(noneIfEmptyString),
      merge(onHoldMusic, ug.onHoldMusic),
      mergeOpt(timeout, ug.timeout).pipe(noneIfZero),
      merge(retryTime, ug.retryTime),
      merge(ringingStrategy, ug.ringingStrategy),
      merge(ringInUse, ug.ringInUse),
      merge(callerId, ug.callerId)
    )
  }

  private def compareWithCreateGroup(newGroup: CreateGroup): Boolean = {
    def optAreEqualsWithEmptinessCheck[T](
        maybeCgProperty: Option[T],
        maybeProperty: Option[T],
        isNotEmpty: T => Boolean
    ): Boolean =
      maybeCgProperty
        .filter(isNotEmpty)
        .forall(maybeProperty.contains(_))

    name.equals(newGroup.name) &&
    context.equals(newGroup.context) &&
    mds.equals(newGroup.mds) &&
    number.equals(newGroup.number) &&
    allowTransferUser == newGroup.allowTransferUser &&
    allowTransferCall == newGroup.allowTransferCall &&
    allowCallerRecord == newGroup.allowCallerRecord &&
    allowCalleeRecord == newGroup.allowCalleeRecord &&
    ignoreForward == newGroup.ignoreForward &&
    optAreEqualsWithEmptinessCheck(
      newGroup.preprocessSubroutine,
      preprocessSubroutine,
      (s: String) => !s.equals("")
    ) &&
    onHoldMusic.equals(newGroup.onHoldMusic) &&
    optAreEqualsWithEmptinessCheck(
      newGroup.timeout,
      timeout,
      (t: Int) => t != 0
    ) &&
    retryTime == newGroup.retryTime &&
    ringingStrategy.equals(newGroup.ringingStrategy) &&
    ringInUse == newGroup.ringInUse &&
    callerId == newGroup.callerId
  }

  private def compareWithUpdateGroup(updateGroup: UpdateGroup): Boolean = {
    def optAreEqualWithEmptinessCheck[T](
        maybeUgProperty: Option[T],
        maybeProperty: Option[T],
        isNotEmpty: T => Boolean
    ): Boolean = maybeUgProperty.forall(props =>
      if (isNotEmpty(props)) maybeProperty.contains(props)
      else maybeProperty.isEmpty
    )

    updateGroup.name.forall(name.contains(_)) &&
    updateGroup.context.forall(context.contains(_)) &&
    updateGroup.mds.forall(mds.contains(_)) &&
    updateGroup.number.forall(_ == number) &&
    updateGroup.allowTransferUser.forall(_ == allowTransferUser) &&
    updateGroup.allowTransferCall.forall(_ == allowTransferCall) &&
    updateGroup.allowCallerRecord.forall(_ == allowCallerRecord) &&
    updateGroup.allowCalleeRecord.forall(_ == allowCalleeRecord) &&
    updateGroup.ignoreForward.forall(_ == ignoreForward) &&
    optAreEqualWithEmptinessCheck(
      updateGroup.preprocessSubroutine,
      preprocessSubroutine,
      (pS: String) => !pS.equals("")
    ) &&
    updateGroup.onHoldMusic.forall(_.equals(onHoldMusic)) &&
    optAreEqualWithEmptinessCheck(
      updateGroup.timeout,
      timeout,
      (t: Int) => t != 0
    ) &&
    updateGroup.retryTime.forall(_ == retryTime) &&
    updateGroup.ringingStrategy.forall(_.equals(ringingStrategy)) &&
    updateGroup.ringInUse.forall(_ == ringInUse) &&
    updateGroup.callerId.forall(_ == callerId)
  }

  override def equals(o: Any): Boolean = o match {
    case ug: UpdateGroup => compareWithUpdateGroup(ug)
    case cg: CreateGroup => compareWithCreateGroup(cg)
    case _               => super.equals(o)
  }
}

object Group {
  implicit val groupWrite: Writes[Group]      = Json.writes[Group]
  implicit val groupRead: Reads[Group]        = Json.reads[Group]
  implicit val groupMapping: RowParser[Group] = Macro.namedParser[Group]

  implicit def columnToBoolean: Column[Boolean] =
    Column.nonNull { (value, meta) =>
      val MetaDataItem(qualified, nullable, clazz) = meta
      value match {
        case bool: Boolean => Right(bool)     // Provided-default case
        case bit: Int      => Right(bit == 1) // Custom conversion
        case _ =>
          Left(
            TypeDoesNotMatch(
              s"Cannot convert $value: ${value.asInstanceOf[AnyRef].getClass} to Boolean for column $qualified"
            )
          )
      }
    }
}

case class CreateGroup(
    name: String,
    context: String,
    mds: String,
    number: String = "",
    allowTransferUser: Boolean = true,
    allowTransferCall: Boolean = false,
    allowCallerRecord: Boolean = false,
    allowCalleeRecord: Boolean = false,
    ignoreForward: Boolean = true,
    preprocessSubroutine: Option[String] = None,
    onHoldMusic: String = "default",
    timeout: Option[Int] = Some(10),
    retryTime: Int = 1,
    ringingStrategy: RingingStrategyType = RingingStrategyType.rRMemory,
    ringInUse: Boolean = false,
    callerId: CallerId = CallerId(None, ""),
    dialactions: Map[
      DialActionEventType,
      DialActionAction
    ] = Map(
      DialActionEventType.noAnswer ->
        DialActionAction(action = DialActionActionType.none),
      DialActionEventType.congestion ->
        DialActionAction(action = DialActionActionType.none),
      DialActionEventType.chanUnavailable ->
        DialActionAction(action = DialActionActionType.none)
    )
) {
  def removeEmptyValues(): CreateGroup =
    this.copy(
      timeout = if (timeout.contains(0)) None else this.timeout,
      preprocessSubroutine =
        if (this.preprocessSubroutine.contains("")) None
        else this.preprocessSubroutine
    )
}

object CreateGroup {
  import DialActionActionType._

  implicit val createGroupWrite: Writes[CreateGroup] =
    Json.writes[CreateGroup]
  implicit val createGroupRead: Reads[CreateGroup] =
    Json.using[Json.WithDefaultValues].reads[CreateGroup]
}

case class UpdateGroup(
    name: Option[String] = None,
    context: Option[String] = None,
    mds: Option[String] = None,
    number: Option[String] = None,
    allowTransferUser: Option[Boolean] = None,
    allowTransferCall: Option[Boolean] = None,
    allowCallerRecord: Option[Boolean] = None,
    allowCalleeRecord: Option[Boolean] = None,
    ignoreForward: Option[Boolean] = None,
    preprocessSubroutine: Option[String] = None,
    onHoldMusic: Option[String] = None,
    timeout: Option[Int] = None,
    retryTime: Option[Int] = None,
    ringingStrategy: Option[RingingStrategyType] = None,
    ringInUse: Option[Boolean] = None,
    callerId: Option[CallerId] = None,
    dialactions: Option[Map[DialActionEventType, DialActionAction]] = None
)

object UpdateGroup {
  import DialActionActionType._

  implicit val updateGroupWrite: Writes[UpdateGroup] =
    Json.writes[UpdateGroup]
  implicit val updateGroupRead: Reads[UpdateGroup] =
    Json.reads[UpdateGroup]
}

case class CallerId(mode: Option[CallerIdModeType], display: String)
object CallerId {
  implicit val callerIdWrite: Writes[CallerId] = Json.writes[CallerId]
  implicit val callerIdRead: Reads[CallerId]   = Json.reads[CallerId]
  implicit val parser: RowParser[CallerId] =
    Macro.parser[CallerId]("mode", "callerdisplay")
}

object CallerIdModeType extends Enumeration {
  implicit val callerIdModeTypeWrite: Writes[CallerIdModeType] =
    Writes.enumNameWrites
  implicit val CallerIdModeTypeRead: Reads[CallerIdModeType] =
    Reads.enumNameReads(CallerIdModeType)
  type CallerIdModeType = Value
  val prepend, overwrite, append = Value

  implicit def rowToCallerIdModeType
      : anorm.Column[CallerIdModeType.CallerIdModeType] = {
    anorm.Column[CallerIdModeType.CallerIdModeType] { (value, meta) =>
      value match {
        case "prepend" =>
          Right(xivo.model.CallerIdModeType.prepend)
        case "overwrite" =>
          Right(xivo.model.CallerIdModeType.overwrite)
        case "append" =>
          Right(xivo.model.CallerIdModeType.append)
        case unknown =>
          Left(TypeDoesNotMatch(s"unknown CallerIdModeType from $unknown"))
      }
    }
  }
}

object RingingStrategyType extends Enumeration {
  type RingingStrategyType = Value
  val leastRecent = Value("leastrecent")
  val fewestCalls = Value("fewestCalls")
  val rRMemory    = Value("rrmemory")
  val random      = Value("random")
  val wRandom     = Value("wrandom")
  val ringAll     = Value("ringall")
  implicit val ringingStrategyTypeWrite: Writes[RingingStrategyType] =
    Writes.enumNameWrites
  implicit val ringingStrategyTypeRead: Reads[RingingStrategyType] =
    Reads.enumNameReads(RingingStrategyType)

  implicit class RingingStrategyTypeValue(event: Value) {
    def toDatabase: String = event match {
      case RingingStrategyType.leastRecent => "leastrecent"
      case RingingStrategyType.fewestCalls => "fewestcalls"
      case RingingStrategyType.rRMemory    => "rrmemory"
      case RingingStrategyType.random      => "random"
      case RingingStrategyType.wRandom     => "wrandom"
      case RingingStrategyType.ringAll     => "ringall"
    }
  }

  implicit def rowToRingingStrategyType
      : anorm.Column[RingingStrategyType.RingingStrategyType] = {
    anorm.Column[RingingStrategyType.RingingStrategyType] { (value, meta) =>
      value match {
        case "leastrecent" =>
          Right(xivo.model.RingingStrategyType.leastRecent)
        case "fewestcalls" =>
          Right(xivo.model.RingingStrategyType.fewestCalls)
        case "rrmemory" =>
          Right(xivo.model.RingingStrategyType.rRMemory)
        case "wrandom" =>
          Right(xivo.model.RingingStrategyType.wRandom)
        case "ringall" =>
          Right(xivo.model.RingingStrategyType.ringAll)
        case "random" =>
          Right(xivo.model.RingingStrategyType.random)
        case unknown =>
          Left(TypeDoesNotMatch(s"unknown RingingStrategyType from $unknown"))
      }
    }
  }
}

sealed trait GroupMembershipAction
case object AddUser    extends GroupMembershipAction
case object RemoveUser extends GroupMembershipAction

sealed trait GroupAction
case object GetAllGroupsAction              extends GroupAction
case class GetGroupAction(groupId: Long)    extends GroupAction
case object CreateGroupAction               extends GroupAction
case class UpdateGroupAction(groupId: Long) extends GroupAction
case class DeleteGroupAction(groupId: Long) extends GroupAction

sealed trait GroupErrorType extends ErrorType

case object UserNotFound extends GroupErrorType {
  val error = "NotFound"
}

case object GroupNotFound extends GroupErrorType {
  val error = "NotFound"
}

case object ContextNotFound extends GroupErrorType {
  val error = "NotFound"
}

case object UserAlreadyInGroup extends GroupErrorType {
  val error = "Duplicate"
}

case object UserNotInGroup extends GroupErrorType {
  val error = "NotFound"
}

case object ConfigurationReloadError extends ErrorType {
  val error = "ConfigurationReloadError"
}

case object DatabaseError extends ErrorType {
  val error = "DatabaseError"
}

case object MediaServerNotFound extends ErrorType {
  val error = "MediaServerNotFound"
}

case object DialActionNotFound extends ErrorType {
  val error = "DialActionNotFound"
}

case class DialActionError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case DialActionNotFound => Results.NotFound(body)
      case DatabaseError      => Results.InternalServerError(body)
      case _                  => Results.InternalServerError(body)
    }
  }
}

object DialActionError {
  implicit val dialActionErrorWrite: Writes[DialActionError] =
    Json.writes[DialActionError]
}

case class GroupError(error: ErrorType, message: String) extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case UserNotFound             => Results.NotFound(body)
      case GroupNotFound            => Results.NotFound(body)
      case ContextNotFound          => Results.NotFound(body)
      case UserAlreadyInGroup       => Results.Conflict(body)
      case UserNotInGroup           => Results.Conflict(body)
      case ConfigurationReloadError => Results.ServiceUnavailable(body)
      case DatabaseError            => Results.InternalServerError(body)
      case MediaServerNotFound      => Results.NotFound(body)
      case _                        => Results.InternalServerError(body)
    }
  }
}

object GroupError {
  implicit val groupErrorWrite: Writes[GroupError] =
    Json.writes[GroupError]
}
