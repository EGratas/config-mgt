package xivo.model.rabbitmq

import play.api.libs.json._
import play.api.libs.functional.syntax._
import simulacrum._
import xivo.model.Xivo
import xivo.model.rabbitmq.BaseRabbitMessage.toJson

sealed trait BaseRabbitMessage {
  def getRabbitData: JsObject
  def getRabbitMessageType: RabbitMessageType
}

final case class ObjectEvent(messageType: RabbitMessageType, id: Long)
    extends BaseRabbitMessage {
  def getRabbitData: JsObject                 = Json.obj("id" -> id)
  def getRabbitMessageType: RabbitMessageType = messageType
}

sealed abstract class IpbxCommand(val command: String)
case object IpbxSipReload      extends IpbxCommand("sip reload")
case object IpbxDialplanReload extends IpbxCommand("dialplan reload")
case object IpbxSccpReload     extends IpbxCommand("module reload chan_sccp.so")
case object IpbxQueueReload    extends IpbxCommand("module reload app_queue.so")

final case class IpbxEvent(commands: List[IpbxCommand])
    extends BaseRabbitMessage {
  def getRabbitMessageType: RabbitMessageType = ReloadIpbx
  def getRabbitData: JsObject                 = Json.obj("commands" -> commands.map(_.command))
}

final case class UserLineEvent(
    messageType: UserLineEventType,
    userid: Long,
    lineid: Long
) extends BaseRabbitMessage {
  def getRabbitMessageType: UserLineEventType = messageType
  def getRabbitData: JsObject =
    Json.obj(
      "user_id"   -> userid,
      "line_id"   -> lineid,
      "main_user" -> true,
      "main_line" -> true
    )
}

object BaseRabbitMessage {

  implicit def rabbitMessageWrites(implicit
      xivo: Xivo
  ): Writes[BaseRabbitMessage] =
    new Writes[BaseRabbitMessage] {
      def writes(o: BaseRabbitMessage): JsObject =
        Json.obj(
          "data"        -> o.getRabbitData,
          "name"        -> o.getRabbitMessageType.event,
          "origin_uuid" -> xivo.uuid
        )
    }
  def toJson(brm: BaseRabbitMessage)(implicit xivo: Xivo): JsValue =
    Json.toJson(brm)

}

object ObjectEvent {

  implicit val objectEventReads: Reads[ObjectEvent] = (
    (JsPath \ "name").read[RabbitMessageType] and
      (JsPath \ "data" \ "id").read[Long]
  )(ObjectEvent.apply _)

  implicit def objectEventWrites(implicit xivo: Xivo): Writes[ObjectEvent] =
    new Writes[ObjectEvent] {
      def writes(oe: ObjectEvent): JsValue = toJson(oe)(xivo)
    }
}

sealed trait ObjectUpdateMode
case object ObjectCreated extends ObjectUpdateMode
case object ObjectEdited  extends ObjectUpdateMode
case object ObjectDeleted extends ObjectUpdateMode

@typeclass trait ToRabbitMessage[A] {
  def toRabbitMessage(a: A, updateMode: ObjectUpdateMode): BaseRabbitMessage
}
