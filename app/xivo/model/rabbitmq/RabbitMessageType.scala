package xivo.model.rabbitmq

import play.api.libs.json._

sealed abstract class RabbitMessageType(
    val routingKey: String,
    val event: String
)
case object QueueEdited
    extends RabbitMessageType("config.queue.edited", "queue_edited")
case object QueueCreated
    extends RabbitMessageType("config.queue.created", "queue_created")
case object QueueDeleted
    extends RabbitMessageType("config.queue.deleted", "queue_deleted")

case object MediaServerEdited
    extends RabbitMessageType("config.mediaserver.edited", "mediaserver_edited")
case object MediaServerCreated
    extends RabbitMessageType(
      "config.mediaserver.created",
      "mediaserver_created"
    )
case object MediaServerDeleted
    extends RabbitMessageType(
      "config.mediaserver.deleted",
      "mediaserver_deleted"
    )

case object LineEdited
    extends RabbitMessageType("config.line.edited", "line_edited")
case object LineCreated
    extends RabbitMessageType("config.line.created", "line_created")
case object LineDeleted
    extends RabbitMessageType("config.line.deleted", "line_deleted")

case object MobilePushTokenAdded
    extends RabbitMessageType(
      "mobile.pushtoken.added",
      "mobile_push_token_added"
    )
case object MobilePushTokenDeleted
    extends RabbitMessageType(
      "mobile.pushtoken.deleted",
      "mobile_push_token_deleted"
    )

case object SipEndpointEdited
    extends RabbitMessageType(
      "config.sip_endpoint.edited",
      "sip_endpoint_edited"
    )
case object SipEndpointCreated
    extends RabbitMessageType(
      "config.sip_endpoint.created",
      "sip_endpoint_created"
    )
case object SipEndpointDeleted
    extends RabbitMessageType(
      "config.sip_endpoint.deleted",
      "sip_endpoint_deleted"
    )

case object SccpEndpointEdited
    extends RabbitMessageType(
      "config.sccp_endpoint.edited",
      "sccp_endpoint_edited"
    )
case object SccpEndpointCreated
    extends RabbitMessageType(
      "config.sccp_endpoint.created",
      "sccp_endpoint_created"
    )
case object SccpEndpointDeleted
    extends RabbitMessageType(
      "config.sccp_endpoint.deleted",
      "sccp_endpoint_deleted"
    )

case object CustomEndpointEdited
    extends RabbitMessageType(
      "config.custom_endpoint.edited",
      "custom_endpoint_edited"
    )
case object CustomEndpointCreated
    extends RabbitMessageType(
      "config.custom_endpoint.created",
      "custom_endpoint_created"
    )
case object CustomEndpointDeleted
    extends RabbitMessageType(
      "config.custom_endpoint.deleted",
      "custom_endpoint_deleted"
    )

case object ReloadIpbx
    extends RabbitMessageType("config.ipbx.reload", "ipbx_reload")

sealed trait UserLineEventType extends RabbitMessageType {}

case object UserLineAssociated
    extends RabbitMessageType(
      "config.user_line_association.created",
      "line_associated"
    )
    with UserLineEventType
case object UserLineDissociated
    extends RabbitMessageType(
      "config.user_line_association.deleted",
      "line_dissociated"
    )
    with UserLineEventType

case object UserServicesEdited
    extends RabbitMessageType(
      "config.user_services.edited",
      "user_services_edited"
    )

case object UserPreferenceEdited
    extends RabbitMessageType(
      "config.user_preference.edited",
      "user_preference_edited"
    )
case object UserPreferenceCreated
    extends RabbitMessageType(
      "config.user_preference.created",
      "user_preference_created"
    )
case object UserPreferenceDeleted
    extends RabbitMessageType(
      "config.user_preference.deleted",
      "user_preference_deleted"
    )

object RabbitMessageType {
  implicit val rabbitMessageTypeReads: Reads[RabbitMessageType] = ((JsPath)
    .read[String])
    .collect(JsonValidationError("event name does not match any known event")) {
      case "queue_edited"            => QueueEdited
      case "queue_created"           => QueueCreated
      case "queue_deleted"           => QueueDeleted
      case "mediaserver_edited"      => MediaServerEdited
      case "mediaserver_created"     => MediaServerCreated
      case "mediaserver_deleted"     => MediaServerDeleted
      case "user_preference_edited"  => UserPreferenceEdited
      case "user_preference_created" => UserPreferenceCreated
      case "user_preference_deleted" => UserPreferenceDeleted
    }
}
