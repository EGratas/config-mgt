package xivo.model

import model.ws.{ErrorResult, ErrorType}
import play.api.libs.json._
import play.api.mvc.{Result, Results}

abstract class QueueDissuasion(id: Long, name: String)

sealed trait QueueDissuasionConfig
abstract class QueueDissuasionSelected(
    id: Long,
    name: String,
    value: Option[QueueDissuasionValue]
) extends QueueDissuasion(id, name)
    with QueueDissuasionConfig

case class QueueDissuasionSoundFileSelected(
    id: Long,
    name: String,
    queueDissuasionSoundFile: QueueDissuasionSoundFile
) extends QueueDissuasionSelected(
      id: Long,
      name: String,
      Some(queueDissuasionSoundFile)
    )
case class QueueDissuasionQueueSelected(
    id: Long,
    name: String,
    queueDissuasionQueue: QueueDissuasionQueue
) extends QueueDissuasionSelected(
      id: Long,
      name: String,
      Some(queueDissuasionQueue)
    )
case class QueueDissuasionOtherSelected(id: Long, name: String)
    extends QueueDissuasionSelected(id: Long, name: String, None)

case class QueueDissuasionList(
    id: Long,
    name: String,
    soundFiles: List[QueueDissuasionSoundFile],
    queues: List[QueueDissuasionQueue]
) extends QueueDissuasion(id, name)
    with QueueDissuasionConfig

trait QueueDissuasionValue
case class QueueDissuasionSoundFile(id: Option[Long], soundFile: String)
    extends QueueDissuasionValue
case class QueueDissuasionQueue(id: Option[Long], queueName: String)
    extends QueueDissuasionValue

trait QueueDissuasionErrorType extends ErrorType

case object InvalidDissuasionParameters extends QueueDissuasionErrorType {
  val error = "Invalid"
}

case object DissuasionFileNotFound extends QueueDissuasionErrorType {
  val error = "FileNotFound"
}

case object RedirectQueueDissuasionToItself extends QueueDissuasionErrorType {
  val error = "RedirectQueueDissuasionToItself"
}

case class QueueDissuasionError(error: ErrorType, message: String)
    extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case InvalidDissuasionParameters => Results.BadRequest(body)
      case DissuasionFileNotFound      => Results.NotFound(body)
      case _                           => Results.InternalServerError(body)
    }
  }
}

class RedirectQueueDissuasionToItselfException(message: String)
    extends Exception(message) {
  def this(message: String, cause: Throwable) = {
    this(message)
    initCause(cause)
  }

  def this(cause: Throwable) = {
    this(Option(cause).map(_.toString).orNull, cause)
  }

  def this() = {
    this(null: String)
  }
}

object QueueDissuasionSoundFileSelected {
  implicit val writes: Writes[QueueDissuasionSoundFileSelected] =
    new Writes[QueueDissuasionSoundFileSelected] {
      def writes(qdsfSelected: QueueDissuasionSoundFileSelected): JsValue = {
        val dissuasion = Json.obj(
          "type"  -> "soundFile",
          "value" -> Json.toJson(qdsfSelected.queueDissuasionSoundFile)
        )
        Json.obj(
          "id"         -> qdsfSelected.id,
          "name"       -> qdsfSelected.name,
          "dissuasion" -> dissuasion
        )
      }
    }
}

object QueueDissuasionQueueSelected {
  implicit val writes: Writes[QueueDissuasionQueueSelected] =
    new Writes[QueueDissuasionQueueSelected] {
      def writes(qdqSelected: QueueDissuasionQueueSelected): JsValue = {
        val dissuasion = Json.obj(
          "type"  -> "queue",
          "value" -> Json.toJson(qdqSelected.queueDissuasionQueue)
        )
        Json.obj(
          "id"         -> qdqSelected.id,
          "name"       -> qdqSelected.name,
          "dissuasion" -> dissuasion
        )
      }
    }
}

object QueueDissuasionOtherSelected {
  implicit val writes: Writes[QueueDissuasionOtherSelected] =
    new Writes[QueueDissuasionOtherSelected] {
      def writes(qoSelected: QueueDissuasionOtherSelected): JsValue = {
        val dissuasion = Json.obj(
          "type" -> "other"
        )
        Json.obj(
          "id"         -> qoSelected.id,
          "name"       -> qoSelected.name,
          "dissuasion" -> dissuasion
        )
      }
    }
}

object QueueDissuasionList {
  implicit val writes: Writes[QueueDissuasionList] =
    new Writes[QueueDissuasionList] {
      def writes(qdl: QueueDissuasionList): JsValue = {
        val dissuasions = Json.obj(
          "soundFiles" -> qdl.soundFiles,
          "queues"     -> qdl.queues
        )

        Json.obj(
          "id"          -> qdl.id,
          "name"        -> qdl.name,
          "dissuasions" -> dissuasions
        )
      }
    }
}

object QueueDissuasionError {
  implicit val writes: Writes[QueueDissuasionError] =
    Json.writes[QueueDissuasionError]
}

object QueueDissuasionSoundFile {
  implicit val reads: Reads[QueueDissuasionSoundFile] =
    Json.reads[QueueDissuasionSoundFile]
  implicit val writes: Writes[QueueDissuasionSoundFile] =
    Json.writes[QueueDissuasionSoundFile]
}

object QueueDissuasionQueue {
  implicit val reads: Reads[QueueDissuasionQueue] =
    Json.reads[QueueDissuasionQueue]
  implicit val writes: Writes[QueueDissuasionQueue] =
    Json.writes[QueueDissuasionQueue]
}
