package xivo.model

import java.util.UUID

case class Xivo(uuid: UUID)
