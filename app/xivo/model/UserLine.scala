package xivo.model

import play.api.libs.json.{Json, Reads, Writes}

case class UserLine(
    id: Option[Long],
    user_id: Option[Long],
    line_id: Long,
    extension_id: Option[Long],
    main_user: Boolean = true,
    main_line: Boolean = true
)

object UserLine {
  implicit val reads: Reads[UserLine]   = Json.reads[UserLine]
  implicit val writes: Writes[UserLine] = Json.writes[UserLine]
}
