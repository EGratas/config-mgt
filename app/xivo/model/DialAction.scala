package xivo.model

import anorm.SqlParser.get
import anorm.{~, RowParser, TypeDoesNotMatch}
import play.api.libs.json.{Json, Reads, Writes}
import xivo.model.DialAction.DialActionMap
import xivo.model.DialActionActionType.DialActionActionType
import xivo.model.DialActionEventType.DialActionEventType

import scala.util.Right

case class DialActionAction(
    action: DialActionActionType.DialActionActionType,
    actionArg1: Option[String] = None,
    actionArg2: Option[String] = None
)

object DialActionAction {
  implicit val dialactionActionWrite: Writes[DialActionAction] =
    Json.writes[DialActionAction]
  implicit val dialactionActionRead: Reads[DialActionAction] =
    Json.reads[DialActionAction]
}
case class DialAction(
    categoryId: Long,
    event: DialActionEventType,
    action: DialActionActionType,
    actionArg1: Option[String] = None,
    actionArg2: Option[String] = None
)

object DialAction {
  type DialActionMap = Map[
    DialActionEventType,
    DialActionAction
  ]

  implicit val dialActionMapping: RowParser[DialAction] = {
    get[String]("categoryval") ~
      get[DialActionEventType]("event") ~
      get[DialActionActionType]("action") ~
      get[Option[String]]("actionarg1") ~
      get[Option[String]]("actionarg2") map {
        case categoryId ~ event ~ action ~ actionArg1 ~ actionArg2 =>
          DialAction(categoryId.toLong, event, action, actionArg1, actionArg2)
      }
  }
}

object DialActionCategoryType extends Enumeration {
  type DialActionCategoryType = Value
  val callFilter, group, inCall, queue, schedule, user, outCall =
    Value
}

object DialActionEventType extends Enumeration {
  implicit val dialActionEventTypeWrite: Writes[DialActionEventType] =
    Writes.enumNameWrites
  implicit val dialActionEventTypeRead: Reads[DialActionEventType] =
    Reads.enumNameReads(DialActionEventType)
  type DialActionEventType = Value
  val answer, noAnswer, congestion, busy, chanUnavailable, inSchedule,
      outSchedule, queueWaitTime, queueWaitRatio = Value

  implicit class DialActionEventTypeValue(event: Value) {
    def toDatabase: String = event match {
      case DialActionEventType.noAnswer        => "noanswer"
      case DialActionEventType.congestion      => "congestion"
      case DialActionEventType.chanUnavailable => "chanunavail"
      case DialActionEventType.answer          => "answer"
      case DialActionEventType.busy            => "busy"
      case DialActionEventType.inSchedule      => "inschedule"
      case DialActionEventType.outSchedule     => "outschedule"
      case DialActionEventType.queueWaitTime   => "qwaittime"
      case DialActionEventType.queueWaitRatio  => "qwaitratio"
    }
  }

  implicit val rowToDialActionEventType
      : anorm.Column[DialActionEventType.DialActionEventType] = {
    anorm.Column[DialActionEventType.DialActionEventType] { (value, _) =>
      value match {
        case "noanswer" =>
          Right(xivo.model.DialActionEventType.noAnswer)
        case "congestion" =>
          Right(xivo.model.DialActionEventType.congestion)
        case "chanunavail" =>
          Right(xivo.model.DialActionEventType.chanUnavailable)
        case "answer" =>
          Right(xivo.model.DialActionEventType.answer)
        case "busy" =>
          Right(xivo.model.DialActionEventType.busy)
        case "inschedule" =>
          Right(xivo.model.DialActionEventType.inSchedule)
        case "outschedule" =>
          Right(xivo.model.DialActionEventType.outSchedule)
        case "qwaittime" =>
          Right(xivo.model.DialActionEventType.queueWaitTime)
        case "qwaitratio" =>
          Right(xivo.model.DialActionEventType.queueWaitRatio)
        case unknown =>
          Left(
            TypeDoesNotMatch(s"unknown DialactionEventType from $unknown")
          )
      }
    }
  }
}

object DialActionActionType extends Enumeration {
  implicit val dialActionActionTypeWrite: Writes[DialActionActionType] =
    Writes.enumNameWrites
  implicit val dialActionActionTypeRead: Reads[DialActionActionType] =
    Reads.enumNameReads(DialActionActionType)
  implicit lazy val dialactionsRead: Reads[DialActionMap] =
    Reads.mapReads[DialActionEventType, DialActionAction](dET =>
      Json
        .toJson(DialActionEventType.withName(dET))
        .validate[DialActionEventType]
    )

  implicit lazy val dialactionsWrites: Writes[Map[
    DialActionEventType,
    DialActionAction
  ]] =
    Writes.keyMapWrites[DialActionEventType, DialActionAction, Map](
      dET => dET.toString,
      Json.writes[DialActionAction]
    )

  type DialActionActionType = Value
  val none, endCallBusy, endCallCongestion, endCallHangup, user, group, queue,
      meetme, meetingRoom, voiceMail, ivr, trunk, schedule, extension, outcall,
      applicationCallbackDisa, applicationDisa, applicationDirectory,
      applicationFaxToMail, applicationVoicemailMain, applicationPassword,
      sound, custom = Value

  implicit class DialActionActionTypeValue(action: Value) {
    def toDatabase: String = action match {
      case DialActionActionType.none              => "none"
      case DialActionActionType.endCallBusy       => "endcall:busy"
      case DialActionActionType.endCallCongestion => "endcall:congestion"
      case DialActionActionType.endCallHangup     => "endcall:hangup"
      case DialActionActionType.user              => "user"
      case DialActionActionType.group             => "group"
      case DialActionActionType.queue             => "queue"
      case DialActionActionType.meetme            => "meetme"
      case DialActionActionType.meetingRoom       => "meetingroom"
      case DialActionActionType.voiceMail         => "voicemail"
      case DialActionActionType.ivr               => "ivr"
      case DialActionActionType.trunk             => "trunk"
      case DialActionActionType.schedule          => "schedule"
      case DialActionActionType.extension         => "extension"
      case DialActionActionType.outcall           => "outcall"
      case DialActionActionType.applicationCallbackDisa =>
        "application:callbackdisa"
      case DialActionActionType.applicationDisa      => "application:disa"
      case DialActionActionType.applicationDirectory => "application:directory"
      case DialActionActionType.applicationFaxToMail => "application:faxtomail"
      case DialActionActionType.applicationVoicemailMain =>
        "application:voicemailmain"
      case DialActionActionType.applicationPassword => "application:password"
      case DialActionActionType.sound               => "sound"
      case DialActionActionType.custom              => "custom"
    }
  }

  implicit def rowToDialActionEventType
      : anorm.Column[DialActionActionType.DialActionActionType] = {
    anorm.Column[DialActionActionType.DialActionActionType] { (value, meta) =>
      value match {
        case "none" =>
          Right(xivo.model.DialActionActionType.none)
        case "endcall:busy" =>
          Right(xivo.model.DialActionActionType.endCallBusy)
        case "endcall:congestion" =>
          Right(xivo.model.DialActionActionType.endCallCongestion)
        case "endcall:hangup" =>
          Right(xivo.model.DialActionActionType.endCallHangup)
        case "user" =>
          Right(xivo.model.DialActionActionType.user)
        case "group" =>
          Right(xivo.model.DialActionActionType.group)
        case "queue" =>
          Right(xivo.model.DialActionActionType.queue)
        case "meetme" =>
          Right(xivo.model.DialActionActionType.meetme)
        case "meetingroom" =>
          Right(xivo.model.DialActionActionType.meetingRoom)
        case "voicemail" =>
          Right(xivo.model.DialActionActionType.voiceMail)
        case "ivr" =>
          Right(xivo.model.DialActionActionType.ivr)
        case "trunk" =>
          Right(xivo.model.DialActionActionType.trunk)
        case "schedule" =>
          Right(xivo.model.DialActionActionType.schedule)
        case "extension" =>
          Right(xivo.model.DialActionActionType.extension)
        case "outcall" =>
          Right(xivo.model.DialActionActionType.outcall)
        case "applicationcallbackdisa" =>
          Right(xivo.model.DialActionActionType.applicationCallbackDisa)
        case "application:disa" =>
          Right(xivo.model.DialActionActionType.applicationDisa)
        case "application:directory" =>
          Right(xivo.model.DialActionActionType.applicationDirectory)
        case "application:faxtomail" =>
          Right(xivo.model.DialActionActionType.applicationFaxToMail)
        case "application:voicemailmain" =>
          Right(xivo.model.DialActionActionType.applicationVoicemailMain)
        case "application:password" =>
          Right(xivo.model.DialActionActionType.applicationPassword)
        case "sound" =>
          Right(xivo.model.DialActionActionType.sound)
        case "custom" =>
          Right(xivo.model.DialActionActionType.custom)
        case unknown =>
          Left(TypeDoesNotMatch(s"unknown DialactionActionType from $unknown"))
      }
    }
  }
}
