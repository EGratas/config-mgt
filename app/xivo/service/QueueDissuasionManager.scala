package xivo.service

import anorm._
import javax.inject.Inject
import play.api.db.{Database, NamedDatabase}
import ws.controllers.QueueDissuasion
import xivo.model.{QueueDissuasionValue, _}

import scala.util.{Failure, Try}

class QueueDissuasionManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database,
    queueFeatureManager: QueueFeatureManager
) {
  def get(id: Long): Try[Option[QueueDissuasionValue]] =
    dbXivo.withConnection { implicit c =>
      val parser: RowParser[Option[QueueDissuasionValue]] =
        RowParser[Option[QueueDissuasionValue]] {
          case Row("sound", actionarg1: Option[_], _) =>
            actionarg1 match {
              case Some(something) =>
                something match {
                  case soundFile: String =>
                    Success(
                      Some(
                        QueueDissuasionSoundFile(
                          None,
                          QueueDissuasion.getFileName(soundFile)
                        )
                      )
                    )
                  case _ =>
                    throw new IllegalStateException(
                      s"Error: in function QueueDissuasionManager.get(), actionarg1 should be an Option[String] but instead is $actionarg1."
                    )
                }
              case None =>
                throw new IllegalStateException(
                  s"Error: the queue with id $id has its dissuasion set to playing a sound file but has no sound file to play."
                )
            }
          case Row("queue", actionarg1: Option[_], _) =>
            actionarg1 match {
              case Some(something) =>
                something match {
                  case queue: String =>
                    queueFeatureManager.get(queue.toLong) match {
                      case util.Success(queueFeature) =>
                        Success(
                          Some(QueueDissuasionQueue(None, queueFeature.name))
                        )
                      case Failure(f) =>
                        throw new IllegalStateException(
                          s"in dissuasion, the queue with $id redirects calls to the queue with id ${queue.toLong}, but the queue with id ${queue.toLong} does not exist. Error message: $f"
                        )
                    }
                  case _ =>
                    throw new IllegalStateException(
                      s"Error: in function QueueDissuasionManager.get(), actionarg1 should be an Option[String] but instead is $actionarg1."
                    )
                }
              case None =>
                throw new IllegalStateException(
                  s"Error: the queue with id $id has its dissuasion set to redirecting calls to another queue but has no queue to redirect calls to."
                )
            }
          case _ => Success(None)
        }

      Try {
        SQL("""
                select action, actionarg1, categoryval
                from dialaction
                where cast (categoryval as integer) = {id}
                and event = 'chanunavail'
                and category = 'queue'
                 """)
          .on("id" -> id)
          .as(parser.single)
      }
    }

  def update(id: Long, action: String, actionarg1: String): Try[Int] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""
        | UPDATE dialaction
        | SET actionarg1={actionarg1}, action = '$action'
        | WHERE categoryval = {id}
        | and event = 'chanunavail'
        | and category = 'queue'
      """.stripMargin)
          .on("id" -> id.toString, "actionarg1" -> actionarg1)
          .executeUpdate()
      )
    }

  def dissuasionAccessToSoundFile(
      id: Long,
      fileName: String
  ): Try[QueueDissuasionSoundFile] = {
    Try {
      update(id, "sound", fileName) match {
        case scala.util.Success(numberOfResults) =>
          if (numberOfResults == 1)
            QueueDissuasionSoundFile(
              Some(id),
              QueueDissuasion.getFileName(fileName)
            )
          else
            throw new IllegalStateException(
              s"Error: update of the dissuasion sound file of the queue with id $id with the sound file $fileName has returned $numberOfResults results but should return one."
            )
        case Failure(f) =>
          throw new IllegalStateException(
            s"Error: update of the dissuasion sound file of the queue with id $id with the sound file $fileName has failed."
          )
      }
    }
  }
  def dissuasionAccessToOtherQueue(
      id: Long,
      otherQueueId: Long,
      otherQueueName: String
  ): Try[QueueDissuasionQueue] =
    dbXivo.withConnection { implicit c =>
      if (id == otherQueueId) {
        Failure(
          new RedirectQueueDissuasionToItselfException(
            s"Error: tried to redirect the queue $otherQueueName (id $id) to itself in case of dissuasion."
          )
        )
      } else {
        Try {
          update(id, "queue", otherQueueId.toString) match {
            case scala.util.Success(numberOfResults) =>
              if (numberOfResults == 1)
                QueueDissuasionQueue(Some(id), otherQueueName)
              else
                throw new IllegalStateException(
                  s"Error: update of the dissuasion queue to redirect calls to of the queue with id $id with the queue $otherQueueName (id $otherQueueId) has returned $numberOfResults results but should return one."
                )
            case Failure(f) =>
              throw new IllegalStateException(
                s"Error: update of the dissuasion queue to redirect calls to of the queue with id $id with the queue $otherQueueName (id $otherQueueId) has failed."
              )
          }
        }
      }
    }
}
