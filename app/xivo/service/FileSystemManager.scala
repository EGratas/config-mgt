package xivo.service

import java.io.File

import ws.controllers.QueueDissuasion

class FileSystemManager {
  def getListOfFiles(dir: String): List[String] = {
    val d = new File(dir)
    if (d.exists && d.isDirectory) {
      d.listFiles.filter(_.isFile).toList.map(_.getName)
    } else {
      List[String]()
    }
  }

  def doesFileExist(path: String, fileName: String): Boolean = {
    val directory = new File(path)
    if (directory.exists && directory.isDirectory) {
      directory.listFiles.exists(file =>
        QueueDissuasion.removeExtension(file.getName).equals(fileName)
      )
    } else {
      false
    }
  }
}
