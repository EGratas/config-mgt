package xivo.service

import com.google.inject.Inject
import play.api.libs.json.Json
import play.api.libs.ws.ahc.AhcWSResponse
import play.api.libs.ws.{WSClient, WSResponse}
import play.api.{Configuration, Logger}

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ExecutionContext, Future}

class SysConfdClient @Inject() (configuration: Configuration, ws: WSClient)(
    implicit ec: ExecutionContext
) {
  val log          = Logger(getClass.getName)
  val sysConfdHost = configuration.get[String]("sysconfd.host")
  val sysConfdPort =
    configuration.getOptional[Int]("sysconfd.port").getOrElse(8668)
  val sysConfdTimeout =
    configuration.getOptional[Int]("sysconfd.timeout").getOrElse(5000)

  val baseUri = s"http://${sysConfdHost}:${sysConfdPort}"

  private val QUEUE_RELOAD_CMD = "module reload app_queue.so"

  def reloadAsteriskQueueConfiguration(): Future[WSResponse] = {
    log.info("Reload asterisk queue configuration from Sysconfd")
    ws.url(s"$baseUri/exec_request_handlers")
      .withHttpHeaders(
        ("Content-Type", "application/json")
      )
      .withRequestTimeout(sysConfdTimeout.millis)
      .withMethod("GET")
      .post(Json.obj("ipbx" -> Seq(QUEUE_RELOAD_CMD)))
      .collect {
        case AhcWSResponse(underlying) if underlying.status >= 400 =>
          Future.failed(
            SysConfdServiceException(
              s"SysConfd request failed with code ${underlying.status} and message ${underlying.body}"
            )
          )
        case success @ _ => Future.successful(success)
      }
      .flatten
      .recoverWith { case f =>
        log.error(
          s"Error when reloading asterisk queue configuration : ${f.getMessage}"
        )
        Future.failed(ConfdServiceException(f.getMessage))
      }
  }
}

case class SysConfdServiceException(message: String)
    extends Exception(message: String)
