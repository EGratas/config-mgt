package xivo.service

import anorm.SqlParser._
import anorm._
import com.google.inject.Inject
import play.api.db.{Database, _}
import ws.model.{PartialUserServices, UserForward, UserServices}

import scala.util.Try
import xivo.service.rabbitmq.XivoRabbitEventsPublisher
import xivo.model.rabbitmq.ObjectEvent
import xivo.model.rabbitmq.UserServicesEdited

class UserServicesManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database,
    eventPublisher: XivoRabbitEventsPublisher
) {

  private def bool2long(value: Boolean): Long = if (value) 1 else 0

  val servicesParser: RowParser[UserServices] =
    for {
      dnd        <- int("enablednd").map(_ != 0)
      enableUnc  <- int("enableunc").map(_ != 0)
      destUnc    <- str("destunc")
      enableNA   <- int("enablerna").map(_ != 0)
      destNA     <- str("destrna")
      enableBusy <- int("enablebusy").map(_ != 0)
      destBusy   <- str("destbusy")
    } yield UserServices(
      dnd,
      UserForward(enableBusy, destBusy),
      UserForward(enableNA, destNA),
      UserForward(enableUnc, destUnc)
    )

  def getServices(userId: Long) =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("""SELECT enablednd, enableunc, destunc, enablerna, destrna, enablebusy, destbusy 
             FROM userfeatures
             WHERE id={userId};
      """)
          .on("userId" -> userId)
          .as(servicesParser.single)
      )
    }

  def updateAny(userId: Long, services: PartialUserServices): Try[Long] = {

    def buildField(field: Option[UserForward], name: String): List[String] = {
      val toggle = s"enable$name = {enable$name}"
      val dest   = s"dest$name = {dest$name}"
      field
        .map(uf =>
          if (uf.enabled)
            List(toggle, dest)
          else List(toggle)
        )
        .getOrElse(List())
    }

    val tlong   = ToParameterValue.apply[Long]
    val tstring = ToParameterValue.apply[String]
    def buildEnableParam(
        field: Option[UserForward],
        name: String
    ): Option[(String, ParameterValue)] = {
      field.flatMap(f => Some(s"enable$name" -> tlong(bool2long(f.enabled))))
    }
    def buildDestParam(
        field: Option[UserForward],
        name: String
    ): Option[(String, ParameterValue)] = {
      field.flatMap(f =>
        if (f.enabled)
          Some(s"dest$name" -> tstring(field.get.destination))
        else None
      )
    }

    val fields = services.dndEnabled
      .map(_ => "enablednd = {enablednd}")
      .toList ++ buildField(services.unconditional, "unc") ++ buildField(
      services.noanswer,
      "rna"
    ) ++ buildField(services.busy, "busy")

    val query =
      s"""UPDATE userfeatures SET ${fields.mkString(",")}
             | WHERE id = {userId};""".stripMargin

    val onList = Seq(
      services.dndEnabled.flatMap(f =>
        Some("enablednd" -> tlong(bool2long(f)))
      ),
      buildEnableParam(services.unconditional, "unc"),
      buildDestParam(services.unconditional, "unc"),
      buildEnableParam(services.noanswer, "rna"),
      buildDestParam(services.noanswer, "rna"),
      buildEnableParam(services.busy, "busy"),
      buildDestParam(services.busy, "busy"),
      Some("userId" -> tlong(userId))
    ).flatten.map(v => NamedParameter(v._1, v._2))

    dbXivo.withConnection { implicit c =>
      Try(
        SQL(query)
          .on(
            onList: _*
          )
          .executeUpdate()
      )
    }
  }

  def notifyServicesUpdated(userId: Long): Unit = {
    eventPublisher.publish(ObjectEvent(UserServicesEdited, userId))
  }

  def updatePartialServices(
      userId: Long,
      services: PartialUserServices
  ): Try[PartialUserServices] = {
    for {
      _ <- updateAny(userId, services)
      _ = notifyServicesUpdated(userId)
    } yield services
  }
}
