package xivo.service

import pdi.jwt.{JwtAlgorithm, JwtJson}
import play.api.libs.json.{JsObject, Json}
import play.api.mvc.{AnyContent, Request}
import xivo.model.{
  MeetingRoomError,
  MeetingRoomException,
  MeetingRoomTokenContent,
  MeetingRoomTokenResponse
}

import java.net.URLEncoder
import java.sql.Timestamp
import java.time.Instant
import java.util.UUID
import javax.inject.Inject
import scala.util.{Failure, Try}

object MeetingRoomAuthentication {
  sealed trait MeetingRoomAuth {
    val uuid: UUID
    val token: String
    val title: MeetingRoomTitle
  }

  case class MeetingRoomTitle(name: String, number: String) {
    private def urlEncodeRoomName(name: String): String = {
      URLEncoder.encode(name, "UTF-8").replaceAll("\\+", "%20")
    }

    def urlEncode: MeetingRoomTitle = this.copy(name = urlEncodeRoomName(name))
    def withNumber                  = s"$name (**$number)"
  }

  case class MeetingRoomAuthValidated(
      uuid: UUID,
      token: String,
      title: MeetingRoomTitle
  ) extends MeetingRoomAuth
  case class MeetingRoomAuthChallenge(
      uuid: UUID,
      token: String,
      title: MeetingRoomTitle
  ) extends MeetingRoomAuth
}

class MeetingRoomInstantTime {
  def now() = Instant.now()
}

class MeetingRoomUUIDGenerator {
  def generateUUID() = UUID.randomUUID()
}

class MeetingRoomAuthentication @Inject() (config: MeetingRoomConfiguration) {
  private val key           = config.secret
  private val algo          = JwtAlgorithm.HS256
  private val applicationId = config.applicationId
  private val domain        = config.domain

  private def claim(content: MeetingRoomTokenContent): JsObject = {
    Json.obj(
      "iss"             -> applicationId,
      "room"            -> content.uuid,
      "sub"             -> domain,
      "aud"             -> applicationId,
      "roomuuid"        -> content.uuid,
      "roomdisplayname" -> content.displayName,
      "roomnumber"      -> content.number,
      "requirepin"      -> content.requirepin,
      "timestamp"       -> content.timestamp,
      "expiry"          -> content.expiry,
      "temporary"       -> content.temporary
    )
  }

  def encode(content: MeetingRoomTokenContent): MeetingRoomTokenResponse = {
    MeetingRoomTokenResponse(
      content.uuid,
      JwtJson.encode(claim(content), key, algo)
    )
  }

  def decode(token: String): Try[MeetingRoomTokenContent] = {
    JwtJson
      .decodeJson(token, key, Seq(algo))
      .map(_.as[MeetingRoomTokenContent])
      .recoverWith { case f =>
        Failure(
          MeetingRoomException(MeetingRoomError.InvalidToken, f.getMessage)
        )
      }
  }

  def extractPin(request: Request[AnyContent]): Option[String] = {
    request.queryString.get("pin").flatMap(_.headOption)
  }

  def extractRedirectFlag(request: Request[AnyContent]): Boolean = {
    request.queryString
      .get("redirect")
      .flatMap(_.headOption)
      .exists(_.toBoolean)
  }

  def recreateToken(oldToken: MeetingRoomTokenContent): String = {
    encode(oldToken).token
  }

  def createExpireTimestamp()(implicit
      clock: MeetingRoomInstantTime
  ): Timestamp =
    Timestamp.from(
      clock.now().plus(java.time.Duration.ofSeconds(config.expiration()))
    )
}
