package xivo.service

import anorm._
import com.google.inject.ImplementedBy
import common.{Crud, CrudMacro, DBUtil}
import model.RecordingModeType
import model.RecordingModeType.Type
import play.api.db._
import xivo.model.QueueFeature

import javax.inject.Inject
import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[QueueFeatureManagerImpl])
trait QueueFeatureManager extends Crud[QueueFeature, Long] {

  def getByName(name: String): Try[QueueFeature]
  def getQueueIdByName(name: String): Option[Long]
  def deleteByName(name: String): Try[QueueFeature]
  def toggleQueueRecordingStatus(status: Int): Try[List[QueueFeature]]
  def getQueueRecordingByStatus(status: Int): Try[List[QueueFeature]]
  def getQueueRecordingByRecordingMode(
      recordingMode: RecordingModeType.Value
  ): Try[List[QueueFeature]]
  def getQueueRecorded: Try[List[QueueFeature]]
}

class QueueFeatureManagerImpl @Inject() (
    @NamedDatabase("xivo") dbXivo: Database
) extends QueueFeatureManager {

  val recordingModeTypeValues: Map[RecordingModeType.Type, String] = Map(
    RecordingModeType.NotRecorded      -> "notrecorded",
    RecordingModeType.Recorded         -> "recorded",
    RecordingModeType.RecordedOnDemand -> "recordedondemand"
  )

  implicit val recordingModeTypeToStatement: ToStatement[Type] =
    DBUtil.enumToStatement(recordingModeTypeValues)
  implicit val recordingModeTypeMetaData: ParameterMetaData[Type] =
    DBUtil.enumTypeMetaData[RecordingModeType.Type]()
  implicit def enumToRecordingModeType: Column[Type] =
    DBUtil.columnToEnum(recordingModeTypeValues)

  val crudParser: RowParser[QueueFeature] = Macro.namedParser[QueueFeature]

  override def create(t: QueueFeature): Try[QueueFeature] =
    dbXivo.withConnection { implicit c =>
      Try({
        val id = CrudMacro
          .query[QueueFeature](
            t,
            "queuefeatures",
            Set("id"),
            Set(),
            true,
            CrudMacro.SnakeCase,
            CrudMacro.SQLInsert
          )
          .executeInsert[Option[Long]]()
        t.copy(id = id)
      })
    }

  override def update(t: QueueFeature): Try[QueueFeature] =
    dbXivo.withConnection { implicit c =>
      val updateCount = CrudMacro
        .query[QueueFeature](
          t,
          "queuefeatures",
          Set("id"),
          Set(),
          false,
          CrudMacro.SnakeCase,
          CrudMacro.SQLUpdate
        )
        .executeUpdate()

      if (updateCount == 1)
        Success(t)
      else
        Failure(new Exception("Cannot update QueueFeature"))
    }

  override def toggleQueueRecordingStatus(
      status: Int
  ): Try[List[QueueFeature]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(
          "update queuefeatures set recording_activated={status} where recording_activated != {status} and recording_mode != 'notrecorded' RETURNING *;"
        )
          .on("status" -> status)
          .as(crudParser.*)
      )
    }

  override def getQueueRecordingByStatus(status: Int): Try[List[QueueFeature]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(
          "select * from queuefeatures where recording_mode != 'notrecorded' and recording_activated = {status};"
        )
          .on("status" -> status)
          .as(crudParser.*)
      )
    }

  override def getQueueRecordingByRecordingMode(
      recordingMode: RecordingModeType.Value
  ): Try[List[QueueFeature]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(
          "select * from queuefeatures where recording_mode = {recordingMode};"
        )
          .on("recordingMode" -> recordingMode.toString)
          .as(crudParser.*)
      )
    }

  override def getQueueRecorded: Try[List[QueueFeature]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(
          "select * from queuefeatures where recording_mode != 'notrecorded';"
        )
          .as(crudParser.*)
      )
    }

  override def get(id: Long): Try[QueueFeature] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from queuefeatures where id={id}")
          .on("id" -> id)
          .as(crudParser.single)
      )
    }

  override def getByName(name: String): Try[QueueFeature] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from queuefeatures where name={name}")
          .on("name" -> name)
          .as(crudParser.single)
      )
    }

  override def getQueueIdByName(name: String): Option[Long] =
    getByName(name) match {
      case Success(queueFeature) =>
        queueFeature.id match {
          case Some(id) => Some(id)
          case None     => None
        }
      case Failure(_) => None
    }

  override def all(): Try[List[QueueFeature]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from queuefeatures")
          .as(crudParser.*)
      )
    }

  override def delete(id: Long): Try[QueueFeature] =
    dbXivo.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure) entry // already deleted
      else {
        val deletedCount = SQL("delete from queuefeatures where id={id}")
          .on("id" -> id)
          .executeUpdate()
        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete QueueFeature"))
      }
    }

  override def deleteByName(name: String): Try[QueueFeature] =
    dbXivo.withConnection { implicit c =>
      val entry = getByName(name)
      if (entry.isFailure) entry // already deleted
      else {
        val deletedCount = SQL("delete from queuefeatures where name={name}")
          .on("name" -> name)
          .executeUpdate()
        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete QueueFeature"))
      }
    }
}
