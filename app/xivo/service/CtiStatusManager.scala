package xivo.service

import anorm.SQL
import cti.models.{
  CtiStatus,
  CtiStatusLegacy,
  CtiStatusLegacySqlResult,
  CtiStatusSqlResult
}
import play.api.db.{Database, NamedDatabase}
import xivo.service.CtiStatusManager.{
  AVAILABLE,
  LOGGEDOFF,
  PAUSE,
  ProfileCtiStatuses
}

import java.sql.Connection
import javax.inject.Inject
import scala.util.Try

class CtiStatusManager @Inject() (@NamedDatabase("xivo") dbXivo: Database) {

  private def getCtiStatusList()(implicit
      c: Connection
  ): Try[ProfileCtiStatuses] = {
    Try(
      SQL(
        s"""
          |
          |
          | SELECT
          |            json_build_object(
          |                cti_profile.id, json_agg(json_build_object('name', ctistatus.name, 'displayName',ctistatus.display_name, 'status',
          |                    case
          |                        WHEN ctistatus.actions = '' THEN 0
          |                        WHEN ctistatus.actions like '%queueunpause_all%' THEN $AVAILABLE
          |                        WHEN ctistatus.actions like '%queuepause_all%' THEN $PAUSE
          |                        WHEN ctistatus.actions like '%agentlogoff%' THEN $LOGGEDOFF
          |                    END
          |                    ))
          |                ) ::text as ctimap
          |
          |        FROM ctistatus, cti_profile, ctipresences
          |
          |        where ctistatus.presence_id = ctipresences.id and ctipresences.id = cti_profile.presence_id and (ctistatus.actions like '%pause_all%' or ctistatus.actions = '' or  ctistatus.actions like '%agentlogoff%' )
          |        GROUP BY cti_profile.id;
          |
          |
          |""".stripMargin
      ).as(CtiStatusSqlResult.CtiStatusSqlResultMapper.*)
        .map(addDefaultPauseIfMissing)
        .reduce(_ ++ _)
    )
  }

  private def addDefaultPauseIfMissing(
      ps: ProfileCtiStatuses
  ): ProfileCtiStatuses = {
    ps.map(prof => {
      if (!prof._2.exists(ctiStatus => ctiStatus.status.contains(PAUSE))) {
        (
          prof._1,
          prof._2 :+ CtiStatus(
            None,
            Option("Pause"),
            Some(PAUSE)
          )
        )
      } else prof
    })
  }

  // Deprecated, To be removed
  private def getCtiStatusLegacyList()(implicit
      c: Connection
  ): Try[Map[String, List[CtiStatusLegacy]]] = {
    Try(
      SQL(
        """
          |
          |   SELECT
          |     json_build_object(
          |      cti_profile.id,
          |      json_agg(
          |         json_build_object(
          |           'name', ctistatus.name,
          |           'color', ctistatus.color,
          |           'longName', ctistatus.display_name,
          |           'actions',
          |          CASE
          |            WHEN ctistatus.actions IS NOT NULL AND ctistatus.actions <> '' THEN
          |               (SELECT json_agg(
          |                 json_build_object(
          |                   'name', split_part(actions, '(', 1),
          |                   'parameters', CASE
          |                     WHEN split_part(actions, '(', 2) <> '' THEN split_part(split_part(actions, '(', 2), ')', 1)
          |                    ELSE ''
          |                   END
          |                 )
          |               ) FROM unnest(string_to_array(ctistatus.actions, ',')) actions)
          |             ELSE
          |               '[]'::json
          |           END
          |         )
          |      )
          |     )::text AS ctimap
          |   FROM ctistatus, cti_profile, ctipresences
          |   WHERE ctistatus.presence_id = ctipresences.id
          |   AND ctipresences.id = cti_profile.presence_id
          |   GROUP BY cti_profile.id;""".stripMargin
      )
        .as(CtiStatusLegacySqlResult.CtiStatusLegacySqlResultMapper.*)
        .reduce(_ ++ _)
    )
  }

  def get: Try[ProfileCtiStatuses] = {
    dbXivo.withConnection { implicit c =>
      getCtiStatusList()
    }
  }
  // Deprecated, To be removed
  def getLegacy: Try[Map[String, List[CtiStatusLegacy]]] = {
    dbXivo.withConnection { implicit c =>
      getCtiStatusLegacyList()
    }
  }
}

object CtiStatusManager {
  type ProfileCtiStatuses = Map[String, List[CtiStatus]]

  val AVAILABLE = 0
  val PAUSE     = 1
  val LOGGEDOFF = 2
}
