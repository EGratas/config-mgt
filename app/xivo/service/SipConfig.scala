package xivo.service

import play.api.libs.json.{JsObject, Json, Reads, Writes}

case class SipConfig(stunAddress: Option[String])

object SipConfig {
  implicit val writer: Writes[SipConfig] = (s: SipConfig) => {
    JsObject(
      Seq(
        "stunAddress" -> Json.toJson(s.stunAddress)
      )
    )
  }
  implicit val read: Reads[SipConfig] = Json.reads[SipConfig]
}
