package xivo.service

import anorm.SQL
import anorm.SqlParser.{bool, get, long, str}
import model.ws.ErrorType
import org.postgresql.util.PSQLException
import play.api.db.{Database, NamedDatabase}
import xivo.model.{
  AddUser,
  CallerId,
  ContextNotFound,
  CreateGroup,
  DatabaseError,
  Group,
  GroupError,
  GroupMembershipAction,
  GroupNotFound,
  MediaServerNotFound,
  RemoveUser,
  UpdateGroup,
  UserAlreadyInGroup,
  UserNotFound,
  UserNotInGroup
}

import java.sql.Connection
import javax.inject.Inject
import scala.util.Try
import scala.util.chaining.scalaUtilChainingOps
class GroupManager @Inject() (@NamedDatabase("xivo") dbXivo: Database) {
  private implicit def boolToInt(bool: Boolean): Int = if (bool) 1 else 0
  def getAllGroups: Either[GroupError, List[Group]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""
             | SELECT gf.id, gf.name, gf.context, ms.name as mds, gf.number,
             | gf.transfer_user as allowTransferUser, gf.transfer_call as allowTransferCall,
             | gf.write_caller as allowCallerRecord, gf.write_calling as allowCalleeRecord,
             | gf.ignore_forward as ignoreForward, gf.preprocess_subroutine as preprocessSubroutine,
             | q.musicclass as onHoldMusic, q.timeout, q.retry as retryTime, q.strategy as ringingStrategy,
             | q.ringinuse as ringInUse, cid.mode, cid.callerdisplay
             | FROM groupfeatures gf
             | INNER JOIN queue q ON gf.name = q.name
             | INNER JOIN mediaserver ms ON ms.id = gf.mediaserverid
             | INNER JOIN callerid cid ON cid.typeval = gf.id
             | WHERE q.category = 'group' AND cid.type = 'group'
             | ORDER BY gf.id
             |""".stripMargin)
          .as(Group.groupMapping.*)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          "Unhandled error happened when getting all groups"
        )
      )
    }

  def getGroupById(groupId: Long): Either[GroupError, Group] =
    dbXivo.withConnection { implicit c =>
      for {
        _     <- isExistingGroup(groupId)
        group <- queryGroup(groupId)
      } yield group
    }

  def createGroup(
      data: CreateGroup
  ): Either[GroupError, Group] = {
    def insertGroupFeature(
        cg: CreateGroup,
        mdsId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | INSERT INTO groupfeatures (name, number, context,
             | transfer_user, transfer_call, write_caller, write_calling, ignore_forward,
             | timeout, preprocess_subroutine, deleted, mediaserverid)
             | VALUES ({name}, {number}, {context},
             | {allowTransferUser}, {allowTransferCall}, {allowCallerRecord}, {allowCalleeRecord}, {ignoreForward},
             | {timeout}, {preprocessSubroutine}, 0, {mds})
             | RETURNING id
             |""".stripMargin)
          .on(
            "name"                 -> cg.name,
            "number"               -> cg.number,
            "context"              -> cg.context,
            "allowTransferUser"    -> cg.allowTransferUser.toInt,
            "allowTransferCall"    -> cg.allowTransferCall.toInt,
            "allowCallerRecord"    -> cg.allowCallerRecord.toInt,
            "allowCalleeRecord"    -> cg.allowCalleeRecord.toInt,
            "ignoreForward"        -> cg.ignoreForward.toInt,
            "timeout"              -> cg.timeout,
            "preprocessSubroutine" -> cg.preprocessSubroutine,
            "mds"                  -> mdsId
          )
          .executeInsert(long("id").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          "An error occurred while inserting group $name into table groupfeatures"
        )
      )

    def insertQueue(
        cg: CreateGroup
    )(implicit c: Connection): Either[GroupError, String] =
      Try(
        SQL(s"""
               | INSERT INTO queue (name, musicclass, timeout,
               | "queue-youarenext", "queue-thereare", "queue-callswaiting",
               | "queue-holdtime", "queue-minutes", "queue-thankyou",
               | "queue-reporthold", "periodic-announce", "announce-frequency",
               | "announce-round-seconds", "announce-holdtime", retry, wrapuptime,
               | maxlen, strategy, joinempty, leavewhenempty,
               | ringinuse, reportholdtime, memberdelay, weight, timeoutrestart,
               | commented, category, timeoutpriority, autofill, autopause, setinterfacevar,
               | setqueueentryvar, setqueuevar, "min-announce-frequency",
               | "random-periodic-announce", "announce-position", "announce-position-limit")
               | VALUES ({name}, {musicClass}, 0,
               | {queueYouAreNext}, {queueThereAre}, {queueCallsWaiting}, {queueHoldTime},
               | {queueMinutes}, {queueThankYou}, {queueReportHold}, {periodicAnnounce},
               | {announceFrequency}, {announceRoundSeconds}, {announceHoldTime}, {retry},
               | {wrapUpTime}, {maxLen}, {strategy}, {joinEmpty}, {leaveWhenEmpty},
               | {ringInUse}, {reportHoldTime}, {memberDelay},
               | {weight}, {timeoutRestart}, {commented}, {category}::queue_category, {timeoutPriority},
               | {autoFill}, {autoPause}, {setInterfaceVar}, {setQueueEntryVar},
               | {setQueueVar}, {minAnnounceFrequency}, {randomPeriodicAnnounce},
               | {announcePosition}, {announcePositionLimit})
               | RETURNING name
               |""".stripMargin)
          .on(
            "name"                   -> cg.name,
            "musicClass"             -> cg.onHoldMusic,
            "queueYouAreNext"        -> "queue-youarenext",
            "queueThereAre"          -> "queue-thereare",
            "queueCallsWaiting"      -> "queue-callswaiting",
            "queueHoldTime"          -> "queue-holdtime",
            "queueMinutes"           -> "queue-minutes",
            "queueThankYou"          -> "queue-thankyou",
            "queueReportHold"        -> "queue-reporthold",
            "periodicAnnounce"       -> "queue-periodic-announce",
            "announceFrequency"      -> 0,
            "announceRoundSeconds"   -> 0,
            "announceHoldTime"       -> "no",
            "retry"                  -> cg.retryTime,
            "wrapUpTime"             -> 0,
            "maxLen"                 -> 0,
            "strategy"               -> cg.ringingStrategy.toDatabase,
            "joinEmpty"              -> "unavailable,invalid,unknown",
            "leaveWhenEmpty"         -> "unavailable,invalid,unknown",
            "ringInUse"              -> cg.ringInUse.toInt,
            "reportHoldTime"         -> 0,
            "memberDelay"            -> 0,
            "weight"                 -> 0,
            "timeoutRestart"         -> 0,
            "commented"              -> 0,
            "category"               -> "group",
            "timeoutPriority"        -> "app",
            "autoFill"               -> 0,
            "autoPause"              -> "no",
            "setInterfaceVar"        -> 0,
            "setQueueEntryVar"       -> 0,
            "setQueueVar"            -> 0,
            "minAnnounceFrequency"   -> 60,
            "randomPeriodicAnnounce" -> 0,
            "announcePosition"       -> "no",
            "announcePositionLimit"  -> 5
          )
          .executeInsert(str("name").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while inserting group ${cg.name} into table queue"
        )
      )

    def insertFuncKey(
        groupId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
               | INSERT INTO func_key(type_id, destination_type_id)
               | VALUES (
               | (SELECT id FROM func_key_type WHERE name = 'speeddial'),
               | (SELECT id FROM func_key_destination_type WHERE name = 'group')
               | )
               |""".stripMargin)
          .executeInsert(long("id").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while inserting group $groupId into table func_key"
        )
      )

    def insertFuncKeyDestGroup(
        funcKeyId: Long,
        groupId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
               | INSERT INTO func_key_dest_group (func_key_id, destination_type_id, group_id)
               | VALUES (
               | {funcKeyId},
               | (SELECT id FROM func_key_destination_type WHERE name = 'group'),
               | {groupId}
               | )
               |""".stripMargin)
          .on(
            "funcKeyId" -> funcKeyId,
            "groupId"   -> groupId
          )
          .executeInsert(long("func_key_id").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while inserting group $groupId into table func_key_dest_group"
        )
      )

    def insertCallerId(
        callerId: CallerId,
        groupFeaturesId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | INSERT INTO callerid (mode, callerdisplay, type, typeval)
             | VALUES ({mode}::callerid_mode, {display}, 'group'::callerid_type, {groupId})
             | RETURNING typeval
             |""".stripMargin)
          .on(
            "mode" -> {
              if (callerId.mode.isEmpty) null else callerId.mode.get.toString
            },
            "display" -> callerId.display,
            "groupId" -> groupFeaturesId
          )
          .executeInsert(long("typeval").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while inserting group $groupFeaturesId into table callerid"
        )
      )

    dbXivo.withTransaction { implicit c =>
      for {
        _ <- isExistingContext(data.context)
        createGroupWithoutEmpty = data.removeEmptyValues()
        mdsId <- getMdsIdFromName(createGroupWithoutEmpty.mds)
        groupFeaturesId <- insertGroupFeature(
          createGroupWithoutEmpty,
          mdsId
        )
        _ <- insertQueue(createGroupWithoutEmpty)
        _ <- insertExtensionIfNotEmpty(
          createGroupWithoutEmpty.number,
          createGroupWithoutEmpty.context,
          groupFeaturesId
        )
        funcKeyId <- insertFuncKey(groupFeaturesId)
        _ <- insertFuncKeyDestGroup(
          funcKeyId,
          groupFeaturesId
        )
        _ <- insertCallerId(
          createGroupWithoutEmpty.callerId,
          groupFeaturesId
        )
        group <- queryGroup(groupFeaturesId)
      } yield group
    }
  }

  def updateGroup(
      groupId: Long,
      data: UpdateGroup
  ): Either[GroupError, Group] = {
    def updateGroupFeature(
        updatedGroup: Group,
        mdsId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | UPDATE groupfeatures
             | SET name = {name}, number = {number}, context = {context},
             | transfer_user = {allowTransferUser},
             | transfer_call = {allowTransferCall},
             | write_caller = {allowCallerRecord},
             | write_calling = {allowCalleeRecord},
             | ignore_forward = {ignoreForward},
             | timeout = {timeout},
             | preprocess_subroutine = {preprocessSubroutine},
             | mediaserverid = {mdsId}
             | WHERE id = {groupId}
             |""".stripMargin)
          .on(
            "groupId"              -> updatedGroup.id,
            "name"                 -> updatedGroup.name,
            "number"               -> updatedGroup.number,
            "context"              -> updatedGroup.context,
            "allowTransferUser"    -> updatedGroup.allowTransferUser.toInt,
            "allowTransferCall"    -> updatedGroup.allowTransferCall.toInt,
            "allowCallerRecord"    -> updatedGroup.allowCallerRecord.toInt,
            "allowCalleeRecord"    -> updatedGroup.allowCalleeRecord.toInt,
            "ignoreForward"        -> updatedGroup.ignoreForward.toInt,
            "timeout"              -> updatedGroup.timeout,
            "preprocessSubroutine" -> updatedGroup.preprocessSubroutine,
            "mdsId"                -> mdsId
          )
          .executeInsert(long("id").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while updating group ${updatedGroup.id} into table groupfeatures"
        )
      )

    def updateQueue(
        updatedGroup: Group,
        oldName: String
    )(implicit c: Connection): Either[GroupError, String] =
      Try(
        SQL(s"""
             | UPDATE queue
             | SET name = {name}, musicclass = {musicClass}, timeout = {timeout},
             | strategy = {strategy}, retry = {retry}, ringinuse = {ringInUse}
             | WHERE name = {oldName} and category = 'group'
             | RETURNING name
             |""".stripMargin)
          .on(
            "groupId"    -> updatedGroup.id,
            "name"       -> updatedGroup.name,
            "timeout"    -> updatedGroup.timeout,
            "retry"      -> updatedGroup.retryTime,
            "strategy"   -> updatedGroup.ringingStrategy.toDatabase,
            "ringInUse"  -> updatedGroup.ringInUse.toInt,
            "musicClass" -> updatedGroup.onHoldMusic,
            "oldName"    -> oldName
          )
          .executeInsert(str("name").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while updating group ${updatedGroup.id} into table queue"
        )
      )

    def updateExtension(
        updatedGroup: Group,
        oldExten: String
    )(implicit c: Connection): Either[GroupError, String] = {
      def updateExten(
          updatedGroup: Group,
          oldExten: String
      ): Either[GroupError, String] = Try(
        SQL(s"""
             | UPDATE extensions
             | SET context = {context}, exten = {exten}
             | WHERE exten = {oldExten} and type = 'group'
             | RETURNING exten
             |""".stripMargin)
          .on(
            "context"  -> updatedGroup.context,
            "exten"    -> updatedGroup.number,
            "oldExten" -> oldExten
          )
          .executeInsert(str("exten").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while updating old extension ${oldExten} for group ${updatedGroup} into table extensions"
        )
      )

      if (!oldExten.equals("") && !updatedGroup.number.equals("")) {
        updateExten(updatedGroup, oldExten)
      } else if (!oldExten.equals("") && updatedGroup.number.equals("")) {
        deleteExtension(updatedGroup.id, oldExten).map(_ => updatedGroup.number)
      } else if (oldExten.equals("") && !updatedGroup.number.equals("")) {
        insertExtensionIfNotEmpty(
          updatedGroup.number,
          updatedGroup.context,
          updatedGroup.id
        ).map(_ => updatedGroup.number)
      } else {
        Right(updatedGroup.number)
      }
    }

    def updateCallerId(updatedGroup: Group)(implicit
        c: Connection
    ): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | UPDATE callerid
             | SET mode = {callerIdMode}::callerid_mode, callerdisplay = {callerIdDisplay}
             | WHERE type = 'group' AND typeval = {groupId}
             | RETURNING typeval
             |""".stripMargin)
          .on(
            "callerIdMode"    -> updatedGroup.callerId.mode.map(_.toString),
            "callerIdDisplay" -> updatedGroup.callerId.display,
            "groupId"         -> updatedGroup.id
          )
          .executeInsert(long("typeval").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while updating callerId ${updatedGroup.callerId.toString} into table callerid"
        )
      )

    dbXivo.withTransaction { implicit c =>
      for {
        group <- getGroupById(groupId)
        updatedGroup = group.updateModifiedProperties(data)
        _        <- isExistingContext(updatedGroup.context)
        mdsId    <- getMdsIdFromName(updatedGroup.mds)
        _        <- updateGroupFeature(updatedGroup, mdsId)
        _        <- updateQueue(updatedGroup, group.name)
        _        <- updateExtension(updatedGroup, group.number)
        _        <- updateCallerId(updatedGroup)
        newGroup <- queryGroup(groupId)
      } yield newGroup
    }
  }

  def deleteGroup(groupId: Long): Either[GroupError, Long] = {
    def deleteQueue(
        groupId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | DELETE FROM queue
             | WHERE name = (SELECT name FROM groupfeatures WHERE id = {groupId})
             | AND category = 'group'
             |""".stripMargin)
          .on(
            "groupId" -> groupId
          )
          .executeUpdate()
          .toLong
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while removing group $groupId from table queue"
        )
      )

    def deleteFuncKeyDestGroup(
        groupId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | DELETE FROM func_key_dest_group WHERE group_id = {groupId}
             | RETURNING func_key_id
             |""".stripMargin)
          .on(
            "groupId" -> groupId
          )
          .executeQuery()
          .as(long("func_key_id").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while removing function key for group $groupId in table func_key_dest_group"
        )
      )

    def deleteFuncKey(funcKeyId: Long)(implicit
        c: Connection
    ): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | DELETE FROM func_key WHERE id = {funcKeyId}
             |""".stripMargin)
          .on(
            "funcKeyId" -> funcKeyId
          )
          .executeUpdate()
          .toLong
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while removing function key $funcKeyId from table func_key"
        )
      )

    def deleteGroupFeature(
        groupId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | DELETE FROM groupfeatures WHERE id = {groupId}
             |""".stripMargin)
          .on(
            "groupId" -> groupId
          )
          .executeUpdate()
          .toLong
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while removing group $groupId from table groupfeatures"
        )
      )

    def deleteCallerId(
        groupId: Long
    )(implicit c: Connection): Either[GroupError, Long] =
      Try(
        SQL(s"""
             | DELETE FROM callerid
             | WHERE type = 'group' and typeval = {groupId}
             |""".stripMargin)
          .on(
            "groupId" -> groupId
          )
          .executeUpdate()
          .toLong
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while removing callerId for group $groupId in table callerid"
        )
      )

    dbXivo.withTransaction { implicit c =>
      for {
        _         <- isExistingGroup(groupId)
        group     <- queryGroup(groupId)
        _         <- deleteQueue(groupId)
        funcKeyId <- deleteFuncKeyDestGroup(groupId)
        _         <- deleteFuncKey(funcKeyId)
        _         <- deleteExtension(groupId, group.number)
        _         <- deleteGroupFeature(groupId)
        _         <- deleteCallerId(groupId)
      } yield groupId
    }
  }

  def addUserToGroup(
      groupId: Long,
      userId: Long
  ): Either[GroupError, Long] =
    dbXivo.withConnection { implicit c =>
      for {
        _         <- isExistingUser(userId)
        _         <- isExistingGroup(groupId)
        groupName <- getGroupName(groupId)
        userId    <- addOrRemoveFromGroup(AddUser, groupName, userId)
      } yield userId
    }

  def removeUserFromGroup(
      groupId: Long,
      userId: Long
  ): Either[GroupError, Long] = dbXivo.withConnection { implicit c =>
    (for {
      _             <- isExistingUser(userId)
      _             <- isExistingGroup(groupId)
      groupName     <- getGroupName(groupId)
      nbRowsUpdated <- addOrRemoveFromGroup(RemoveUser, groupName, userId)
    } yield nbRowsUpdated).filterOrElse(
      _ > 0,
      GroupError(
        UserNotInGroup,
        s"User $userId was not in group $groupId"
      )
    )
  }

  private def isExistingGroup(
      groupId: Long
  )(implicit c: Connection): Either[GroupError, Boolean] =
    Try(
      SQL(s"""
           | SELECT EXISTS (SELECT id FROM groupfeatures
           | WHERE id = {groupId})
           |""".stripMargin)
        .on(
          "groupId" -> groupId
        )
        .executeQuery()
        .as(bool("exists").single)
    ).filter(e => e)
      .pipe(
        toGroupEither(_)(
          GroupNotFound,
          s"Group $groupId does not exist",
          withFailureMessage = false
        )
      )

  private def isExistingUser(
      userId: Long
  )(implicit c: Connection): Either[GroupError, Boolean] =
    Try(
      SQL(s"""
           | SELECT EXISTS (SELECT id FROM userfeatures
           | WHERE id = {userId})
           |""".stripMargin)
        .on(
          "userId" -> userId
        )
        .executeQuery()
        .as(bool("exists").single)
    ).filter(exist => exist)
      .pipe(
        toGroupEither(_)(
          UserNotFound,
          s"User $userId does not exist",
          withFailureMessage = false
        )
      )

  private def isExistingContext(
      contextName: String
  )(implicit c: Connection): Either[GroupError, Boolean] =
    Try(
      SQL(s"""
         | SELECT EXISTS (SELECT name FROM context
         | WHERE name = {context})
         |""".stripMargin)
        .on(
          "context" -> contextName
        )
        .executeQuery()
        .as(bool("exists").single)
    )
      .filter(exist => exist)
      .pipe(
        toGroupEither(_)(
          ContextNotFound,
          s"Context $contextName does not exist",
          withFailureMessage = false
        )
      )

  private def addOrRemoveFromGroup(
      action: GroupMembershipAction,
      groupName: String,
      userId: Long
  )(implicit
      c: Connection
  ): Either[GroupError, Long] = {
    def addToGroup(groupName: String, userId: Long)(implicit
        c: Connection
    ): Try[Long] =
      Try(
        SQL(s"""
             | INSERT INTO queuemember (queue_name, interface, penalty, commented, usertype, userid, channel, category, position)
             | VALUES (
             | {groupName},
             | {interface}, 0, 0, 'user', {userId}, 'Local', 'group', COALESCE ((SELECT MAX(position) + 1 FROM queuemember WHERE category='group' AND queue_name = {groupName}), 1)
             | )
             |""".stripMargin)
          .on(
            "groupName" -> groupName,
            "userId"    -> userId,
            "interface" -> s"Local/id-$userId@usercallback"
          )
          .executeInsert(long("userid").single)
      )

    def removeFromGroup(groupName: String, userId: Long)(implicit
        c: Connection
    ): Try[Long] =
      Try(
        SQL(s"""
             | DELETE FROM queuemember
             | WHERE queue_name = {groupName} AND userid = {userId}
             |""".stripMargin)
          .on(
            "groupName" -> groupName,
            "userId"    -> userId
          )
          .executeUpdate()
      )
    (action match {
      case AddUser    => addToGroup(groupName, userId)
      case RemoveUser => removeFromGroup(groupName, userId)
    }).toEither.left.map {
      case e: PSQLException
          if e.getSQLState == "23505" => // unique constraint violation
        GroupError(
          UserAlreadyInGroup,
          s"User $userId is already in group $groupName"
        )
      case f =>
        GroupError(
          DatabaseError,
          s"Unhandled error happen when adding or removing user $userId from group $groupName : ${f.getMessage}"
        )
    }
  }

  private def getGroupName(groupId: Long)(implicit
      c: Connection
  ): Either[GroupError, String] = Try(
    SQL(s"""
           | SELECT name FROM groupfeatures WHERE id = {groupId}
           |""".stripMargin)
      .on(
        "groupId" -> groupId
      )
      .executeQuery()
      .as(get[String]("name").single)
  ).toEither.left.map { f =>
    GroupError(
      DatabaseError,
      s"Unhandled error when getting group name for group $groupId : ${f.getMessage}"
    )
  }

  private def getMdsIdFromName(mdsName: String)(implicit
      c: Connection
  ): Either[GroupError, Long] =
    Try(
      SQL(s"""
           | SELECT id FROM mediaserver WHERE name = {mdsName}
           |""".stripMargin)
        .on(
          "mdsName" -> mdsName
        )
        .executeQuery()
        .as(long("id").single)
    ).pipe(
      toGroupEither(_)(
        MediaServerNotFound,
        s"Media server $mdsName does not exist",
        withFailureMessage = false
      )
    )

  private def queryGroup(groupId: Long)(implicit
      c: Connection
  ): Either[GroupError, Group] =
    Try(
      SQL(s"""
           SELECT gf.id, gf.name, gf.context, ms.name as mds, gf.number,
           | gf.transfer_user as allowTransferUser, gf.transfer_call as allowTransferCall,
           | gf.write_caller as allowCallerRecord, gf.write_calling as allowCalleeRecord,
           | gf.ignore_forward as ignoreForward, gf.preprocess_subroutine as preprocessSubroutine,
           | q.musicclass as onHoldMusic, gf.timeout as timeout, q.retry as retryTime, q.strategy as ringingStrategy,
           | q.ringinuse, cid.mode, cid.callerdisplay
           | FROM groupfeatures gf INNER JOIN queue q ON gf.name = q.name
           | INNER JOIN mediaserver ms ON ms.id = gf.mediaserverid
           | INNER JOIN callerid cid ON cid.typeval = gf.id
           | WHERE q.category = 'group'
           | AND gf.id = {groupId}
           | AND cid.type = 'group'
           |""".stripMargin)
        .on(
          "groupId" -> groupId
        )
        .as(Group.groupMapping.single)
    ).pipe(
      toGroupEither(_)(
        DatabaseError,
        s"Unhandled error happened when fetching group $groupId"
      )
    )

  private def insertExtensionIfNotEmpty(
      number: String,
      context: String,
      groupId: Long
  )(implicit c: Connection): Either[GroupError, Long] =
    if (number.equals("")) {
      Right(0)
    } else {
      Try(
        SQL(s"""
             | INSERT INTO extensions (commented, context, exten, type, typeval)
             | VALUES (0, {context}, {number}, 'group', {groupId})
             |""".stripMargin)
          .on(
            "context" -> context,
            "number"  -> number,
            "groupId" -> groupId
          )
          .executeInsert(long("id").single)
      ).pipe(
        toGroupEither(_)(
          DatabaseError,
          s"An error occurred while inserting group $groupId into table extensions"
        )
      )
    }

  private def deleteExtension(
      groupId: Long,
      number: String
  )(implicit c: Connection): Either[GroupError, Long] =
    Try(
      SQL(s"""
           | DELETE FROM extensions
           | WHERE exten = {number}
           | AND type = 'group'
           |""".stripMargin)
        .on(
          "groupId" -> groupId,
          "number"  -> number
        )
        .executeUpdate()
        .toLong
    ).pipe(
      toGroupEither(_)(
        DatabaseError,
        s"An error occurred while removing extension for group $groupId in table extensions"
      )
    )

  private def toGroupEither[T](tryValue: Try[T])(
      errorType: ErrorType,
      message: String,
      withFailureMessage: Boolean = true
  ): Either[GroupError, T] =
    tryValue.toEither.left.map(f =>
      GroupError(
        errorType,
        message.concat(if (withFailureMessage) s": ${f.getMessage}" else "")
      )
    )
}
