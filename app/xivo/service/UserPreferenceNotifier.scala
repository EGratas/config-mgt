package xivo.service

import com.google.inject.Inject
import xivo.model.rabbitmq.{
  ObjectEvent,
  UserPreferenceCreated,
  UserPreferenceDeleted,
  UserPreferenceEdited
}
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

class UserPreferenceNotifier @Inject() (
    eventPublisher: XivoRabbitEventsPublisher
) {
  def onUserPreferenceEdited(userid: Long): Unit =
    eventPublisher.publish(ObjectEvent(UserPreferenceEdited, userid))
  def onUserPreferenceCreated(userid: Long): Unit =
    eventPublisher.publish(ObjectEvent(UserPreferenceCreated, userid))
  def onUserPreferenceDeleted(userid: Long): Unit =
    eventPublisher.publish(ObjectEvent(UserPreferenceDeleted, userid))
}
