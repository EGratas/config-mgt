package xivo.service

import javax.inject.Inject
import anorm._

import scala.util.{Failure, Success, Try}
import com.google.inject.ImplementedBy
import common.{Crud, CrudMacro, DBUtil}
import play.api.db.{Database, _}
import xivo.model.GenericBsFilter.Type
import xivo.model.{GenericBsFilter, UserFeature}

@ImplementedBy(classOf[UserFeatureManagerImpl])
trait UserFeatureManager extends Crud[UserFeature, Long] {
  def get(username: String): Try[UserFeature]
}

class UserFeatureManagerImpl @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends UserFeatureManager {

  /** Implicits for type conversion
    */
  protected val genericBsFilterValues: Map[GenericBsFilter.Type, String] = Map(
    GenericBsFilter.Boss      -> "boss",
    GenericBsFilter.No        -> "no",
    GenericBsFilter.Secretary -> "secretary"
  )

  implicit val genericBsFilterToStatement: ToStatement[Type] =
    DBUtil.enumToStatement(genericBsFilterValues)
  implicit val optionBsFilterToStatement: ParameterMetaData[Type] =
    DBUtil.enumTypeMetaData[GenericBsFilter.Type]()
  implicit def enumToGenericBsFilter: Column[Type] =
    DBUtil.columnToEnum(genericBsFilterValues)

  val crudParser: RowParser[UserFeature] = Macro.namedParser[UserFeature]

  override def create(t: UserFeature): Try[UserFeature] =
    dbXivo.withConnection { implicit c =>
      Try({
        val id = CrudMacro
          .query[UserFeature](
            t,
            "userfeatures",
            Set("id"),
            Set(),
            true,
            CrudMacro.SnakeCase,
            CrudMacro.SQLInsert
          )
          .executeInsert[Option[Long]]()
        t.copy(id = id)
      })
    }

  override def update(t: UserFeature): Try[UserFeature] =
    dbXivo.withConnection { implicit c =>
      val updateCount = CrudMacro
        .query[UserFeature](
          t,
          "userfeatures",
          Set("id"),
          Set(),
          true,
          CrudMacro.SnakeCase,
          CrudMacro.SQLUpdate
        )
        .executeUpdate()

      if (updateCount == 1)
        Success(t)
      else
        Failure(new Exception("Cannot update UserFeature"))
    }

  override def all(): Try[List[UserFeature]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from userfeatures")
          .as(crudParser.*)
      )
    }

  override def delete(id: Long): Try[UserFeature] =
    dbXivo.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure) entry
      else {
        val deletedCount = SQL("delete from userfeatures where id={id}")
          .on("id" -> id)
          .executeUpdate()
        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete UserFeature"))
      }
    }

  override def get(id: Long): Try[UserFeature] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from userfeatures where id={id}")
          .on("id" -> id)
          .as(crudParser.single)
      )
    }

  override def get(username: String): Try[UserFeature] = {
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select * from userfeatures where loginclient={username}")
          .on("username" -> username)
          .as(crudParser.single)
      )
    }
  }
}
