package xivo.service

import anorm.SQL
import anorm.SqlParser.scalar
import com.google.inject.Inject
import play.api.db.{Database, NamedDatabase}

import scala.util.Try

class StaticSipManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database
) {

  def getStunAddress(): Try[SipConfig] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("SELECT var_val FROM staticsip WHERE var_name = 'stunaddr'")
          .as(scalar[String].singleOpt)
          .map(addr => SipConfig(Some(addr)))
          .getOrElse(SipConfig(None))
      )
    }
}
