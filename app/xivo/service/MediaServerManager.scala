package xivo.service

import anorm.SqlParser.{get => getColumn}
import anorm.{~, RowParser, SQL}
import common.CrudMacro
import javax.inject.Inject
import play.api.db.{Database, NamedDatabase}
import xivo.model._
import xivo.model.rabbitmq._
import xivo.service.rabbitmq.XivoRabbitEventsPublisher

import scala.util.{Failure, Success, Try}

class MediaServerManager @Inject() (
    @NamedDatabase("xivo") dbXivo: Database,
    rabbitPublisher: XivoRabbitEventsPublisher
) {

  val simple: RowParser[MediaServerConfig] =
    getColumn[Long]("id") ~
      getColumn[String]("name") ~
      getColumn[String]("display_name") ~
      getColumn[Option[String]]("voip_ip") ~
      getColumn[Boolean]("read_only") map {
        case id ~ name ~ display_name ~ voip_ip ~ read_only =>
          MediaServerConfig(id, name, display_name, voip_ip, read_only)
      }

  val simpleWithLines: RowParser[MediaServerWithLineCount] =
    getColumn[Long]("id") ~
      getColumn[String]("name") ~
      getColumn[String]("display_name") ~
      getColumn[Option[String]]("voip_ip") ~
      getColumn[Boolean]("read_only") ~
      getColumn[Long]("line_count") map {
        case id ~ name ~ display_name ~ voip_ip ~ read_only ~ line_count =>
          MediaServerWithLineCount(
            MediaServerConfig(id, name, display_name, voip_ip, read_only),
            line_count
          )
      }

  def create(mediaServerConfig: MediaServerConfigNoId): Try[MediaServerConfig] =
    dbXivo.withConnection { implicit c =>
      Try({
        CrudMacro
          .query[MediaServerConfigNoId](
            mediaServerConfig,
            "mediaserver",
            Set("id"),
            Set(),
            true,
            CrudMacro.SnakeCase,
            CrudMacro.SQLInsert
          )
          .executeInsert[Option[Long]]()
          .get
      })
        .map(mediaServerConfig.withId(_))
        .map(mds => {
          rabbitPublisher.publish(mds, ObjectCreated)
          mds
        })
    }

  def delete(id: Long): Try[MediaServerConfig] =
    dbXivo.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure)
        entry
      else {
        val deletedCount = SQL("delete from mediaserver where id={id}")
          .on("id" -> id)
          .executeUpdate()
        if (deletedCount == 1) {
          entry.foreach(rabbitPublisher.publish(_, ObjectDeleted))
          entry
        } else Failure(new Exception("Cannot delete Media Server"))
      }
    }

  def get(id: Long): Try[MediaServerConfig] =
    dbXivo.withConnection { implicit c =>
      Try({
        SQL(s"""SELECT
                 id, name, display_name, voip_ip, read_only
          FROM
                 mediaserver
          WHERE
                 id={id}""")
          .on("id" -> id)
          .as(simple.single)
      })
    }

  def all(): Try[MediaServerListConfig] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""SELECT
                 id, name, display_name, voip_ip, read_only
          FROM
                 mediaserver
          ORDER BY
                 read_only DESC,
                 display_name
        """.stripMargin)
          .as(simple.*)
      )
        .map(MediaServerListConfig(_))
    }

  def linesCount(): Try[MediaServerListWithLineCount] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL(s"""SELECT
                mediaserver.id, mediaserver.name, mediaserver.display_name,
                mediaserver.voip_ip, mediaserver.read_only, COUNT(linefeatures.*) AS line_count
        FROM
                mediaserver
        INNER JOIN
                linefeatures ON mediaserver.name = linefeatures.configregistrar
          GROUP BY
                 mediaserver.id, mediaserver.name, mediaserver.display_name,
                mediaserver.voip_ip, mediaserver.read_only
          ORDER BY
                 display_name
                 """.stripMargin)
          .as(simpleWithLines.*)
      )
        .map(MediaServerListWithLineCount(_))
    }

  def update(mds: MediaServerConfig): Try[MediaServerConfig] =
    dbXivo.withConnection { implicit c =>
      val update = Try(
        SQL(""" UPDATE mediaserver SET
        |       display_name={display_name}, voip_ip={voip_ip}
        | WHERE
        |       id={id};
        | """.stripMargin)
          .on(
            Symbol("id")           -> mds.id,
            Symbol("display_name") -> mds.display_name,
            Symbol("voip_ip")      -> mds.voip_ip
          )
          .executeUpdate()
      )

      update match {
        case Success(value) =>
          rabbitPublisher.publish(mds, ObjectEdited)
          get(mds.id)
        case Failure(f) => Failure(f)
      }
    }
}
