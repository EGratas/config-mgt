package xivo.service

import javax.inject.Inject
import anorm._
import com.google.inject.ImplementedBy
import common.{AnormMacro, Crud, CrudMacro, DBUtil}
import play.api.db.{Database, _}
import xivo.model.QueueMonitorType.Type
import xivo.model.{Queue, QueueCategory, QueueMonitorType}

import scala.util.{Failure, Success, Try}

@ImplementedBy(classOf[QueueManagerImpl])
trait QueueManager extends Crud[Queue, String] {}

class QueueManagerImpl @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends QueueManager {

  /** Implicits for type conversion
    */
  val queueMonitorTypeValues: Map[QueueMonitorType.Type, String] = Map(
    QueueMonitorType.No         -> "no",
    QueueMonitorType.MixMonitor -> "mixmonitor"
  )

  implicit val queueMonitorTypeToStatement: ToStatement[Type] =
    DBUtil.enumToStatement(queueMonitorTypeValues)
  implicit val optionQueueMonitorTypeMetaData: ParameterMetaData[Type] =
    DBUtil.enumTypeMetaData[QueueMonitorType.Type]()
  implicit def enumToQueueMonitorType: Column[Type] =
    DBUtil.columnToEnum(queueMonitorTypeValues)

  val queueCategoryValues: Map[QueueCategory.Type, String] =
    Map(QueueCategory.Queue -> "queue", QueueCategory.Group -> "group")

  implicit val queueCategoryToStatement: ToStatement[QueueCategory.Type] =
    DBUtil.enumToStatement(queueCategoryValues)
  implicit val optionQueueCategoryMetaData
      : ParameterMetaData[QueueCategory.Type] =
    DBUtil.enumTypeMetaData[QueueCategory.Type]()
  implicit def enumToQueueCategory: Column[QueueCategory.Type] =
    DBUtil.columnToEnum(queueCategoryValues)

  val crudParser: RowParser[Queue] =
    AnormMacro.namedParser[Queue](AnormMacro.ColumnNaming.LispCase)

  override def create(t: Queue): Try[Queue] =
    dbXivo.withConnection { implicit c =>
      Try({
        CrudMacro
          .query[Queue](
            t,
            "queue",
            Set("name"),
            Set(),
            false,
            CrudMacro.LispCase,
            CrudMacro.SQLInsert
          )
          .execute()
        t
      })
    }

  override def update(t: Queue): Try[Queue] =
    dbXivo.withConnection { implicit c =>
      val updateCount = CrudMacro
        .query[Queue](
          t,
          "queue",
          Set("name"),
          Set(),
          false,
          CrudMacro.LispCase,
          CrudMacro.SQLUpdate
        )
        .executeUpdate()

      if (updateCount == 1)
        Success(t)
      else
        Failure(new Exception("Cannot update Queue"))
    }

  override def get(id: String): Try[Queue] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from queue where name={id}")
          .on("id" -> id)
          .as(crudParser.single)
      )
    }

  override def all(): Try[List[Queue]] =
    dbXivo.withConnection { implicit c =>
      Try(
        SQL("select  * from queue")
          .as(crudParser.*)
      )
    }

  override def delete(id: String): Try[Queue] =
    dbXivo.withConnection { implicit c =>
      val entry = get(id)
      if (entry.isFailure) entry // already deleted
      else {
        val deletedCount = SQL("delete from queue where name={id}")
          .on("id" -> id)
          .executeUpdate()
        if (deletedCount == 1) entry
        else Failure(new Exception("Cannot delete Queue"))
      }
    }
}
