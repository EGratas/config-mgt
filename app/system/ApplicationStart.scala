package system

import javax.inject._
import play.api.inject.ApplicationLifecycle
import play.api.{Configuration, Logger}

import scala.concurrent.Future

@Singleton
class ApplicationStart @Inject() (
    lifecycle: ApplicationLifecycle,
    configuration: Configuration
) {
  val log: Logger = Logger(getClass.getName)

  // Shut-down hook
  lifecycle.addStopHook { () =>
    Future.successful(log.info("Stopping configuration server"))
  }

  log.info("Starting")

  printConfiguration()

  lifecycle.addStopHook { () =>
    Future.successful(log.info("Stopping configuration server"))
  }

  private def printConfiguration(): Unit = {
    val xivohost = configuration.get[String]("xivohost")
    val dbhost   = configuration.get[String]("dbhost")
    val defaultQueueDissuasion =
      configuration.get[String]("defaultQueueDissuasion")

    log.info("Starting configuration server")
    log.info("----------Configuration----------------------------")
    log.info(s"Xivo Host                : $xivohost")
    log.info(s"Db Host                  : $dbhost")
    log.info(s"Default queue dissuasion : $defaultQueueDissuasion")
    log.info("---------------------------------------------------")
  }
}
