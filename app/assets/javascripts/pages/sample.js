var authToken = $('#cmgt_auth_token').val();

function logIn() {
    var username = $('#cmgt_username').val();
    var password = $('#cmgt_password').val();

    localStorage.setItem('sampleUsername', username);
    localStorage.setItem('samplePassword', password);

    console.log("sign in " + " : " + username + " " + password);

    makeAuthCall("POST", "/configmgt/login",
        {"login": username, "mdp": password}).then(function(user) {
        console.log("Logging in...");

        $('#cmgt_logon_panel').hide();
        $('#nav-tabs').show();
        $('#tab-content').show();
        $('#cmgt_restart').show();
        $('#welcome').hide();

        displayFindFilters('fcr');
        displayFindFilters('fur');
        displayFindFilters('fmr');
    }).fail(function() {
        var error = "Impossible to retrieve auth token for user " + username;
        generateMessage(error, true, true);
    });
}

function logOut() {
    makeAuthCall("GET", "/configmgt/logout").then(function() {
        console.log("Logging out...");
        cleanAll();

        $('#cmgt_logon_panel').show();
        $('#nav-tabs').hide();
        $('#tab-content').hide();
        $('#cmgt_restart').hide();
        $('#welcome').show();

        $('#show_search_filters').show();
        $('#hide_search_filters').hide();
    }).fail(function() {
        var error = "Log out failed!";
        generateMessage(error, true, true);
    });
}

function cleanAll() {
    //Create Callbacks List
    $('#cmgt_ccl_name').val('');
    $('#cmgt_ccl_queueId').val('');
    cleanCreateCallbacksList();

    //Get Callbacks List
    $('#cmgt_gcl_withRequest').val('');
    cleanGetCallbacksList();

    //Delete Callbacks List
    $('#cmgt_dcl_uuid').val('');
    cleanDeleteCallbacksList();

    //Import Callbacks requests
    $('#cmgt_icr_listUuid').val('');
    $('#cmgt_icr_phoneNumber').val('');
    $('#cmgt_icr_mobilePhoneNumber').val('');
    $('#cmgt_icr_firstName').val('');
    $('#cmgt_icr_lastName').val('');
    $('#cmgt_icr_company').val('');
    $('#cmgt_icr_description').val('');
    $('#cmgt_icr_dueDate').val('');
    $('#cmgt_icr_period').val('');
    cleanImportCallbacksRequests();

    //Create Callback request
    $('#cmgt_ccr_listUuid').val('');
    $('#cmgt_ccr_phoneNumber').val('');
    $('#cmgt_ccr_mobilePhoneNumber').val('');
    $('#cmgt_ccr_firstName').val('');
    $('#cmgt_ccr_lastName').val('');
    $('#cmgt_ccr_company').val('');
    $('#cmgt_ccr_description').val('');
    $('#cmgt_ccr_dueDate').val('');
    $('#cmgt_ccr_period').val('');
    cleanCreateCallbackRequest();

    //Find Callback request
    $('#cmgt_fcr_offset').val('');
    $('#cmgt_fcr_limit').val('');
    displayFindFilters('fcr');
    displayFindFilters('fur');
    cleanFindCallbackRequest();

    //Find Meeting Room
    $('#cmgt_find_meetingroom_offset').val('');
    $('#cmgt_find_meetingroom_limit').val('');
    displayFindFilters('fmr');
    cleanFindMeetingRoom();

    //Users
    cleanGetUserList();
    $('#cmgt_fur_offset').val('');
    $('#cmgt_fur_limit').val('');
    cleanFindUsers();

    //User Line
    cleanUserLine('get');
    cleanUserLine('del');
    cleanUserLine('upd');
    $('#cmgt_gul_userid').val('');
    $('#cmgt_dul_userid').val('');
    $('#cmgt_uul_userid').val('');
}

function cleanCreateCallbacksList() {
    $('#create_callbacks_list').empty();
}

function cleanGetCallbacksList() {
    $('#get_callbacks_list').empty();
}

function cleanDeleteCallbacksList() {
    $('#delete_callbacks_list').empty();
}

function cleanImportCallbacksRequests() {
    $('#import_callbacks_requests').empty();
}

function cleanCreateCallbackRequest() {
    $('#create_callback_request').empty();
}

function cleanFindCallbackRequest() {
    $('#find_callback_request').empty();
}

function cleanGetUserList() {
    $('#get_user_list').empty();
}

function cleanGetUserPreferences() {
    $('#get-user-preferences').empty();
}

function printGetUserPreferences() {
    $('#get-user-preferences').empty();
}

function cleanFindUsers() {
    $('#find_users').empty();
}

function cleanUserLine(action) {
    $('#'+action+'_user_line').empty();
}

function cleanFindMeetingRoom() {
    $('#find_meetingroom').empty();
}

function printCreateCallbacksList(list) {
    console.log('Creating callbacks list...' + JSON.stringify(list));
    $('#create_callbacks_list').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(list) + '</code></pre></li>');
}

function printGetCallbacksList(list) {
    $('#get_callbacks_list').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(list) + '</code></pre></li>');
}

function printDeleteCallbacksList() {
    $('#delete_callbacks_list').prepend('<li class="list-group-item"><pre><code>Successfully deleted this callback list!</code></pre></li>');
}

function printImportCallbacksRequests() {
    $('#import_callbacks_requests').prepend('<li class="list-group-item"><pre><code>Successfully imported the CSV file!</code></pre></li>');
}

function printCreateCallbackRequest(request) {
    $('#create_callback_request').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(request) + '</code></pre></li>');
}

function printFindCallbackRequest(request) {
    $('#find_callback_request').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(request) + '</code></pre></li>');
}

function printGetUserList(list) {
    $('#get_user_list').prepend('<li class="list-group-item"><pre><code>' + list + '</code></pre></li>');
}

function printGetUserPreferences(list) {
    $('#get-user-preferences').prepend('<li class="list-group-item"><pre><code>' + list + '</code></pre></li>');
}

function printFindUsers(results) {
    $('#find_users').prepend('<li class="list-group-item"><pre><code>' + results + '</code></pre></li>');
}

function printUserLine(line,action) {
    $('#'+action+'_user_line').prepend('<li class="list-group-item"><pre><code>' + line + '</code></pre></li>');
}

function printMeetingRoomsList(list) {
    $('#get_meetingrooms_list').prepend('<li class="list-group-item"><pre><code>' + list + '</code></pre></li>');
}

function printMeetingRoom(room) {
    $('#get_meetingroom').prepend('<li class="list-group-item"><pre><code>' + room + '</code></pre></li>');
}

function printCreateMeetingRoom(room) {
    $('#create_meetingroom').prepend('<li class="list-group-item"><pre><code>' + room + '</code></pre></li>');
}

function printDeleteMeetingRoom(room) {
    $('#delete_meetingroom').prepend('<li class="list-group-item"><pre><code>' + room + '</code></pre></li>');
}

function printFindMeetinRoomList(list) {
    $('#find_meetingroom').prepend('<li class="list-group-item"><pre><code>' + list + '</code></pre></li>');
}

function generateMessage(message, isError) {
    var className = (isError) ? 'danger' : 'success';
    $('#main_errors').html('<p class="alert alert-'+className+' alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + new Date().toLocaleString() + ": " + JSON.stringify(message) + '</p>');
    console.log(message);
}

function displayFindFilters(id) {
    $('#cmgt_'+id+'_filters').html(
        "<div class=\"entry inline-form col-xs-12\">\n" +
        "<label class=\"col-sm-4 control-label\">field</label>\n" +
        "<div class=\"col-sm-8\">\n" +
        "<label for=\"cmgt_"+id+"_fields\"></label><input class=\"form-control\" name=\"cmgt_"+id+"_fields[]\" type=\"text\" placeholder=\"Field to query on\"/>\n" +
        "</div>\n" +
        "<label class=\"col-sm-4 control-label\">operator</label>\n" +
        "<div class=\"col-sm-8\">\n" +
        "<label for=\"cmgt_"+id+"_operators\"></label><input class=\"form-control\" name=\"cmgt_"+id+"_operators[]\" type=\"text\" placeholder=\"=,!=,>,>=,<,<=,like,ilike, is null, is not null\"/>\n" +
        "</div>\n" +
        "<label class=\"col-sm-4 control-label\">value</label>\n" +
        "<div class=\"col-sm-8\">\n" +
        "<label for=\"cmgt_"+id+"_values\"></label><input class=\"form-control\" name=\"cmgt_"+id+"_values[]\" type=\"text\" placeholder=\"Value to filter on\"/>\n" +
        "</div>\n" +
        "<label class=\"col-sm-4 control-label\">list</label>\n" +
        "<div class=\"col-sm-8\">\n" +
        "<label for=\"cmgt_"+id+"_list\"></label><input class=\"form-control\" name=\"cmgt_"+id+"_list[]\" type=\"text\" placeholder=\"List to filter on (for contains_all operator). Separate value with comma\"/>\n" +
        "</div>\n" +
        "<label class=\"col-sm-4 control-label\">order</label>\n" +
        "<div class=\"col-sm-8\">\n" +
        "<label for=\"cmgt_"+id+"_orders\"></label>\n" +
        "<label for=\"cmgt_"+id+"_orders\"></label><input class=\"form-control\" name=\"cmgt_"+id+"_orders[]\" type=\"text\" placeholder=\"ASC or DESC\"/>\n" +
        "</div>\n" +
        "<button type=\"button\" class=\"btn-custom btn-success-custom btn-lg btn-add "+id+"\">\n" +
        "<span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\n" +
        "</button>\n" +
        "</div>"
    );
}

function addFindFilter(e) {
  e.preventDefault();
  var controlForm = $(this).parents('.entry:first').parent(),
      currentEntry = $(this).parents('.entry:first'),
      newEntry = $(currentEntry.clone()).prependTo(controlForm);
  newEntry.find('input').val('');
  controlForm.find('.entry:not(:last) .btn-add')
      .removeClass('btn-add').addClass('btn-remove')
      .removeClass('btn-success-custom').addClass('btn')
      .addClass('btn-danger').addClass('btn-custom')
      .html('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>');
}

function removeFindFilter(e) {
  e.preventDefault();
  $(this).parents('.entry:first').remove();
  return false;
}

$(function() {
    console.log("Initializing sample page...");

    logOut();

    var configMGT = window.location.hostname+":"+window.location.port;
    $('#configMGT').val(configMGT);

    $('#cmgt_username').val(localStorage.getItem('sampleUsername'));
    $('#cmgt_password').val(localStorage.getItem('samplePassword'));

    $('#cmgt_sign_in').click(function() {
        logIn();
    });

    $('#cmgt_restart').click(function() {
        logOut();
    });

    $(document).on('click', '.btn-add.fcr', addFindFilter)
               .on('click', '.btn-remove', removeFindFilter);

    $(document).on('click', '.btn-add.fur', addFindFilter)
               .on('click', '.btn-remove', removeFindFilter);

   $(document).on('click', '.btn-add.fmr', addFindFilter)
              .on('click', '.btn-remove', removeFindFilter);

    $('#cmgt_clean_create_callbacks_list').click(function() {
        cleanCreateCallbacksList();
    });

    $('#cmgt_clean_get_callbacks_list').click(function() {
        cleanGetCallbacksList();
    });

    $('#cmgt_clean_delete_callbacks_list').click(function() {
        cleanDeleteCallbacksList();
    });

    $('#cmgt_clean_import_callbacks_requests').click(function() {
        cleanImportCallbacksRequests();
    });

    $('#cmgt_clean_create_callback_request').click(function() {
        cleanCreateCallbackRequest();
    });

    $('#cmgt_clean_find_callback_request').click(function() {
        cleanFindCallbackRequest();
    });

    $('#cmgt_clean_get_user_list').click(function() {
        cleanGetUserList();
    });

    $('#cmgt_clean_get_user_preferences').click(function() {
        cleanGetUserPreferences();
    });

    $('#cmgt_clean_find_users').click(function() {
        cleanFindUsers();
    });

    $('#cmgt_clean_get_user_line').click(function() {
        cleanUserLine('get');
    });

    $('#cmgt_clean_upd_user_line').click(function() {
        cleanUserLine('upd');
    });


    $('#cmgt_get_create_callbacks_list').click(function() {
        var name = $('#cmgt_ccl_name').val();
        var queueId = Number($('#cmgt_ccl_queueId').val());

        if(!name) generateMessage("Cannot create callbacks list with no name", true, true);
        else if(!queueId) generateMessage("Cannot create callbacks list with no queue ID", true, true);
        else {
            var json = {};
            json.name = name;
            json.queueId = queueId;

            makeAuthCall('POST', "/configmgt/api/1.0/callback_lists", json).then(function(list) {
                printCreateCallbacksList(list);
            }).fail(function() {
                var error = "Impossible to create callbacks list";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_get_get_callbacks_list').click(function() {
       var withRequest = $('#cmgt_gcl_withRequest').val();

       var param = "";
       if(withRequest) param = "?withRequest=" + withRequest;
        makeAuthCall('GET', "/configmgt/api/1.0/callback_lists" + param).then(function(list) {
            printGetCallbacksList(list);
        }).fail(function() {
            var error = "Impossible to get callbacks list";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_get_delete_callbacks_list').click(function() {
        var uuid = $('#cmgt_dcl_uuid').val();

        if(!uuid) generateMessage("Cannot delete a callbacks list without its ID", true, true);
        else {
            makeAuthCall('DELETE', "/configmgt/api/1.0/callback_lists/" + uuid).then(function() {
                printDeleteCallbacksList();
            }).fail(function() {
                var error = "Impossible to delete callbacks list";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_get_import_callbacks_requests').click(function() {
        var listUuid = $('#cmgt_icr_listUuid').val();

        var data = $('#cmgt_icr_file')[0].files[0];

        if(!listUuid) generateMessage("Cannot import callbacks requests without an ID", true, true);
        else {
            makeAuthCallCsv('POST', "/configmgt/api/1.0/callback_lists/" + listUuid + "/callback_requests/csv", data).then(function() {
                printImportCallbacksRequests();
            }).fail(function() {
                var error = "Impossible to import callbacks requests";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_get_create_callback_request').click(function() {
        var listUuid = $('#cmgt_ccr_listUuid').val();
        var phoneNumber = $('#cmgt_ccr_phoneNumber').val();
        var mobilePhoneNumber = $('#cmgt_ccr_mobilePhoneNumber').val();
        var firstName = $('#cmgt_ccr_firstName').val();
        var lastName = $('#cmgt_ccr_lastName').val();
        var company = $('#cmgt_ccr_company').val();
        var description = $('#cmgt_ccr_description').val();
        var dueDate = $('#cmgt_ccr_dueDate').val();
        var period = $('#cmgt_ccr_period').val();

        if(!listUuid) generateMessage("Cannot create callback request without an ID", true, true);
        else {
            var json = {};

            if(phoneNumber) json.phoneNumber = phoneNumber;
            if(mobilePhoneNumber) json.mobilePhoneNumber = mobilePhoneNumber;
            if(firstName) json.firstName = firstName;
            if(lastName) json.lastName = lastName;
            if(company) json.company = company;
            if(description) json.description = description;
            if(dueDate) json.dueDate = dueDate;
            if(period) json.period = period;

            makeAuthCall('POST', "/configmgt/api/1.0/callback_lists/" + listUuid + "/callback_requests", json).then(function(request) {
                printCreateCallbackRequest(request);
            }).fail(function() {
                var error = "Impossible to create callback request";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_get_find_callback_request').click(function() {
        var offset = $('#cmgt_fcr_offset').val();
        var limit = $('#cmgt_fcr_limit').val();

        if(!offset) generateMessage("Cannot find callback requests without an offset", true, true);
        else if(!limit) generateMessage("Cannot find callback requests without a limit", true, true);
        else {
            var fields = [];
            var operators = [];
            var values = [];
            var orders = [];

            $('input[name^="cmgt_fcr_fields"]').each(function() {
                if($(this).val()) fields.push($(this).val());
            });

            $('input[name^="cmgt_fcr_operators"]').each(function() {
                if($(this).val()) operators.push($(this).val());
            });

            $('input[name^="cmgt_fcr_values"]').each(function() {
                if($(this).val()) values.push($(this).val());
            });

            $('input[name^="cmgt_fcr_orders"]').each(function() {
                if($(this).val()) orders.push($(this).val());
            });

            var filters = [];
            var i;
            for(i=0; i<fields.length; i++) {
                filters.push({
                    "field":fields[i],
                    "operator":operators[i],
                    "value":values[i],
                    "order":orders[i]
                });
            }

            var json = {};
            if(filters) json.filters = filters;
            if(offset) json.offset = Number(offset);
            if(limit) json.limit = Number(limit);

            makeAuthCall('POST', "/configmgt/api/1.0/callback_requests/find", json).then(function(request) {
                printFindCallbackRequest(request);
            }).fail(function() {
                var error = "Impossible to find callback request";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_get_user_list').click(function() {

        makeAuthCall('GET', "/configmgt/api/2.0/users").then(function(list) {
            printGetUserList(list);
        }).fail(function() {
            var error = "Impossible to get Xivo User list";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_get_user_preferences').click(function() {
        var userId = $('#cmgt_preferences-user-id').val();
        makeAuthCall('GET', "/configmgt/api/2.0/users/" + userId + "/preferences").then(function(list) {
            printGetUserPreferences(list);
        }).fail(function() {
            var error = "Impossible to get user preferences";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_get_user_preference').click(function() {
        var userId = $('#cmgt_preferences-user-id').val();
        var preferenceKey = $('#cmgt_preferences_key').val();
        makeAuthCall('GET', "/configmgt/api/2.0/users/" + userId + "/preferences/" + preferenceKey).then(function(list) {
            printGetUserPreferences(list);
        }).fail(function() {
            var error = "Impossible to get user preferences";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_create_user_preference').click(function() {
        var userId = $('#cmgt_preferences-user-id').val();
        var preferenceKey = $('#cmgt_preferences_key').val();
        var preferenceValue = $('#cmgt_preferences_value').val();
        var preferenceType = $('#cmgt_preferences_type').val();
        makeAuthCall('POST', "/configmgt/api/2.0/users/" + userId + "/preferences/" + preferenceKey, {value: preferenceValue, value_type: preferenceType}).then(function(list) {
            printGetUserPreferences(list);
        }).fail(function() {
            var error = "Impossible to get user preferences";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_update_user_preference').click(function() {
        var userId = $('#cmgt_preferences-user-id').val();
        var preferenceKey = $('#cmgt_preferences_key').val();
        var preferenceValue = $('#cmgt_preferences_value').val();
        var preferenceType = $('#cmgt_preferences_type').val();
        makeAuthCall('PUT', "/configmgt/api/2.0/users/" + userId + "/preferences/" + preferenceKey, {value: preferenceValue, value_type: preferenceType}).then(function(list) {
            printGetUserPreferences(list);
        }).fail(function() {
            var error = "Impossible to get user preferences";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_delete_user_preference').click(function() {
        var userId = $('#cmgt_preferences-user-id').val();
        var preferenceKey = $('#cmgt_preferences_key').val();
        makeAuthCall('DELETE', "/configmgt/api/2.0/users/" + userId + "/preferences/" + preferenceKey).then(function(list) {
        }).fail(function() {
            var error = "Impossible to get user preferences";
            generateMessage(error, true, true);
        });
    });

     $('#cmgt_get_find_users').click(function() {
        var offset = $('#cmgt_fur_offset').val();
        var limit = $('#cmgt_fur_limit').val();

        if(!offset) generateMessage("Cannot find users without an offset", true, true);
        else if(!limit) generateMessage("Cannot find users without a limit", true, true);
        else {
          var fields = [];
          var operators = [];
          var values = [];
          var lists = [];
          var orders = [];

          $('input[name^="cmgt_fur_fields"]').each(function() {
              if($(this).val()) fields.push($(this).val());
          });

          $('input[name^="cmgt_fur_operators"]').each(function() {
              if($(this).val()) operators.push($(this).val());
          });

          $('input[name^="cmgt_fur_values"]').each(function() {
              values.push($(this).val());
          });

          $('input[name^="cmgt_fur_list"]').each(function() {
            lists.push($(this).val().split(',').map(function (elem) {return elem.trim();}));
          });

          $('input[name^="cmgt_fur_orders"]').each(function() {
              if($(this).val()) orders.push($(this).val());
          });

          var filters = [];
          var i;
          for(i=0; i<fields.length; i++) {
              filters.push({
                  "field":fields[i],
                  "operator":operators[i],
                  "value":values[i],
                  "list":lists[i],
                  "order":orders[i]
              });
          }

          var json = {};
          if(filters) json.filters = filters;
          json.offset = Number(offset);
          json.limit = Number(limit);

          makeAuthCall('POST', "/configmgt/api/2.0/users/find", json).then(function(request) {
              printFindUsers(request);
          }).fail(function() {
              var error = "Impossible to find users";
              generateMessage(error, true, true);
          });
        }
    });

    $('#cmgt_get_user_line').click(function() {

        var userId = $('#cmgt_gul_userid').val();

        if(!userId) { generateMessage("Cannot get a line without XiVO user id", true, true); }
        else {
          makeAuthCall('GET', "/configmgt/api/2.0/users/"+userId+"/line").then(function(line) {
              printUserLine(line, 'get');
          }).fail(function() {
              var error = "Impossible to get Xivo User line";
              generateMessage(error, true, true);
          });
        }
    });

    $('#cmgt_del_user_line').click(function() {

        var userId = $('#cmgt_dul_userid').val();

        if(!userId) { generateMessage("Cannot delete a line without XiVO user id", true, true); }
        else {
          makeAuthCall('DELETE', "/configmgt/api/2.0/users/"+userId+"/line").then(function(line) {
              printUserLine(line, 'del');
          }).fail(function() {
              var error = "Impossible to delete Xivo User line";
              generateMessage(error, true, true);
          });
        }
    });

    $('#cmgt_cre_user_line').click(function() {
        var userId = $('#cmgt_uul_userid').val();
        var lineType = $('#cmgt_uul_lineType').val();
        var context = $('#cmgt_uul_context').val();
        var site = $('#cmgt_uul_site').val();
        var extension = $('#cmgt_uul_extension').val();
        var device = $('#cmgt_uul_device').val();
        var lineNum = Number($('#cmgt_uul_lineNum').val());


        if(!userId) { generateMessage("Cannot create a line without XiVO user id", true, true); }
        else {
          var json = {};
          json.lineType = lineType;
          json.context = context;
          json.site = site;
          json.extension = extension;
          if (device !== "") json.device = device;
          json.lineNum = lineNum;

          makeAuthCall('POST', "/configmgt/api/2.0/users/"+userId+"/line", json).then(function(line) {
              printUserLine(line, 'upd');
          }).fail(function() {
              var error = "Impossible to create Xivo User line";
              generateMessage(error, true, true);
          });
        }
    });

    $('#cmgt_upd_user_line').click(function() {
        var userId = $('#cmgt_uul_userid').val();
        var lineType = $('#cmgt_uul_lineType').val();
        var context = $('#cmgt_uul_context').val();
        var site = $('#cmgt_uul_site').val();
        var extension = $('#cmgt_uul_extension').val();
        var device = $('#cmgt_uul_device').val();
        var lineNum = Number($('#cmgt_uul_lineNum').val());


        if(!userId) { generateMessage("Cannot update a line without XiVO user id", true, true); }
        else {
          var json = {};
          json.lineType = lineType;
          json.context = context;
          json.site = site;
          json.extension = extension;
          if (device !== "") json.device = device;
          json.lineNum = lineNum;

          makeAuthCall('PUT', "/configmgt/api/2.0/users/"+userId+"/line", json).then(function(line) {
              printUserLine(line, 'upd');
          }).fail(function() {
              var error = "Impossible to update Xivo User line";
              generateMessage(error, true, true);
          });
        }
    });

    $('#cmgt_get_meetingrooms_list').click(function() {
        makeAuthCall('GET', "/configmgt/api/2.0/meetingrooms").then(function(list) {
            printMeetingRoomsList(list);
        }).fail(function() {
            var error = "Impossible to get Meeting Rooms list";
            generateMessage(error, true, true);
        });
    });

    $('#cmgt_clean_get_meetingrooms_list').click(function() {
        $('#get_meetingrooms_list').empty();
    });

    $('#cmgt_get_meetingroom').click(function(id) {
        var roomId = $('#cmgt_get_meetingroom_userid').val();
        if(!roomId) { generateMessage("Cannot get a meeting room without id", true, true); }
        else {
            makeAuthCall('GET', "/configmgt/api/2.0/meetingrooms/"+roomId).then(function(room) {
                printMeetingRoom(room);
            }).fail(function() {
                var error = "Impossible to get meeting room";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_clean_get_meetingroom').click(function() {
        $('#get_meetingroom').empty();
    });

    $('#cmgt_create_meetingroom').click(function(id) {

    var name = $('#cmgt_create_meetingroom_name').val();
    var displayName = $('#cmgt_create_meetingroom_display_name').val();
    var number = Number($('#cmgt_create_meetingroom_number').val());
    var userPin = Number($('#cmgt_create_meetingroom_user_pin').val());

    if(!displayName) generateMessage("Cannot create meeting room with no display name", true, true);
    else if(!number) generateMessage("Cannot create meeting room with no number", true, true);
    else {
            var json = {};
            json.name = name;
            json.displayName = displayName;
            json.number = number;
            if (userPin !== 0) json.userPin = userPin;

            makeAuthCall('POST', "/configmgt/api/2.0/meetingrooms", json).then(function(room) {
                printCreateMeetingRoom(room);
            }).fail(function() {
                var error = "Impossible to create meeting room";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_update_meetingroom').click(function(id) {
    var id = Number($('#cmgt_create_meetingroom_id').val());
    var name = $('#cmgt_create_meetingroom_name').val();
    var displayName = $('#cmgt_create_meetingroom_display_name').val();
    var number = Number($('#cmgt_create_meetingroom_number').val());
    var userPin = Number($('#cmgt_create_meetingroom_user_pin').val());

    if(!id) generateMessage("Cannot update meeting room with no id", true, true);
    else if(!displayName) generateMessage("Cannot update meeting room with no display name", true, true);
    else if(!number) generateMessage("Cannot update meeting room with no number", true, true);
    else {
            var json = {};
            json.id = id
            json.name = name;
            json.displayName = displayName;
            json.number = number;
            if (userPin !== 0) json.userPin = userPin;

            makeAuthCall('PUT', "/configmgt/api/2.0/meetingrooms", json).then(function(room) {
                printCreateMeetingRoom(room);
            }).fail(function() {
                var error = "Impossible to create meeting room";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_clean_create_meetingroom').click(function() {
        $('#create_meetingroom').empty();
    });

    $('#cmgt_delete_meetingroom').click(function(id) {
        var roomId = $('#cmgt_delete_meetingroom_id').val();
        if(!roomId) { generateMessage("Cannot delete a meeting room without id", true, true); }
        else {
            makeAuthCall('DELETE', "/configmgt/api/2.0/meetingrooms/"+roomId).then(function(room) {
                printDeleteMeetingRoom(room);
            }).fail(function() {
                var error = "Impossible to delete meeting room";
                generateMessage(error, true, true);
            });
        }
    });

    $('#cmgt_clean_delete_meetingroom').click(function() {
        $('#delete_meetingroom').empty();
    });

    $('#cmgt_find_meetingroom').click(function(id) {
        var offset = $('#cmgt_find_meetingroom_offset').val();
        var limit = $('#cmgt_find_meetingroom_limit').val();

        if(!offset) generateMessage("Cannot find meeting room without an offset", true, true);
        else if(!limit) generateMessage("Cannot find meeting room without a limit", true, true);
        else {
                var fields = [];
                var operators = [];
                var values = [];
                var orders = [];

                $('input[name^="cmgt_fmr_fields"]').each(function() {
                    if($(this).val()) fields.push($(this).val());
                });

                $('input[name^="cmgt_fmr_operators"]').each(function() {
                    if($(this).val()) operators.push($(this).val());
                });

                $('input[name^="cmgt_fmr_values"]').each(function() {
                    if($(this).val()) values.push($(this).val());
                });

                $('input[name^="cmgt_fmr_orders"]').each(function() {
                    if($(this).val()) orders.push($(this).val());
                });

                var filters = [];
                var i;
                for(i=0; i<fields.length; i++) {
                    filters.push({
                        "field":fields[i],
                        "operator":operators[i],
                        "value":values[i],
                        "order":orders[i]
                    });
                }

                var json = {};
                if(filters) json.filters = filters;
                if(offset) json.offset = Number(offset);
                if(limit) json.limit = Number(limit);

                makeAuthCall('POST', "/configmgt/api/2.0/meetingrooms/find", json).then(function(list) {
                    printFindMeetinRoomList(list);
                }).fail(function() {
                    var error = "Impossible to find callback request";
                    generateMessage(error, true, true);
                });
            }
    });

    $('#cmgt_clean_find_meetingroom').click(function() {
        cleanFindMeetingRoom();
    });
});
