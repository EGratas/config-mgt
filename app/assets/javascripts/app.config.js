(function() {
  'use strict';

  angular.module('users').config(config);
  angular.module('callbacks').config(config);

  config.$inject = ['$logProvider', '$translateProvider', '$translatePartialLoaderProvider'];

  function config($logProvider, $translateProvider, $translatePartialLoaderProvider) {
    $logProvider.debugEnabled(false);

    $translatePartialLoaderProvider.addPart('config');
    $translateProvider.useLoader('$translatePartialLoader', {
      urlTemplate: 'assets/i18n/{part}-{lang}.json'
    });
    $translateProvider.registerAvailableLanguageKeys(['en','fr'], {
      'en_*': 'en',
      'fr_*': 'fr'
    });
    $translateProvider.preferredLanguage(document.body.getAttribute('data-preferredlang'));
    $translateProvider.fallbackLanguage(['fr']);
    $translateProvider.forceAsyncReload(true);
  }
})();