package model

import java.util.Date

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import javax.inject.Inject
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.Logger
import play.api.db.{Database, _}
import play.api.libs.json.{JsArray, JsValue, Json, Writes}
import model.ws.{ErrorResult, ErrorType}
import play.api.mvc.{Result, Results}

sealed trait Right {
  val name: String
  def data: JsValue

  def toJson: JsValue = Json.obj("type" -> name, "data" -> data)

}

case class AdminRight() extends Right {
  val name = "admin"

  override def data: JsValue = Json.obj()

}
case class SupervisorRight(
    queueIds: List[Int],
    groupIds: List[Int],
    incallIds: List[Int],
    recordingAccess: Boolean = true,
    dissuasionAccess: Boolean = false
) extends Right {
  val name = "supervisor"

  override def data: JsValue =
    Json.obj(
      "queueIds"         -> queueIds,
      "groupIds"         -> groupIds,
      "incallIds"        -> incallIds,
      "recordingAccess"  -> recordingAccess,
      "dissuasionAccess" -> dissuasionAccess
    )

}

case class TeacherRight(
    queueIds: List[Int],
    groupIds: List[Int],
    incallIds: List[Int],
    start: DateTime,
    end: DateTime
) extends Right {
  val name = "teacher"

  override def data: JsValue =
    Json.obj(
      "queueIds"  -> queueIds,
      "groupIds"  -> groupIds,
      "incallIds" -> incallIds,
      "start"     -> start.toString(TeacherRight.formatter),
      "end"       -> end.toString(TeacherRight.formatter)
    )

}

@ImplementedBy(classOf[RightManagerImpl])
trait RightManager {
  def userCanCreateRight(modifyingUser: String, right: Right): Boolean
  def userCanModifyUser(modifyingUser: String, modifiedUser: String): Boolean
  def deleteByLogin(login: String): Unit
  def fromJson(value: JsValue): Right
  def createOrReplace(login: String, right: Right): Unit
  def forUser(login: String): Option[Right]
  def insert(login: String, right: Right): Unit
}

class RightManagerImpl @Inject() (@NamedDatabase("xivo") db: Database)
    extends RightManager {
  val log: Logger = Logger(getClass.getName)

  override def insert(login: String, right: Right): Unit =
    db.withConnection { implicit c =>
      val userId = SQL(
        "INSERT INTO users(login, type) VALUES ({login}, {type}::user_type)"
      ).on(Symbol("login") -> login, Symbol("type") -> right.name)
        .executeInsert[Option[Long]]()
        .get

      right match {
        case _: AdminRight      => // Do nothing specific
        case s: SupervisorRight => insertSpecific(userId, s)
        case t: TeacherRight    => insertSpecific(userId, t)
      }

    }

  private def insertSpecific(userId: Long, s: SupervisorRight): Unit = {
    val uid = userId.toInt

    insertQueueRights(uid, s.queueIds)
    insertGroupRights(uid, s.groupIds)
    insertIncallRights(uid, s.incallIds)
    insertAccessRights(uid, s.recordingAccess, "recording_access")
    insertDissuasionAccessRights(uid, s.dissuasionAccess, "dissuasion_access")
  }

  private def insertSpecific(userId: Long, t: TeacherRight): Unit =
    db.withConnection { implicit c =>
      SQL(
        "UPDATE users SET validity_start = {start}, validity_end = {end} WHERE id = {userId}"
      )
        .on(
          Symbol("start")  -> t.start.toDate,
          Symbol("end")    -> t.end.toDate,
          Symbol("userId") -> userId
        )
        .executeUpdate()
      insertQueueRights(userId.toInt, t.queueIds)
      insertGroupRights(userId.toInt, t.groupIds)
      insertIncallRights(userId.toInt, t.incallIds)
    }

  def userCanCreateRight(modifyingUser: String, right: Right): Boolean = {
    forUser(modifyingUser) match {
      case Some(
            SupervisorRight(userQueueIds, userGroupIds, userIncallIds, true, _)
          ) =>
        right match {
          case TeacherRight(queueIds, groupIds, incallIds, _, _) =>
            queueIds.forall(userQueueIds.contains(_)) &&
              groupIds.forall(userGroupIds.contains(_)) &&
              incallIds.forall(userIncallIds.contains(_))
          case _ => false
        }
      case _ => false
    }

  }

  def userCanModifyUser(
      modifyingUser: String,
      modifiedUser: String
  ): Boolean = {
    forUser(modifiedUser) match {
      case Some(_: AdminRight) | Some(_: SupervisorRight) => return false
      case _                                              =>
    }
    forUser(modifyingUser) match {
      case Some(_: SupervisorRight) => true
      case _                        => false
    }
  }

  def deleteRightsByLogin(login: String): Int =
    db.withConnection { implicit c =>
      SQL(
        "DELETE FROM rights WHERE user_id IN (SELECT id FROM users WHERE login = {login})"
      ).on(Symbol("login") -> login).executeUpdate()
    }

  def deleteByLogin(login: String): Unit =
    db.withConnection { implicit c =>
      deleteRightsByLogin(login)
      SQL("DELETE FROM users WHERE login = {login}")
        .on(Symbol("login") -> login)
        .executeUpdate()
      ()
    }

  def fromJson(value: JsValue): Right = {
    val queueIdsSQL = (value \ "data" \ "queueIds")
      .asOpt[JsArray]
      .getOrElse(JsArray())
      .value
      .map(v => v.as[Int])
      .toList
    val groupIdsSQL = (value \ "data" \ "groupIds")
      .asOpt[JsArray]
      .getOrElse(JsArray())
      .value
      .map(v => v.as[Int])
      .toList
    val incallIdsSQL = (value \ "data" \ "incallIds")
      .asOpt[JsArray]
      .getOrElse(JsArray())
      .value
      .map(v => v.as[Int])
      .toList
    (value \ "type").as[String] match {
      case "admin" => AdminRight()
      case "supervisor" =>
        SupervisorRight(
          queueIdsSQL,
          groupIdsSQL,
          incallIdsSQL,
          (value \ "data" \ "recordingAccess").asOpt[Boolean].getOrElse(true),
          (value \ "data" \ "dissuasionAccess").asOpt[Boolean].getOrElse(false)
        )
      case "teacher" =>
        TeacherRight(
          queueIdsSQL,
          groupIdsSQL,
          incallIdsSQL,
          (value \ "data" \ "start").as[String],
          (value \ "data" \ "end").as[String]
        )
    }
  }

  private def cleanUserAndRights(login: String) =
    db.withConnection { implicit c =>
      deleteRightsByLogin(login)
      SQL("DELETE FROM users WHERE login = {login}")
        .on(Symbol("login") -> login)
        .executeUpdate()
    }

  override def createOrReplace(login: String, right: Right): Unit =
    db.withConnection { implicit c =>
      cleanUserAndRights(login)
      insert(login, right)
    }

  private def simpleInt(name: String) = get[Int](name)

  val baseUser: RowParser[(String, Option[Date], Option[Date])] = get[String](
    "type"
  ) ~ get[Option[Date]]("validity_start") ~ get[Option[Date]](
    "validity_end"
  ) map { case thetype ~ start ~ end =>
    (thetype, start, end)
  }

  def forUser(login: String): Option[Right] =
    db.withConnection { implicit c =>
      SQL(
        "SELECT type::varchar AS type, validity_start, validity_end FROM users WHERE login = {login}"
      ).on(Symbol("login") -> login).as(baseUser.*).headOption match {
        case None                  => None
        case Some(("admin", _, _)) => Some(AdminRight())
        case Some(("supervisor", _, _)) =>
          Some(
            SupervisorRight(
              queueIdsForUser(login),
              groupIdsForUser(login),
              incallIdsForUser(login),
              accessForUser(login, "recording_access"),
              accessDissuasionForUser(login, "dissuasion_access")
            )
          )
        case Some(("teacher", start, end)) =>
          Some(
            TeacherRight(
              queueIdsForUser(login),
              groupIdsForUser(login),
              incallIdsForUser(login),
              new DateTime(start.get),
              new DateTime(end.get)
            )
          )
        case _ => None
      }
    }

  private def queueIdsForUser(login: String): List[Int] =
    categoryIdForUserAndType(login, "queue")

  private def groupIdsForUser(login: String): List[Int] =
    categoryIdForUserAndType(login, "agentgroup")

  private def incallIdsForUser(login: String): List[Int] =
    categoryIdForUserAndType(login, "incall")

  private def accessForUser(login: String, to: String): Boolean = {
    val access = categoryIdForUserAndType(login, to)
    access.isEmpty || access.contains(1)
  }

  private def accessDissuasionForUser(login: String, to: String): Boolean = {
    categoryIdForUserAndType(login, to).contains(1)
  }

  private def categoryIdForUserAndType(
      login: String,
      category: String
  ): List[Int] =
    db.withConnection { implicit c =>
      SQL(
        "SELECT category_id FROM rights r JOIN users u ON u.id = r.user_id WHERE u.login = {login} AND r.category = {category}::right_type"
      )
        .on(Symbol("login") -> login, Symbol("category") -> category)
        .as(simpleInt("category_id").*)
    }

  private def insertCategoryRights(
      userId: Int,
      categoryName: String,
      categoryIds: List[Int]
  ): Unit =
    db.withConnection { implicit c =>
      categoryIds.foreach(id =>
        SQL(
          "INSERT INTO rights(user_id, category, category_id) VALUES ({user_id}, {category}::right_type, {category_id})"
        )
          .on(
            Symbol("user_id")     -> userId,
            Symbol("category")    -> categoryName,
            Symbol("category_id") -> id
          )
          .executeUpdate()
      )
    }

  private def insertAccessRights(
      userId: Int,
      access: Boolean,
      to: String
  ): Unit =
    db.withConnection { implicit c =>
      SQL(
        "INSERT INTO rights(user_id, category, category_id) VALUES ({user_id}, {to}::right_type, {access})"
      )
        .on(
          Symbol("user_id") -> userId,
          Symbol("to")      -> to,
          Symbol("access")  -> (if (access) 1 else 0)
        )
        .execute()
      ()
    }

  private def insertDissuasionAccessRights(
      userId: Int,
      dissuasion: Boolean,
      to: String
  ): Unit =
    db.withConnection { implicit c =>
      SQL(
        "INSERT INTO rights(user_id, category, category_id) VALUES ({user_id}, {to}::right_type, {dissuasion})"
      )
        .on(
          Symbol("user_id")    -> userId,
          Symbol("to")         -> to,
          Symbol("dissuasion") -> (if (dissuasion) 1 else 0)
        )
        .execute()
      ()
    }

  private def insertQueueRights(userId: Int, queueIds: List[Int]): Unit =
    insertCategoryRights(userId, "queue", queueIds)

  private def insertGroupRights(userId: Int, groupIds: List[Int]): Unit =
    insertCategoryRights(userId, "agentgroup", groupIds)

  private def insertIncallRights(userId: Int, incallIds: List[Int]): Unit =
    insertCategoryRights(userId, "incall", incallIds)

}

object TeacherRight {

  val formatter: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  def apply(
      queueIds: List[Int],
      groupIds: List[Int],
      incallIds: List[Int],
      start: String,
      end: String
  ): TeacherRight =
    TeacherRight(
      queueIds,
      groupIds,
      incallIds,
      formatter.parseDateTime(start),
      formatter.parseDateTime(end).withTime(23, 59, 59, 0)
    )
}

trait RightErrorType     extends ErrorType
case object UserNotFound extends RightErrorType { val error = "UserNotFound" }
case object InsufficientCreateRight extends RightErrorType {
  val error = "InsufficientCreateRight"
}
case object InsufficientDeleteRight extends RightErrorType {
  val error = "InsufficientDeleteRight"
}

case class RightError(error: ErrorType, message: String) extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    val body = Json.toJson(this)
    error match {
      case UserNotFound => Results.NotFound(body)
      case InsufficientCreateRight | InsufficientDeleteRight =>
        Results.Unauthorized(body)
      case _ => Results.InternalServerError(body)
    }
  }
}

object RightError {
  implicit val rightErrorWrites: Writes[RightError] = Json.writes[RightError]
}
