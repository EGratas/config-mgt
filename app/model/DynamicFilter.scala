package model

import java.security.InvalidParameterException
import anorm.{NamedParameter, ParameterValue, ToParameterValue}
import play.api.libs.json._
import play.api.libs.functional.syntax._

sealed trait DynamicFilterOperator {
  def toSqlOperator(): String
}

case object OperatorEq extends DynamicFilterOperator {
  val toSqlOperator = "="
}

case object OperatorNeq extends DynamicFilterOperator {
  val toSqlOperator = "!="
}

case object OperatorGt extends DynamicFilterOperator {
  val toSqlOperator = ">"
}

case object OperatorGte extends DynamicFilterOperator {
  val toSqlOperator = ">="
}

case object OperatorLt extends DynamicFilterOperator {
  val toSqlOperator = "<"
}

case object OperatorLte extends DynamicFilterOperator {
  val toSqlOperator = "<="
}

case object OperatorLike extends DynamicFilterOperator {
  val toSqlOperator = "like"
}

case object OperatorIlike extends DynamicFilterOperator {
  val toSqlOperator = "ilike"
}

case object OperatorIsNull extends DynamicFilterOperator {
  val toSqlOperator = "is null"
}

case object OperatorIsNotNull extends DynamicFilterOperator {
  val toSqlOperator = "is not null"
}

case object OperatorContainsAll extends DynamicFilterOperator {
  val toSqlOperator = "contains_all"
}

sealed trait DynamicFilterOrder {
  def toSqlOperator(): String
}

case object OrderAsc extends DynamicFilterOrder {
  val toSqlOperator = "ASC"
}

case object OrderDesc extends DynamicFilterOrder {
  val toSqlOperator = "DESC"
}

object DynamicFilterOperator {
  def toObject(s: Option[String]) =
    s.map(_ match {
      case OperatorEq.toSqlOperator          => OperatorEq
      case OperatorNeq.toSqlOperator         => OperatorNeq
      case OperatorGt.toSqlOperator          => OperatorGt
      case OperatorGte.toSqlOperator         => OperatorGte
      case OperatorLt.toSqlOperator          => OperatorLt
      case OperatorLte.toSqlOperator         => OperatorLte
      case OperatorLike.toSqlOperator        => OperatorLike
      case OperatorIlike.toSqlOperator       => OperatorIlike
      case OperatorIsNull.toSqlOperator      => OperatorIsNull
      case OperatorIsNotNull.toSqlOperator   => OperatorIsNotNull
      case OperatorContainsAll.toSqlOperator => OperatorContainsAll
    })
}

object DynamicFilterOrder {
  def toObject(s: Option[String]): Option[DynamicFilterOrder] =
    s.map(_ match {
      case OrderAsc.toSqlOperator  => OrderAsc
      case OrderDesc.toSqlOperator => OrderDesc
    })
}

case class DynamicFilter(
    field: String,
    operator: Option[DynamicFilterOperator],
    value: Option[String],
    order: Option[DynamicFilterOrder] = None,
    list: Option[List[String]] = None
)

trait CasingTransformation {
  def transform(s: String): String
}

object NoCasing extends CasingTransformation {
  override def transform(s: String): String = s
}

object LowerCasing extends CasingTransformation {
  override def transform(s: String): String = s.toLowerCase()
}

object ToSnakeCase extends CasingTransformation {
  override def transform(s: String): String =
    "([A-Z])".r.replaceAllIn(s, m => "_" + m.group(1).toLowerCase)
}

trait FieldValueTyper {
  def to(field: String, value: String): ParameterValue
}

object AllFieldsString extends FieldValueTyper {
  override def to(field: String, value: String): ParameterValue = {
    val t = ToParameterValue.apply[String]
    t(value)
  }
}

object DynamicFilter {

  def sanitizeField(s: String): String =
    s
      .replaceAll("[.|':=\\]\\[\n\t]", "")
      .replaceAll(" +", "")

  def toSqlWhereCondition(
      filter: DynamicFilter
  )(implicit casing: CasingTransformation): String =
    filter.operator match {
      case Some(operator) =>
        s"${casing.transform(filter.field)} ${operator.toSqlOperator()}" + filter.value
          .map(_ => s" {${sanitizeField(filter.field)}}")
          .getOrElse("")
      case _ => ""
    }

  def toSqlWhereCondition(
      filters: List[DynamicFilter]
  )(implicit casing: CasingTransformation): String = {
    val whereFilters = filters.filter(o =>
      !o.operator.contains(OperatorContainsAll) && o.operator.isDefined
    )
    if (whereFilters.nonEmpty)
      whereFilters
        .map(toSqlWhereCondition)
        .mkString(" WHERE ", " AND ", "\n")
    else ""
  }

  def toSqlHavingCondition(
      filters: List[DynamicFilter]
  )(implicit casing: CasingTransformation): String = {
    val havingFilters = filters.filter(o =>
      o.operator.contains(OperatorContainsAll) && o.operator.isDefined
    )
    if (havingFilters.nonEmpty)
      havingFilters
        .map(toSqlHavingCondition)
        .mkString(" HAVING ", " AND ", "\n")
    else ""
  }

  def toSqlHavingCondition(
      filter: DynamicFilter
  )(implicit casing: CasingTransformation): String =
    filter.operator match {
      case Some(_) =>
        s" COUNT(*) FILTER( WHERE " + filter.list
          .map(l =>
            s" ${l
              .map(e => s"${casing.transform(filter.field)} = '$e'")
              .mkString(" OR ")}) > ${filter.list.getOrElse(List()).length - 1}"
          )
          .getOrElse("")
      case _ => ""
    }

  def toOrderClause(
      filter: DynamicFilter
  )(implicit casing: CasingTransformation): Option[String] =
    filter.order.map(o =>
      casing.transform(filter.field) + " " + o.toSqlOperator()
    )

  def toOrderClause(
      filters: List[DynamicFilter]
  )(implicit casing: CasingTransformation): String = {
    val orderFields = filters map toOrderClause filter (_.isDefined) map (_.get)
    if (orderFields.isEmpty) ""
    else " ORDER BY " + orderFields.mkString(", ")
  }

  def toNamedParameter(
      filter: DynamicFilter
  )(implicit typer: FieldValueTyper): NamedParameter =
    NamedParameter(
      sanitizeField(filter.field),
      typer.to(sanitizeField(filter.field), filter.value.getOrElse(""))
    )

  def toNamedParameterList(
      filters: List[DynamicFilter]
  )(implicit typer: FieldValueTyper): List[NamedParameter] =
    filters.filter(_.value.isDefined) map toNamedParameter

  implicit val filterReads: Reads[DynamicFilter] = (
    (JsPath \ "field").read[String] and
      (JsPath \ "operator")
        .readNullable[String]
        .map(DynamicFilterOperator.toObject) and
      (JsPath \ "value").readNullable[String] and
      (JsPath \ "order")
        .readNullable[String]
        .map(DynamicFilterOrder.toObject) and
      (JsPath \ "list").readNullable[List[String]]
  )(DynamicFilter.apply _) map DynamicFilter.validateFilter

  implicit val filterWrites: Writes[DynamicFilter] = new Writes[DynamicFilter] {
    def writes(f: DynamicFilter) =
      Json.obj(
        "field"    -> f.field,
        "operator" -> f.operator.map(_.toSqlOperator()),
        "value"    -> f.value,
        "order"    -> f.order.map(_.toSqlOperator())
      )
  }

  def validateFilter(filter: DynamicFilter): DynamicFilter = {
    filter.operator.foreach {
      case OperatorContainsAll =>
        if (filter.list.isEmpty)
          throw new InvalidParameterException(
            "List parameter must be provided with contains_all operator"
          )
      case _ =>
        if (filter.value.isEmpty)
          throw new InvalidParameterException(
            "Value parameter must be provided with an operator"
          )
    }
    filter
  }
}
