package model
import javax.inject.Inject

import anorm.SqlParser._
import anorm._
import com.google.inject.ImplementedBy
import play.api.db.{Database, _}
import play.api.libs.functional.syntax._
import play.api.libs.json._

case class Queue(id: Option[Long], name: String, displayName: String)

object Queue {
  implicit val queueWrites: OWrites[Queue] =
    ((__ \ "id").write[Option[Long]] and
      (__ \ "name").write[String] and
      (__ \ "displayName").write[String])(unlift(Queue.unapply))

}

@ImplementedBy(classOf[QueueManagerImpl])
trait QueueManager {
  def all(): List[Queue]

  def withIds(ids: List[Int]): List[Queue]
}

class QueueManagerImpl @Inject() (@NamedDatabase("xivo") dbXivo: Database)
    extends QueueManager {

  override def withIds(ids: List[Int]): List[Queue] =
    dbXivo.withConnection { implicit c =>
      if (ids.isEmpty) List()
      else
        SQL(
          "SELECT id, name, displayname FROM queuefeatures WHERE id IN ({ids})"
        ).on(Symbol("ids") -> ids.asInstanceOf[Seq[Int]]).as(simple.*)
    }

  val simple: RowParser[Queue] =
    get[Long]("id") ~ get[String]("name") ~ get[String]("displayname") map {
      case id ~ name ~ displayName => Queue(Some(id), name, displayName)
    }
  override def all(): List[Queue] =
    dbXivo.withConnection { implicit c =>
      SQL("SELECT id, name, displayname FROM queuefeatures").as(simple.*)
    }
}
