package cti.controllers

import controllers.Secured
import model.ws.{GenericError, NotFoundError}
import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.json.Json
import play.api.mvc.{EssentialAction, InjectedController}
import ws.controllers.WithExceptionCatching
import xivo.service.CtiStatusManager

import javax.inject.Inject
import scala.util.{Failure, Success}

class CtiStatusController @Inject() (
    ctiStatusManager: CtiStatusManager,
    secured: Secured
) extends InjectedController
    with WithExceptionCatching {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def get(): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Line Req: <get> for ctiStatus")

            ctiStatusManager.get match {
              case Success(ctiStatusList) => Ok(Json.toJson(ctiStatusList))
              case Failure(f) =>
                GenericError(
                  NotFoundError,
                  s"Unable to get CtiStatus, $f"
                ).toResult
            }
          }
    )
// Deprecated, To be removed
  def getLegacy(): EssentialAction =
    secured.WithAuthenticatedUser(
      None,
      user =>
        request =>
          withExceptionCatching {
            logger.info(s"$user - Line Req: <get> for ctiStatus")

            ctiStatusManager.getLegacy match {
              case Success(ctiStatusList) => Ok(Json.toJson(ctiStatusList))
              case Failure(f) =>
                GenericError(
                  NotFoundError,
                  s"Unable to get CtiStatusLegacy, $f"
                ).toResult
            }
          }
    )

}
