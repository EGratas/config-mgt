package cti.models
import anorm.SqlParser.get
import anorm.{~, RowParser}
import play.api.libs.json.{Json, Reads, Writes}

case class CtiStatus(
    name: Option[String],
    displayName: Option[String],
    status: Option[Int]
)

object CtiStatus {
  implicit val CtiStatusWrite: Writes[CtiStatus] =
    Json.writes[CtiStatus]
  implicit val CtiStatusRead: Reads[CtiStatus] =
    Json.reads[CtiStatus]

  val simple: RowParser[CtiStatus] = {
    get[Option[String]]("name") ~
      get[Option[String]]("displayName") ~
      get[Option[Int]]("status") map { case name ~ display_name ~ status =>
        CtiStatus(name, display_name, status)
      }
  }
}

object CtiStatusSqlResult {
  def CtiStatusSqlResultMapper: RowParser[Map[String, List[CtiStatus]]] = {
    get[String]("ctimap") map { ctimap: String =>
      Json.parse(ctimap).as[Map[String, List[CtiStatus]]]
    }
  }
}

case class CtiStatusActionLegacy(name: String, parameters: Option[String])

object CtiStatusActionLegacy {
  implicit val CtiStatusActionWrite: Writes[CtiStatusActionLegacy] =
    Json.writes[CtiStatusActionLegacy]
  implicit val CtiStatusActionRead: Reads[CtiStatusActionLegacy] =
    Json.reads[CtiStatusActionLegacy]

  implicit val CtiStatusActionLegacy: RowParser[CtiStatusActionLegacy] = {
    get[String]("name") ~
      get[Option[String]]("parameters") map { case name ~ parameters =>
        new CtiStatusActionLegacy(name, parameters)
      }
  }

}

case class CtiStatusLegacy(
    name: Option[String],
    longName: Option[String],
    color: Option[String],
    actions: List[CtiStatusActionLegacy]
)

object CtiStatusLegacy {
  implicit val CtiStatusWrite: Writes[CtiStatusLegacy] =
    Json.writes[CtiStatusLegacy]
  implicit val CtiStatusRead: Reads[CtiStatusLegacy] =
    Json.reads[CtiStatusLegacy]

  val simple: RowParser[CtiStatusLegacy] = {
    get[Option[String]]("name") ~
      get[Option[String]]("longName") ~
      get[Option[String]]("color") ~
      get[Option[String]]("actions") map {
        case name ~ longName ~ color ~ actions =>
          CtiStatusLegacy(
            name,
            longName,
            color,
            Json.parse(actions.getOrElse("[]")).as[List[CtiStatusActionLegacy]]
          )
      }
  }
}

object CtiStatusLegacySqlResult {
  def CtiStatusLegacySqlResultMapper
      : RowParser[Map[String, List[CtiStatusLegacy]]] = {
    get[String]("ctimap") map { ctimap: String =>
      Json.parse(ctimap).as[Map[String, List[CtiStatusLegacy]]]
    }
  }
}
