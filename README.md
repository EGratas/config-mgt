# Xivo Configuration Server

 This is the configuration server of Xivo
 
# Configuration
## play.http.context
This configuration key allows you to move the "root" URL to for example /configmgt. When you can't use a subdomain, 
this is a way allowing two application to exists in one http server. We use this key to make configMgt application
co-exist with the xivo administration interface. 
!! Be careful, when changing this key value, you need to update also the font path in `_variables.less` file.
As we call the configmgt webservices from the XiVO web interface, we also modified the session path in the 
configuration to by /, otherwise the XSRF protection mechanism is broken. This is a risk, we need to be sure that 
the configmgt is the only server pushing the XSRF token.  

# Development
# Install dependencies

```bash
git clone git@gitlab.com:xivoxc/play-authentication.git
cd play-authentication
git checkout 2023.08.00-play2.8
sbt clean compile publishLocal
```
## Run the test

You can run the test
    
	$ sbt test

## Running in dev

    Create a conf file named application-*.conf like `application-dev.conf` and fill the variable : 
    ```
    include "application.conf"

    XIVO_HOST = 
    DB_HOST = 
    PLAY_AUTH_TOKEN = # Can be found in custom env
    XIVO_CONFD_AUTH_TOKEN = # Can be found in custom env
    ```

    You can also directly override a configuration key if no env variable is defined.

    Starting the application in play framework :

    ```sh
    sbt run -Dconfig.file=conf/application-dev.conf
    ```

    $ 

## Docker (not use anymore !)

    $ docker run -ti --rm --name config-mgtdev -e XIVO_HOST=192.168.56.3  -Dauthentication.token=PLAY_AUTH_TOKEN --add-host=db:192.168.XX.XXX -p 9999:9000 xivoxc/config-mgt:1.4.0

## Connection from webi

Edit `/etc/nginx/locations/https/xivo-web-interface` on your XiVO
Change everywhere `proxy_pass http://$server_addr:9100;` to e.g. `proxy_pass http://192.168.56.1:9000;`
Reload nginx

## Environment variables

- DB_HOST
- DB_NAME
- XIVO_HOST


## Documentation

    http://xivocc.readthedocs.org

## License


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.

## samples

### Callback lists

#### Get

    curl -XGET  -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: confmgt" http://localhost:9000/api/1.0/callback_lists

    [
        {
            "callbacks": [],
            "name": "testreppels",
            "queueId": 173,
            "uuid": "0bda53c3-c744-46d1-9d91-a0a46c750aae"
        },
            {
                "callbacks": [
                    {
                        "agentId": null,
                        "clotured": false,
                        "company": null,
                        "description": null,
                        "dueDate": "2016-03-13",
                        "firstName": null,
                        "lastName": null,
                        "listUuid": "2a3d4b44-1085-433f-b2ab-f8fcc21d9572",
                        "mobilePhoneNumber": null,
                        "phoneNumber": null,
                        "preferredPeriod": {
                            "default": true,
                            "name": "Toute la journ\u00e9e",
                            "periodEnd": "17:00:00",
                            "periodStart": "09:00:00",
                            "uuid": "67f0eca6-2a09-42f5-a298-61a104c3caf8"
                        },
                        "queueId": null,
                        "uuid": "38c857bd-f78e-41ff-8ac1-7c8f640ffdd3"
                    },
                    {
                        "agentId": null,
                        "clotured": false,
                        "company": "Cie",
                        "description": null,
                        "dueDate": "2016-03-13",
                        "firstName": null,
                        "lastName": null,
                        "listUuid": "2a3d4b44-1085-433f-b2ab-f8fcc21d9572",
                        "mobilePhoneNumber": null,
                        "phoneNumber": "0987654321",
                        "preferredPeriod": {
                            "default": true,
                            "name": "Toute la journ\u00e9e",
                            "periodEnd": "17:00:00",
                            "periodStart": "09:00:00",
                            "uuid": "67f0eca6-2a09-42f5-a298-61a104c3caf8"
                        },
                        "queueId": null,
                        "uuid": "02f26f3e-c30f-4089-a579-b410cbd37069"
                    }
                ],
                "name": "newlist",
                "queueId": 1,
                "uuid": "2a3d4b44-1085-433f-b2ab-f8fcc21d9572"
            }
    ]

#### Create

    curl -XPOST  -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: confmgt" http://localhost:9000/api/1.0/callback_lists -d '{"name":"newlist", "queueId":1}'
    
    {
        "callbacks": [],
        "name": "newlist",
        "queueId": 1,
        "uuid": "9d28d8fe-0548-4d45-aa08-9623ef69a04b"
    }

#### Remove

curl -XDELETE  -H "X-Auth-Token: confmgt" http://localhost:9000/api/1.0/callback_lists/9d28d8fe-0548-4d45-aa08-9623ef69a04b


### Callback Requests

#### Create
    
    curl -XPOST  -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: confmgt" http://localhost:9000/api/1.0/callback_lists/2a3d4b44-1085-433f-b2ab-f8fcc21d9572/callback_requests -d '
    
    { 
        "company":"Cie", 
        "phoneNumber":"0298765432",
        "mobilePhoneNumber":"0654321234",
        "firstName":"Jack",
        "preferredPeriodUuid":"e4bbb858-2765-40fb-9150-bd0c36435b39"
    }'

### Periods

#### List

    curl -XGET  -H "Content-Type: application/json" -H "Accept: application/json" -H "X-Auth-Token: confmgt"  http://localhost:9000/api/1.0/preferred_callback_periods 
    
    [
        {
            "default": true,
            "name": "Toute la journ\u00e9e",
            "periodEnd": "17:00:00",
            "periodStart": "09:00:00",
            "uuid": "67f0eca6-2a09-42f5-a298-61a104c3caf8"
        },
        {
            "default": false,
            "name": "Matin",
            "periodEnd": "12:00:00",
            "periodStart": "09:00:00",
            "uuid": "e4bbb858-2765-40fb-9150-bd0c36435b39"
        },
        {
            "default": false,
            "name": "En soir\u00e9e",
            "periodEnd": "00:00:00",
            "periodStart": "23:00:00",
            "uuid": "9afdc455-3f45-4ed9-ae3a-19471ea5395d"
        },
        {
            "default": false,
            "name": "Apr\u00e8s-midi",
            "periodEnd": "17:00:00",
            "periodStart": "15:00:00",
            "uuid": "10615d99-cf3c-4efd-ad9c-625829c730c5"
        }
    ]
   
### Get queue dissuasion
    
#### Get selected dissuasion sound file for a queue

    curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" localhost:9000/configmgt/api/1.0/queue/3/dissuasion

```json
{
  "id": 3,
  "name": "promotion",
  "soundFile": "promotion_file_exemple"
}
```

#### Get list of dissuasion sound file for a queue

    curl -XGET -H "Content-Type: application/json" -H "Accept: application/json" localhost:9000/configmgt/api/1.0/queue/3/dissuasion/sound_files
   
```json
{
  "id": 3,
  "soundFiles": [
    {
      "name": "promotion_file_exemple"
    },
    {
      "name": "promotion_file_exemple_2"
    }
  ]
}
```
