# --- !Ups
ALTER TABLE callback_request ADD COLUMN agent_id INTEGER;

# --- !Downs
ALTER TABLE callback_request DROP COLUMN agent_id;