# --- !Ups
ALTER TABLE callback_list DROP COLUMN period_start;
ALTER TABLE callback_list DROP COLUMN period_end;

# --- !Downs
ALTER TABLE callback_list ADD COLUMN period_start TIME WITHOUT TIME ZONE NOT NULL;
ALTER TABLE callback_list ADD COLUMN period_end TIME WITHOUT TIME ZONE NOT NULL;