# --- !Ups
CREATE OR REPLACE FUNCTION uuid_generator()
RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
BEGIN
    NEW.uuid := uuid_generate_v4();;
    RETURN NEW;;
END;;
$$;

CREATE TABLE callback_list (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    queue_id INTEGER NOT NULL,
    period_start TIME WITHOUT TIME ZONE NOT NULL,
    period_end TIME WITHOUT TIME ZONE NOT NULL
);

CREATE TRIGGER callback_list_uuid_trigger BEFORE INSERT ON callback_list FOR EACH ROW EXECUTE PROCEDURE uuid_generator();

CREATE TABLE callback_request (
    uuid UUID PRIMARY KEY,
    list_uuid UUID REFERENCES callback_list(uuid) NOT NULL,
    phone_number VARCHAR(40),
    mobile_phone_number VARCHAR(40),
    firstname VARCHAR(128),
    lastname VARCHAR(128),
    company VARCHAR(128),
    description TEXT
);

CREATE TRIGGER callback_request_uuid_trigger BEFORE INSERT ON callback_request FOR EACH ROW EXECUTE PROCEDURE uuid_generator();

# --- !Downs
DROP TABLE IF EXISTS callback_request;
DROP TABLE IF EXISTS callback_list;
