# --- !Ups
ALTER TABLE callback_request ADD COLUMN clotured BOOLEAN DEFAULT FALSE;

# --- !Downs
ALTER TABLE callback_request DROP COLUMN clotured;