# --- !Ups
ALTER TABLE callback_request ADD COLUMN voice_message_ref VARCHAR(128);

# --- !Downs
ALTER TABLE callback_request DROP COLUMN voice_message_ref;