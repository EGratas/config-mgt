import sbt.{Path, _}
import play.sbt.PlayImport._

object Version {
  val rabbitmq = "5.19.0"
  val playws   = "2.1.11"
}

object Library {
  val playauthentication =
    "solutions.xivo" %% "play-authentication" % "2023.08.00-play2.8"
  val postgresql    = "org.postgresql"           % "postgresql"             % "42.6.0"
  val dbunit        = "org.dbunit"               % "dbunit"                 % "2.7.3"
  val mockito       = "org.mockito"              % "mockito-all"            % "1.10.19"
  val scalatestplus = "org.scalatestplus.play"  %% "scalatestplus-play"     % "5.1.0"
  val anorm         = "org.playframework.anorm" %% "anorm"                  % "2.6.10"
  val playws        = "com.typesafe.play"       %% "play-ahc-ws-standalone" % Version.playws
  val playwsjson =
    "com.typesafe.play" %% "play-ws-standalone-json" % Version.playws
  val dockertest    = "com.whisk"            %% "docker-testkit-scalatest"    % "0.9.9"
  val dockerSpotify = "com.whisk"            %% "docker-testkit-impl-spotify" % "0.9.9"
  val playjsonjoda  = "com.typesafe.play"    %% "play-json-joda"              % "2.9.4"
  val rabbitmq      = "com.rabbitmq"          % "amqp-client"                 % Version.rabbitmq
  val simulacrum    = "org.typelevel"        %% "simulacrum"                  % "1.0.1"
  val refined       = "eu.timepit"           %% "refined"                     % "0.10.2"
  val playRefined   = "be.venneborg"         %% "play28-refined"              % "0.6.0"
  val scalatestmock = "org.scalatestplus"    %% "mockito-3-4"                 % "3.2.10.0"
  val swaggerui     = "org.webjars"           % "swagger-ui"                  % "3.51.1"
  val jwt           = "com.github.jwt-scala" %% "jwt-play-json"               % "9.0.5"
}

object Dependencies {

  import Library._

  val scalaVersion = "2.13.12"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/")
  )

  val runDep = run(
    jdbc,
    anorm,
    evolutions,
    ws,
    guice,
    playws,
    playwsjson,
    playjsonjoda,
    postgresql,
    playauthentication,
    rabbitmq,
    simulacrum,
    refined,
    playRefined,
    swaggerui,
    jwt
  )

  val testDep = test(
    dbunit,
    mockito,
    dockertest,
    scalatestplus,
    scalatestmock,
    dockerSpotify
  )

  def run(deps: ModuleID*): Seq[ModuleID]  = deps
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
