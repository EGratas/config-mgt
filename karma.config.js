var webpackConfig = require('./webpack.config.js');

delete webpackConfig.entry;
webpackConfig.plugins.splice(1);
//For debugging only as too slow for tdd, see https://webpack.js.org/configuration/devtool/
//webpackConfig.devtool='inline-source-map';

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],

    reporters: ['progress'],
    port: 9876,
    colors: false,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './node_modules/jquery/dist/jquery.js',
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
      './node_modules/angular-translate/dist/angular-translate.js',
      './node_modules/angular-translate-loader-partial/angular-translate-loader-partial.js',
      './app/assets/javascripts/rights.js',
      './app/assets/javascripts/callbacks.js',
      './app/assets/javascripts/app.config.js',
      {pattern: './public/**/*', watched: false, served: true, included: false},
      './test/karma/**/*.spec.js'
    ],

    preprocessors: {
      './app/assets/javascripts/*.js': ['webpack', 'sourcemap'],
      './test/karma/**/*.spec.js': ['webpack', 'sourcemap'],
      './app/assets/javascripts/**/*.html': ['ng-html2js']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: 'errors-only'
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
      moduleName: 'html-templates'
    }
  });
};
